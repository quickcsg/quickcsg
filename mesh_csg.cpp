/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */
#include <cstdio>
#include <cstdlib>

#include "mCSG.hpp"
#include "kdtree.hpp"
#include "random_perturbation.hpp"

using namespace mCSG;

void usage() {
  fprintf(stderr, 
"Usage: mesh_csg -O mesh.off [options] mesh_1.off ... mesh_N.off\n"
"\n" 
" Compute CSG operation on a set of N meshses\n" 
"\n" 
"     Operation to perform\n" 
"\n" 
"-inter           intersection (default)\n"
"-union           union\n"
"-xor             XOR\n"
"-min2            keep if at least 2 polyhedra\n"
"-min-2           keep if at least n-2 polyhedra\n"
"-diff k          union of k first polyhedra minus union of n-k others\n"
"-csgprog '01&2|' binary tree of csg operations in postfix notation (0..9 = push this bit, & = and, | = or, * = xor).\n" 
"                 The example means (mesh0 inter mesh1) union mesh2\n"
"\n" 
"     KDTree options\n" 
"\n" 
"-kdtree.expand4inter         build kdtree around intersection\n"
"-kdtree.max_in_leaf M        set max nb of facets per leaf to M (default 20)\n"
"-kdtree.split_threshold th   if volume of one of child cells < th * volume of parent cell, split parent in half\n"
"-kdtree.keep_nodes           keep kdtree nodes when vertices are extracted\n"
"-kdtree.log_stats file.log   stores exploration timings and statistics in file.log\n"
"-kdtree.min_volume v         min volume of a kdtree cell. If smaller, don't subdivide more (is multiplied by bbox volume)\n"
"-kdtree.breadth_depth d      explore breadth-first until depth d\n"
"\n" 
"     Input (+ randomization to process degenerate cases)\n" 
"\n" 
"-random.seed N           set random seed\n"
"-random.rotation         apply random rotation of all meshes before processing\n"
"-random.translation R    apply separate random translation per mesh (max amplitude R)\n"
"-random.revert N         revert flags. 1 = revert rotation, 2 = revert translation, 4 = merge vertices after revert (combine with bitwise or)\n"
"-random.jitter mag       apply random jitter to contour pixel coordinates\n"
"-incombined              input is a combined OFF with several meshes\n"
"-contours                input is a list of contours + a calibration matrix\n"
"-cropbbox xmin,ymin,zmin,xmax,ymax,zmax    crop input to this bbox\n"
"-contourrange 1,6        use only cameras 1 to 5 (excluding 6) in contours\n"
"-contourcams 2,5,7       use only cameras 2,5,7 in contours\n"
"-contourdepth d          all cones are of height d\n"
"\n" 
"     Output options\n" 
"\n" 
"-v 52203                  set verbosity of stage 0 to 5, stage 1 to 2, etc.\n"
"-combined out.off         store out.off containing all meshes (with correct vertex numbers for visualization in meshlab)\n"
"-shrink                   shrink facets in output (to view through edges)\n"
"-tess                     tesselate output to convex polygons\n"
"-tess3                    tesselate output to triangles\n"
"-keepAllVertices          keep all vertices even if they do not belong to output polyhedron\n"
"-trimesh out.dat          output mesh with vertices of triplets of facets\n"
"-vertices out.dat         output vertices as triplets of facets\n"
"-savenode name fname.off  output node name to file fname.off\n"     
"-saveface fno fname.eps   output facet fno to fname.eps\n" 
"-log_make_facets out.log  log times for makeFacets\n"
"\n" 
"     Parallelization (if compiled with TBB enabled)\n" 
"\n" 
"-parallel.nt N         use N computation threads (default nb of cores)\n"
"-parallel.depth d      switch to sequential above this depth\n"
"-parallel.min_npoly N  switch to sequential when there are < N polys\n"
"-parallel.split_depth  perform split in parallel below this depth\n"
"-parallel.split_npoly  paralellize also over polygons when there are more than this\n"
"\n"  
"mesh_csg SVN version 6244 (online version 2.2)\n"
"\n"  
         );
  exit(1);
}


int main(int argc, char** args)
{
  
  const char* outputFilename = NULL;
  bool expand4inter = false; 
  std::string op = "-inter";  
  std::string oparg_string;
  int oparg = 0, oparg2 = 0;
  std::string offFlavor = "-default";
  float border_color[3] = {-1, -1, -1};
  const char* trimeshOut = NULL, *combinedOut = NULL, *vertexOut = NULL;
  std::vector<const char *> infiles;
  bool cropbbox = false; 
  Vec3 bbMin, bbMax;  

  CSGParameters csgparams; 

  enum {
    IN_default, 
    IN_combined, 
    IN_contours, 
    IN_obj   
  } input_format = IN_default;  

  double contour_depth = 10.0;
  int contour_cam0 = 0, contour_cam1 = -1;
  std::vector<int> contour_cam_selection; 

  RandomPerturbation random_perturbation; 
  int revert_perturbation = 7;
  double random_contour_jitter = 0; 
  
  const char *per_stage_verbose = "0000000000000";
  const char *kdtree_log_stats = NULL;
  const char *make_facets_log = NULL; 
  const char *kdtree_save_off = NULL;
  int log_stats_format = 1;



  int i = 1;
  while(i < argc) {
    const char * a = args[i++]; 
    std::string as(a); 
    if(as == "-h") usage(); 
    else if(as == "-kdtree.expand4inter") expand4inter = true;
    else if(as == "-kdtree.max_in_leaf" && args[i] && sscanf(args[i], "%d", &csgparams.max_in_leaf)) i++;
    else if(as == "-kdtree.split_threshold" && args[i] && sscanf(args[i], "%lf", &csgparams.split_threshold)) i++;
    else if(as == "-kdtree.min_volume" && args[i] && sscanf(args[i], "%lf", &csgparams.min_volume_factor)) i++;
    else if(as == "-kdtree.keep_nodes") csgparams.alloc_policy = 0;
    else if(as == "-kdtree.alloc_policy" && args[i] && sscanf(args[i], "%d", &csgparams.alloc_policy)) i++;
    else if(as == "-kdtree.tight_bbox_depth" && args[i] && sscanf(args[i], "%d", &csgparams.tight_bbox_depth)) i++;
    else if(as == "-kdtree.log_stats" && args[i]) {kdtree_log_stats = args[i++]; log_stats_format = 1; }
    else if(as == "-kdtree.log_stats2" && args[i]) {kdtree_log_stats = args[i++]; log_stats_format = 2; }
    else if(as == "-kdtree.save_off" && args[i]) {kdtree_save_off = args[i++]; }
    else if(as == "-kdtree.breadth_depth" && args[i] && sscanf(args[i], "%d", &csgparams.breadth_depth)) i++; 
    else if(as == "-log_make_facets" && args[i]) {make_facets_log = args[i++]; }

    else if(as == "-parallel.nt" && args[i] && sscanf(args[i], "%d", &csgparams.parallel_nt)) i++;
    else if(as == "-parallel.depth" && args[i] && sscanf(args[i], "%d", &csgparams.parallel_depth)) i++;
    else if(as == "-parallel.split_depth" && args[i] && sscanf(args[i], "%d", &csgparams.parallel_split_depth)) i++;
    else if(as == "-parallel.split_npoly" && args[i] && sscanf(args[i], "%d", &csgparams.parallel_split_npoly)) i++;
    else if(as == "-parallel.min_npoly" && args[i] && sscanf(args[i], "%d", &csgparams.parallel_min_npoly)) i++;
    
    else if(as == "-random.seed" && args[i] && sscanf(args[i], "%d", &random_perturbation.random_seed)) i++;
    else if(as == "-random.jitter" && args[i] && sscanf(args[i], "%lf", &random_contour_jitter)) i++;
    else if(as == "-random.rotation") random_perturbation.do_rr = true; 
    else if(as == "-random.revert" && args[i] && sscanf(args[i], "%d", &revert_perturbation)) i++;
    else if(as == "-random.translation" && args[i] && sscanf(args[i], "%lf", &random_perturbation.rt)) i++;
    else if((as == "-O" || as == "-o") && args[i]) outputFilename = args[i++]; 
    else if(as == "-trimesh" && args[i]) trimeshOut = args[i++]; 
    else if(as == "-vertices" && args[i]) vertexOut = args[i++]; 
    else if(as == "-combined" && args[i]) combinedOut = args[i++]; 
    else if(as == "-shrink" || as == "-keepAllVertices") offFlavor = as;
    else if(as == "-save.border_color" && args[i] && 
	    sscanf(args[i], "%f,%f,%f", border_color, border_color + 1, border_color + 2) == 3) i++;
    else if(as == "-tess") csgparams.tesselate = 1;
    else if(as == "-tess3") csgparams.tesselate = 3;
    else if(as == "-incombined") input_format = IN_combined;
    else if(as == "-inobj") input_format = IN_obj;    
    else if(as == "-contours") input_format = IN_contours;
    else if(as == "-contourdepth" && args[i] && sscanf(args[i], "%lf", &contour_depth)) i++;
    else if(as == "-contourrange" && args[i] && sscanf(args[i], "%d,%d", &contour_cam0, &contour_cam1)) i++;
    else if(as == "-contourcams" && args[i]) {
      char *s = args[i]; 
      for(;;) {
        int camno, j; 
        sscanf(s, "%d%n", &camno, &j); 
        contour_cam_selection.push_back(camno); 
        if(s[j] != ',') break;
        s += j + 1;
      }
      i++;
    }
    else if(as == "-cropbbox" && args[i] && 
            sscanf(args[i], "%lf,%lf,%lf,%lf,%lf,%lf", 
                   &bbMin.x, &bbMin.y, &bbMin.z, &bbMax.x, &bbMax.y, &bbMax.z) == 6)
      {i++; cropbbox = true; }           

    else if(as == "-v" && args[i]) per_stage_verbose = args[i++];

    else if(as == "-savenode" && args[i] && args[i + 1]) {csgparams.nodeToSave = args[i++]; csgparams.nodeFname = args[i++]; }
    else if(as == "-saveface" && 
            args[i] && sscanf(args[i], "%d", &csgparams.facetToSave) == 1 &&
            args[i + 1]) {i++; csgparams.facetFname = args[i++]; }
    else if(as == "-followvertex" && args[i] && sscanf(args[i], "%d", &csgparams.primaryVertexToFollow) == 1) i++; 
    else if(as == "-followcoords" && args[i] && 
            sscanf(args[i], "%lf,%lf,%lf", &csgparams.vertexToFollow.x, &csgparams.vertexToFollow.y, &csgparams.vertexToFollow.z) == 3) {
      csgparams.primaryVertexToFollow = -1;
      i++; 
    }
    else if(as == "-inter" || as == "-union" || as == "-xor" || 
	    as == "-min2" || as == "-min-2" || as == "-AuBmC" || as == "-BuCmA" || as == "-AmBiC" || as == "-dither")
      op = as;
    else if((as == "-diff" || as == "-inter2union" || as == "-csgtable" || as == "-csgnmesh" || as == "-csgminn" ) && 
	    args[i] && sscanf(args[i], "%d", &oparg)) {i++; op = as; }
    else if((as == "-csgminmax" ) && 
	    args[i] && sscanf(args[i], "%d,%d", &oparg, &oparg2) == 2) {i++; op = as; }
    else if((as == "-csgprog" || as == "-csglargetable") && args[i]) {oparg_string = args[i++]; op = as; }
    else if(a[0] == '-') {
      std::cerr << "unknown argument " << a << std::endl; 
      exit(1);
    } else 
      infiles.push_back(a);
  }

  
  if(infiles.size() < 2 && input_format != IN_combined) {
    std::cout << "Not enough args, need at least 2 input meshes " << std::endl;
    return 1;
  }

  csgparams.collect_processing_stats = kdtree_log_stats || make_facets_log || kdtree_save_off;
  if(kdtree_log_stats) csgparams.alloc_policy = 0;

  CSG csg(csgparams);  

#define SET_VERBOSE_STAGE(st) csg.verbose = st < strlen(per_stage_verbose) ? (per_stage_verbose[st] - '0') : 0; 
  int & verbose = csg.verbose; 

#ifdef ENABLE_TBB
  oneapi::tbb::global_control global_limit(
       oneapi::tbb::global_control::max_allowed_parallelism, csgparams.parallel_nt);
#endif  

  SET_VERBOSE_STAGE(0);

  if(input_format != IN_contours) {
    double t0 = getmillisecs(); 
    for(int i = 0; i < infiles.size(); i++) {
      const char * fname = infiles[i];
      IFV(0) printf("Loading %s %s\n", fname, 
                    input_format == IN_combined ? "(combined off)" : 
                    input_format == IN_obj ? "(obj)" : "(off)");
      if(input_format == IN_obj) 
        csg.addMeshFromObj(fname); 
      else 
        csg.addMeshFromOff(fname, input_format == IN_combined); 
    }
    IFV(0) printf("  Load input %.3f ms\n", getmillisecs() - t0); 
  } else { 
    double t0 = getmillisecs(); 
    assert(infiles.size() == 2 || !"contours requires 2 input files: contour file and calib file");     
    csg.loadContoursAndCalib(infiles[0], infiles[1], contour_depth, 
                             random_contour_jitter, 
                             contour_cam0, contour_cam1, contour_cam_selection.size() > 0 ? &contour_cam_selection : NULL); 
    IFV(0) printf("  Load contours %.3f ms\n", getmillisecs() - t0); 
  }


  IFV(0) printf("  %d meshes, %zd vertices, %zd facets\n", csg.nmesh, csg.vertices.size(), csg.facets.size());

  if(!random_perturbation.isId()) {
    IFV(0) printf("Applying random perturbation\n");
    double t_begin_preprocess = getmillisecs(); 
    random_perturbation.apply(csg);
    IFV(0) printf("  Preprocessing time: %.3f ms\n", getmillisecs() - t_begin_preprocess); 
  }

  SET_VERBOSE_STAGE(1);

  if(input_format != IN_contours) {
    IFV(0) printf("Register edge-facet links\n");    
    double t0 = getmillisecs();
    csg.afterLoad();
    IFV(0) printf("  topology time %.3f ms\n", getmillisecs() - t0);  
  }

  if(combinedOut) {
    IFV(0) printf("storing combined meshes to %s\n", combinedOut);
    csg.combinedToOff(combinedOut); 
  }

  SET_VERBOSE_STAGE(2);

  IFV(1) {
    printf("intial edges:\n"); 
    csg.print();
  } 


  CSGOperation *csgop = NULL; 

  if(op == "-inter") csgop = new CSGIntersection(csg.nmesh);
  else if(op == "-union") csgop = new CSGUnion(csg.nmesh);
  else if(op == "-xor") csgop = new CSGXOR(csg.nmesh);
  else if(op == "-diff") csgop = new CSGDiff(csg.nmesh, oparg);
  else if(op == "-min2") csgop = new CSGMinMesh(csg.nmesh, 2);
  else if(op == "-min-2") csgop = new CSGMinMesh(csg.nmesh, csg.nmesh - 2);
  else if(op == "-csgminn") csgop = new CSGMinMesh(csg.nmesh, oparg);
  else if(op == "-AuBmC") csgop = new CSGAuBmC();
  else if(op == "-BuCmA") csgop = new CSGBuCmA();
  else if(op == "-AmBiC") csgop = new CSGAmBiC();
  else if(op == "-dither") csgop = new CSGDither();
  else if(op == "-inter2union") csgop = new CSGInter2Union(csg.nmesh, oparg);
  else if(op == "-csgnmesh") csgop = new CSGMinMaxMesh(csg.nmesh, oparg, oparg);
  else if(op == "-csgminmax") csgop = new CSGMinMaxMesh(csg.nmesh, oparg, oparg2);
  else if(op == "-csgprog") csgop = new CSGProgram(oparg_string.c_str());
  else if(op == "-csglargetable") csgop = new CSGLargeTable(csg.nmesh,oparg_string.c_str());
  else if(op == "-csgtable") csgop = new CSGTable(csg.nmesh, oparg);
  else assert(0);

  SET_VERBOSE_STAGE(3);

  csg.initKDTree();  

  if(expand4inter) {
    if(verbose > 0) printf("Expanding kdtree for intersection\n");
    double t0 = getmillisecs();
    csg.expandForIntersection(); 
    double t1 = getmillisecs(); 
    if(verbose > 0) {
      printf("  time %.3f ms\n", t1 - t0);
      csg.printKDTreeStats();      
    }
  }

  if(cropbbox) {
    IFV(0) printf("Cropping root node to provided bbox\n");     
    double t0 = getmillisecs();
    csg.cropRootToBBox(bbMin, bbMax); 
    IFV(0) printf("  time %.3f ms\n", getmillisecs() - t0);

  }
     
  SET_VERBOSE_STAGE(4);

  {
    IFV(0) printf("KDTree to vertices\n");
    double t0, t1; 
    t0 = getmillisecs();     
    csg.exploreKDTree(csgop);   
    t1 = getmillisecs(); 
    IFV(0) printf("  exploration in %.3f ms\n", t1 - t0); 
    IFV(1) {
      csg.printStats(); 
      csg.printKDTreeStats();
    }    
  }

  if(vertexOut) {
    printf("Storing vertices %s\n", vertexOut); 
    csg.saveVertices(vertexOut); 
  }
  
  SET_VERBOSE_STAGE(6);

  {    
    IFV(0) printf("Building output facets\n");
    double t0 = getmillisecs();     
    csg.makeFacets(csgop); 
    double t1 = getmillisecs(); 
    IFV(0) printf("  facet time %.3f ms\n", t1 - t0); 
  }

  SET_VERBOSE_STAGE(7);

  if(revert_perturbation && !random_perturbation.isId()) {
    IFV(0) printf("Reverting random perturbation flags %d\n", revert_perturbation);
    double t0 = getmillisecs();
    random_perturbation.revert(csg, revert_perturbation);
    IFV(0) printf("  revert: %.3f ms\n", getmillisecs() - t0);
  }

  if(kdtree_log_stats) {
    printf("logging kdtree stats to %s (format %d)\n", kdtree_log_stats, log_stats_format); 
    csg.logKDTreeStats(kdtree_log_stats, log_stats_format); 
  }

  if(kdtree_save_off) {
    printf("saving kdtree to %s\n", kdtree_save_off); 
    csg.kdtreeToOff(kdtree_save_off);
  }
  
  if(make_facets_log) {
    printf("logging facet stats to %s\n", make_facets_log);     
    csg.logFacetStats(make_facets_log);
  }

  if(csg.errors.haveErrors()) {
    printf("Encountered errors during CSG computation:\n");
    csg.errors.print(); 
  }

  if(trimeshOut) {
    printf("Storing triplet mesh %s\n", trimeshOut); 
    csg.saveAsTrimesh(trimeshOut); 
  }

  if(outputFilename) {
    printf("Storing output %s\n", outputFilename); 
    double t0 = getmillisecs();         
    if(offFlavor == "-keepAllVertices" || offFlavor == "-default") 
      csg.toOff(outputFilename, offFlavor == "-keepAllVertices", border_color[0] < 0 ? NULL : border_color);
    else if(offFlavor == "-shrink")
      csg.toOffShrunk(outputFilename, 0.1);
    else assert(0);
    IFV(0) printf("  output time %.3f ms\n", getmillisecs() - t0); 
  }
  

  return 0;
}
