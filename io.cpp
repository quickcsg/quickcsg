/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */
#include <cstdio>
#include <cmath>
#include <cstring>

#include "mCSG.hpp"
#include "PolygonTesselator.hpp"

using namespace mCSG;


/**********************************************************************
 *  print debug info
 **********************************************************************/



void CSG::print() const {
  printf("Vertices:\n"); 
  for(int i = 0; i < vertices.size(); i++) {
    const Vertex & v = vertices[i];
    printf("   %d: coords (%g %g %g), from facets [ ", 
           i, v.coords.x, v.coords.y, v.coords.z); 
    for(int j = 0; j < v.facetNos.size(); j++) 
      printf("%d ", v.facetNos[j]); 
    printf("]\n");
  }
  printf("Facets:\n");
  for(int i = 0; i < facets.size(); i++) {
    const Facet &facet = facets[i];
    printf("  %d: from mesh %d, eq (%g %g %g).x = %g, points ", 
           i, facet.meshno,
           facet.normal.x, facet.normal.y, facet.normal.z, facet.b); 
    printf("[ ");
    for(int j = 0; j < facet.vertexNos.size(); j++) 
      if(facet.oppositeFacetNos.size() > 0)
        printf("(%d) %d ", facet.oppositeFacetNos[j], facet.vertexNos[j]);
      else
        printf("%d ", facet.vertexNos[j]);
    printf("]\n");
  }
  

}


/**********************************************************************
 * Mesh ouput
 **********************************************************************/



void CSG::saveAsTrimesh(const char *fname) const {
  FILE *f = fopen(fname, "w"); 
  assert(f); 
  for(int i = 0; i < facets.size(); i++) {
    const Facet &facet = facets[i]; 
    int l0 = 0;    
    for(int li = 0; li < facet.loops.size(); li++) {
      int l1 = facet.loops[li];      
      for(int j = l0; j < l1; j++) {
	const Vertex & v = vertices[facet.outputVertexNos[j]];
        for(int k = 0; k < v.facetNos.size(); k++) 
          fprintf(f, "%d%s", v.facetNos[k], k + 1 == v.facetNos.size() ? " " : ",");      
      }
      fprintf(f, "\n");
      l0 = l1;
    }
  }

  fclose(f);
}

void CSG::printStats() {

  printf("CSG stats:\n"
         "  nb facets: %zd\n"
         "  nb vertices: %zd (allocated %zd)\n",
         facets.size(), vertices.size(), vertices.capacity()); 
}

void CSGErrors::add(const char *str) {
  IFV(1) printf("ERROR: %s\n", str);
  
#ifdef ENABLE_TBB
  tbb::mutex::scoped_lock slock(lock);
#endif

  for(int i = 0; i < errs.size(); i++) 
    if(errs[i].str == str) {
      errs[i].n++; 
      return;
    }
  errs.push_back(Err()); 
  errs.back().str = str;
  errs.back().n = 1;  
}

void CSGErrors::print() const {
  for(int i = 0; i < errs.size(); i++) {
    printf("  %d times %s\n", errs[i].n, errs[i].str.c_str());
  }    
}

int CSGErrors::count() const {
  int count = 0; 
  for(int i = 0; i < errs.size(); i++) count += errs[i].n; 
  return count; 
}


static float colormap [6][3] = {
    {1, 0.5, 0.5}, 
    {0.5, 1, 0.5}, 
    {0.5, 0.5, 1}, 
    {0.5, 1, 1}, 
    {1, 0.5, 1}, 
    {1, 1, 0.5}, 
};




void CSG::toOff(const char *fname, bool keepAllvertices, const float *nonprim_color) const {
  FILE *f = fopen(fname, "w"); 
  assert(f); 

  std::vector<int> vmap;
  int nv, nv_prim = 0, nv_double = 0, nv_triple = 0; 

  vmap.resize(vertices.size(), 0);
  if(!keepAllvertices) {
    for(int i = 0; i < facets.size(); i++) {
      const Facet &facet = facets[i];
      for(int j = 0; j < facet.outputVertexNos.size(); j++) 
        vmap[facet.outputVertexNos[j]] = 1; 
    }
    nv = nv_prim = 0;
    for(int i = 0; i < vertices.size(); i++) {
      if(vmap[i]) {
        vmap[i] = nv++;     
        if(i < nvert1) {
	  nv_prim++; 
	} else {
	  int order = nmesh - vertices[i].meshpos.nbKnown(); 
	  if(order == 2) nv_double++; 
	  else if(order == 3) nv_triple++; 
	  else assert(!"cannot determine vertex order");
	}
      } else vmap[i] = -1;
    }
  } else {
    nv = vertices.size();
    for(int i = 0; i < nv; i++) 
      vmap[i] = i;
  }

  int totloops = 0, totloops_prim = 0;
  for(int i = 0; i < facets.size(); i++) {
    const Facet &facet = facets[i];
    totloops += facet.loops.size();
  }
  
  fprintf(f, "OFF\n%d %d %d\n", nv, totloops, 0);
  for(int i = 0; i < vertices.size(); i++) if(vmap[i] != -1) {
    const Vertex & v = vertices[i];
    fprintf(f, "%.10g %.10g %.10g\n", 
            v.coords.x, v.coords.y, v.coords.z); 
  }
 
  for(int i = 0; i < facets.size(); i++) {
    const Facet &facet = facets[i];
    int l0 = 0;    
    for(int li = 0; li < facet.loops.size(); li++) {
      int l1 = facet.loops[li];      
      bool isprim = true; 
      fprintf(f, "%d ", l1 - l0);
      for(int j = l0; j < l1; j++) {
        if(facet.outputVertexNos[j] >= nvert1) isprim = false; 
        fprintf(f, "%d ", vmap[facet.outputVertexNos[j]]);
      }
      const float * color = colormap[facet.meshno % 6];
      if(!isprim && nonprim_color) color = nonprim_color; 
      fprintf(f, "%g %g %g\n", color[0] * 0.999999, color[1], color[2]);
      l0 = l1;
      if(isprim) totloops_prim++;
    }
  }

  IFV(1) 
    printf("  %d vertices (%d primary, %d double, %d triple), %d facets (%d primary)\n", 
           nv, nv_prim, nv_double, nv_triple, totloops, totloops_prim); 

  fclose(f);
}

void CSG::combinedToOff(const char *fname) const {
  FILE *f = fopen(fname, "w"); 
  assert(f); 

  fprintf(f, "OFF\n%zd %zd %d\n", vertices.size(), facets.size(), 0);
  for(int i = 0; i < vertices.size(); i++) {
    const Vertex & v = vertices[i];
    fprintf(f, "%g %g %g\n", v.coords.x, v.coords.y, v.coords.z); 
  }
 
  for(int i = 0; i < facets.size(); i++) {
    const Facet &facet = facets[i];
    int nv = facet.vertexNos.size();
    fprintf(f, "%d ", nv);
    for(int j = 0; j < nv; j++) 
      fprintf(f, "%d ", facet.vertexNos[j]);
    float * color = colormap[facet.meshno % 6];
    fprintf(f, "%g %g %g\n", color[0] * 0.999999, color[1], color[2]);
  }

  fclose(f);
}


void Off::removeUnusedVertices() {
  std::vector<int> vmap; 
  vmap.resize(vertices.size(), 0);

  for(int i = 0; i < faces.size(); i++) {
    std::vector<int> &vf = faces[i]; 
    for(int j = 0; j < vf.size(); j++) 
      vmap[vf[j]] = 1; 
  }
  
  int nv = 0; 
  for(int i = 0; i < vertices.size(); i++) if(vmap[i]) {
    vertices[nv] = vertices[i];
    vmap[i] = nv;
    nv++; 
  }
  for(int i = 0; i < faces.size(); i++) {
    std::vector<int> &vf = faces[i]; 
    for(int j = 0; j < vf.size(); j++) 
      vf[j] = vmap[vf[j]];
  }
  vertices.erase(vertices.begin() + nv, vertices.end());
}

void Off::toFile(const char *fname) {
  FILE *f = fopen(fname, "w"); 
  if(!f) {
    perror("cannot open Off output file "); 
    return;
  }
  fprintf(f, "OFF\n%zd %zd 0\n", vertices.size(), faces.size()); 
  for(int i = 0; i < vertices.size(); i++) {
    fprintf(f, "%g %g %g\n", vertices[i][0], vertices[i][1], vertices[i][2]); 
  }
  for(int i = 0; i < faces.size(); i++) {
    std::vector<int> &pti = faces[i]; 
    fprintf(f, "%ld  ", pti.size());
    for(int j = 0; j < pti.size(); j++) 
      fprintf(f, "%d ", pti[j]); 
    float * color = colormap[meshnos[i] % 6];
    fprintf(f, "%g %g %g 1\n", color[0], color[1], color[2]);
  }
  fclose(f); 
}

static int box_faces[6 * 4] = {
  0,1,3,2, 
  0,4,5,1,
  0,2,6,4,
  3,7,6,2,
  5,4,6,7,
  3,1,5,7
};


void Off::addBox(const Vec3 &center, real w, int meshno) {
  Vec3 w3(w, w, w);
  addBox(center - w3, center + w3, meshno); 
}

void Off::addBox(const Vec3 &bbmin, const Vec3 &bbmax, int meshno) {
  int v0 = vertices.size(); 
  for(int i = 0; i < 8; i++) {
    Vec3 v(i & 1, (i >> 1) & 1, (i >> 2) & 1); 
    vertices.push_back(bbmin + (bbmax - bbmin) * v); 
  }
  for(int i = 0; i < 6; i++) {
    faces.push_back(std::vector<int>()); 
    std::vector<int> &p = faces.back(); 
    for(int j = 0; j < 4; j++) p.push_back(v0 + box_faces[i * 4 + (3 - j)]); 
    meshnos.push_back(meshno);
  }
}


void Off::addPolygon(std::vector<Vec3> &v, int meshno) {
  faces.push_back(std::vector<int>()); 
  std::vector<int> &p = faces.back(); 
  for(int i = 0; i < v.size(); i++) {
    p.push_back(vertices.size()); 
    vertices.push_back(v[i]); 
  }
  meshnos.push_back(meshno);
}

class Tesselator: public PolygonTesselator {   
public: 
  
  Off & off; 
  int meshno;

  Tesselator(Off & off): PolygonTesselator(true), off(off) {}
    
  virtual void emitPolygon(int n, const int * idx) {
    off.faces.push_back(std::vector<int>(idx, idx + 3));
    off.meshnos.push_back(meshno);
  }

  void addFacet(const Facet & facet) {
    beginPolygon();
    int l0 = 0;    
    meshno = facet.meshno; 
    for(int li = 0; li < facet.loops.size(); li++) {
      int l1 = facet.loops[li];      
      beginContour();
 
      for(int j = l0; j < l1; j++) {
        Vec3 x = off.vertices[facet.vertexNos[j]];
        vertex(facet.outputVertexNos[j], x);
      }	
      endContour();
      l0 = l1;
    }
    endPolygon(); 
  }

  void addFacetShrunk(const Facet & facet, const std::vector<Vertex> & vertices, real shrinkfactor) {
    int l0 = 0;    
    meshno = facet.meshno; 
    for(int li = 0; li < facet.loops.size(); li++) {
      int l1 = facet.loops[li];      

      Vec3 sum = Vec3::zero(); 
      for(int j = l0; j < l1; j++) 
	     sum += vertices[facet.outputVertexNos[j]].coords;       
      Vec3 center = sum / (l1 - l0); 
      
      beginPolygon();
      beginContour();
 
      for(int j = l0; j < l1; j++) {
	Vec3 x = vertices[facet.outputVertexNos[j]].coords;
	x = center + (1 - shrinkfactor) * (x - center);
	off.vertices.push_back(x); 
	vertex(off.vertices.size() - 1, x);
      }	
      endContour();
      endPolygon(); 
      l0 = l1;
    }
  }

};



void CSG::toOffTesselated(const char * outfilename) const {
  Off off; 

  for(int i = 0; i < vertices.size(); i++) 
    off.vertices.push_back(vertices[i].coords);
  
  Tesselator tess(off); 

  for(int i = 0; i < facets.size(); i++) 
    tess.addFacet(facets[i]);
  
  off.removeUnusedVertices();
  off.toFile(outfilename);
}

void CSG::toOffShrunk(const char * outfilename, real shrinkfactor) const {
  Off off; 
  Tesselator tess(off); 

  for(int i = 0; i < facets.size(); i++) 
    tess.addFacetShrunk(facets[i], vertices, shrinkfactor);

  off.toFile(outfilename);
}




void BitVector::toString(int l, char *s) const {
  *s++ = '[';
  for(int i = 0; i < l; i++) {
    int t = getTernary(i); 
    *s++ = t < 0 ? '.' : '0' + t;
  }
  *s++ = ']';
  *s++ = 0;
}

std::string BitVector::toString(int l) const {
  char buf[BitVector::maxbits + 16]; 
  toString(l, buf);
  return std::string(buf);
}

/*****************************************************************
 * Input 
 ****************************************************************/

void CSG::addMeshFromOff(const char *cone, bool combined) {
  if(nmesh >= BitVector::maxbits) {
    printf("cannot combine more than %d meshes\n", BitVector::maxbits);
    return;
  }

  FILE *f = fopen(cone, "r");
  if(!f) {
    fprintf(stderr, "cannot open %s\n", cone); 
    exit(1);
  }
    
  int nVertices, nFacets, nEdges; 
  bool is_coff = false;
  int ret = fscanf(f, "OFF %d %d %d ", &nVertices, &nFacets, &nEdges); 
  if(ret != 3) {
    int ret = fscanf(f, "COFF %d %d %d ", &nVertices, &nFacets, &nEdges); 
    if(ret != 3) {
      fprintf(stderr, "cannot parse header %s\n", cone); 
      exit(1);
    } 
    is_coff = true;
  }

  int p0 = vertices.size();
  for(int j = 0; j < nVertices; j++) {
    double x, y, z;
    if(!is_coff) 
      ret = fscanf(f, "%lf %lf %lf ", &x, &y, &z); 
    else
      ret = fscanf(f, "%lf %lf %lf %*d %*d %*d %*d ", &x, &y, &z); 
    if(ret != 3) {
      fprintf(stderr, "error when reading vertex %d in OFF file %s\n", j, cone);
      exit(1); 
    }
    vertices.push_back(Vertex(x, y, z));
  }
  
  struct {
    float r, g, b; 
  } prev_color = {-1, -1, -1};  
  
  for(int j = 0; j < nFacets; j++) {
    facets.push_back(Facet(nmesh)); 
    Facet &facet = facets.back();
    char buf[10000];
    fgets(buf, 10000, f);
    int np, nr = 0, nri;
    ret = sscanf(buf + nr, "%d %n", &np, &nri); 
    assert(ret == 1);
    nr += nri;
    for(int k = 0; k < np; k++) {
      int ptno;
      ret = sscanf(buf + nr, "%d %n", &ptno, &nri);
      assert(ret == 1);
      nr += nri;
      assert(ptno >= 0 && ptno < nVertices); 
      facet.vertexNos.push_back(ptno + p0);
    }
    if(combined) {
      float r, g, b; 
      ret = sscanf(buf + nr, "%f %f %f", &r, &g, &b); 
      assert(ret == 3 || !"combined off requires facet colors"); 
      if(r != prev_color.r || g != prev_color.g || b != prev_color.b) {
        if(prev_color.r != -1) {
          nmesh ++; 
          facet.meshno = nmesh; 
        }
        prev_color.r = r; 
        prev_color.g = g; 
        prev_color.b = b;
      }
    }
  }
  fclose(f);
  meshno_to_vertex.push_back(vertices.size());
  nmesh++;
}


void CSG::addMeshFromObj(const char *fname) {
  if(nmesh >= BitVector::maxbits) {
    printf("cannot combine more than %d meshes\n", BitVector::maxbits);
    return;
  }

  FILE *f = fopen(fname, "r");
  if(!f) {
    fprintf(stderr, "cannot open %s\n", fname); 
    exit(1);
  }
  
  int bufsz = 128 * 1024;
  char * buf = new char[bufsz];

  int p0 = vertices.size();
  for(;;) {
    char *s = fgets(buf, bufsz, f); 
    if(!s) break;
    char *saveptr; 
    char *token = strtok_r(s, " \t\n\r", &saveptr);

    if(!token || token[0] == '#') continue; 

    if(!strcmp(token, "v")) {
      double x, y, z;
      int ret = sscanf(saveptr, "%lf %lf %lf ", &x, &y, &z); 
      assert(ret == 3);
      vertices.push_back(Vertex(x, y, z));      
    } else if(!strcmp(token, "f")) {
      facets.push_back(Facet(nmesh));       
      Facet &facet = facets.back();
      while((token = strtok_r(NULL, " \t\n\r", &saveptr)) != NULL) {
        int ptno;
        int ret = sscanf(token, "%d", &ptno); 
        assert(ret == 1); 
        facet.vertexNos.push_back(ptno - 1 + p0);              
      }
    } else if(!strcmp(token, "vt") || !strcmp(token, "vn") || !strcmp(token, "vp") || 
              !strcmp(token, "s") || !strcmp(token, "mtllib") || !strcmp(token, "usmtl") || 
              !strcmp(token, "g")) {
      // ignore
    } else {
      printf("addMeshFromObj %s: unknown OBJ line start %s\n", fname, token);
    }
  }
  delete [] buf;
  fclose(f);
  meshno_to_vertex.push_back(vertices.size());
  nmesh++;
}




void CSG::addMeshFromVFArray(int nVertices, 
                             const real (*pts)[3], 
                             const int *facets_in, int facetsSize) {
  if(nmesh >= BitVector::maxbits) {
    printf("cannot combine more than %d meshes\n", BitVector::maxbits);
    return;
  }
    
  int p0 = vertices.size();
  for(int i = 0; i < nVertices; i++) {
    vertices.push_back(Vertex(pts[i][0], pts[i][1], pts[i][2]));
  }

  int i = 0; 
  while(i < facetsSize) {
    facets.push_back(Facet(nmesh)); 
    Facet &facet = facets.back();
    int np = facets_in[i++];
    assert(np >= 3); 
    for(int k = 0; k < np; k++) {
      assert(i < facetsSize); 
      int ptno = facets_in[i++];
      assert(ptno < nVertices);
      facet.vertexNos.push_back(ptno + p0);
    }
  }
  meshno_to_vertex.push_back(vertices.size());
  nmesh++;
}




void CSG::saveVertices(const char *fname) const {
  FILE *f = fopen(fname, "w"); 
  if(!f) {
    fprintf(stderr, "could not open output %s\n", fname); 
    return; 
  }
  for(int i = 0; i < vertices.size(); i++) {
    const Vertex & v = vertices[i]; 
    if(v.keep == 2 || v.keep == 0) continue;
    fprintf(f, "%d %d %d      %g %g %g   %d\n", 
            v.facetNos[0], v.facetNos[1], v.facetNos[2], 
            v.coords.x, v.coords.y, v.coords.z, v.keep); 
  }

  fclose(f); 
}

/**********************************************************************
 *  Contours input
 **********************************************************************/

/* from Povray source code */
static bool invert4x4(double r[4][4], const double m[4][4])
{
  double d00, d01, d02, d03;
  double d10, d11, d12, d13;
  double d20, d21, d22, d23;
  double d30, d31, d32, d33;
  double m00, m01, m02, m03;
  double m10, m11, m12, m13;
  double m20, m21, m22, m23;
  double m30, m31, m32, m33;
  
  m00 = m[0][0]; m01 = m[0][1]; m02 = m[0][2]; m03 = m[0][3];
  m10 = m[1][0]; m11 = m[1][1]; m12 = m[1][2]; m13 = m[1][3];
  m20 = m[2][0]; m21 = m[2][1]; m22 = m[2][2]; m23 = m[2][3];
  m30 = m[3][0]; m31 = m[3][1]; m32 = m[3][2]; m33 = m[3][3];
  
  d00 = m11*m22*m33 + m12*m23*m31 + m13*m21*m32 - m31*m22*m13 - m32*m23*m11 - m33*m21*m12;
  d01 = m10*m22*m33 + m12*m23*m30 + m13*m20*m32 - m30*m22*m13 - m32*m23*m10 - m33*m20*m12;
  d02 = m10*m21*m33 + m11*m23*m30 + m13*m20*m31 - m30*m21*m13 - m31*m23*m10 - m33*m20*m11;
  d03 = m10*m21*m32 + m11*m22*m30 + m12*m20*m31 - m30*m21*m12 - m31*m22*m10 - m32*m20*m11;
  
  d10 = m01*m22*m33 + m02*m23*m31 + m03*m21*m32 - m31*m22*m03 - m32*m23*m01 - m33*m21*m02;
  d11 = m00*m22*m33 + m02*m23*m30 + m03*m20*m32 - m30*m22*m03 - m32*m23*m00 - m33*m20*m02;
  d12 = m00*m21*m33 + m01*m23*m30 + m03*m20*m31 - m30*m21*m03 - m31*m23*m00 - m33*m20*m01;
  d13 = m00*m21*m32 + m01*m22*m30 + m02*m20*m31 - m30*m21*m02 - m31*m22*m00 - m32*m20*m01;
  
  d20 = m01*m12*m33 + m02*m13*m31 + m03*m11*m32 - m31*m12*m03 - m32*m13*m01 - m33*m11*m02;
  d21 = m00*m12*m33 + m02*m13*m30 + m03*m10*m32 - m30*m12*m03 - m32*m13*m00 - m33*m10*m02;
  d22 = m00*m11*m33 + m01*m13*m30 + m03*m10*m31 - m30*m11*m03 - m31*m13*m00 - m33*m10*m01;
  d23 = m00*m11*m32 + m01*m12*m30 + m02*m10*m31 - m30*m11*m02 - m31*m12*m00 - m32*m10*m01;

  d30 = m01*m12*m23 + m02*m13*m21 + m03*m11*m22 - m21*m12*m03 - m22*m13*m01 - m23*m11*m02;
  d31 = m00*m12*m23 + m02*m13*m20 + m03*m10*m22 - m20*m12*m03 - m22*m13*m00 - m23*m10*m02;
  d32 = m00*m11*m23 + m01*m13*m20 + m03*m10*m21 - m20*m11*m03 - m21*m13*m00 - m23*m10*m01;
  d33 = m00*m11*m22 + m01*m12*m20 + m02*m10*m21 - m20*m11*m02 - m21*m12*m00 - m22*m10*m01;
  
  double D = m00*d00 - m01*d01 + m02*d02 - m03*d03;

  if (D == 0.0) {
    printf("Singular matrix in invert4x4");
    return false;
  }

  r[0][0] = d00/D; r[0][1] = -d10/D; r[0][2] = d20/D; r[0][3] = -d30/D;
  r[1][0] = -d01/D; r[1][1] = d11/D; r[1][2] = -d21/D; r[1][3] = d31/D;
  r[2][0] = d02/D; r[2][1] = -d12/D; r[2][2] = d22/D; r[2][3] = -d32/D;
  r[3][0] = -d03/D; r[3][1] = d13/D; r[3][2] = -d23/D; r[3][3] = d33/D;

  return true;
}

void CSG::addContoursAndCalib(int npt, 
                              const double (*contourpoints)[2], 
                              const double calib[12], 
                              double depth) {
  double P[4][4]; 
  double Pinv[4][4]; 
  IFV(1) printf("adding %d contour points as mesh %d\n", npt, nmesh); 
  if(nmesh >= BitVector::maxbits) {
    printf("cannot combine more than %d meshes\n", BitVector::maxbits);
    return;
  }

  memcpy(P, calib, sizeof(P[0][0]) * 12); 
  P[3][0] = 0; P[3][1] = 0; P[3][2] = 0; P[3][3] = 1; 
  bool ok = invert4x4(Pinv, P); 
  assert(ok || !"could not invert calib matrix"); 
  
  // optical centre
  Vec3 o(Pinv[0][3], Pinv[1][3], Pinv[2][3]);   
  
  int oi = -1; 

  // compute normalization factor to send all elements to depth
  Vec3 zaxis(Pinv[0][2], Pinv[1][2], Pinv[2][2]);   
  double zfac = depth / zaxis.length(); 
  IFV(2)
    printf("optical centre of cam %d: %g %g %g z axis: %g %g %g\n", nmesh, 
           o.x, o.y, o.z, zaxis.x, zaxis.y, zaxis.z);

  int i0 = -1;   
  double x0 = -1e30, y0 = -1e30;  // -1e30: assume there will be no points there...
  for(int i = 0; i < npt; i++) {
    double x = contourpoints[i][0]; 
    double y = contourpoints[i][1]; 
    if(!(x == x0 && y == y0)) {  // normal vertex
      if(i0 < 0) { // begin loop
        x0 = x; y0 = y;         
        oi = vertices.size(); 
        vertices.push_back(Vertex(o));   // we have to duplicate optical centre vertex so that the facetNo loop is closed
        i0 = vertices.size(); 
      } 
      Vec3 p = o;
      p.x += (Pinv[0][0] * x + Pinv[0][1] * y + Pinv[0][2]) * zfac; 
      p.y += (Pinv[1][0] * x + Pinv[1][1] * y + Pinv[1][2]) * zfac; 
      p.z += (Pinv[2][0] * x + Pinv[2][1] * y + Pinv[2][2]) * zfac; 

      vertices.push_back(Vertex(p)); 
    } else { // close loop
      assert(i0 >= 0);
      int i1 = vertices.size(); 
      int prev = i1 - 1; 
      int f0 = facets.size(), nf = i1 - i0, fno = 0; 
      IFV(2) printf("   making cone from top %d, basis %d:%d, first facet %zd\n", oi, i0, i1, facets.size());
      assert(i0 != -1);
      for(int j = i0; j < i1; j++) {        
        facets.push_back(Facet(nmesh)); 
        Facet & facet = facets.back();   
        facet.oppositeFacetNos.push_back(f0 + (fno + 1) % nf); 
        facet.vertexNos.push_back(oi); 
        facet.oppositeFacetNos.push_back(f0 + (nf + fno - 1) % nf); 
        facet.vertexNos.push_back(prev); 
        facet.oppositeFacetNos.push_back(-1); 
        facet.vertexNos.push_back(j); 
        
        // these should not be used
        vertices[j].facetNos.push_back(-1); 
        vertices[j].facetNos.push_back(-1); 
        vertices[oi].facetNos.push_back(f0 + fno); 
                
        facet.afterLoad(vertices);

        prev = j; 
        fno++; 
      }
      i0 = -1;
      x0 = y0 = -1e30;
    }
  }
  nvert1 = vertices.size();
  nmesh++; 
  meshno_to_vertex.push_back(vertices.size());
}

struct CalibMatrix {double m[12]; }; 

void CSG::loadContoursAndCalib(const char *contoursFileName, 
                               const char *calibFilename, 
                               double depth, 
                               double jitter, 
                               int cam0, int cam1, 
                               const std::vector<int> * cam_selection) {
  std::vector<CalibMatrix> calibs; 
  
  {
    FILE *f = fopen(calibFilename, "r"); 

    if(!f) {
      fprintf(stderr, "could not open calib file %s\n", calibFilename); 
      exit(1); 
    }
    for(int i = 0; ; i++) {
      CalibMatrix mat; 
      for(int j = 0; j < 12; j++) {
        int ret = fscanf(f, " %lf", &mat.m[j]); 
        if(ret != 1 && j == 0)  // EOF
          goto close; 
        assert(ret == 1 || !"could not parse calib file"); 
      }
      calibs.push_back(mat); 
    }
  close: 
    fclose(f); 
    IFV(1) printf("loaded %zd calibation matrices from %s\n", calibs.size(), calibFilename); 
  }

  int ncam = calibs.size();  
  if(cam1 == -1) cam1 = ncam;

  std::vector<int> my_cam_selection; 
  if(cam0 != 0 || cam1 != ncam) {
    for(int i = cam0; i < cam1; i++) my_cam_selection.push_back(i); 
    cam_selection = &my_cam_selection; 
  }

  std::vector<int> cam_map(ncam, -1); // maps mesh nb in file to calib matrix (or -1 if not used)
 
  if(cam_selection) {  
    for(int i = 0; i < cam_selection->size(); i++) {
      int camno = (*cam_selection)[i];
      assert(camno >= 0 && camno < ncam); 
      printf("adding cam %d\n", camno); 
      cam_map[camno] = i; 
    }
  } else {
    for(int i = 0; i < ncam; i++) cam_map[i] = i;    
  }
      
  {    
    FILE *f = fopen(contoursFileName, "r"); 

    if(!f) {
      fprintf(stderr, "could not open contour file %s\n", contoursFileName); 
      exit(1); 
    }
 
    int cur_mesh = -1;     
    std::vector<double> pts; 
    
    while(true) {      
      int meshno, npt; 
      if(fscanf(f, "%d %d ", &meshno, &npt) != 2) break; 
      assert((0 <= meshno && meshno < ncam) || !"meshes in countour file that are not in calib"); 
      assert(npt > 2 || !"contour should have at least 3 pts"); 
      if(meshno != cur_mesh) {
        if(cur_mesh != -1) {
          if(cam_map[cur_mesh] != -1)
            addContoursAndCalib(pts.size() / 2, (double(*)[2])&pts[0], calibs[cur_mesh].m, depth); 
          pts.clear();
        }
        cur_mesh = meshno; 
      }
      int c0 = pts.size(); 
      for(int j = 0; j < npt; j++) {
        double x, y; 
        int ret = fscanf(f, "%lf %lf", &x, &y);        
        assert(ret == 2);         
        x += jitter * (drand48() - 0.5); 
        y += jitter * (drand48() - 0.5); 
        pts.push_back(x); 
        pts.push_back(y); 
        assert(j == 0 || !(x == pts[c0] && y == pts[c0 + 1]) || !"self-intersecting contour"); 
      }      
      pts.push_back(pts[c0]);
      pts.push_back(pts[c0 + 1]);
    }        
    fclose(f); 
    if(cam_map[cur_mesh] != -1)
      addContoursAndCalib(pts.size() / 2, (double(*)[2])&pts[0], calibs[cur_mesh].m, depth); 
  }

}




void CSG::saveFacetToEPS(const char *fname, int f0, const int *vnos, int nv) {
  printf("saving facet %d to %s\n", f0, fname); 
  FILE *f = fopen(fname, "w"); 
  assert(f);
  // find projection
  Facet & facet = facets[f0]; 
  const Vec3 normal = facet.normal;
  Vec3 u(0, 0, 0); 
  real best_ul = 0; 
  for(int dim = 0; dim < 3; dim++) {
    Vec3 udim(0, 0, 0); 
    int dim1 = (dim + 1) % 3; 
    udim[dim] = -normal[dim1]; 
    udim[dim1] = normal[dim];
    real ul = udim.length();
    if(ul > best_ul) {
      best_ul = ul; 
      u = udim / ul;
    }      
  }
  Vec3 v = normal.cross(u); 
  
  real xmin = HUGE_VAL, ymin = HUGE_VAL, xmax = -xmin, ymax = -ymin; 
  
  for(int i = 0; i < facet.vertexNos.size(); i++) {
    int vno = facet.vertexNos[i]; 
    double x = u.dot(vertices[vno].coords); 
    double y = v.dot(vertices[vno].coords); 
    if(x < xmin) xmin = x; 
    if(y < ymin) ymin = y; 
    if(x > xmax) xmax = x; 
    if(y > ymax) ymax = y; 
  }
  
  real scale = 1500 / std::max(xmax - xmin, ymax - ymin); 
  
  fprintf(f, "%%!PS-Adobe-3.0\n%%%%BoundingBox: %d %d %d %d\n", 
          int(scale * xmin) - 30, int(scale * ymin) - 30, 
          int(scale * xmax) + 30, int(scale * ymax) + 30); 
  
  fprintf(f, "/Helvetica findfont 12 scalefont setfont\n"); 

  fprintf(f, "1 0 0 setrgbcolor\n"); 

  const char *ins = "moveto"; 
  for(int i = 0; i < facet.vertexNos.size(); i++) {
    int vno = facet.vertexNos[i]; 
    double x = u.dot(vertices[vno].coords); 
    double y = v.dot(vertices[vno].coords); 
    fprintf(f, "%g %g %s\n", x * scale, y * scale, ins); 
    ins = "lineto";
  }
  fprintf(f, "closepath stroke\n"); 
  fprintf(f, "0 setgray\n"); 

  for(int i = 0; i < facet.vertexNos.size(); i++) {
    int vno = facet.vertexNos[i]; 
    double x = u.dot(vertices[vno].coords); 
    double y = v.dot(vertices[vno].coords); 
    fprintf(f, "%g %g moveto (%d) show\n", x * scale, y * scale, vno); 
  }
  
  for(int i = 0; i < nv; i++) {
    int vno = vnos[i]; 
    const Vertex & vert = vertices[vno];
    double x = u.dot(vert.coords); 
    double y = v.dot(vert.coords); 
    fprintf(f, "%g %g 2 copy moveto 2 0 360 arc stroke\n", x * scale, y * scale); 
    fprintf(f, "%g %g moveto (%d/%d,%d,%d) show\n", 
            x * scale, y * scale, vno, vert.facetNos[0], vert.facetNos[1], vert.facetNos[2]);    
  }

  fclose(f);
}


void CSG::logFacetStats(const char *fname) {
  FILE *f = fopen(fname, "w"); 
  assert(f);
  {
    ProcessingStats &ps = processingStats.mergeVertexTables;   
    fprintf(f, "mergeVertexTables %ld %ld %ld\n", ps.begin_cy, ps.end_cy, ps.thread_id); 
  }
  {
    ProcessingStats &ps = processingStats.preFacet;   
    fprintf(f, "preFacet %ld %ld %ld\n", ps.begin_cy, ps.end_cy, ps.thread_id); 
  }
  for(int i = 0; i < facetProcessingStats.size(); i++) {
    ProcessingStats &ps = facetProcessingStats[i]; 
    fprintf(f, "F%d %ld %ld %ld\n", i, ps.begin_cy, ps.end_cy, ps.thread_id); 
  }  
  fclose(f);
}



/************************************************************
 * Serialization / de-serialization
 */


#define SERIALIZE(var) { \
  assert(bufsz >= sizeof(var)); \
  memcpy(buf, &(var), sizeof(var)); \
  bufsz -= sizeof(var); buf += sizeof(var); \
}


#define DESERIALIZE(var) { \
  assert(bufsz >= sizeof(var)); \
  memcpy(&(var), buf, sizeof(var));  \
  bufsz -= sizeof(var); buf += sizeof(var); \
}

size_t Vertex::serialSize() const {
  return 
    sizeof(coords) + 
    sizeof(int) + facetNos.size() * sizeof(int) + 
    sizeof(meshpos) + sizeof(int); 
}

size_t Vertex::serialize(char *buf, size_t bufsz) const {
  SERIALIZE(coords); 
  int fsz = facetNos.size();
  SERIALIZE(fsz); 
  for(int i = 0; i < fsz; i++) SERIALIZE(facetNos[i]); 
  SERIALIZE(meshpos); 
  SERIALIZE(keep);
  return bufsz; 
}



size_t Vertex::deserialize(const char *buf, size_t bufsz) {
  DESERIALIZE(coords); 
  int fsz;
  DESERIALIZE(fsz); 
  facetNos.resize(fsz); 
  for(int i = 0; i < fsz; i++) DESERIALIZE(facetNos[i]); 
  DESERIALIZE(meshpos); 
  DESERIALIZE(keep); 
  return bufsz;
}



size_t Facet::serialSize() const {
  return 
    sizeof(normal) + sizeof(b) + sizeof(meshno) + 
    sizeof(int) + vertexNos.size() * 2 * sizeof(int); 
}

size_t Facet::serialize(char *buf, size_t bufsz) const {
  SERIALIZE(normal); 
  SERIALIZE(b); 
  SERIALIZE(meshno); 
  int vsz = vertexNos.size(); 
  SERIALIZE(vsz);
  for(int i = 0; i < vsz; i++) SERIALIZE(vertexNos[i]); 
  for(int i = 0; i < vsz; i++) SERIALIZE(oppositeFacetNos[i]); 
  // do not serialize output stuff
  return bufsz;
}

size_t Facet::deserialize(const char *buf, size_t bufsz) {
  DESERIALIZE(normal); 
  DESERIALIZE(b); 
  DESERIALIZE(meshno); 
  int vsz;
  DESERIALIZE(vsz);
  vertexNos.resize(vsz); 
  oppositeFacetNos.resize(vsz); 
  for(int i = 0; i < vsz; i++) DESERIALIZE(vertexNos[i]); 
  for(int i = 0; i < vsz; i++) DESERIALIZE(oppositeFacetNos[i]); 
  return bufsz;
}


