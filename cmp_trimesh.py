# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import sys



def normalize_triplet(a, facet_no_shift = 0):
  a = [int(x) - facet_no_shift for x in a.split(',')]
  a.sort()
  return tuple(a)


def load_trimesh(fname, facet_no_shift = 0):
  facets = set()
  for l in open(fname, "r"):
    verts = [normalize_triplet(x, facet_no_shift) for x in l.split()]
    minv = min(verts)
    imin = verts.index(minv)
    verts = tuple(verts[imin:] + verts[:imin])
    facets.add(verts)
  return facets

if len(sys.argv) > 3: 
  facet_no_shift = int(sys.argv[3])
else:
  facet_no_shift = 0

ref = load_trimesh(sys.argv[1])
new = load_trimesh(sys.argv[2], facet_no_shift)



err = 0
for a in ref:
  if a not in new:
    print "MISSING", a
    err = 1

for a in new:
  if a not in ref:
    print "EXTRA", a
    err = 2


if err:
  print "failed:", " ".join(sys.argv)

sys.exit(err)
