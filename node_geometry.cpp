/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */

#include <cmath>
#include <cstdio>

#ifndef _WIN32

#include <sys/types.h>
#include <sys/time.h>

#else

#include <Windows.h>

#endif

#include "mCSG.hpp"
#include "kdtree.hpp"




using namespace mCSG;




/**********************************************************************
 * Exploration remaining within a kdtree node
 **********************************************************************/

#ifdef _WIN32
double mCSG::getmillisecs() {
  SYSTEMTIME  system_time;
  FILETIME    file_time;
  uint64_t    time;
  GetSystemTime(&system_time);
  SystemTimeToFileTime(&system_time, &file_time);
  time = ((uint64_t)file_time.dwLowDateTime);
  time += ((uint64_t)file_time.dwHighDateTime) << 32; 
  return 100e-9 * 1000 * time; 
}

long mCSG::getcycles(void) {
  return 0;
}

long pthread_self() {
  return 0;
}

#else

double mCSG::getmillisecs() {
  struct timeval tv;
  gettimeofday (&tv,NULL);
  return tv.tv_sec*1e3 +tv.tv_usec*1e-3;
}


long mCSG::getcycles(void) {
  unsigned int lo, hi;
#ifdef __clang__
  lo = hi = 0;
#else 
  __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
#endif
  return ((long) hi << 32 | (long)lo);
}
  
#endif
  
void ProcessingStats::begin() {
  if(begin_cy != 0) return;
  begin_cy = getcycles();
  thread_id = (long)pthread_self();
}

void ProcessingStats::end() {
  if(end_cy != 0) return;
  end_cy = getcycles();
}

static bool vec3Larger(Vec3 a, Vec3 b) {
  return a.x > b.x && a.y > b.y && a.z > b.z;
}


static void check_final_node(Node *node, CSGOperation *csgop, int nmesh, int verbose) {
  // check if node is entirely inside or outside, set state if needed

  int in = csgop->isInside(node->meshpos);

  IFV(2) printf("  Checking node %s state %d meshpos %s in=%d \n", 
                node->toString().c_str(), node->state, node->meshpos.toString(nmesh).c_str(), in );

  if(in == 0 || in == 1) {
    node->state = 3; 
    IFV(3) printf("    No need to expand node\n"); 
    return; 
  }

}


static void node_debugging(const CSG & csg, const Node *node) {
  if(csg.primaryVertexToFollow >= -1) { // for debugging
    Vec3 query; 
    if(csg.primaryVertexToFollow == -1) 
      query = csg.vertexToFollow;
    else
      query = csg.vertices[csg.primaryVertexToFollow].coords;
    
    if(vec3Larger(query, node->bbMin) && vec3Larger(node->bbMax, query)) {
      printf("Vertex %d [%g %g %g] should be in node %s bbox [%g %g %g] [%g %g %g]\n", 
             csg.primaryVertexToFollow, query.x, query.y, query.z, node->toString().c_str(),
             node->bbMin[0], node->bbMin[1], node->bbMin[2], 
             node->bbMax[0], node->bbMax[1], node->bbMax[2]);
      if(csg.primaryVertexToFollow >= 0) {
        for(int i = 0; i < node->polygons.size(); i++) {
          const ConvexPolygon * poly = node->polygons[i];
          for(int j = 0; j < poly->vertices.size(); j++) 
            if(poly->vertices[j].primaryNo == csg.primaryVertexToFollow) 
              printf("  found as vertex %d of polygon %d, facetNo %d\n", 
                     j, i, poly->facetNo);
          
        }
      }
    }
  }
  
  if(csg.nodeToSave && node->toString() == csg.nodeToSave) {
    printf("storing node %s to %s\n", node->toString().c_str(), csg.nodeFname); 
    node->toOff(csg.nodeFname); 
  }

  


}

// make children or vertices
static void split_or_find_vertices(CSG & csg, Node *node, CSGOperation * csgop) {

  int verbose = csg.verbose;

  IFV(2) printf("  Split or find vertices in node %s, state %d, meshpos %s, %zd polygons\n", 
                node->toString().c_str(), node->state, node->meshpos.toString(csg.nmesh).c_str(), node->polygons.size());

  node_debugging(csg, node);

  node->processingStats.begin(); 

  if(node->state == 0 && node->meshpos.nbKnown() < csg.nmesh - 1) {  // do not split single-mesh nodes
    IFV(2) printf("    Attempting to split node\n"); 
    
    node->split(csg);    
  }  

  if(node->state == 0 || node->state == 2) { 
    IFV(2) printf("    No further split: Find vertices in node\n"); 
    csg.findNodeVertices(node, csgop);
  }

  if(node->state == 1) {
    check_final_node(node->child1, csgop, csg.nmesh, verbose); 
    check_final_node(node->child2, csgop, csg.nmesh, verbose); 
  }

  node->processingStats.end(); 
    
}

void CSG::exploreNode(Node *node, CSGOperation *csgop) {

  if(node->state == 3) return;
  
  split_or_find_vertices(*this, node, csgop); 
  clearNodePolygons(node);

  if(node->state == 1) { // children need processing

#ifdef ENABLE_TBB
    bool parallel = parallel_nt != 1 && 
      (node->depth < parallel_depth || 
       node->child1->polygons.size() + node->child2->polygons.size() > parallel_min_npoly);
    
    if(parallel) {
      
      tbb::task_group g;
      if(node->child1->state != 3)
	g.run( [&]{exploreNode(node->child1, csgop);} );
      if(node->child2->state != 3)
	g.run( [&]{exploreNode(node->child2, csgop);} );
      g.wait();  

      clearNodePolygons(node->child1);
      clearNodePolygons(node->child2);
      
      if(alloc_policy >= 2) 
        deleteNodeChildren(node);
     
      
    } else
#endif
    { // sequential mode
      if(node->child1->state != 3) {
	exploreNode(node->child1, csgop); 
      }
      clearNodePolygons(node->child1);
      
      if(node->child2->state != 3) {
	exploreNode(node->child2, csgop);         
      }
      clearNodePolygons(node->child2);
      if(alloc_policy != 0) 
        deleteNodeChildren(node);

    }
  } 

}


void CSG::exploreBreadth(CSGOperation *csgop) {
  
  std::vector<Node *> cur_nodes; 
  cur_nodes.push_back(root);

  IFV(2) printf("  Explore breadth-first until depth %d\n", breadth_depth); 

  for(int depth = 0; depth < breadth_depth; depth++) {
    std::vector<Node *> next_nodes; 
    IFV(2) printf("    Explore depth %d\n", depth);     
    
#ifdef ENABLE_TBB
    bool parallel = parallel_nt != 1; 
    
    if(parallel) {      
      
      tbb::task_group g;     
      
      for(int i = 0; i < cur_nodes.size(); i++) {
        Node * node = cur_nodes[i]; 
        g.run( [this, node, csgop] {
            split_or_find_vertices(*this, node, csgop); 
            clearNodePolygons(node);
          } );         
      }
      
      g.wait();             
      
    } else
#endif
    { // sequential mode
              
      for(int i = 0; i < cur_nodes.size(); i++) {
        Node * node = cur_nodes[i]; 
        split_or_find_vertices(*this, node, csgop);    
        clearNodePolygons(node);
      }
    }


    for(int i = 0; i < cur_nodes.size(); i++) {
      Node * node = cur_nodes[i]; 
      if(node->state == 1) {
        
        if(node->child1->state != 3) 
          next_nodes.push_back(node->child1);           
        else
          clearNodePolygons(node->child1);

        if(node->child2->state != 3) 
          next_nodes.push_back(node->child2);                              
        else
          clearNodePolygons(node->child2);
      }
    }    
    cur_nodes.swap(next_nodes); 
  }

  //  hand over exploration to depth-first
#ifdef ENABLE_TBB
  bool parallel = parallel_nt != 1; 
  if(parallel) {
    tbb::task_group g;     
    
    for(int i = 0; i < cur_nodes.size(); i++) {
      Node * node = cur_nodes[i]; 
      g.run( [this, node, csgop] { exploreNode(node, csgop); } );         
    }
    
    g.wait();       

  } else 
#endif
  { 
    for(int i = 0; i < cur_nodes.size(); i++) {
      Node * node = cur_nodes[i];       
      exploreNode(node, csgop); 
    }
  }            
  
}

void CSG::clearNodePolygons(Node * node) {
  for(int i = 0; i < node->polygons.size(); i++) {
    ConvexPolygon *poly = node->polygons[i];
    poly->~ConvexPolygon();
    polygonPool.dealloc(poly); 
  }
  node->polygons.clear();
}

void CSG::deleteNodeChildren(Node *node) {
  node->child1->~Node(); nodePool.dealloc(node->child1); 
  node->child2->~Node(); nodePool.dealloc(node->child2); 
  node->child1 = node->child2 = NULL;
}


void CSG::mergeVertexTables() {
  processingStats.mergeVertexTables.begin();

#if 0
  // clean way of doing it
  for(ConcurrentVertices::iterator i=threadLocalVertices.begin(); i!=threadLocalVertices.end(); ++i) {
    vertices.insert(vertices.end(),i->begin(),i->end());
    i->clear();
  }
#endif 

#ifdef ENABLE_TBB
  {
    // really ugly: replace C++ copy constructors with memcpy. 
    int nv = vertices.size(); 
    for(ConcurrentVertices::iterator i=threadLocalVertices.begin(); i!=threadLocalVertices.end(); ++i) {
      nv += i->size(); 
    }
    int idx = vertices.size(); 
    { 
      std::vector<Vertex> new_vertices; 
      new_vertices.resize(nv); 
      memcpy(&new_vertices[0], &vertices[0], sizeof(vertices[0]) * vertices.size()); 
      memset(&vertices[0], 0, sizeof(vertices[0]) * vertices.size()); // avoid dealloc of VectorPreferredSize's 
      std::swap(new_vertices, vertices);
    }
    // facet VectorPreferredSize's can be copied with memcpy because they all have size == 3
    for(ConcurrentVertices::iterator i=threadLocalVertices.begin(); i!=threadLocalVertices.end(); ++i) {
      int sz = i->size();
      memcpy(&vertices[idx], &(*i)[0], sizeof(vertices[0]) * sz); 
      (*i).clear();
      idx += sz;
    }
    assert(idx == nv);    
  }
#endif

  processingStats.mergeVertexTables.end();
}



static Vertex &newVertex(const Vec3 &coords, int f0, int f1, int f2, std::vector<Vertex> &vertices, int verbose){

  IFV(3) printf("        New vertex V%zd\n", vertices.size()); 
  
  vertices.push_back(Vertex(coords.x,coords.y,coords.z));
  Vertex &v = vertices.back();

  v.facetNos.push_back(f0);
  v.facetNos.push_back(f1);
  v.facetNos.push_back(f2);
  v.keep = 0;
  return v;
}


                            

void CSG::findNodeVertices(Node *node, CSGOperation *csgop) {

  std::vector<Vertex> *localVertices;

#if ENABLE_TBB
  if(parallel_nt != 1) 
    localVertices = &threadLocalVertices.local(); 
  else 
#endif
  localVertices = &vertices;


  int cell_nmesh = nmesh - node->meshpos.nbKnown(); // nb of meshes that this node is fully inside
  
  int n_polygon = node->polygons.size();

  IFV(2) {
    printf("Exploring node %s, bbox (%g %g %g)-(%g %g %g), %d polygons", 
           node->toString().c_str(), 
           node->bbMin.x, node->bbMin.y, node->bbMin.z, 
           node->bbMax.x, node->bbMax.y, node->bbMax.z, 
           n_polygon); 
    IFV(4) {
      printf(" ["); 
      for(int i = 0; i < n_polygon; i++) 
        printf("F%d ", node->polygons[i]->facetNo); 
      printf("]\n"); 
    } else printf("\n");     
  }
 
  if(cell_nmesh == 0) return;

  IFV(2) printf("Explore node %s: primitive pts\n", node->toString().c_str()); 
  for(int i = 0; i < n_polygon; i++) {
    const ConvexPolygon *poly = node->polygons[i];
    for(int j = 0; j < poly->vertices.size(); j++) {
      int vno = poly->vertices[j].primaryNo;           
      if(vno < 0) continue;

      Vertex & v = vertices[vno];      

      if(v.keep != 0) continue; // already processed

      IFV(3) printf("   Candidate simple point: V%d (%g %g %g) mesh %d\n", 
                    vno, v.coords.x, v.coords.y, v.coords.z, poly->meshno);       
          
      int in2 = node->computeMeshposLazy(v.coords, csgop, poly->meshno, -1, -1, nmesh, v.meshpos, verbose); 
            
      IFV(3) printf("      meshpos %s, in2=%02x: %s\n", 
                    v.meshpos.toString(nmesh).c_str(), in2, is_corner_in2(in2) ? "keep" : "skip"); 

      if(!is_corner_in2(in2)) {
        v.keep = 2;
        continue;
      }
      
      v.keep = 1;      
    }
  }

  if(cell_nmesh == 1) return;

  IFV(2) printf("Explore node %s: double & triple pts\n", node->toString().c_str()); 

  for(int i = 0; i < n_polygon; i++) {
    const ConvexPolygon *poly0 = node->polygons[i]; 
    int f0 = poly0->facetNo; 
    for(int j = i + 1; j < n_polygon; j++) {
      const ConvexPolygon *poly1 = node->polygons[j]; 
      int f1 = poly1->facetNo; 
      
      if(poly0->meshno == poly1->meshno) continue; 
      
      IFV(3) printf("   Intersection edge of polygons F%d F%d (meshes %d %d)\n", f0, f1, poly0->meshno, poly1->meshno); 
      
      // the polygons are convex, so there are at most 4 intersections
      InPlaneIntersection inters[4];       
      Vec3 origin, direction;

      int ninter = poly0->polygonIntersections(*poly1, origin, direction, inters, *this); //?  which origin/direction
      
      if(ninter < 0) {
        IFV(3) printf("      !!! ERROR inconsistent output!\n");
        errors.add("polygonIntersections: inconsistent output");
        continue;
      }

      IFV(3) {       

        if(ninter == 0) printf("     no intersection\n"); 
        else {
          printf("     Origin (%g %g %g) direction (%g %g %g), intersections: [", 
                 origin.x, origin.y, origin.z, 
                 direction.x, direction.y, direction.z);         
          for(int k = 0; k < ninter; k++) 
            printf("%g(%d,poly%d) ", inters[k].t, inters[k].exist, inters[k].which);
          printf("] %s\n", ninter == 0 ? "(skip)" : "(process)");
        }
      }
      
      if(ninter == 0) continue; 

      for(int k = 0; k < ninter; k++) {         
        InPlaneIntersection &inter = inters[k];          	      

	int f2 = (inter.which == 0 ? poly0 : poly1)->vertices[inter.pi].borderFacetNo;

	IFV(3) {
          Vec3 pt = origin + inter.t * direction; 
          printf("     Candidate double point t=%g (%g %g %g), border facet F%d, inter exist=%d\n",
                 inter.t, pt.x, pt.y, pt.z, f2, inter.exist);        
        }
        
        if(f2 < 0) {
          IFV(3) printf("       point is on kdtree cell border: skip\n"); 
          continue;
        }

	if((inter.which == 0 && f0 > f2) ||
	   (inter.which == 1 && f1 > f2)) {
          IFV(3) printf("       should be found when exploring F%d F%d edge: skip\n", 
			inter.which == 0 ? f0 : f1, f2); 
          continue;	  
	}

        
        BitVector meshpos; 
        int in4 = node->computeMeshposLazy(inter.coords, csgop, poly0->meshno, poly1->meshno, -1, nmesh, meshpos, verbose);         

	IFV(3) printf("     meshpos %s in4=%04x: %s\n", 
                      meshpos.toString(nmesh).c_str(), in4, 
                      is_corner_in4(in4) ? "keep" : "skip");
        
        if(!is_corner_in4(in4)) continue;

        //Add Vertex

        Vertex &v = newVertex(inter.coords,f0,f1,f2,*localVertices, verbose);

        v.keep = 1; 
        v.meshpos = meshpos;
        
      }
      
      if(cell_nmesh < 3) continue;
      
      // handle triple points here
      
      for(int k = j + 1; k < n_polygon; k++) {
        const ConvexPolygon *poly2 = node->polygons[k]; 
	int f2 = poly2->facetNo;
	if(poly2->meshno == poly0->meshno || poly2->meshno == poly1->meshno) continue; 
        
	real t = (poly2->b - poly2->normal.dot(origin)) / poly2->normal.dot(direction); 
	
	Vec3 x = origin + t * direction; 

	IFV(3) 
          printf("     Candidate triple point: intersection of F%d inter F%d with F%d, t=%g, coords (%g %g %g)\n", 
                 f0, f1, f2, t, x.x, x.y, x.z);
        
	// see if it falls in one of the intervals
        int l;
	for(l = 0; l < ninter; l += 2) {
	  if(inters[l].t < t && t < inters[l + 1].t) break;
	}
	if(l == ninter) {
	  IFV(3) printf("       Intersection not in segment F%d inter F%d\n", f0, f1);            
	  continue; // no it does not
	}

	// see if it falls in the polygon        
        int is_inside = poly2->isPointInside(x); 
        
        if(is_inside < 0) {
          errors.add("CSG::findNodeVertices: could not determine insideness");
          continue; 
        }

	if(!is_inside) {
	  IFV(3) printf("     Intersection not in polygon F%d\n", f2);            
	  continue;
	}

                
        BitVector meshpos; 
        int in8 = node->computeMeshposLazy(x, csgop, poly0->meshno, poly1->meshno, poly2->meshno, nmesh, meshpos, verbose);         

	IFV(3) printf("     meshpos %s in8=%08x: %s\n", 
                      meshpos.toString(nmesh).c_str(), in8, 
                      is_corner_in8(in8) ? "keep" : "skip");
        
        if(!is_corner_in8(in8)) continue;

        //add vertex

        Vertex &v = newVertex(x, f0, f1, f2, *localVertices, verbose);

        v.keep = 1; 
        v.meshpos = meshpos;

        IFV(3) printf("     New vertex\n");
        
      }
    }    
  }
}



/**********************************************************************
 * Polygon operations 
 **********************************************************************/




static int solve_2x2(real a11, real a12, real b1, 
                     real a21, real a22, real b2,
                     real *x1, real *x2) {
  real det = a21 * a12 - a22 * a11; 
  if(det == 0) { // TODO eps
    return 0; 
  } 
  
  *x2 = (b1 * a21 - b2 * a11) / det; 
  *x1 = (b2 * a12 - b1 * a22) / det; 
  return det < 0 ? 1 : 2;
}


static int argMax3(real v0, real v1, real v2) {
  if(v0 > v1) {
    if(v0 > v2) return 0;
    else return 2;
  } else {
    if(v1 > v2) return 1;
    else return 2;
  }
}

static int argMaxVec3(const Vec3 &v) {
  return argMax3(v[0], v[1], v[2]);
}

static int argMinAbsVec3(const Vec3 &v) {
  return argMax3(-fabs(v[0]), -fabs(v[1]), -fabs(v[2]));
}

static int argMaxAbsVec3(const Vec3 &v) {
  return argMax3(fabs(v[0]), fabs(v[1]), fabs(v[2]));
}

int ConvexPolygon::allLineInPlaneIntersections(const Vec3 &orig, const Vec3 &dir, 
                                               InPlaneIntersection * inters) const {
    
  // we work in projection on plane (d1, d2)
  int od = argMaxAbsVec3(normal);
  int d1 = (od + 1) % 3, d2 = (od + 2) % 3;   

  int ninter = 0;
  int prev_i = vertices.size() - 1;

  
  for(int i = 0; i < vertices.size(); i++) {
    const Vec3 &p0 = vertices[prev_i].coords;
    const Vec3 &p1 = vertices[i].coords;
    InPlaneIntersection &inter = inters[ninter];
    
    real u, t;
    u = t = std::numeric_limits<real>::quiet_NaN();
    
    int ret = solve_2x2(dir[d1], p0[d1] - p1[d1], p0[d1] - orig[d1], 
                        dir[d2], p0[d2] - p1[d2], p0[d2] - orig[d2], 
                        &t, &u); 
    
    if(ret && u >= 0 && u <= 1) { // there is an intersection      
      if(ninter >= 2) {
	return 3; 
      }
      inter.exist = normal[od] > 0 ? ret : 3 - ret; // swap 1 and 2
      inter.t = t; 
      inter.pi = i;
      inter.coords = orig + t * dir;
      ninter++;
    }
    prev_i = i;
  }
  return ninter;
}

static bool cmpInPlaneIntersections(const InPlaneIntersection &i, const InPlaneIntersection &j) { return i.t < j.t; }


int ConvexPolygon::polygonIntersections(const ConvexPolygon & other,
                                        Vec3 & origin, Vec3 & direction,  
                                        InPlaneIntersection *inters, CSG & csg) const {
  int verbose = csg.verbose; 
  
  IFV(4) 
    printf("      polygonIntersections F%d F%d:\n", 
           facetNo, other.facetNo);


  for(int dim = 0; dim < 3; dim++) 
    if(bbMin[dim] > other.bbMax[dim] ||
       bbMax[dim] < other.bbMin[dim]) {
      IFV(4) printf("        no bbox intersection\n");
      return 0; 
    }
  
  // compute intersection line
  direction = normal.cross(other.normal); 

  if(direction.lengthSquared() == 0) {
    if(b != other.b) return 0;     
    csg.errors.add("ConvexPolygon::polygonIntersections: facets are on same plane");
    return -1; // then we are in real trouble
  }
  direction.normalize();
  Vec3 p0 = vertices[0].coords;
  Vec3 dorth = normal.cross(direction); 
  real t = (other.b - p0.dot(other.normal)) / dorth.dot(other.normal); 
  
  
  origin = p0 + t * dorth;  

  // test on bbox
  real t0 = -HUGE_VAL, t1 = HUGE_VAL; 

  for(int dim = 0; dim < 3; dim++) {
    real a = direction[dim]; 
    real b = origin[dim];   
    if(a == 0) continue; 
    real t_min_0 = (bbMin[dim] - b) / a; 
    real t_max_0 = (bbMax[dim] - b) / a; 
    real t_min_1 = (other.bbMin[dim] - b) / a; 
    real t_max_1 = (other.bbMax[dim] - b) / a; 
    if(a > 0) {
      if(t_min_0 > t0) t0 = t_min_0; 
      if(t_min_1 > t0) t0 = t_min_1; 
      if(t_max_0 < t1) t1 = t_max_0; 
      if(t_max_1 < t1) t1 = t_max_1; 
    } else {
      if(t_max_0 > t0) t0 = t_max_0; 
      if(t_max_1 > t0) t0 = t_max_1; 
      if(t_min_0 < t1) t1 = t_min_0; 
      if(t_min_1 < t1) t1 = t_min_1; 
    }
  }


  IFV(4) 
    printf("      origin (%g %g %g) dir (%g %g %g) t range in bbox: [%g %g]\n", 
           origin.x, origin.y, origin.z, direction.x, direction.y, direction.z, t0, t1);

  if(t1 < t0) {
    IFV(4) printf("        intersection not in bbox\n"); 
    return 0; 
  }

  // compute intersections with polygon boundaries
  
  int n0 = allLineInPlaneIntersections(origin, direction, inters); 
  if(n0 == 3) {
    csg.errors.add("ConvexPolygon::allLineInPlaneIntersections: too many intersections for a convex polygon");
    n0 = 2; 
  }
  for(int i = 0; i < n0; i++) inters[i].which = 0; 

  int n1 = other.allLineInPlaneIntersections(origin, direction, inters + n0); 
  if(n1 == 3) {
    csg.errors.add("ConvexPolygon::allLineInPlaneIntersections: too many intersections for a convex polygon");
    n1 = 2; 
  }
  for(int i = 0; i < n1; i++) inters[i + n0].which = 1; 
  
  std::sort(inters, inters + n0 + n1, cmpInPlaneIntersections); 

  IFV(4) {
    printf("       sorted intersections: ["); 
    for(int i = 0; i < n0 + n1; i++) 
      printf("(%g %s border F%d exist %d) ", 
             inters[i].t, 
             inters[i].which == 0 ? "this" : "other", 
             inters[i].which == 0 ? vertices[inters[i].pi].borderFacetNo : other.vertices[inters[i].pi].borderFacetNo,
             inters[i].exist); 
    printf("] \n"); 
  }
  
  
  // compute intervals
  bool in0 = false, in1 = false, in = false;
  int ninter = 0;
  for(int i = 0; i < n0 + n1; i++) {
    InPlaneIntersection &inter = inters[i];
    if(inter.which == 0) in0 = !in0; 
    else                 in1 = !in1;
    bool new_in = in0 && in1; 

    if(new_in != in) {
      if(!((inter.exist == 1 && new_in) || (inter.exist == 2 && !new_in))) {
	return -1;
      }
      inters[ninter++] = inter;      
    }
    in = new_in;
  }
  if(!(ninter % 2 == 0 && !in)) {
    return -1;
  }

  return ninter;

}



int ConvexPolygon::isPointInside(const Vec3 & x) const {
  // select the most orthogonal of canonic directions with plane
  int dim = argMinAbsVec3(normal);
  
  // consider plane with eq x[dim] = v
  real v = x[dim];

  // compute intersections of this plane with edges of facet  
  int prev_i = vertices.size() - 1;
  real nearest_d2 = HUGE_VAL;  
  int inside = 0; 

  for(int i = 0; i < vertices.size(); i++) {
    const Vec3 &p0 = vertices[prev_i].coords;
    const Vec3 &p1 = vertices[i].coords;
    if((p0[dim] > v) != (p1[dim] > v)) { // edge crosses plane
	
      // find coordinates of intersection
      real u = (v - p0[dim]) / (p1[dim] - p0[dim]);
      Vec3 y = p0 + u * (p1 - p0); 
      real d2 = (x - y).lengthSquared();
      
      if(d2 < nearest_d2) {
        Vec3 cp = (x - y).cross(p1 - p0); 
        real dp = cp.dot(normal);
       
        if(dp < 0) 
          inside = 1;
        else 
          inside = 0;
        nearest_d2 = d2;
      }      
    }      
    prev_i = i;
  }
  if(nearest_d2 == 0)  // TODO epsilon
    return -1; 
  return inside;
}


void ConvexPolygon::lineIntersection(const Vec3 &orig, const Vec3 &dir, 
                      Intersection & inter) const {

  real den = -normal.dot(dir); 
  if(den == 0) { // TODO replace with eps-checks
    if(normal.dot(orig) == b) {
      inter.exist = -1;
    } else
      inter.exist = 0;
    return;
  }

  inter.t = (normal.dot(orig) - b) / den;
  inter.coords = orig + inter.t * dir;

  int inside = isPointInside(inter.coords); 
  if(inside == -1) inter.exist = -1; 
  else if(!inside) inter.exist = 0;     
  else             inter.exist = den > 0 ? 1 : 2; 
}


int Node::isInsideMesh(const Vec3 &orig, int meshno, int verbose) const {  
  Vec3 dir;
  Intersection inter; 
  inter.t = HUGE_VAL; 
  inter.exist = 0; 
  IFV(5) printf("      isInsideMesh (%g %g %g) mesh %d\n", orig.x, orig.y, orig.z, meshno); 
  for(int i = 0; i < polygons.size(); i++) {
    const ConvexPolygon *polygon = polygons[i];
    if(polygon->meshno != meshno) continue; 
    IFV(5) printf("        polygon %d, F%d\n", i, polygon->facetNo);         
    if(inter.exist == 0) { // first polygon we find
      Vec3 pp = polygon->center(); // choose arbitray point on facet and in kdtree node
      dir = pp - orig;
      real planepos = polygon->b - orig.dot(polygon->normal); 
      IFV(5) printf("          line with polygon centre (%g %g %g), planepos = %g\n", 
                    pp.x, pp.y, pp.z, planepos); 
      if(planepos == 0) return -1; // danger! TODO eps check
      bool inside = planepos > 0; 
      // fake intersection
      inter.exist = inside ? 2 : 1; 
      inter.t = 1; 
      inter.coords = pp; 
      IFV(5) printf("          exist = %d t = %g\n", inter.exist, inter.t); 
    } else { // see if new polygon is closer        
      Intersection new_inter; 
      polygon->lineIntersection(orig, dir, new_inter); 
      IFV(5) printf("          additional intersection, exist = %d t = %g\n", new_inter.exist, new_inter.t); 
      if(new_inter.exist < 0) {
        inter = new_inter; 
        return -1;  // cannot determine intersection: danger!
      } else if(new_inter.exist > 0 && new_inter.t > 0 && new_inter.t < inter.t) {
        IFV(5) printf("            keep\n"); 
        inter = new_inter;       
      }
    }
  }
  IFV(5) printf("      nearest intersection exist = %d t = %g\n", inter.exist, inter.t); 
  if(inter.exist == 0) return 0; // we did not cross any facet -> we are outside
  if(inter.exist == 1) return 0; // first intersection was going into mesh -> we were outside before
  if(inter.exist == 2) return 1; // was going out of mesh -> we were inside before
  assert(!"a case we did not handle ???");
}

int Node::setMeshpos(const Vec3 &orig, BitVector &meshpos, int nmesh, int verbose,
                     int ignore_mesh0, int ignore_mesh1, int ignore_mesh2) const {
  
  meshpos = this->meshpos; 
  
  for(int i = 0; i < nmesh; i++) {
    if(meshpos.isKnown(i) ||
       i == ignore_mesh0 || i == ignore_mesh1 || i == ignore_mesh2) continue; 
    int imesh = isInsideMesh(orig, i, verbose); 
    if(imesh < 0) return -1;  
    meshpos.setSide(i, imesh); 
  }       
  
  return 1;
  
}

/**************************************************************************************
 * Lazy evaluation
 **************************************************************************************/

int Node::isInsidePolygon(const Vec3 &orig, int i, int verbose) const { 
  const ConvexPolygon *polygon = polygons[i];
    
  real planepos = polygon->b - orig.dot(polygon->normal); 
  IFV(5) printf("          line with polygon %d, F%d planepos = %g\n", 
                i, polygon->facetNo, planepos); 
  if(planepos == 0) return -1; // danger! TODO eps check
  if(planepos > 0) return 1; 
  else return 0; 
}

int Node::isInsidePolygons(const Vec3 &orig, int i, int j, int verbose) const { 
  Vec3 dir;
  Intersection inter; 
  IFV(5) printf("      isInsidePolygons (%g %g %g) %d to %d\n", orig.x, orig.y, orig.z, i, j); 

  {
    const ConvexPolygon *polygon = polygons[i];
    IFV(5) printf("        first polygon %d, F%d (mesh %d)\n", i, polygon->facetNo, polygon->meshno);         
    
    Vec3 pp = polygon->center(); // choose arbitray point on facet and in kdtree node
    dir = pp - orig;
    real planepos = polygon->b - orig.dot(polygon->normal); 
    IFV(5) printf("          line with polygon centre (%g %g %g), planepos = %g\n", 
                  pp.x, pp.y, pp.z, planepos); 
    if(planepos == 0) return -1; // danger! TODO eps check
    bool inside = planepos > 0; 
    // fake intersection
    inter.exist = inside ? 2 : 1; 
    inter.t = 1; 
    inter.coords = pp; 
    IFV(5) printf("          exist = %d t = %g\n", inter.exist, inter.t); 
  }
  
  for(i++; i < j; i++) {    
    const ConvexPolygon *polygon = polygons[i];
    IFV(5) printf("        additional polygon %d, F%d (mesh %d)\n", i, polygon->facetNo, polygon->meshno);         
    
    Intersection new_inter; 
    polygon->lineIntersection(orig, dir, new_inter); 
    IFV(5) printf("          intersection exist = %d t = %g\n", new_inter.exist, new_inter.t); 
    if(new_inter.exist < 0) {
      inter = new_inter; 
      return -1;  // cannot determine intersection: danger!
    } else if(new_inter.exist > 0 && new_inter.t > 0 && new_inter.t < inter.t) {
      IFV(5) printf("            keep\n"); 
      inter = new_inter;       
    }
  }
  
  IFV(5) printf("      nearest intersection exist = %d t = %g\n", inter.exist, inter.t); 
  if(inter.exist == 0) return 0; // we did not cross any facet -> we are outside
  if(inter.exist == 1) return 0; // first intersection was going into mesh -> we were outside before
  if(inter.exist == 2) return 1; // was going out of mesh -> we were inside before
  assert(!"a case we did not handle ???");
  
}



static int compute_inX(CSGOperation *csgop, const BitVector &meshpos, int im_0, int im_1, int im_2, 
                       int &inX) {

  if(im_1 < 0) {
    inX = csgop->in2(meshpos, im_0);                  
    if(inX >= 0) return is_corner_in2(inX); 
    else         return -1; 
  }

  if(im_2 < 0) {
    inX = csgop->in4(meshpos, im_0, im_1);                  
    if(inX >= 0) return is_corner_in4(inX); 
    else         return -1; 
  }

  {
    inX = csgop->in8(meshpos, im_0, im_1, im_2);                  
    if(inX >= 0) return is_corner_in8(inX); 
    else         return -1; 
  }
}



int Node::computeMeshposLazy(const Vec3 &orig, CSGOperation *csgop, int im_0, int im_1, int im_2, int nmesh,
                             BitVector &meshpos, int verbose) const {
  meshpos = this->meshpos; 
  int npoly = polygons.size(); 
  int inX; 
  int keep = compute_inX(csgop, meshpos, im_0, im_1, im_2, inX); 
  int vertex_order = im_1 < 0 ? 1 : im_2 < 0 ? 2 : 3;

  IFV(4) printf("     computeMeshposLazy (%g %g %g) ignore meshes %d %d %d (order %d), init keep = %d, init meshpos=%s\n", 
                orig.x, orig.y, orig.z, im_0, im_1, im_2, vertex_order, 
                keep, meshpos.toString(nmesh).c_str()); 

  // early return cases:
  // we know we can drop the vertex
  if(keep == 0) return inX;
 
  // we know we keep it and the bit vector is known
  if(keep == 1 && meshpos.nbKnown() + vertex_order == nmesh) 
    return inX;
     
  int i = 0; 
  while(i < npoly) {
    int meshno = polygons[i]->meshno; 
    int j = i + 1; 
    while(j < npoly && polygons[j]->meshno == meshno) j++;       

    if(meshno != im_0 && meshno != im_1 && meshno != im_2 && !meshpos.isKnown(meshno)) {
      int in; 
      
      IFV(4) printf("      handling mesh %d: polygons %d:%d\n", meshno, i, j); 

      if(j == i + 1) in = isInsidePolygon(orig, i, verbose); 
      else           in = isInsidePolygons(orig, i, j, verbose); 

      if(in == -1) return -1; 
      assert(in == 0 || in == 1); 
      meshpos.setSide(meshno, in);      

      IFV(4) printf("         update meshpos (meshno = %d, in = %d) -> %s\n", 
                    meshno, in, meshpos.toString(nmesh).c_str()); 
      
      if(keep < 0) {
        keep = compute_inX(csgop, meshpos, im_0, im_1, im_2, inX);
        IFV(4) printf("         inX = %x, keep=%d\n", inX, keep); 
        if(keep == 0) return inX; 
        // if keep == 1 we continue computing to get the full bit vector
      }

    }
    i = j;    
  }
  
  return inX;
}
