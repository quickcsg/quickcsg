# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import pdb, sys, time
import numpy as np

np.random.seed(0)

import csg_objects



############# simplistic .csg file parser. 


def parse(f):
    op = ''
    while True: 
        c = f.read(1)
        if c == '}': 
            assert op.strip() == ''
            return None
        if c == ';' or c == '{' or c == '': 
            break
        op += c
    
    if c == '{':
        args = []
        while True:                    
            a = parse(f)
            if a == None: break
            args.append(a)
    else: 
        args = None
    
    return (op, args)



def pr(xxx_todo_changeme, ind = 0): 
    (op, args) = xxx_todo_changeme
    print(" " * ind, op.strip(), len(args) if type(args) == list else "(no args)")
    if args != None: 
        for a in args: 
            pr(a, ind + 2)

# pr(l)

####################### 
# implementation of the openscad primitives. Called with eval() so that the 
# calls need not to be parsed

def multmatrix(m): 
    return np.array(m)


def cube(size = 1.0, center = False): 
    if type(size) != list: size = [size] * 3    

    box_faces = np.empty((6, 5), dtype = 'int32')
    box_faces[:, 0] = 4
    box_faces[:, 1:] = csg_objects.box_faces

    box_points = csg_objects.box_points * 0.5
    if not center: box_points + 0.5
    
    box_points *= size

    return (box_points, box_faces.ravel())
    



def sphere(r = None, d = None, fa = None, fs = None, fn = None): 
    if d != None: r = d / 2
    if fn == None: fn = 10
    vertices, triangles = csg_objects.subdivided_icosahedron(8) # fn / 10)

    tri2 = np.empty((triangles.shape[0], 4), dtype = 'int32')
    tri2[:, 0] = 3
    tri2[:, 1:] = triangles
    vertices *= r
    return (vertices, tri2.ravel())

def cone(fn): 
    pts = np.empty((fn + 1, 3))
    pts[:fn, 2] = 0
    pts[fn, :] = [0, 0, 1]
    angles = np.arange(fn) * np.pi * 2 / fn
    pts[:fn, 1] = np.cos(angles)
    pts[:fn, 0] = np.sin(angles)
    faces = []
    for i in range(fn):
        faces += [3, i, fn, (i + 1) % fn]
    faces += [fn] + list(range(fn))
    return pts, np.array(faces, dtype = 'int32')

def cylinder(h = 1.0, r = None, r1 = 1, r2 = 1, center = False, fn = None, fa = None, fs = None): 
    if r != None: r1 = r2 = r
    if r1 == None: r1 = 1.0
    if r2 == None: r2 = 1.0
    if fn == None: fn = 30        

    assert r1 > 0 or r2 > 0

    if r2 == 0: 
        pts, faces = cone(fn)
        pts *= [r1, r1, h]
        if center: pts[:, 2] -= h / 2.0
        return pts, faces

    if r1 == 0: 
        pts, faces = cone(fn)
        pts *= [-r2, r2, -h]
        if center: pts[:, 2] += h / 2.0
        else:      pts[:, 2] += h
        return pts, faces

    pts = np.empty((2, fn, 3))
    pts[0, :, 2] = 0 
    pts[1, :, 2] = h 
    if center: pts[:, :, 2] -= h / 2.0
    angles = np.arange(fn) * np.pi * 2 / fn
    pts[0, :, 1] = np.cos(angles) * r1
    pts[0, :, 0] = np.sin(angles) * r1
    pts[1, :, 1] = np.cos(angles) * r2
    pts[1, :, 0] = np.sin(angles) * r2
    faces = []
    for i in range(fn): 
        i1 = (i + 1) % fn
        faces.append([i, i + fn, i1 + fn, i1])
    faces.append(list(range(fn)))
    linv = list(range(fn, 2 * fn))
    linv.reverse()
    faces.append(linv)
    fl = []
    for vl in faces: 
        fl.append(len(vl))
        fl += vl    
    return (pts.reshape(-1, 3), np.array(fl, dtype = 'int32'))
    
true = True
false = False

# OFF output routine


def to_off(fname, xxx_todo_changeme1, fnos = None):
    (pts, faces) = xxx_todo_changeme1
    f = open(fname, "w")
    print("OFF", file=f)
    nface = i = 0
    while i < len(faces): 
        nface += 1
        i += faces[i] + 1
    print(pts.shape[0], nface, 0, file=f)
    np.savetxt(f, pts)

    colors = None
    if fnos != None: 

        colormap = np.array([ 
                [1, 0.5, 0.5],
                [0.5, 1, 0.5], 
                [0.5, 0.5, 1], 
                [0.5, 1, 1], 
                [1, 0.5, 1], 
                [1, 1, 0.5]], dtype = 'float32')        
        colormap *= 0.999
        colormap = (np.random.rand(fnos.max() + 1, 3) * 0.5 + 0.5).astype('float32')
        colors = colormap[fnos]
    f.close()
    pymCSG.facets_fwrite(fname, faces, colors)


# to_off("test_cylinder.off", cylinder())
# to_off("/tmp/test_sphere.off", sphere(fn = 50))
# to_off("test_cone.off", cone(10))
# to_off("test_cylinder.off", cylinder(r2 = 0))
# to_off("test_cube.off", cube())
# sys.exit(0)


############################# 
# CSG computation

csgops = ['difference', 'union', 'intersection', 'group']
objects = ['sphere', 'cube', 'cylinder']

def collect_objects(xxx_todo_changeme2, transfo): 
    (op, args) = xxx_todo_changeme2
    op = op.strip()
    if op.startswith("multmatrix"): 
        ti = eval(op)

        transfo_sub = np.dot(transfo, ti)
        if len(args) == 0: 
            return ("empty", None)
        assert len(args) == 1, pdb.set_trace()
        return collect_objects(args[0], transfo_sub)
    elif any([op.startswith(x) for x in csgops]):
        op = op[:op.find('(')].strip()
        return (op, [collect_objects(a, transfo) for a in args])
    else: 
        assert not args, pdb.set_trace()
        return (op, transfo)


# remove empty primitives (there are a lot of them)
def remove_empty(xxx_todo_changeme3): 
    (op, args) = xxx_todo_changeme3
    if op == "empty": 
        return (op, args)
    if op == "group" or op == "union": 
        args2 = [remove_empty(a) for a in args]
        
        args2 = [a for a in args2 if a[0] != "empty"]

        if len(args2) == 0: return ("empty", None)
        if len(args2) == 1: return args2[0]
        
        return (op, args2)
    
    if type(args) == list: 
        return (op, [remove_empty(a) for a in args])

    return (op, args)


# debug output

def pr_csg_tree(xxx_todo_changeme4, ind = 0): 
    (op, args) = xxx_todo_changeme4
    if op in csgops: 
        print(" " * ind, op)
        for a in args:
            pr_csg_tree(a, ind + 2)
    else: 
        print(" " * ind, op, repr(str(args)))

# some transformations transform inside <-> outside. We don't like
# this as we rely on normals. Therefore, explicity revert facet order
# for these.

def revert_faces(faces): 
    faces = faces.copy()
    i = 0
    while i < len(faces): 
        n = faces[i]
        fi = faces[i + 1 : i + 1 + n] 
        fi[:] = fi[::-1]
        i += n + 1
    return faces

def count_faces(faces): 
    i = nf = 0
    while i < len(faces): 
        n = faces[i]
        i += n + 1
        nf += 1 
    return nf
    

############################### The actual evaluation

nerr = 0
opno = 0
t_render_prim = 0

def report_errors(errs, op):

    if errs.haveErrors():
        print("error op=", repr(op), "opno=", opno)
        errs._print()
        errs.clear()        
        global nerr
        nerr += 1

def render_primitive(op, m):
    t0 = time.time()
    op = op.replace('$', '')
    pts, faces = eval(op)

    # print "det=", np.linalg.det(m[:3, :3])
    pts = (np.dot(m[:3, :3], pts.T) + m[:3, 3:4]).T.copy() + np.random.random((1, 3)) * rt

    if np.linalg.det(m[:3,:3]) < 0: 
        faces = revert_faces(faces)
        
    nf = count_faces(faces)

    global t_render_prim
    t_render_prim += time.time() - t0

    return pts, faces, np.zeros(nf, dtype = 'int32')



def render_binary(xxx_todo_changeme5): 
    (op, args) = xxx_todo_changeme5
    if any([op.startswith(x) for x in objects]):
        pts, faces, _ = render_primitive(op, args)
    else: 
        assert op in csgops
        
        global opno 
        opno += 1

        params = pymCSG.CSGParameters()
        params.tesselate = 1
        csg = pymCSG.CSG(params)
        
        for a in args: 
            pts, faces = render_binary(a)
            # print pts.shape, pts.dtype
            csg.addVFArray(pts, faces, 0)

        nmesh = len(args)

        csgop = (pymCSG.CSGIntersection(nmesh) if op == "intersection" else
                 pymCSG.CSGUnion(nmesh) if op == "union" or op == "group" else
                 pymCSG.CSGDiff(nmesh, 1) if op == "difference" else
                 1/0)
        # csgop = pymCSG.CSGUnion(nmesh)

        # if opno == 144:
        # pymCSG.cvar.verbose = 4

        csg.afterLoad()
        csg.exploreKDTree(csgop)
        csg.makeFacets(csgop)
        
        pts, faces, facet_tab, normals = csg.toVFArray()
        assert not np.any(np.isnan(pts))
        
        report_errors(csg.errors, op)


    return pts, faces

def render_instructions(instructions): 
    global opno
    opno += 1
    
    ts = []
    ts.append(time.time())
    
    params = pymCSG.CSGParameters()
    params.tesselate = 1
    # params.parallel_nt = 1
    csg = pymCSG.CSG(params)

    instructions_string = ''
    nmesh = 0
    tot_np = 0
    tot_fnos = []

    for ins in instructions: 
        if type(ins) == str: 
            instructions_string += ins
        else:
            pts, faces, fnos, npi = ins
            
            # assert not np.any(np.isnan(pts))

            if nmesh < 64:
                instructions_string += chr(ord('0') + nmesh)
            else: 
                instructions_string += chr(0x80 | (nmesh >> 7)) + chr(0x80 | (nmesh & 0x7f))

            nmesh += 1       
            # print pts.shape, faces.shape, faces.max()
            # print pts[-1]
            csg.addVFArray(pts, faces, 0)
            tot_fnos.append(fnos + tot_np)
            tot_np += npi

            # print "%d meshes, %d pts" % (csg.nmesh, csg.vertices.size())
    ts.append(time.time())

    tot_fnos = np.hstack(tot_fnos)

    print("render %s (%d meshes, %d primitives)" % (repr(instructions_string), nmesh, tot_np))
    if nmesh > 2: 
        csgop = pymCSG.CSGProgram(instructions_string)
    else:
        op = instructions[-1]
        csgop = (pymCSG.CSGIntersection(nmesh) if op == "&" else
                 pymCSG.CSGUnion(nmesh) if op == "|" else
                 pymCSG.CSGDiff(nmesh, 1) if op == "-" else
                 1/0)
        
 
    if False:
        csg.collect_processing_stats = True
        csg.delete_on_the_fly = False

    csg.verbose = 0

    if False: 
        rp = pymCSG.RandomPerturbation()    
        rp.do_rr = True
        rp.rt = 1e-6    
        rp.eps_merge = 0
        rp.random_seed = opno
        rp.apply(csg)
        ts.append(time.time())
    else: 
        rp = None


    csg.afterLoad()
    ts.append(time.time())

    csg.exploreKDTree(csgop)
    ts.append(time.time())

    csg.makeFacets(csgop)
    ts.append(time.time())

    if rp:
        rp.revert(csg)
        ts.append(time.time())
        
    pts, faces, facet_tab, normals = csg.toVFArray()
    
    fnos = tot_fnos[facet_tab]

    if False: 
        fname = "/tmp/cad/%d_out.off" % opno
        fname_combined = "/tmp/cad/%d_combined.off" % opno
        print("storing", fname, fname_combined)
        csg.toOff(fname)
        csg.combinedToOff(fname_combined)

    report_errors(csg.errors, instructions_string)
    
    print("  times:", " ".join(["%.3f" % (t1 - t0) for t1, t0 in 
                                zip(ts[1:], ts[:-1])]))

    return pts, faces, fnos, tot_np
    

def render_prog(xxx_todo_changeme6): 
    (op, args) = xxx_todo_changeme6
    if any([op.startswith(x) for x in objects]):
        pts, faces, fnos = render_primitive(op, args)
        return (1, [(pts, faces, fnos, 1)])
    else: 
        assert op in csgops
        
        global opno 
        opno += 1
        
        subtree = []

        tot_primitives = 0
        instructions = []        
        
        for a in args: 
            n_primitives, ins = render_prog(a)
            tot_primitives += n_primitives
            instructions += ins
            
        instructions.append('&' if op == "intersection" else
                            '|' if op == "union" else
                            '-' if op == "difference" else
                            '+' if op == "group" else 1/0)

        if tot_primitives < max_prims: 
            return (tot_primitives, instructions)
        else: 
            return (1, [render_instructions(instructions)])
            

if __name__ == "__main__": 
    
    infilename = None
    outfilename = None
    bvwidth = 1
    max_prims = 24
    verbose = 0
    rt = 1e-6 

    args = sys.argv[1:]
    while args: 
        a = args.pop(0)
        if a == "-h": usage()
        elif a == "-bvwidth":    bvwidth = int(args.pop(0))
        elif a == "-max_prims":  max_prims = int(args.pop(0))
        elif a == "-o":          outfilename = args.pop(0)
        elif a == "-v":          verbose += 1
        elif a == "-rt":         rt = float(args.pop(0))
        elif not infilename:     infilename = a
        else:
            assert False, "unknown argument " + a
    
    l = parse(open(infilename, "r"))

    if verbose: 
        pr(l)

    # random rotation to avoid axis-aligned data (degeneracies with kdtree)

    rr = np.eye(4)
    if True: 
        rr3, _ = np.linalg.qr(np.random.randn(9).reshape(3, 3))
        print("random rotation:", rr3)
        rr[:3, :3] = rr3
        rt = 1e-6
    else: 
        rt = 0
    
    csg_tree = collect_objects(l, rr)
    csg_tree = remove_empty(csg_tree)

    if verbose: 
        pr_csg_tree(csg_tree)

        
    global pymCSG

    if bvwidth == 1: 
        import pymCSG
    elif bvwidth == 2: 
        import pymCSG_2 as pymCSG
    elif bvwidth == 3: 
        import pymCSG_3 as pymCSG

    print("pymCSG=", pymCSG)
            
    t0 = time.time()

    # pts, faces = render_binary(csg_tree)

    nprim, instructions = render_prog(csg_tree)
    if nprim != 1: 
        pts, faces, fnos, nf = render_instructions(instructions)
    else: 
        pts, faces, fnos, nf = instructions[0]

    print("render time: %.3f s (includes %.3f s primitive render)" % (time.time() - t0, t_render_prim))

    # revert random rotation
    pts = np.dot(rr[:3,:3].T, pts.T).T.copy()

    if outfilename:         
        to_off(outfilename, (pts, faces), fnos)




