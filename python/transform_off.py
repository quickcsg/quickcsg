# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com




import sys
import math
import numpy as np
import pdb


args = sys.argv[1:]

infile = None
outfile = "/dev/stdout"
trans = np.eye(4)

def rotmat(dim, angle): 
    r = np.eye(4)
    d1 = (dim + 1) % 3
    d2 = (dim + 2) % 3
    r[d1, d1] = np.cos(angle)
    r[d1, d2] = -np.sin(angle)
    r[d2, d1] = np.sin(angle)
    r[d2, d2] = np.cos(angle)
    return r

while args: 
    a = args.pop(0)
    if a == "-rotate": 
        dim = ord(args.pop(0)) - ord('x')
        angle = np.pi * float(args.pop(0)) / 180
        trans = np.dot(rotmat(dim, angle), trans)
    elif a == "-translate": 
        dim = ord(args.pop(0)) - ord('x')
        v = float(args.pop(0))
        trans[dim, 3] += v
    elif a == '-scale':
        dim = ord(args.pop(0)) - ord('x')
        diag = np.ones(4)
        diag[dim] = float(args.pop(0))
        trans = np.dot(np.diag(diag), trans)
        # print trans
    elif a == "-o": 
        outfile = args.pop(0)
    elif infile == None: 
        infile = a
    else: 
        print("unknown arg", a, file=sys.stderr)


f = open(infile, "r")

assert f.readline() == "OFF\n"

nvertex, nfacet, zero = f.readline().split()

nvertex = int(nvertex)
nfacet = int(nfacet)

print("%d vertices, %d facets" % (nvertex, nfacet))

assert zero == "0"

vertices = np.fromfile(f, dtype = float, count = 3 * nvertex, sep = " ")
vertices = vertices.reshape(-1, 3)

# the actual transformation
vertices = np.dot(vertices, trans[:3,:3]) + trans[:3, 3:4].T

rest = f.read()

of = open(outfile, "w")

print("OFF", file=of)
print("%d %d 0" % (nvertex, nfacet), file=of)

np.savetxt(of, vertices, "%g")

of.write(rest)


