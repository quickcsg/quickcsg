# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import sys
import numpy as np

n = int(sys.argv[2])

################# pyr

points = [
    [0, 0, 0],
    [0, 1, 0],
    [1, 0, 0],
    [1, 1, 0],
    [0, 0.5, 1],
    [1, 0.5, 1]]

points1 = np.array(points)
n1 = points1.shape[0]
    
indices1 = [
    [0, 1, 3, 2],
    [0, 4, 1], 
    [2, 3, 5],
    [0, 2, 5, 4],
    [3, 1, 4, 5]]

################# box

points2 = np.empty((8, 3))
for i in range(8):
    x = points2[i]
    for d in range(3):
        x[d] = ((i >> d) & 1)

indices2 = [
    (0,1,3,2), 
    (0,4,5,1),
    (0,2,6,4),
    (3,7,6,2),
    (5,4,6,7),
    (3,1,5,7)
    ]

indices2 = [pp[::-1] for pp in indices2]

###################### 

points = []
indices = []
n0 = 0
no = int(sys.argv[1])

for i in range(n): 
    if no == 0:
        a = np.array([1, 0.999/n, 1]).reshape(1, 3)
        b = np.array([0, i/float(n), 0]).reshape(1, 3)
        pa = points1 * a + b
        ii = indices1
    elif no == 1: 
        a = np.array([0.999/n, -1, 1]).reshape(1, 3)
        b = np.array([i/float(n), 1, 0]).reshape(1, 3)
        pa = points1[:, [1, 0, 2]] * a + b
        ii = indices1
    else: 
        a = np.array([1, 1, 0.999 * (n - 1 - i + 0.5) / (n * n)]).reshape(1, 3)
        b = np.array([0, 0, i / float(n)]).reshape(1, 3)        
        pa = points2 * a + b
        ii = indices2

    points.append(pa)
    indices += [[x + n0 for x in l] for l in ii]
    n0 += pa.shape[0]

points = np.vstack(points)

of = open("/tmp/yy_%d.off" % no, "w")

print("OFF", file=of)
print("%d %d 0" % (points.shape[0], len(indices)), file=of)

np.savetxt(of, points, "%g")

for l in indices:
    print(len(l), " ".join(["%d" % x for x in l]), file=of)
