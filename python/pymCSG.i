/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */

// -*- c++ -*-


%{
#define SWIG_FILE_WITH_INIT

#include <numpy/arrayobject.h>

#ifdef ENABLE_TBB
#include "tbb/tbb.h"
#endif

#include "mCSG.hpp"
#include "random_perturbation.hpp"

using namespace mCSG;

static void Vec3to3float(float dst[3], const Vec3 & src) {
  dst[0] = src[0]; 
  dst[1] = src[1]; 
  dst[2] = src[2]; 
}

PyObject* vec3tonp(const mCSG::Vec3 &v) {
  npy_intp three = 3;
  PyObject *array = PyArray_SimpleNew(1, &three, NPY_FLOAT32);    
  float *va = (float*)PyArray_DATA(array); 
  va[0] = v.x;
  va[1] = v.y;
  va[2] = v.z;
  return array; 
}

void facets_fwrite(FILE *f, PyObject *facet_array, PyObject *color_array) {
  if(!(PyArray_Check(facet_array) &&
       PyArray_NDIM(facet_array) == 1 && 
       PyArray_TYPE(facet_array) == NPY_INT32 && 
       PyArray_ISCONTIGUOUS(facet_array))) {
    fprintf(stderr, "facets_fwrite: facet array: need 1D int array\n"); 
    return;    
  }
  
 
  int facetsSize = PyArray_DIMS(facet_array)[0];
  int *fa = (int*)PyArray_DATA(facet_array);

  int ncolor = 0; 
  float *colors = NULL; 

  if(color_array != Py_None) {
    if(!(PyArray_Check(color_array) &&
         PyArray_NDIM(color_array) == 2 && 
         PyArray_DIMS(color_array)[1] == 3 &&
         PyArray_TYPE(color_array) == NPY_FLOAT32 && 
         PyArray_ISCONTIGUOUS(color_array))) {
      fprintf(stderr, "facets_fwrite: color aray : need n*3 float32 array\n"); 
      return;    
    }
    ncolor = PyArray_DIMS(color_array)[0];
    colors = (float*)PyArray_DATA(color_array);
  }


  int i = 0, nf = 0; 
  while(i < facetsSize) {
    int n = fa[i]; 
    for(int j = 0; j <= n; j++) 
      fprintf(f, "%d ", fa[i + j]); 
    if(colors && nf < ncolor) 
      fprintf(f, "%g %g %g", colors[3 * nf], colors[3 * nf + 1], colors[3 * nf + 2]); 
    fprintf(f, "\n"); 
    i += n + 1;
    nf++; 
  }  
  
}

void facets_write(const char *fname, PyObject *facet_array, PyObject *color_array) {
  FILE *f = fopen(fname, "w");
  assert(f);
  facets_fwrite(f, facet_array, color_array);
  fclose(f); 
}

  


%}

// input contours

%typemap(in,numinputs=1) (int npt, const double (*contourpoints)[2]) {
  if(!(PyArray_Check($input) &&
       PyArray_NDIM($input) == 2 &&
       PyArray_DIMS($input)[1] == 2 &&
       PyArray_TYPE($input) == NPY_FLOAT64 &&
       PyArray_ISCONTIGUOUS($input))) {
    PyErr_SetString(PyExc_TypeError, "need double n*2 array");
    return NULL; 
  }
  $1 = PyArray_DIMS($input)[0]; 
  $2 = (double (*)[2])PyArray_DATA($input); 
}

%typemap(in,numinputs=1) (const double calib[12]) {
  if(!(PyArray_Check($input) &&
       PyArray_NDIM($input) == 2 &&
       PyArray_DIMS($input)[0] == 3 &&
       PyArray_DIMS($input)[1] == 4 &&
       PyArray_TYPE($input) == NPY_FLOAT64 &&
       PyArray_ISCONTIGUOUS($input))) {
    PyErr_SetString(PyExc_TypeError, "contours: need 4*3 double array"); 
    return NULL; 
  }
  $1 = (double*)PyArray_DATA($input); 
}





// release GIL in functions
%exception {
  Py_BEGIN_ALLOW_THREADS
  $action
  Py_END_ALLOW_THREADS
}

%include "std_vector.i"

%template(FacetVector) std::vector< mCSG::Facet >; 
%template(VertexVector) std::vector< mCSG::Vertex >; 


%include "cinder-lite/Vector.h"

%template(cVec3) cinderlite::Vec3<double>;

#ifndef BVWIDTH
%include "bitvectors.hpp"
#else
%include "bitvectors_wide.hpp"
#endif

%immutable mCSG::CSG::errors;
%ignore mCSG::CSG::nodePool;
%ignore mCSG::CSG::polygonPool;

%include "kdtree.hpp"
%include "mCSG.hpp"

PyObject* vec3tonp(const mCSG::Vec3 &v); 

%include "random_perturbation.hpp"

 // cancel GIL removal
%exception  {
  $action
}


%exception mCSG::CSG::addVFArray {
  $action
    if(result) {
      PyErr_SetString(PyExc_RuntimeError, result);
      return NULL;
    }
}



%extend mCSG::CSG {

  PyObject *toTriArray() {
    // return an array of points + normals 
    // + an array of triangles 
    // + an array that indicates the start of each mesh

    npy_intp n_face_vert = 0, n_loop = 0; 
    for(int i = 0; i < $self->facets.size(); i++) {
      const Facet &facet = $self->facets[i]; 
      n_loop += facet.loops.size(); 
      n_face_vert += facet.outputVertexNos.size();
    }

    npy_intp sz_vertex[2] = {n_face_vert, 3};
    PyObject *vertex_array = PyArray_SimpleNew(2, sz_vertex, NPY_FLOAT32);    
    PyObject *normal_array = PyArray_SimpleNew(2, sz_vertex, NPY_FLOAT32);
    float *vertex = (float*)PyArray_DATA(vertex_array); 
    float *normal = (float*)PyArray_DATA(normal_array);
    
    npy_intp n_tri = n_face_vert - 2 * n_loop; // all vertices generate 
    npy_intp sz_index[2] = {n_tri, 3};
    PyObject *index_array = PyArray_SimpleNew(2, sz_index, NPY_INT32);
    int *indices = (int*)PyArray_DATA(index_array);
    
    npy_intp nmesh_2[2] = {$self->nmesh, 2};
    PyObject *mesh_limits_array = PyArray_SimpleNew(2, nmesh_2, NPY_INT32);
    int *mesh_limits = (int*)PyArray_DATA(mesh_limits_array);

    int idx = 0;  // vertex counter
    int i_tri = 0; 

    int prev_meshno = 0;

    for(int i = 0; i < $self->facets.size(); i++) {
      const Facet &facet = $self->facets[i]; 

      while(prev_meshno != facet.meshno) {
	mesh_limits[2 * prev_meshno] = i_tri; 
	mesh_limits[2 * prev_meshno + 1] = idx; 
	prev_meshno++; 
      }

      int l0 = 0;
      Vec3 facet_normal = facet.normal; 
      
      for(int j = 0; j < facet.loops.size(); j++) {
	if(j == facet.n_loops_same) facet_normal = -facet_normal;
        int l1 = facet.loops[j];
	
	int i0 = idx;
	
        for(int k = l0; k < l1; k++) {
          assert(idx < n_face_vert);
          Vec3to3float(vertex + 3 * idx, $self->vertices[facet.outputVertexNos[k]].coords);
	  Vec3to3float(normal + 3 * idx, facet_normal);
	  idx++;
        }             

	for(int i = i0 + 2; i < idx; i++) {
	  assert(i_tri < n_tri);
	  indices[3 * i_tri    ] = i0; 
	  indices[3 * i_tri + 1] = i - 1; 
	  indices[3 * i_tri + 2] = i;
	  i_tri++; 
	}
	
        l0 = l1;
      }
    }
    while(prev_meshno != $self->nmesh) {
      mesh_limits[2 * prev_meshno] = i_tri; 
      mesh_limits[2 * prev_meshno + 1] = idx; 
      prev_meshno++; 
    }
    assert(i_tri == n_tri); 
    assert(idx == n_face_vert);
    return Py_BuildValue("(NNNN)", vertex_array, normal_array, index_array, mesh_limits_array);
  }

  PyObject *toVFArray() {

    // map to remove unused (primary) vertices
    std::vector<int> vmap;  
    vmap.resize($self->vertices.size(), -1);
    int nv = 0;
    for(int i = 0; i < $self->vertices.size(); i++) {
      const Vertex & v = $self->vertices[i];
      if(v.keep == 1) vmap[i] = nv++;
    }

    npy_intp sz[2] = {nv, 3};
    PyObject *points_array = PyArray_SimpleNew(2, sz, NPY_FLOAT64);
    double *points = (double*)PyArray_DATA(points_array);

    int j = 0; 
    for(int i = 0; i < $self->vertices.size(); i++) {
      const Vertex & v = $self->vertices[i];
      if(v.keep != 1) continue;
      double *po = points + j * 3;
      po[0] = v.coords[0];
      po[1] = v.coords[1];
      po[2] = v.coords[2];
      j++; 
    }
    
    npy_intp n_face_vert = 0, n_loop = 0; 
    for(int i = 0; i < $self->facets.size(); i++) {
      const Facet &facet = $self->facets[i]; 
      n_loop += facet.loops.size(); 
      n_face_vert += facet.outputVertexNos.size();
    }
    
    npy_intp facet_array_size = n_face_vert + n_loop;

    PyObject *facet_array = PyArray_SimpleNew(1, &facet_array_size, NPY_INT32);
    int *facets = (int*)PyArray_DATA(facet_array); 


    PyObject *facet_tab_array = PyArray_SimpleNew(1, &n_loop, NPY_INT32);
    int *facet_tab = (int*) PyArray_DATA(facet_tab_array); 

    npy_intp normals_size[2] = {n_loop, 3}; 
    PyObject *facet_normal_array = PyArray_SimpleNew(2, normals_size, NPY_FLOAT64);
    double *facet_normals = (double*) PyArray_DATA(facet_normal_array); 
    
    int idx = 0, loopidx = 0;
    
    for(int i = 0; i < $self->facets.size(); i++) {
      const Facet &facet = $self->facets[i]; 
      // printf("facet %d: %d/%zd same\n", i, facet.n_loops_same, facet.loops.size());
      int l0 = 0;
      Vec3 normal = facet.normal; 
      normal.normalize();      
      int idx0 = idx;
      for(int j = 0; j < facet.loops.size(); j++) {
        int l1 = facet.loops[j];
        assert(idx < facet_array_size);
        facets[idx++] = l1 - l0; 
        facet_tab[loopidx] = i;
        for(int k = l0; k < l1; k++) {
          assert(idx < facet_array_size);
	  assert(facet.outputVertexNos[k] < vmap.size());
          facets[idx++] = vmap[facet.outputVertexNos[k]];
        }               
        
        assert(loopidx < n_loop); 
        Vec3 nl = j < facet.n_loops_same ? normal : -normal; 
        double *norm = facet_normals + loopidx * 3; 
        norm[0] = nl[0]; 
        norm[1] = nl[1]; 
        norm[2] = nl[2]; 

        loopidx ++; 
        l0 = l1;
      }
      assert(idx - idx0 == facet.loops.size() + facet.outputVertexNos.size());
    }
    assert(idx == facet_array_size);
    return Py_BuildValue("(NNNN)", points_array, facet_array, facet_tab_array, facet_normal_array); 
    
  }

  const char* addVFArray(PyObject *vertex_array, PyObject *facet_array, int flags) {
    if(!(PyArray_Check(vertex_array) &&
         PyArray_NDIM(vertex_array) == 2 &&
         PyArray_DIMS(vertex_array)[1] == 3 &&
         PyArray_TYPE(vertex_array) == NPY_FLOAT64 &&
         PyArray_ISCONTIGUOUS(vertex_array))) {
      return "vertex array: need n*3 double array";
    }
    
    int npt = PyArray_DIMS(vertex_array)[0];
    
    if(!(PyArray_Check(facet_array) &&
         PyArray_NDIM(facet_array) == 1 && 
         PyArray_TYPE(facet_array) == NPY_INT32 && 
         PyArray_ISCONTIGUOUS(facet_array))) {
      return "facet array: need 1D int array";
    }
    
    int facetsSize = PyArray_DIMS(facet_array)[0];
     
    $self->addMeshFromVFArray(npt, (double (*)[3])PyArray_DATA(vertex_array), (int*)PyArray_DATA(facet_array), facetsSize); 
    
    return NULL;    
  }
  
};


void facets_write(const char * fname, PyObject *facet_array, PyObject *color_array); 






%init %{
  /* needed, else crash at runtime */
  import_array();

#ifdef ENABLE_TBB
  oneapi::tbb::global_control global_limit(
       oneapi::tbb::global_control::max_allowed_parallelism,
       oneapi::tbb::this_task_arena::max_concurrency()
  );

#endif

%}
