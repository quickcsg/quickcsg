# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

# -*- makefile -*-

.SUFFIXES: .cpp .o



CC=g++
CFLAGS=-g -Wall -O3 -Wno-sign-compare -fPIC

# comment out (do not set to no)
ENABLE_TBB=yes


ifeq ($(shell uname),Darwin)
  # mac flags
  SHARED_LDFLAGS=-Wl,-F. -bundle -undefined dynamic_lookup -g
else 
  # linux flags
  SHARED_LDFLAGS=-shared -g
endif

ifdef ENABLE_TBB
  ifeq ($(shell uname),Darwin)
    TBB_ROOT=/Users/matthijs/Desktop/src/oneTBB/installed
    TBB_CFLAGS+=-I$(TBB_ROOT)/include
    TBB_LDFLAGS+=-L$(TBB_ROOT)/lib -rpath $(TBB_ROOT)/lib
  endif
  TBB_CFLAGS+=-DENABLE_TBB -std=c++11
  TBB_LDFLAGS+=-ltbb -ltbbmalloc
endif


ifdef BVWIDTH
  CFLAGS+=-DBVWIDTH=$(BVWIDTH)
  SWIGFLAGS=-DBVWIDTH=$(BVWIDTH)
  modname=pymCSG_$(BVWIDTH)
else
  SWIGFLAGS=
  modname=pymCSG
endif

ifdef EPS1
  CFLAGS+=-DEPS1=$(EPS1) -DEPS2=$(EPS2) -DEPS3=$(EPS3)
endif

all: _$(modname).so

.cpp.o:
	$(CC) -c -o $@ $(CFLAGS) -I../libtess $(CL_CFLAGS) $(TBB_CFLAGS) $<

OBJ=../node_geometry.o ../vertices_to_facets.o ../kdtree.o ../topology.o ../io.o ../random_perturbation.o

# ../grid_geometry.o

TESS_LDFLAGS=../libtess/libtess.a

###################
# Python interface

$(modname)_wrap.cxx: pymCSG.i ../mCSG.hpp ../bitvectors.hpp ../bitvectors_wide.hpp ../kdtree.hpp ../random_perturbation.hpp
	swig -python $(SWIGFLAGS) -c++ -I.. -module $(modname) -o $@ $< 

$(modname)_wrap.o: $(modname)_wrap.cxx 
	$(CC) -c $(CFLAGS) $(TBB_CFLAGS) $< -o $@ -I.. \
            -I $(shell python -c "import distutils.sysconfig; print(distutils.sysconfig.get_python_inc())" ) \
            -I $(shell python -c "import numpy; print(numpy.get_include())")

_$(modname).so: $(modname)_wrap.o $(OBJ)
	$(CC) -o $@ -g $(SHARED_LDFLAGS) $^ $(TESS_LDFLAGS) $(TBB_LDFLAGS) $(CL_LDFLAGS)


clean:
	rm -f ../*.o $(modname)_wrap.cxx _$(modname).so

#################
# dependencies due to includes
# for i in ../*.cpp; do cpp -MM $i; done

#grid_geometry.o: ../grid_geometry.cpp ../mCSG.hpp ../cinder-lite/Vector.h \
# ../cinder-lite/CinderMath.h ../VectorPreferredSize.hpp ../kdtree.hpp \
# ../bitvectors.hpp ../MemoryPool.hpp
io.o: ../io.cpp ../mCSG.hpp ../cinder-lite/Vector.h \
 ../cinder-lite/CinderMath.h ../VectorPreferredSize.hpp ../kdtree.hpp \
 ../bitvectors.hpp ../MemoryPool.hpp ../PolygonTesselator.hpp
kdtree.o: ../kdtree.cpp ../mCSG.hpp ../cinder-lite/Vector.h \
 ../cinder-lite/CinderMath.h ../VectorPreferredSize.hpp ../kdtree.hpp \
 ../bitvectors.hpp ../MemoryPool.hpp ../kdtree_parallel.cpp

node_geometry.o: ../node_geometry.cpp ../mCSG.hpp ../cinder-lite/Vector.h \
 ../cinder-lite/CinderMath.h ../VectorPreferredSize.hpp ../kdtree.hpp \
 ../bitvectors.hpp ../MemoryPool.hpp
random_perturbation.o: ../random_perturbation.cpp \
 ../random_perturbation.hpp ../mCSG.hpp ../cinder-lite/Vector.h \
 ../cinder-lite/CinderMath.h ../VectorPreferredSize.hpp ../kdtree.hpp \
 ../bitvectors.hpp ../MemoryPool.hpp
topology.o: ../topology.cpp ../mCSG.hpp ../cinder-lite/Vector.h \
 ../cinder-lite/CinderMath.h ../VectorPreferredSize.hpp ../kdtree.hpp \
 ../bitvectors.hpp ../MemoryPool.hpp
vertices_to_facets.o: ../vertices_to_facets.cpp ../mCSG.hpp \
 ../cinder-lite/Vector.h ../cinder-lite/CinderMath.h \
 ../VectorPreferredSize.hpp ../kdtree.hpp ../bitvectors.hpp \
 ../MemoryPool.hpp ../PolygonTesselator.hpp

# to compile all bitwidths: 
# for bw in 2 3 4 8; do make BVWIDTH=$bw clean all; done
