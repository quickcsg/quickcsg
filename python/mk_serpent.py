# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import pdb, sys
import numpy as np

cos = np.cos
sin = np.sin
pi = np.pi

def normalize(x): 
    norms = np.sqrt(np.sum(x ** 2, 1))
    return x / norms.reshape(-1, 1)
    
    
def make_along(n, n2, rec, rfinal): 
    " make the vertices and indices of the torus"

    print("generating mesh of %d*%d vertices" % (n, n2))

    # integer ranges 
    urange = np.arange(n2) 
    vrange = np.arange(n)

    # float ranges in [0, 2*pi]
    usteps = urange * (2 * np.pi / n2)
    vsteps = vrange * (2 * np.pi / n)
        
    points0 = np.zeros((n2, 3))
    dpoints0 = np.zeros((n2, 3))
    points1 = np.vstack((sin(usteps), cos(usteps), np.zeros(n2))).T * 2
    dpoints1 = np.vstack((cos(usteps), -sin(usteps),  np.zeros(n2))).T * 2

    for r, freq in rec:
        unit0 = normalize(dpoints1)
        unit1 = normalize(np.cross(points1 - points0, unit0))
        unit2 = np.cross(unit1, unit0)
        
        sins = sin(usteps * freq).reshape(-1, 1)
        coss = cos(usteps * freq).reshape(-1, 1)

        points2 = points1 + r * unit1 * sins + r * unit2 * coss
        dpoints2 = dpoints1 + unit1 * coss * freq * r - unit2 * sins * freq * r

        points0 = points1
        points1 = points2
        dpoints1 = dpoints2
    
    pc0 = points0.reshape(n2, 1, 3)
    pc1 = rfinal * sin(vsteps).reshape(1, n, 1) * unit1.reshape(n2, 1, 3)
    pc2 = rfinal * cos(vsteps).reshape(1, n, 1) * unit2.reshape(n2, 1, 3)

    points = pc0 + pc1 + pc2
    
    points = points.reshape(-1, 3)
    
    triangles = np.empty((n2, n, 2, 3), dtype = 'int32')

    # indices of the 4 corners of each patch on the surface
    urange = urange.reshape(-1, 1)
    indices00 = urange * n + vrange
    indices01 = ((urange + 1) % urange.size) * n + vrange
    indices10 = urange * n + ((vrange + 1) % vrange.size)
    indices11 = ((urange + 1) % urange.size) * n + ((vrange + 1) % vrange.size)
    
    triangles[:, :, 0, 0] = indices11
    triangles[:, :, 0, 1] = indices01
    triangles[:, :, 0, 2] = indices10
    triangles[:, :, 1, 0] = indices10
    triangles[:, :, 1, 1] = indices01
    triangles[:, :, 1, 2] = indices00

    triangles = triangles.reshape(-1, 3)

    return (points, triangles)


def f1(u): 
    return np.array([sin(u), cos(u), np.zeros(u.size)]).T

def f1x(u): 
    return np.array([sin(u), cos(u), 0.1 * sin(20 * u)]).T

def recf(r, rot, f, u, eps): 

    x = f(u)

    # eps = 1e-5
    du = eps * (u[1] - u[0])
    dx = (f(u + du) - x) / eps
    dx2 = (f(u - du) - x) / eps
    
    d0 = normalize(dx)
    d1 = normalize(np.cross(dx2, dx))
    # d1 = normalize(x)
    d2 = np.cross(d0, d1)
    
    n = u.size
    x2 = x + r * d1 * sin(u * rot).reshape(-1, 1) + r * d2 * cos(u * rot).reshape(-1, 1)

    return x2
    

def f2(u): 
    return recf(0.2, 6, f1, u, 1e-5)
    
def f3(u): 
    return recf(0.05, 6, f2, u, 1e-5)

def prod(k, n): 
    t = 1
    for i in range(n): 
        t *= k
        k -= 1
    return t

tp = [5, 5, 4, 4, 4, 3, 3, 3]

niter = int(sys.argv[1])
points, triangles = make_along(40, 4 * 5 ** niter, 
                               [(0.5 ** i, np.prod(tp[:i])) for i in range(1, niter)], 2 * 0.5 ** niter)

if len(sys.argv) > 1:
    angle = float(sys.argv[2]) * pi / 180
    R = np.array([
            [cos(angle), -sin(angle), 0],
            [sin(angle), cos(angle), 0],
            [0, 0, 1]])
    points = np.dot(points, R)



of = open("/tmp/serp_%d.off" % niter, "w")

print("OFF", file=of)
print("%d %d 0" % (points.shape[0], triangles.shape[0]), file=of)

np.savetxt(of, points, "%g")

triangles4 = np.empty((triangles.shape[0], 4))

triangles4[:, 0] = 3
triangles4[:, 1:] = triangles

np.savetxt(of, triangles4, "%d")
