# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com


import OpenGL
#import logging
#logging.basicConfig( level=logging.INFO )
OpenGL.USE_ACCELERATE = False
OpenGL.ERROR_ON_COPY = True

# sometimes better to remove this to see python errrors
OpenGL.ERROR_CHECKING = False
from OpenGL.GL import *
from OpenGL.GLUT import *
import sys, time
from math import sin,cos,sqrt,pi
from OpenGL.constants import GLfloat
import math

    
vec4 = GLfloat_4


class View:

    def __init__(self): 
    
        self.view_rotx, self.view_roty, self.view_rotz = (20.0, 30.0, 0.0)
        self.view_scale = 1.0
        self.tx, self.ty, self.tz = (0.0, 0.0, 0.0)
        self.mouse_clic_coords = None
        self.mouse_action = None

        glutDisplayFunc(self.draw)
        glutReshapeFunc(self.reshape)
        glutKeyboardFunc(self.key)
        try:
            glutMouseWheelFunc(self.mousewheel)
        except OpenGL.error.NullFunctionError: 
            print("could not set mouse wheel callback")
        glutMouseFunc(self.mouse)
        glutMotionFunc(self.mousemotion)
        glutSpecialFunc(self.special)
        
        glClearColor(1.0, 1.0, 1.0, 1.0)
        pos = vec4(5.0, 5.0, 10.0, 0.0)
        glLightfv(GL_LIGHT0, GL_POSITION, pos)
        black = vec4(0, 0, 0, 1)
        glLightfv(GL_LIGHT0, GL_AMBIENT, black)
        glEnable(GL_CULL_FACE)
        glEnable(GL_LIGHTING)
        glEnable(GL_LIGHT0)
        glEnable(GL_DEPTH_TEST)

        glEnable(GL_NORMALIZE)
        
    def draw(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        
        glPushMatrix()

        # print "rot=", self.view_rotx, self.view_roty, self.view_rotz
        
        glRotatef(self.view_rotx, 1.0, 0.0, 0.0)
        glRotatef(self.view_roty, 0.0, 1.0, 0.0)
        glRotatef(self.view_rotz, 0.0, 0.0, 1.0)
        glScalef(self.view_scale, self.view_scale, self.view_scale)
        glTranslatef(self.tx, self.ty, self.tz)
        
        self.draw_object()

        glPopMatrix()

        glutSwapBuffers()


    # change view angle, exit upon ESC
    def key(self, k, x, y):
        if k == 'z':
            self.view_rotz += 5.0
        elif k == 'Z':
            self.view_rotz -= 5.0
        elif k == '+': 
            self.view_scale *= 1.2
        elif k == '-':
            self.view_scale /= 1.2
        elif ord(k) == 27: # Escape
            sys.exit(0)   # does not close the window on the Mac, find something else 
        else:
            return
        glutPostRedisplay()

    # change view angle
    def special(self, k, x, y):

        if k == GLUT_KEY_UP:
            self.view_rotx += 5.0
        elif k == GLUT_KEY_DOWN:
            self.view_rotx -= 5.0
        elif k == GLUT_KEY_LEFT:
            self.view_roty += 5.0
        elif k == GLUT_KEY_RIGHT:
            self.view_roty -= 5.0
        else:
            return
        glutPostRedisplay()


    def mouse(self, button, state, x, y): 
        # print button, state
        if  button == GLUT_LEFT_BUTTON:
            if state == GLUT_DOWN: 
                self.mouse_clic_coords = (x, y, self.view_rotx, self.view_roty)
                self.mouse_action = "rotate"
            else: 
                self.mouse_action = None
        elif button == GLUT_MIDDLE_BUTTON:
            if state == GLUT_DOWN: 
                self.mouse_clic_coords = (x, y, self.tx, self.ty, self.tz)
                self.mouse_action = "pan"
            else: 
                self.mouse_action = None            
        elif button == 4:
            if state == GLUT_DOWN: 
                self.view_scale /= 1.2
                glutPostRedisplay()
        elif button == 3:
            if state == GLUT_DOWN:
                self.view_scale *= 1.2
                glutPostRedisplay()


    def mousewheel(self, wheel, direction, x, y): 
        if direction > 0: 
            self.view_scale /= 1.2
        else: 
            self.view_scale *= 1.2
        glutPostRedisplay()

    def mousemotion(self, x, y): 
        if self.mouse_action == "rotate": 
            (x0, y0, view_rotx0, view_roty0) = self.mouse_clic_coords
            self.view_roty = view_roty0 + (x - x0) * 0.2
            self.view_rotx = view_rotx0 + (y - y0) * 0.2
            glutPostRedisplay()
        elif self.mouse_action == "pan":
            (x0, y0, tx0, ty0, tz0) = self.mouse_clic_coords
            
            ax = self.view_rotx * math.pi / 180
            ay = self.view_roty * math.pi / 180       

            ux = math.cos(ay)
            uy = -math.sin(ay)

            tr = math.sin(ax)

            tx = tr * math.sin(ay)
            ty = tr * math.cos(ay)
            tz = math.cos(ax)

            dx = (x - x0) * 0.05 / self.view_scale
            dy = -(y - y0) * 0.05 / self.view_scale
            
            self.tx = tx0 + dx * ux + dy * tx
            self.ty = ty0           + dy * tz
            self.tz = tz0 - dx * uy - dy * ty
            glutPostRedisplay()


    # new window size or exposure
    def reshape(self, width, height):
        h = float(height) / float(width);
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glFrustum(-1.0, 1.0, -h, h, 5.0, 60.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glTranslatef(0.0, 0.0, -40.0)

    def draw_text(self, s):
        
        # code very weird... should it be so complicated?

        glMatrixMode(GL_PROJECTION)
        glPushMatrix()
        glLoadIdentity()
        glMatrixMode(GL_MODELVIEW)
        glPushMatrix()
        glLoadIdentity()

        glColor4f(1, 1, 1, 1)
        glColor4f(0, 0, 0, 1)
        glDisable(GL_LIGHTING)
        glDisable(GL_DEPTH_TEST)
        glRasterPos2f(-1, -1)
                
        for c in s:
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, ord(c))
            
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_LIGHTING)
        glMatrixMode(GL_PROJECTION)
        glPopMatrix()
        glMatrixMode(GL_MODELVIEW)
        glPopMatrix()

    def draw_object(self):
        pass

if __name__ == "__main__": 
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
    
    # glutInitWindowPosition(0, 0)
    glutInitWindowSize(600, 600)
    glutCreateWindow("View")

    View()
    
    glutMainLoop()
