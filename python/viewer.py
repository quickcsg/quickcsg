# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import OpenGL
#import logging
#logging.basicConfig( level=logging.INFO )
OpenGL.USE_ACCELERATE = False
OpenGL.ERROR_ON_COPY = True

# sometimes better to remove this to see python errrors
OpenGL.ERROR_CHECKING = False
from OpenGL.GL import *
from OpenGL.GLUT import *
import sys, time
from math import sin,cos,sqrt,pi
from OpenGL.constants import GLfloat

from basic_gl import View

import numpy as np

import _thread, pdb, random


import csg_objects
import pymCSG

# function that generates the objects (the current time t is passed in)
obj_generator = csg_objects.BoxSeries()

if len(sys.argv) > 1: 
    genname = sys.argv[1]
    obj_generator = getattr(csg_objects, genname)()


norm = np.linalg.norm

class Op: 
    # maps key to function that makes operator
    op_map = {
        'i': pymCSG.CSGIntersection, 
        'u': pymCSG.CSGUnion, 
        'x': pymCSG.CSGXOR,
        '2': lambda nmesh: pymCSG.CSGMinMesh(nmesh, 2), 
        'm': lambda nmesh: pymCSG.CSGDiff(nmesh, 2), 
        'r': lambda nmesh: pymCSG.CSGTable(nmesh, random.randrange(2**(2 ** nmesh)) & -2)
        }
    def __init__(self): 
        self.set_op('u')        

    def set_op(self, key): 
        if key not in Op.op_map: return False
            
        self.op_key = key
        self.csgop_fun = Op.op_map[key]
        self.cur_nmesh = 0
        return True

    def get_op(self, nmesh): 
        if nmesh != self.cur_nmesh:
            self.csgop = self.csgop_fun(nmesh)
            if self.op_key == 'r': 
                print("randval=%x" % self.csgop.table)
            self.cur_nmesh = nmesh
        return self.csgop
            
        

class CSGRun: 
    # maps key to operator

    nerr = 0

    def __init__(self, t, operator, keep_kdtree = False):
        self.t = t
        self.operator = operator     
        self.keep_kdtree = keep_kdtree
        
        self.finished = False        
        _thread.start_new_thread(self.compute, ())
        
    def compute(self): 
        self.input_objects = obj_generator.get(self.t)      
        nmesh = len(self.input_objects)
        csgop = self.operator.get_op(nmesh)
               
        self.start_t = time.time()
        params = pymCSG.CSGParameters()
        params.tesselate = 3
        params.delete_on_the_fly = 0 if self.keep_kdtree else 2
        csg = pymCSG.CSG(params)        

        self.mesh_bounds = [0]
        for color, vertices, faces in self.input_objects:
            csg.addVFArray(vertices, faces, 0)
            self.mesh_bounds.append(csg.facets.size())

        csg.afterLoad()
        
        csg.exploreKDTree(csgop)
        csg.makeFacets(csgop)
        
        self.end_t = time.time()

        errs = csg.errors

        self.err_count = 0
        if errs.haveErrors():
            #csg.combinedToOff("/tmp/err_meshes_%d.off" % CSGRun.nerr)
            CSGRun.nerr += 1
            errs._print()
            #errs.clear()        
            self.err_count = errs.count()
        # else: 
        #     csg.combinedToOff("/tmp/meshes_fine.off")

        self.output = csg.toTriArray()

        if self.keep_kdtree: 
            self.kdtree = self.collect_leaves(csg.root)
        else: 
            self.kdtree = []
        
        self.finished = True

    def collect_leaves(self, node): 
        if node.child1 == None and node.child2 == None:
            return [(pymCSG.vec3tonp(node.bbMin), 
                     pymCSG.vec3tonp(node.bbMax), node.state)]
        else: 
            return self.collect_leaves(node.child1) + self.collect_leaves(node.child2)

    def __str__(self): 
        return "operation %s on %d meshes, %.1f ms, output %d vertices, %d faces%s" % (
            self.operator.op_key, 
            len(self.input_objects), 
            1000*(self.end_t - self.start_t), 
            self.output[0].shape[0], 
            self.output[2].shape[0], 
            ' [%d ERRORS]' % self.err_count if self.err_count > 0 else '')


class ViewCSG(View): 

    def __init__(self):
        View.__init__(self)
                
        self.tStart = self.t0 = time.time()
        self.timer_period = 10

        glutTimerFunc(self.timer_period, self.timer, 0)     
        self.operation = Op()
        self.show_result = True
        self.pause = False
        self.show_kdtree = False
        self.csg_run = self.csg_run_next = None

        self.explode_fact = 0

        # we will not need more buffers than that
        self.vbo_buffers = glGenBuffers(3 * 64)

    def compute_csg(self): 
        if self.pause: return 
        if not self.csg_run_next or self.csg_run_next.finished: 
            self.csg_run = self.csg_run_next 
            self.csg_run_next = CSGRun(time.time() - self.tStart, self.operation, self.show_kdtree)
            if self.csg_run:
                # copy meshes to buffers
                self.copy_results_to_vbo(*self.csg_run.output)

    def copy_results_to_vbo(self, vertices, normals, indices, mesh_limits): 
        self.to_display = []
        tri0 = vert0 = 0

        if self.explode_fact > 0: 
            vertices += normals * self.explode_fact
        
        vert_buf, norm_buf = self.vbo_buffers[:2]

        glBindBuffer(GL_ARRAY_BUFFER, vert_buf)
        glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW)
        
        glBindBuffer(GL_ARRAY_BUFFER, norm_buf)
        glBufferData(GL_ARRAY_BUFFER, normals, GL_STATIC_DRAW) 
        
        for meshno, (tri1, vert1) in enumerate(mesh_limits):
            if tri1 == tri0: continue
            index_buf = self.vbo_buffers[3 + meshno]

            glBindBuffer(GL_ARRAY_BUFFER, index_buf)
            glBufferData(GL_ARRAY_BUFFER, indices[tri0:tri1, :], GL_STATIC_DRAW)
            
            self.to_display.append((meshno, vert_buf, norm_buf, index_buf, tri1 - tri0))

            tri0, vert0 = tri1, vert1
        glBindBuffer(GL_ARRAY_BUFFER, 0)

    def draw_object(self):
        self.compute_csg()
        if not self.csg_run: 
            self.draw_text("Output not ready")
            return

        if self.show_result: 
            self.draw_results()
        else: 
            self.draw_inputs()
        
        if self.show_kdtree: 
            self.draw_kdtree()

        self.draw_text(str(self.csg_run))


    def draw_kdtree(self): 
        node_colormap = np.array([
            (0.5, 1, 1),  # cyan
            (1, 0.5, 1),  # magenta
            (1, 1, 0.5),  # yellow
            (1, 0.5, 0.5) # redish
            ]).astype('float32')
        box_faces = csg_objects.box_faces
        box_normals = csg_objects.box_normals
        box_vertices = csg_objects.box_points
        for bbMin, bbMax, state in self.csg_run.kdtree:             
            glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, node_colormap[state])
            bb_vert = (box_vertices + 1) * 0.5 * (bbMax - bbMin) + bbMin
            glBegin(GL_QUADS)
            for i, v in enumerate(box_faces):
                glNormal3dv(box_normals[i])
                for j in v: 
                    glVertex3dv(bb_vert[j])
            glEnd()
            


    def draw_face(self, pts, faces, i): 
        npt = faces[i]; i += 1
        glBegin(GL_POLYGON)
        i0, i1, i2 = faces[i:i+3]
        normal = np.cross(pts[i0] - pts[i1], pts[i0] - pts[i2])
        nn = norm(normal)
        normal /= norm(normal)
        glNormal3dv(normal)
        for j in range(npt):
            glVertex3dv(pts[faces[i]])
            i += 1
        glEnd()            
        return i

    def draw_inputs(self): 
        for color, pts, faces in self.csg_run.input_objects: 
            glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color)            
            i = 0
            while i < len(faces): 
                i = self.draw_face(pts, faces, i)
        
    def draw_results(self): 

        glEnableClientState(GL_NORMAL_ARRAY);
        glEnableClientState(GL_VERTEX_ARRAY);

        for meshno, vert_buf, norm_buf, index_buf, ntri in self.to_display: 

            glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, self.csg_run.input_objects[meshno][0])
            
            glBindBuffer(GL_ARRAY_BUFFER, norm_buf); 
            glNormalPointer(GL_FLOAT, 0, None)
            glBindBuffer(GL_ARRAY_BUFFER, vert_buf);    
            glVertexPointer(3, GL_FLOAT, 0, None)

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buf)
            glDrawElements(GL_TRIANGLES, 3 * ntri, GL_UNSIGNED_INT, None)

        glDisableClientState(GL_VERTEX_ARRAY)
        glDisableClientState(GL_NORMAL_ARRAY)


        
            
    def timer(self, v):        
        glutPostRedisplay()
        glutTimerFunc(self.timer_period, self.timer, 0)

    def key(self, k, x, y):
        k = k.decode('ascii')
        if self.operation.set_op(k): 
            glutPostRedisplay()
        elif hasattr(obj_generator, 'key') and obj_generator.key(k): 
            glutPostRedisplay()
        elif k == '1': 
            self.show_result = not self.show_result
            glutPostRedisplay()
        elif k == 'e': 
            self.explode_fact = 0.1 - self.explode_fact
        elif k == ' ':
            self.pause = not self.pause
        elif k == 'k': 
            self.show_kdtree = not self.show_kdtree
            glutPostRedisplay()
        else: 
            View.key(self, k, x, y)


if __name__ == '__main__':
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)

    glutInitWindowPosition(0, 0)
    glutInitWindowSize(600, 600)
    glutCreateWindow("pyCSG")

    vg = ViewCSG()

    if "-info" in sys.argv:
        print(("GL_RENDERER   = ", glGetString(GL_RENDERER)))
        print(("GL_VERSION    = ", glGetString(GL_VERSION)))
        print(("GL_VENDOR     = ", glGetString(GL_VENDOR)))
        print(("GL_EXTENSIONS = ", glGetString(GL_EXTENSIONS)))

    glutMainLoop()


