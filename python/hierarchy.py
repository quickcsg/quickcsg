# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import pdb, time, sys

import pymCSG

import numpy as np

####################### I/O operations


def load_contours(fname): 
    contours = []
    per_camera_contours = [contours]
    f = open(fname, "r")
    while True: 
        l = f.readline()
        if not l: break    
        camno, npt = l.split()
        camno = int(camno); npt = int(npt)
        if camno > len(per_camera_contours) - 1: 
            assert camno == len(per_camera_contours), pdb.set_trace()
            contours = []
            per_camera_contours.append(contours)
        pts = np.fromstring(f.readline(), dtype = float, sep = ' ')
        contours.append(pts.reshape(npt, 2))
    return per_camera_contours


def load_calib(fname): 
    calib = np.fromfile(fname, sep = ' ', dtype = float)
    return calib.reshape((-1, 3, 4))

def convert_contours(contours, jitter): 
    # convert to the format required by mkCSG and add some jitter
    tocat = []
    for pts in contours: 
        pts += jitter * (np.random.random(pts.shape) - 0.5)
        tocat.append(pts) 
        tocat.append(pts[0, :])
    return np.vstack(tocat)



####################### CSG operations


def contours_to_polygons(all_contours, calib): 
    nmesh = len(all_contours)
    params = pymCSG.CSGParameters()
    params.tesselate = 1
    csg = pymCSG.CSG(params)        
    intersection = pymCSG.CSGIntersection(nmesh)

    times = np.empty(10)
    nt = 0

    times[nt] = time.time(); nt += 1

    for calib_matrix, contours in zip(calib, all_contours): 
        csg.addContoursAndCalib(contours, calib_matrix, depth)


    times[nt] = time.time(); nt += 1
    print("%d meshes, %d vertices, %d facets" % (nmesh, csg.vertices.size(), csg.facets.size()))

    csg.initKDTree()

    times[nt] = time.time(); nt += 1

    bbMin = pymCSG.cVec3(-2, -2, -2)
    bbMax = pymCSG.cVec3(2, 2, 2)

    #pymCSG.cvar.verbose = 0
    csg.cropRootToBBox(bbMin, bbMax)

    times[nt] = time.time(); nt += 1

    csg.exploreKDTree(intersection)
    times[nt] = time.time(); nt += 1

    csg.makeFacets(intersection)
    times[nt] = time.time(); nt += 1

    errs = pymCSG.cvar.errors

    if errs.haveErrors():
        errs._print()
        errs.clear()        

    print("""times: 
       add contours %7.3f ms
       init kdtree  %7.3f ms
       crop root    %7.3f ms
       explore      %7.3f ms
       facets       %7.3f ms
       total        %7.3f ms""" % (tuple((times[1:nt] - times[:nt-1]) * 1000) + ((times[nt - 1] - times[0]) * 1000, )))

    return csg


def intersect_csgs(sub_csgs): 
    nmesh = len(sub_csgs)
    intersection = pymCSG.CSGIntersection(nmesh)

    params = pymCSG.CSGParameters()
    params.tesselate = 1
    csg = pymCSG.CSG(params)        

    times = np.empty(10)
    nt = 0
    times[nt] = time.time(); nt += 1

    for sub_csg in sub_csgs: 
        points, faces, facet_tab, _ = sub_csg.toVFArray()    
        csg.addVFArray(points, faces, 0)
    print("%d meshes, %d vertices, %d facets" % (nmesh, csg.vertices.size(), csg.facets.size()))

    times[nt] = time.time(); nt += 1
    csg.afterLoad()

    times[nt] = time.time(); nt += 1 

    csg.initKDTree()

    times[nt] = time.time(); nt += 1

    csg.exploreKDTree(intersection)
    times[nt] = time.time(); nt += 1

    csg.makeFacets(intersection)
    times[nt] = time.time(); nt += 1

    errs = pymCSG.cvar.errors

    if errs.haveErrors():
        errs._print()
        errs.clear()        

    print("""times: 
       add meshes   %7.3f ms
       after load   %7.3f ms
       init kdtree  %7.3f ms
       explore      %7.3f ms
       facets       %7.3f ms
       total        %7.3f ms""" % (tuple((times[1:nt] - times[:nt-1]) * 1000) + ((times[nt - 1] - times[0]) * 1000, )))
    
    return csg


print("========== Loading")

depth = 20
jitter = 0.001

calib = load_calib("../data/knots42/calib")
all_contours = load_contours("../data/knots42/contours")
all_contours = [convert_contours(contours, jitter) for contours in all_contours]
assert calib.shape[0] == len(all_contours)
nmesh = len(all_contours)


print("========== Reference run")

ref_csg = contours_to_polygons(all_contours, calib)
ref_csg.toOff("/tmp/out.off", False)

# nsub = (10, 3)
nsub = int(sys.argv[1])

if type(nsub) == int:

    print("========== Subtrees")

    sub_csgs = []
    for i in range(nsub): 
        csg = contours_to_polygons(all_contours[i::nsub], calib[i::nsub])
        csg.toOff("/tmp/out_sub_%d.off" % i, False)
        sub_csgs.append(csg)


    print("============= Root")

    new_csg = intersect_csgs(sub_csgs)

    new_csg.toOff("/tmp/out2.off", False)

else: 
    
    nsub0 = nsub[0]
    print("========== Subtrees", nsub[0])

    sub_csgs = []
    for i in range(nsub0): 
        csg = contours_to_polygons(all_contours[i::nsub0], calib[i::nsub0])
        csg.toOff("/tmp/out_sub_%d.off" % i, False)
        sub_csgs.append(csg)

    for subi in nsub[1:]:
        print("========== Subtree", subi)
        new_sub_csgs = []
        for i in range(subi): 
            csg = intersect_csgs(sub_csgs[i::subi])
            new_sub_csgs.append(csg)
        sub_csgs = new_sub_csgs
    
    print("============= Root")

    new_csg = intersect_csgs(sub_csgs)

    new_csg.toOff("/tmp/out2.off", False)
