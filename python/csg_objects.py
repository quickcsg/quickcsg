# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import pdb
import numpy as np

box_points = np.empty((8, 3))
for i in range(8):
    x = box_points[i]
    for d in range(3):
        x[d] = ((i >> d) & 1) * 2.0 - 1.0

box_faces = [
    (0,1,3,2), 
    (0,4,5,1),
    (0,2,6,4),
    (3,7,6,2),
    (5,4,6,7),
    (3,1,5,7)
    ]
box_faces = np.array([pp[::-1] for pp in box_faces])

box_normals = np.array([
    [ 0.,  0., -1.],
    [ 0., -1.,  0.],
    [-1.,  0.,  0.],
    [ 0.,  1.,  0.],
    [ 0.,  0.,  1.],
    [ 1.,  0.,  0.]])


def rotation_matrix(axis, angle):
    # http://math.stackexchange.com/questions/142821/matrix-for-rotation-around-a-vector
    I = np.eye(3) 
    ux, uy, uz = axis
    W = np.array([[0, -uz, uy], 
                  [uz, 0, -ux], 
                  [-uy, ux, 0]])
    return I + np.sin(angle) * W + (1 - np.cos(angle)) * np.dot(W, W) 

def normalize(x): 
    return x / np.linalg.norm(x)

class ThreeBoxes:
    def get(self, t):

        pts0 = box_points.copy()
        faces0 = np.empty((6, 5))
        faces0[:, 0] = 4

        faces0[:, 1:] = box_faces

        objs = []

        for d in range(3): 
            pts = pts0 * (1 + 0.1 * d)
            pts[:, d] *= 0.3        
            color = [0, 0, 0]
            color[d] = 1
            if d != 0: 
                r = rotation_matrix(color, t * (0.2 + d * 0.1))
                pts = np.dot(pts, r.T)
            objs.append((np.array(color, dtype = 'float32'), pts, faces0.astype('int32').ravel()))

        return objs



class BoxSeries: 

    def __init__(self): 
        self.n = 7

    def get(self, t): 
        n = self.n

        pts0 = box_points.copy()
        faces0 = np.empty((6, 5))
        faces0[:, 0] = 4

        faces0[:, 1:] = box_faces

        objs = []

        # rotate whole scene a little bit to avoid singularities with kd-tree
        ax = np.array([1, 2, 3])
        scene_rotation = rotation_matrix(normalize(ax), 0.02)

        for d in range(3): 
            p0 = 0
            color = [0, 0, 0]
            color[d] = 1
            all_pts = []
            all_faces = []
            for j in range(n):
                # mag = {0: 1.1, 2: 1.1, 1: 1.2}
                pts = pts0 * (1 + 0.1 * d)
                #pdb.set_trace()
                pts[:, d] = pts[:, d] * (0.4 / n) + ((j + 0.5) * 2.0 / n - 1)         
                if d != 1: 
                    axis = np.zeros(3)
                    an = t * (1 + d * 0.1) + j / float(n)
                    axis[(d + 1) % 3] = np.sin(an)
                    axis[(d + 2) % 3] = np.cos(an)
                    r = rotation_matrix(axis, 0.1)
                    pts = np.dot(pts, r.T)
                pts = np.dot(pts, scene_rotation)
                all_pts.append(pts)
                faces = faces0.copy()
                faces[:, 1:] += p0
                all_faces.append(faces.ravel())
                p0 += pts.shape[0]
            objs.append((np.array(color, dtype = 'float32'), 
                         np.vstack(all_pts), 
                         np.hstack(all_faces).astype('int32')))

        return objs

    def key(self, k): 
        if k == 'g': self.n += 1
        elif k == 'f': self.n -= 1
        else: return False
        return True

def make_torus(r, n, n2): 

    # integer ranges 
    urange = np.arange(n2) 
    urange = urange.reshape(-1, 1)
    vrange = np.arange(n)
    
    # float ranges in [0, 2*pi]
    usteps = urange * (2 * np.pi / n2)
    vsteps = vrange * (2 * np.pi / n)
    
    # relief on surface of the torus, size n2-by-n
    # r = 1 + 0.2 * np.abs(np.sin(usteps * 6) + np.sin(vsteps *3))
        
    points = np.empty((n2, n, 3), dtype = 'float32') 

    points[:, :, 2] = np.sin(vsteps) * r
    points[:, :, 0] = (np.cos(vsteps) * r + 1) * np.sin(usteps)
    points[:, :, 1] = (np.cos(vsteps) * r + 1) * np.cos(usteps)
    
    # indices of the 4 corners of each patch on the surface
    indices00 = urange * n + vrange
    indices01 = ((urange + 1) % urange.size) * n + vrange
    indices10 = urange * n + ((vrange + 1) % vrange.size)
    indices11 = ((urange + 1) % urange.size) * n + ((vrange + 1) % vrange.size)

    faces = np.empty((n2, n, 5), dtype = 'int32')
    faces[:, :, 0] = 4
    faces[:, :, 1] = indices00
    faces[:, :, 2] = indices10
    faces[:, :, 3] = indices11
    faces[:, :, 4] = indices01
    
    return (points.reshape(-1, 3), faces.reshape(-1, 5))

class ConcentricToruses: 

    def __init__(self): 
        np.random.seed(0)
        rand = np.random.random

        self.objs = []

        for i in range(16): 
            color = (1 - rand(3)**4).astype('float32')
            tb = rand() * np.pi 
            ta = np.pi / 40 * (rand() + 1)
            if rand() > 0.5: ta = -ta
            axis = normalize(rand(3))
            torus_points, torus_faces = make_torus((0.1 + rand() * 0.02) * 0.4, 10, 20)
            self.objs.append((color, ta, tb, axis, torus_points, torus_faces.ravel()))

    def get(self, t): 
        objs = []
    
        for color, ta, tb, axis, points, faces in self.objs: 
            points = np.dot(points, rotation_matrix(axis, t * ta + tb))
            objs.append((color, points, faces))
            
        return objs

class ThreeToruses: 

    def __init__(self): 
        self.seed = 1        
        self.generate()
    
    def generate(self): 
        np.random.seed(self.seed)
        rand = np.random.random
        self.objs = []

        for i in range(3): 
            color = (1 - rand(3)**4).astype('float32')
            axis = normalize(rand(3))
            torus_points, torus_faces = make_torus(0.3 + 0.2 * rand(), 50, 100)
            
            torus_points[:, 0] += 1
            torus_points = np.dot(torus_points, rotation_matrix(axis, rand() * 2 * np.pi))
            
            self.objs.append((color, torus_points, torus_faces.ravel()))

    def get(self, t): 
        return self.objs


    def key(self, k): 
        if k != 'g': return False
        self.seed += 1
        self.generate()
        return True


def icosahedron():
    """ Icosahedron with subdivided faces (that approaches a sphere)"""
    from numpy import sin, cos, pi, sqrt

    # side of triangle and height
    alpha  = sqrt(50+10*sqrt(5))/5 # = 1/sin(pi/5)
    a = sqrt(4 - alpha ** 2)
    h = 1 - a ** 2 / 2
    r = a / 2 * alpha

    vertices = np.empty((12, 3))

    for i in range(5): 
        an = pi * 2 * i / 5
        vertices[i, :] = [r * cos(an), r * sin(an), h]
        an += pi * 2 / 10    
        vertices[i + 5, :] = [r * cos(an), r * sin(an), -h]

    vertices[10, :] = [0, 0, 1]
    vertices[11, :] = [0, 0, -1]

    triangles = np.empty((20, 3), dtype = int)

    a5 = np.arange(5)
    a51 = (a5 + 1) % 5

    # top cap
    triangles[:5, 0] = 10
    triangles[:5, 1] = a5
    triangles[:5, 2] = a51

    # middle band up
    triangles[5:10, 0] = a51
    triangles[5:10, 1] = a5
    triangles[5:10, 2] = a5 + 5

    # middle band down 
    triangles[10:15, 0] = a51
    triangles[10:15, 1] = a5 + 5
    triangles[10:15, 2] = a51 + 5

    # bottom 
    triangles[15:, 0] = a5 + 5
    triangles[15:, 2] = a51 + 5
    triangles[15:, 1] = 11
    
    return (vertices, triangles)

class Edges:
    " remembers wherther this edge was already subdivided"
    def __init__(self, evert): 
        self.evert = evert
        self.known_edges = {}
        
    def get_indices(self, a, b, coords): 
        if (b, a) in self.known_edges: 
            return self.known_edges[(b, a)][::-1]
        else:
            vno = sum([vtab.size // 3 for vtab in self.evert]) 
            self.evert.append(coords)
            n = coords.shape[0]
            r = list(range(vno, vno + n))
            self.known_edges[(a, b)] = r
            return r



def subdivided_icosahedron(subdiv):
    vertices, triangles = icosahedron()
    evert = [vertices]
    etri = []
    edges = Edges(evert)
    for tri in triangles: 
        indices = np.arange(subdiv + 1).reshape(-1, 1) + np.arange(subdiv + 1)
        indices[indices > subdiv] = -1

        v0, v1, v2 = tri

        # corners 
        indices[0, 0] = v0
        indices[0, -1] = v1
        indices[-1, 0] = v2

        c0, c1, c2 = vertices[tri]
        u1 = (c1 - c0) / subdiv
        u2 = (c2 - c0) / subdiv

        # inside vertices
        vno = sum([vtab.size / 3 for vtab in evert]) 
        # print vno
        for i in range(1, subdiv): 
            for j in range(1, subdiv - i): 
                indices[j, i] = vno
                evert.append(c0 + u1 * i + u2 * j)
                vno += 1

        # edges
        coords = c0 + u1 * np.arange(1, subdiv).reshape(-1, 1)
        indices[0, 1:-1] = edges.get_indices(v0, v1, coords)

        coords = c0 + u2 * np.arange(subdiv - 1, 0, -1).reshape(-1, 1)
        indices[-2:0:-1, 0] = edges.get_indices(v2, v0, coords)
        
        ii = np.arange(1, subdiv); jj = subdiv - ii
        coords = c0 + u2 * ii.reshape(-1, 1) + u1 * jj.reshape(-1, 1)
        indices[ii, jj] = edges.get_indices(v1, v2, coords)

        # build triangles
        for i in range(subdiv): 
            for j in range(subdiv - i): 
                etri.append((indices[i, j], indices[i, j+1], indices[i+1, j]))
                if j != subdiv - i - 1: 
                    etri.append((indices[i+1, j+1], indices[i+1, j], indices[i, j+1]))

    vertices = np.vstack(evert)
    vertices /= np.sqrt((vertices ** 2).sum(1)).reshape(-1, 1)
    triangles =  np.vstack(etri)
    assert triangles.max() < vertices.shape[0], pdb.set_trace()
    return vertices, triangles

class ThreeSpheres: 

    def __init__(self): 
        self.seed = 1        
        self.generate(0)
           

    def generate(self, t): 
        np.random.seed(self.seed)
        rand = np.random.random
        self.objs = []

        for i in range(3): 
            color = (1 - rand(3)**4).astype('float32')
            axis = normalize(rand(3))

            points1, triangles = subdivided_icosahedron(6)            
            nstep = 5
            faces = np.empty((nstep, triangles.shape[0], 4), dtype = 'int32')
            npi = points1.shape[0]
            points = np.empty((nstep * npi, 3))
        
            p0 = 0
            for step in range(nstep): 
                faces[step, :, 0] = 3
                if step % 2 == 0: 
                    faces[step, :, 1:] = triangles + p0
                else: 
                    faces[step, :, 1:] = triangles[:, ::-1] + p0
                points[p0 : p0 + npi, :] = points1 * (1 - 0.8 * step / nstep)
                p0 += npi
            
            axis = np.zeros(3); axis[i] = 1
            R = rotation_matrix(axis, t * 0.2)
            points = np.dot(points, R)
            points += np.random.random(3) * 0.75
            self.objs.append((color, points, faces.ravel()))

    def get(self, t): 
        self.generate(t)
        return self.objs


    def key(self, k): 
        if k != 'g': return False
        self.seed += 1
        self.generate()
        return True


def load_off(fname): 
    f = open(fname, "r")
    assert f.readline() == "OFF\n"

    nvertex, nfacet, zero = f.readline().split()

    nvertex = int(nvertex)
    nfacet = int(nfacet)

    print("%d vertices, %d facets" % (nvertex, nfacet))

    assert zero == "0"

    vertices = np.fromfile(f, dtype = float, count = 3 * nvertex, sep = " ")
    vertices = vertices.reshape(-1, 3)

    facets = np.fromfile(f, dtype = int, count = 4 * nfacet, sep = " ")
    assert facets.size == 4 * nfacet
    # facets = facets.reshape(-1, 4)[:, 1:]

    return vertices, facets.astype('int32')


class BubbleBunny: 

    def __init__(self):
        vertices, facets = load_off("../data/stanford_models/bunny/reconstruction/bunny_res2.off")
        
        self.objs = []

        self.objs.append(((1.0, 1.0, 1.0), vertices, facets.ravel()))
        
    
    def get(self, t):
        return self.objs



class IcoWaves: 

    def __init__(self): 
        self.seed = 1        
        self.subdiv = 10
        self.rfact = 0.3
        self.freq = 4.0
        self.ico = None
        self.ico3 = None
        
        normalize = lambda x: x / np.linalg.norm(x)

        self.axes = [normalize(np.random.randn(3)) 
                     for i in range(3)]
        self.generate(0)
           

    def generate(self, t): 
        
        if self.ico == None: 
            self.ico = subdivided_icosahedron(self.subdiv)
            self.ico3 = None
        
        if self.ico3 == None: 
            self.ico3 = []
            points1, triangles = self.ico
            faces = np.empty((triangles.shape[0], 4), dtype = 'int32')
            faces[:, 0] = 3
            faces[:, 1:] = triangles
            for i in range(3): 
                freq = 10.0
                r2 = 1 + self.rfact * (np.sin(points1[:,0] * self.freq) * 
                                       np.sin(points1[:,1] * self.freq) * 
                                       np.sin(points1[:,2] * self.freq))
                vertices = points1 * r2.reshape(-1, 1)
                self.ico3.append((vertices, faces.ravel()))
        
        self.objs = []
        np.random.seed(0)
        for i, (points, faces) in enumerate(self.ico3): 
            # axis = np.zeros(3); axis[i] = 1
            axis = self.axes[i]
            R = rotation_matrix(axis, t * (0.2 + 0.06 * i))
            points = np.dot(points, R) + np.random.rand(3) * 0.0001
            color = np.zeros(3, dtype = 'float32') 
            color[i] = 1.0
            self.objs.append((color, points, faces))

    def get(self, t): 
        self.generate(t)
        return self.objs


    def key(self, k): 
        if k in 'fg': 
            self.subdiv += 1 if k == 'f' else -1
            print("subdiv=", self.subdiv)
            self.ico = None
            return True
        if k in 'vb':
            self.rfact *= 1.2 if k == 'v' else 1/1.2
            self.freq /= 1.2 if k == 'v' else 1/1.2
            print("rfact=%g freq=%g" % (self.rfact, self.freq))
            self.ico3 = None
            return True
        return False


    
if __name__ == "__main__":

    vertices, triangles = icosahedron()
    
    norm = np.linalg.norm
    for t1, t2, t3 in triangles:
        print(norm(vertices[t1] - vertices[t2]), norm(vertices[t1] - vertices[t3]), norm(vertices[t2] - vertices[t3]))  
