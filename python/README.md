# Python interface of QuickCSG

The C++ mCSG module is interfaced in Python via SWIG. 
The code is recompiled in the `Makefile` in this directory, in addition of the SWIG generated code. 
It exposes most of the mCSG API, and in particular the `CSG` class.

The resulting module is called `pymCSG`. 

## How to compile 

Requires SWIG (and pyopengl for the graphical demo), use your favorite package manager to install (on the mac, miniconda works fine).

For TBB, use [OneTBB](https://github.com/oneapi-src/oneTBB) and set the `TBB_ROOT` path in the Makefile. The package is easy to compile even on non-intel platforms.

## Mesh I/O with pymCSG 

The CSG class is enriched with two functions: 

- `addVFArray` adds a mesh to the CSG object, in the form of an array of points and an array of facets

- `toVFArray` converts the result of the CSG computation to a set of points and facets that is easy to consume from eg. OpenGL.

A very short demo is shown in `test_pymCSG.py`

## OpenSCAD parsing 

`parse_openscad.py` parses the OpenSCAD .csg format and computes the result. 

## OpenGL demo 

The openGL demo shows an evolving object on which CSG operations can be performed in real time. 

There are several objects to try (see `csg_objects.py`): `ThreeBoxes`, `ConcentricToruses`, `IcoWaves`.
Select the object and launch the window with 
```
python viewer.py ConcentricToruses
```

During the demo, the object can be rotated with the mouse. Use the following: 

- escape to quit 

- +/- zoom in and out

- i, u, x, 2, m, r: choose the type of CSG operation to apply

- e: "explode" objects to show the facets 

- space: pause animation

- g, f: increase / reduce complexity of object

- v, b: change object (try it out!)

- k: show kdtree (crashes currently)

![Viewer](viewer_screenshot.jpg "Image Title")



