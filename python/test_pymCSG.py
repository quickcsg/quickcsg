# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com





import pymCSG

params = pymCSG.CSGParameters()

print("parameters:")

for name in dir(params): 
    if name[0] == '_': continue
    print("  ", name, "=", getattr(params, name))


csg = pymCSG.CSG(params)

csg.addMeshFromOff("../data/nonconvex/to0.off")
csg.addMeshFromOff("../data/nonconvex/to1.off")
csg.addMeshFromOff("../data/nonconvex/to2.off")

print(csg.addVFArray)

print("%d meshes, %d vertices, %d facets" % (csg.nmesh, csg.vertices.size(), csg.facets.size()))

csg.afterLoad()

csgop = pymCSG.CSGDiff(csg.nmesh, 1)

print(csgop)

csg.exploreKDTree(csgop)
csg.makeFacets(csgop)

# print csg.toVFArray()

csg.toOff("/tmp/xx.off")

(pts, facets, facet_tab, facet_norm) = csg.toVFArray()

print(pts.shape, facets.shape)

