
For a presentation of QuickCSG, see [the QuickCSG project page](http://morpheo.inrialpes.fr/static/QuickCSG/). 

If you use QuickCSG in a research paper, please cite [this paper](https://arxiv.org/abs/1706.01558):
```
@misc{https://doi.org/10.48550/arxiv.1706.01558,
  doi = {10.48550/ARXIV.1706.01558},  
  url = {https://arxiv.org/abs/1706.01558},  
  author = {Douze, Matthijs and Franco, Jean-Sébastien and Raffin, Bruno},
  keywords = {Graphics (cs.GR), FOS: Computer and information sciences, FOS: Computer and information sciences},
  title = {QuickCSG: Fast Arbitrary Boolean Combinations of N Solids},
  publisher = {arXiv},
  year = {2017},
}
```



Compilation
===========

The compilation is run via cmake. There are several parameters that can be 
set at compile time:

- threading enabled or not with -DENABLE_TBB, [OneTBB](https://github.com/oneapi-src/oneTBB) is needed as a dependency

- with of the bit fields, by multiples of 64, eg. -DBWIDTH=3 means you
  can combine up to 192 meshes.

- whether to use the provided libtess or the system GLU tesselator,
  see comments in the CMakeLists.txt

- whether to use float or double to store coordinates, with USE_FLOAT.

Code layout
===========

- `mesh_csg.cpp`: 
   entry point, and driver.

- `mCSG.hpp`: 
   defines the mCSG object, that contains the input meshes + the
   current state of the output mesh.

- `bitvectors.hpp` `bitvectors_wide.hpp`:
   bit vector routines, including the CSG operations. The wide version
   is for bit vectors of more than 64 bit.

- `kdtree.hpp` `kdtree.cpp`:
   definition of the kdtree node structure, splitting routines and
   convex polygon

- `topology.cpp`: 
   data preparation (registration of neighbor facets and such)

- `node_geometry.cpp`: 
   finding vertices within a kdtree node

- `io.cpp`: 
   I/O functions + some error reporting

- `vertices_to_facets.cpp`:
   conversion of edges and vertices to facets + tesselization

- `random_perturbation.hpp` `random_perturbation.cpp`:
   applying and reverting a random perturbation on the input meshes.

- `VectorPreferredSize.hpp`, `MemoryPool.hpp`:
   efficient implementation of recurrent data structures: vectors 
   that are small most of the time and fixed-size memory pools.

Mapping of the paper to the code
================================

Between the writing of the code and the paper, the terminology has
evolved (a lot).

**Indicator vector**: 
   the corresponding type is BitVector, that has a maximum fixed size. 
   's' and 'u' values are "unknown". The indicator of a vertex or a node 
   is called "meshpos" (mesh position)

**order-1/-2/-3 vertices**:  primary, double and triple vertices

**looplet**: HEPair (pair of half-edges)

**CSGVertices**: the closest function is findNodeVertices()

**CSGFacets**:  makeFacets(), that calls makeFacet

**topology phase**:   afterLoad()

A few key routines:

`nmesh` = nb of input polyhedra

kdtree splitting algorithm: `Node::split()`

kdtree orientation computation:  `getMeshSideMin`/`getMeshSideMax` (in `kdtree.cpp`)

ray shooting within a node: `Node::isInsidePolygons`

computing the indicator vector: `Node::computeMeshposLazy`



Coding conventions 
==================

- as basic C++ as possible. Most often, no public/private but structs,
  const when applicable, no mutable. As few templates as possible:
  bitvector size and float flavour set at compile time.

- intensive logging. Switch on/off via the verbose field.

- indices rather than pointers to identify faces, vertices, etc. More
  human readable in logs and can be followed in Meshlab.

- generous data duplication (in particular between kdtree nodes and
  global facet structures): RAM is cheap and cache misses are
  expensive.

- a few long code files, no global variables, 80 columns (emacs
  friendly)

- assertions for internal inconsistency errors, error logging for
  logical CSG errors. 

- i/o with stdio

- as few dependencies as possible (currently TBB)

- as little as possible code specific to the parallel version (with
  ENABLE_TBB). Only parallel code is C++11, the rest is normal
  C++. Don't spawn new threads when parallel_nt <= 1

- performance guidelines that can be forgotten: 
  ++i faster than i++
  iterators faster than indexing 
  tests hurt performance 
  templates faster than virtual functions 

- performance guidelines that are enforced:
  keep related data in nearby memory -- avoid random accesses to global memory
  sequential access is best
    

Built on mkCSG
==============

Some side projects in subdirectories.

python/
   Python interface implemented in SWIG (pymCSG.i)
   CSG animation and OpenGL viewer (viewer.py)
   Openscad parser (parse_openscad.py)

experiments/
   source for the experiments in the paper

data/
   all input data for experiments. Should be checked out from 
   https://wiki-grimage.imag.fr/svn/grimage/trunk/dev/bpetit/pCSG/data

distributed/
   parallelization on several machines with FlowVR. 

Debugging strategies
====================

Use the verbose option wisely it will log almost everything. 

If the output does not contain a patch of vertices, look at the
combined mesh with meshlab, get a vertex number that is missing, and
track it through the subdivisions:

```
./mesh_csg data/cmp_meshworks/{buddha.off,lion.off} -union -o /tmp/out.off -tess  -v 1111111 -combined /tmp/combined.off

# out.off misses a part, open it side-by-side with combined, use the
# info tool of Meshlab to see what is missing: vertex 606647

./mesh_csg data/cmp_meshworks/{buddha.off,lion.off} -union -o /tmp/out.off -tess  -v 1111111 -combined /tmp/combined.off -followvertex 606647

# this indicates that the vertex was last seen in node <1221112111211>. 
# save the node to an off file:

./mesh_csg data/cmp_meshworks/{buddha.off,lion.off} -union -o /tmp/out.off -tess  -v 1111111 -combined /tmp/combined.off -savenode "<1221112111211>" /tmp/node.off

# what? is this mini-snippet deciding what to do with the green patch?
# How could that happen? Let's see the parent, then the grand-parent.
 
./mesh_csg data/cmp_meshworks/{buddha.off,lion.off} -union -o /tmp/out.off -tess  -v 1111111 -combined /tmp/combined.off -savenode "<12211121112>" /tmp/node_parent.off
```



Run & test
==========
```
cmake

make 

# see help 

./mesh_csg -h

# mini test
./mesh_csg data/3boxes/box{0,1,2}.off -o out.off 

# see result with 
meshlab out.off

# main motivating test

cd ../knots42

../../mCSG/mesh_csg  cones{0..41}.off -tess -o out.off 
```
Notes on parallelization
========================

Subtrees can be explored in parallel if compiled with TBB. 

```
cmake -DENABLE_TBB=TRUE
```

License 
=======

Copyright (C) by INRIA 2013-2015
License: Free software license agreement for non-commercial purposes 
See text of the license in the root of the repository

Contact: matthijs.douze@gmail.com jean-sebastien.franco@inria.fr

INRIA, 2013-2015
Author: Matthijs Douze
