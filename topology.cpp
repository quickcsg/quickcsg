/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */
#include <cmath>
#include <cstdio>


#include "mCSG.hpp"
#include "kdtree.hpp"

#ifdef ENABLE_TBB

#include <tbb/parallel_for.h>
#include <tbb/blocked_range.h>

#endif

using namespace mCSG;



			     
/**********************************************************************
 *  Basic routines
 **********************************************************************/


static int argMax3(real v0, real v1, real v2) {
  if(v0 > v1) {
    if(v0 > v2) return 0;
    else return 2;
  } else {
    if(v1 > v2) return 1;
    else return 2;
  }
}

static int argMaxVec3(const Vec3 &v) {
  return argMax3(v[0], v[1], v[2]);
}




void Facet::afterLoad(const std::vector<Vertex> &vertices) { 

  int nv = vertexNos.size();

  // compute bbox
  Vec3 bbMin = Vec3::max();
  Vec3 bbMax = -Vec3::max(); 

  for(int i = 0; i < nv; i++) {
    const Vec3 &pt = vertices[vertexNos[i]].coords;
    for(int d = 0; d < 3; d++) {
      if(bbMin[d] > pt[d]) bbMin[d] = pt[d];
      if(bbMax[d] < pt[d]) bbMax[d] = pt[d];
    }
  }

  // compute normal around first point 
  
  // find extremal point 
  Vec3 bbSize = bbMax - bbMin; 
  int dim = argMaxVec3(bbSize);  // dimension with largest variance

  int i1;
  for(i1 = 0; i1 < nv; i1++) {
    const Vec3 &pt = vertices[vertexNos[i1]].coords;
    if(pt[dim] == bbMin[dim]) break;
  }
  int i0 = i1 > 0 ? i1 - 1 : nv - 1;
  int i2 = i1 + 1 < nv ? i1 + 1 : 0;
  
  // 3 successive points 
  const Vec3 &p0 = vertices[vertexNos[i0]].coords;
  const Vec3 &p1 = vertices[vertexNos[i1]].coords;
  const Vec3 &p2 = vertices[vertexNos[i2]].coords;
  
  normal = -(p1 - p0).cross(p1 - p2);
  normal.normalize(); 
  b = p1.dot(normal);
    

}

int Facet::findVertex(int p1, int *pi) const {
  int nv = vertexNos.size();
  int prev_i = nv - 1;
  for(int i = 0; i < nv; i++) {
    if(vertexNos[i] == p1) {
      *pi = i; 
      return vertexNos[prev_i];
    }
    prev_i = i;
  }
  *pi = -1; 
  return -1;  
}

int Facet::findOppositeFacet(int fno, int *pi) const {
  int nv = vertexNos.size();
  int prev_i = nv - 1;
  for(int i = 0; i < nv; i++) {
    if(oppositeFacetNos[i] == fno) {
      *pi = i;
      return vertexNos[prev_i];
    }
    prev_i = i;
  }
  *pi = -1; 
  return -1;  
}


Vec3 CSG::edgeDirection(int f0, int f1) {
  const Facet & facet0 = facets[f0]; 
  const Facet & facet1 = facets[f1]; 
  
  if(facet0.meshno == facet1.meshno) {
    int nv = facet0.vertexNos.size(); 
    for(int i = 0; i < nv; i++) {
      if(facet0.oppositeFacetNos[i] == f1) {
	int vno1 = facet0.vertexNos[i]; 
	int vno0 = facet0.vertexNos[i > 0 ? i - 1 : nv - 1]; 	
	return vertices[vno1].coords - vertices[vno0].coords;
      }
    }
    errors.add("edgeDirection: could not find opposite direction"); 
    return Vec3(0, 0, 0);
  } else {
    return facet1.normal.cross(facet0.normal);
  }

}



struct Corner {
  int fno;  // facet no 
  int pi;   // index of vertex i in the list of vertices of the facet
  int prev, next; // previous and next vertex in the list of vertices
  int link_prev, link_next; // link to corner that should go before
}; 

static void reorderFacetsInVertex(int vno, Vertex & v, std::vector<Facet> & facets, CSG & csg) {     
  int verbose = csg.verbose;

  VectorPreferredSize<6, Corner> corners;
  int nf = v.facetNos.size();

  if(nf < 2) return; // nothing to reorder, will cause errors later
  
  for(int j = 0; j < nf; j++) {
    Corner c; 
    c.fno = v.facetNos[j]; 
    Facet & facet0 = facets[c.fno];
    c.prev = facet0.findVertex(vno, &c.pi);       
    assert(c.prev >= 0);       
    c.next = facet0.vertexNos[(c.pi + 1) % facet0.vertexNos.size()]; 
    IFV(4) printf("      F%d (V%d at index %d): prev %d next %d\n", c.fno, vno, c.pi, c.prev, c.next); 
    c.link_prev = c.link_next = -1; 
    corners.push_back(c);
  }
  
  IFV(3) printf("    linking %d corners\n", nf); 
  int n_link = 0; 
  for(int j = 0; j < nf; j++) {
    for(int k = 0; k < nf; k++) {
      if(corners[j].prev == corners[k].next) {
        corners[j].link_next = k;
        corners[k].link_prev = j;
        IFV(4) printf("      link F%d -> F%d\n", corners[j].fno, corners[k].fno);           
        n_link++; 
        break;
      }
    }     
  }
  
  IFV(3) printf("    %d/%d corners have links\n", n_link, nf); 
  int start_corner; 
  
  if(n_link == nf) {
    start_corner = 0; // arbitrary
  } else if(n_link == nf - 1) {
    for(start_corner = 0; start_corner < nf; start_corner++) 
      if(corners[start_corner].link_prev < 0) break;
    assert(start_corner < nf);       
  } else {
    csg.errors.add("reorderFacetsInVertex: several disjoint polygons"); 
    return;
  }
  
  int cno = start_corner; 
  for(int j = 0; j < nf; j++) {
    Corner & c = corners[cno];
    v.facetNos[j] = c.fno; 
    cno = c.link_next; 

    if(cno < 0) {
      if(j != nf - 1) 
        csg.errors.add("reorderFacetsInVertex: inconsistent corners around vertex"); 
      break;
    }
    Corner & c1 = corners[cno]; 
    Facet & facet0 = facets[c.fno]; 
    Facet & facet1 = facets[c1.fno]; 
    
    facet0.oppositeFacetNos[c.pi] = c1.fno; 
    facet1.oppositeFacetNos[(c1.pi + 1) % facet1.oppositeFacetNos.size()] = c.fno;     
    
  }
}

#ifdef ENABLE_TBB
struct LoopReorderFacetsInVertex {
  CSG & csg;
   
  void operator()(const tbb::blocked_range<int>&r) const{
    for (int i=r.begin() ; i!=r.end(); i++ ) {
      reorderFacetsInVertex(i, csg.vertices[i], csg.facets, csg);
    }
  }
};
#endif

void CSG::afterLoad() {

  nvert1 = vertices.size();
  double t0 = getmillisecs();
  
  IFV(2) printf("  afterLoad: set face normals and register vertex->face refs\n");  

  // this loop is hard to parallelize
  for(int i = 0; i < facets.size(); i++) {
    Facet & facet = facets[i]; 
    facet.afterLoad(vertices); // set normals
    if(!std::isfinite(facet.b)) errors.add("CSG::afterLoad: normal is NaN (0-surface facet?)"); 

    // register with vertices 
    for(int j = 0; j < facet.vertexNos.size(); j++) {
      Vertex &v = vertices[facet.vertexNos[j]]; 
      v.facetNos.push_back(i); 
    }       
    facet.oppositeFacetNos.resize(facet.vertexNos.size(), -1);
  }


  double t1 = getmillisecs();
  IFV(2) printf("    reorder facet references in vertices & fill in opposite facets\n"); 

  // fill in facets after edge & reorder facets for each vertex

#ifdef ENABLE_TBB
  if(parallel_nt != 1) {
    // this loop can be parallelized because oppositeFacetNos is
    // "write only", for each normal edge, it written twice, but the
    // writes are consistent
    LoopReorderFacetsInVertex loop = { *this }; 
    tbb::parallel_for(tbb::blocked_range<int>(0, vertices.size()), loop);
  } else 
#endif
  {
    for(int i = 0; i < vertices.size(); i++) {
      IFV(3) printf("    reordering V%d\n", i); 
      reorderFacetsInVertex(i, vertices[i], facets, *this);     
    }
  }
  
  double t2 = getmillisecs();
  IFV(2) printf("    times %.3f ms + %.3f ms\n", t1 - t0, t2 - t1); 

}



/**********************************************************************
 *  Graph exploration
 **********************************************************************/



CSG::~CSG() {deleteTree(); }


void CSG::exploreKDTree(CSGOperation *csgop) {
  if(!root) initKDTree(); 

  if(breadth_depth == 0) 
    exploreNode(root, csgop);   
  else
    exploreBreadth(csgop);   
}


