/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */
#ifndef BITVECTORS_HPP_INCLUDED
#define BITVECTORS_HPP_INCLUDED

/*
  Implements the same functions as bitvectors.cpp but for vectors of arbitrary width. 
 */


namespace mCSG {
  

  typedef unsigned long u64;

  inline int bitcount(u64 b) {
#ifdef __GNUC__
    return __builtin_popcountll(b);
#else
    int count = 0;
    for(int i = 0; i < sizeof(b) * 8; i++) {
      if(b & 1) count++; 
      b >>= 1;
    }
    return count;
#endif
  }

  // encodes the position of a point wrt a set of meshes (at most 64,
  // since we are using 64-bit longs to encode the bit vector). 
  // 3 states: 0 = outside 
  //           1 = inside
  //           u = unknown
  // some operations are difficult to read in binary notation. In this
  // case, the "readable" version is in comment.
  
  struct BitVector {

    static const int maxbits = BVWIDTH * 64;

#define ILO (i & 63)
#define IHI (i >> 6)

    u64 known[BVWIDTH];  // bit #i == 1: insideness of mesh i is known 
    u64 inside[BVWIDTH]; // bit #i == 1: if insideness i is known, then means is inside else is outside. Set to 0 if unknown. 
    BitVector() {
      for(int i = 0; i < BVWIDTH; i++) known[i] = inside[i] = 0;
    }

    void setBit(int i) {known[IHI] |= 1UL << ILO; inside[IHI] |= 1UL << ILO; }
    void clearBit(int i) {known[IHI] |= 1UL << ILO; inside[IHI] &= ~(1UL << ILO); }
    void setSide(int i, bool v) {
      // if(v) setBit(i); else clearBit(i); 
      known[IHI] |= 1UL << ILO; inside[IHI] = (inside[IHI] & ~(1L << ILO)) | u64(v) << ILO;
    }
    void setUnknown(int i) {known[IHI] &= ~(1UL << ILO); inside[IHI] &= ~(1UL << ILO); }
    void flipBit(int i) {inside[IHI] ^= 1UL << ILO; }
    void toString(int l, char *s) const; 
    std::string toString(int l) const; 
    bool isKnown(int i) const {return (known[IHI] >> ILO) & 1; }
    bool bit(int i) const {return (inside[IHI] >> ILO) & 1; }

    void copyBitFrom(const BitVector &other, int src, int i) {
      u64 destmask = 1UL << ILO; 
      inside[IHI] = (inside[IHI] & ~destmask) | (((other.inside[src >> 6] >> (src & 63)) & 1) << ILO); 
      known[IHI]  = (known[IHI]  & ~destmask) | (((other.known[src >> 6]  >> (src & 63)) & 1) << ILO); 
    }
    
    void orEq(const BitVector &other) {
      for(int i = 0; i < BVWIDTH; i++) {
        inside[i] |= other.inside[i];
        known[i] |= other.known[i];
      }
    }

    void setAllKnown(int nmesh) {
      for(int i = 0; i < (nmesh >> 6); i++) {
        known[i] |= ~0; 
      }
      if(nmesh & 63) known[nmesh >> 6] = (1UL << (nmesh & 63)) - 1; 
    }
    bool cmpKnown(const BitVector & other) {
      for(int i = 0; i < BVWIDTH; i++) 
        if(known[i] != other.known[i]) return false; 
      return true;
    }

    int nbKnown() const {
      int accu = 0; 
      for(int i = 0; i < BVWIDTH; i++) accu += bitcount(known[i]);
      return accu;
    }
    // ternary = +1, 0, -1 for inside, outside, unknown

    int getTernary(int i) const {
      // if(!isKnown(i)) return -1; 
      // return bit(i); 
      return ((int((known[IHI] >> ILO) & 1)) - 1) | ((inside[IHI] >> ILO) & 1); 
    }
    
    void setTernary(int i, int t) {
      // if(t < 0) setUnknown(i); 
      // else setSide(i, t); 
      
      u64 mask = 1UL << ILO; 
      u64 knownbit = u64(((t >> 1) ^ 1) & 1) << i;
      known[IHI]  = (known[IHI]  & ~mask) | knownbit; 
      inside[IHI] = (inside[IHI] & ~mask) | (knownbit & u64(t & 1) << i);       
    }

  };

  
  
  struct CSGOperation {

    // From the insideness of a set of meshes, determine if we are
    // inside the result mesh
    // returns 1 = inside 
    // 0 = outside 
    // -1 = unknown 
    virtual int isInside(const BitVector &meshpos) = 0;   
    
    virtual ~CSGOperation() {}

    // compute insideness at 2 sides of mesh, meshpos should be fully known
    // 2 bits used. Changes meshpos!
    int compute_in2(BitVector &meshpos, int meshno) {
      meshpos.clearBit(meshno); 
      int in0 = isInside(meshpos);
      meshpos.setBit(meshno);
      int in1 = isInside(meshpos);
      return in0 << 4 | in1; 
    }

    int in2(const BitVector &meshpos, int meshno) {
      BitVector meshpos2 = meshpos; 
      return compute_in2(meshpos2, meshno); 
    }
  
    // compute insideness at intersections of 2 meshes (quadrant)
    // 4 bits used
    int compute_in4(BitVector &meshpos, int meshno0, int meshno1) {
      meshpos.clearBit(meshno0);
      int in0 = compute_in2(meshpos, meshno1); 
      meshpos.setBit(meshno0);
      int in1 = compute_in2(meshpos, meshno1); 
      return in0 << 8 | in1; 
    }

    int in4(const BitVector &meshpos, int meshno0, int meshno1) {
      BitVector meshpos2 = meshpos; 
      return compute_in4(meshpos2, meshno0, meshno1); 
    }

    // compute insideness at 8 sides (octant)
    // 8 bits used
    int compute_in8(BitVector &meshpos, int meshno0, int meshno1, int meshno2) {
      meshpos.clearBit(meshno0);
      int in0 = compute_in4(meshpos, meshno1, meshno2); 
      meshpos.setBit(meshno0);
      int in1 = compute_in4(meshpos, meshno1, meshno2); 
      return in0 << 16 | in1; 
    }

    int in8(const BitVector &meshpos, int meshno0, int meshno1, int meshno2) {
      BitVector meshpos2 = meshpos; 
      return compute_in8(meshpos2, meshno0, meshno1, meshno2); 
    }

  };

  // definition of op1, op2, op3

  inline bool is_corner_in2(int in2) {
    if(((in2 ^ (in2 >> 4)) & 1) == 0) return 0; 
    return 1; 
  }
  
  inline bool is_corner_in4(int in4) {
    if(((in4 ^ (in4 >> 4)) & 0x0101) == 0) return 0; 
    if(((in4 ^ (in4 >> 8)) & 0x0011) == 0) return 0; 
    return 1; 
  }

  inline bool is_corner_in8(int in8) {
    if(((in8 ^ (in8 >> 4)) & 0x01010101) == 0) return 0; 
    if(((in8 ^ (in8 >> 8)) & 0x00110011) == 0) return 0; 
    if(((in8 ^ (in8 >> 16)) & 0x00001111) == 0) return 0; 
    return 1; 
  }
  

  struct CSGIntersection: CSGOperation {
    int nmesh; 
    
    CSGIntersection(int nmesh): nmesh(nmesh) {}
    
    virtual int isInside(const BitVector &meshpos) {
      int ret = 1; 
      for(int i = 0; i < nmesh; i++) {
        int b = meshpos.getTernary(i); 
        if(b == 0) return 0; 
        if(b == -1) ret = -1; 
      }
      return ret; 
    }
        
  }; 

  
  struct CSGUnion: CSGOperation {
    int nmesh; 
    
    CSGUnion(int nmesh): nmesh(nmesh) {}
    
    virtual int isInside(const BitVector &meshpos) {
      assert(!"not implemented"); 
      return 0; 
    }
    
  }; 
  
  
  struct CSGMinMesh: CSGOperation {
    int nmesh, nmin; 
    
    CSGMinMesh(int nmesh, int nmin): nmesh(nmesh), nmin(nmin) {}
    
    virtual int isInside(const BitVector &meshpos) {
      int n_in = 0, n_unknown = 0;
      for(int i = 0; i < nmesh; i++) {
        if(meshpos.isKnown(i)) {
          if(meshpos.bit(i)) n_in++; 
        } else 
          n_unknown++; 
      }
      return n_in >= nmin ? 1 : n_in + n_unknown >= nmin ? -1 : 0; 
    }
    
  }; 
  
  struct CSGDiff: CSGOperation {
    int nmesh, npos; 
    
    CSGDiff(int nmesh, int npos): nmesh(nmesh), npos(npos) {}
    
    virtual int isInside(const BitVector &meshpos) {
      int inside = 0; 
      for(int i = 0; i < npos; i++) {
        int b = meshpos.getTernary(i); 
        if(b == 1) inside |= 1;
        if(b == -1) inside = -1; 
      }
      for(int i = npos; i < nmesh; i++) {
        int b = meshpos.getTernary(i); 
        if(b == 1) return 0; 
        if(b == -1) inside = -1; 
      }        
      return inside; 
    }    
    
  }; 
  
  struct CSGXOR: CSGOperation {
    int nmesh; 
    
    CSGXOR(int nmesh): nmesh(nmesh) {}
    
    virtual int isInside(const BitVector &meshpos) {
      assert(!"not implemented"); 
      return 0; 
    }
    
  }; 
  
  struct ThreeMeshOp: CSGOperation {
    //  ThreeMeshOp(int nmesh) {assert(nmesh == 3);}
    
    virtual bool isInside3(bool a, bool b, bool c) = 0;
    
    virtual int isInside(const BitVector &meshpos) {
      assert(!"not implemented"); 
      return 0; 
    }
        
  };
  
  struct CSGInter2Union: CSGOperation {
    int nmesh, npos; 
    
    CSGInter2Union(int nmesh, int npos): nmesh(nmesh), npos(npos) {}
    
    virtual int isInside(const BitVector &meshpos) {
      assert(!"not implemented"); 
      return 0;
    }

  }; 

  struct CSGLargeTable: CSGOperation {
    int nmesh;
    std::vector<unsigned char> table; 

    CSGLargeTable(int nmesh, const char * filename): nmesh(nmesh) {}
    virtual int isInside(const BitVector &meshpos) {
      assert(!"not implemented"); 
      return 0;
    }

  } ;
  
  struct CSGNmesh: CSGOperation {
    int nmesh, nm; 
    
    CSGNmesh(int nmesh, int nm): nmesh(nmesh), nm(nm) {}
    
    virtual int isInside(const BitVector &meshpos) {
      assert(!"not implemented"); 
      return 0;
    }
    
  }; 

  
  struct CSGAmBuC: CSGOperation {
    
    virtual bool isInside3(bool a, bool b, bool c) {
      return c && !(a || b); 
    }
    
  };
  
  struct CSGAuBmC: ThreeMeshOp {
    
    //  CSGAuBmC(
    virtual bool isInside3(bool a, bool b, bool c) {
      return (a || b) && !c; 
    }
    
  };
  
  struct CSGBuCmA: ThreeMeshOp {
    
    virtual bool isInside3(bool a, bool b, bool c) {
      return (c || b) && !a; 
    }
    
  };

  struct CSGAmBiC: ThreeMeshOp {
    
    virtual bool isInside3(bool a, bool b, bool c) {
      return a && !b && c; 
    }
    
  };

  struct CSGTable: CSGOperation {
    int nmesh;
    unsigned long table; 
    
    CSGTable(int nmesh, unsigned long table): nmesh(nmesh), table(table) {}
    
    virtual int isInside(const BitVector &meshpos) {
      assert(!"not implemented"); 
      return 0; 
    }
    
  }; 


  // basic operations on ternary representation (+1, 0, -1)

  static int oand(int a, int b) {
    /*
    if(a == 0 || b == 0) return 0; 
    if(a == 1 && b == 1) return 1; 
    return -1; 
    */
    return -(-a & -b); 
    
  }
  
  static int oor(int a, int b) {
    /*
    if(a == 1 || b == 1) return 1; 
    if(a == 0 && b == 0) return 0; 
    return -1; 
    */
    return -(-a | -b);
  }
  
  static int onot(int a) {
    /*
    if(a == 0) return 1; 
    if(a == 1) return 0; 
    return -1; 
    */
    return (a >> 1) | (a ^ 1);
  }

  struct CSGDither: CSGOperation {
    /* 
       compute (m[0] & sel) | (m[1] & !sel)
       with sel = m[2] | (m[3] & m[4] & m[5])
     */

    virtual int isInside(const BitVector &meshpos) {     
      int m[6]; 
      for(int i = 0; i < 6; i++) m[i] = meshpos.getTernary(i);

      int sel = oor(m[2], oand(m[3], oand(m[4], m[5]))); 

      return oor(oand(m[0], sel), oand(m[1], onot(sel)));
    }
    
  }; 
  
  struct CSGProgram: CSGOperation {
    // Tree of CSG operations
    std::string instructions; 
    // instructions are: & = and, - = diff, | = or, + = group, 0..p = push this number
    // >= 128: read lower 7 bits from next instruction (a la intel)

    CSGProgram(const char *instructions): instructions(instructions) {}

    virtual int isInside(const BitVector & meshpos) {
      BitVector stack;
      // stack pointer and instruction pointer 
      int sp = 0;
      const char *ip = instructions.c_str(); 
      for(;;) {
	char i = *ip++; 
	if(i == 0) break;
	if('0' <= i && i < '0' + 64) {
	  // push index to stack
	  stack.copyBitFrom(meshpos, i - '0', sp++);	  
	  //printf("push %d ?= %d from %d sp %d\n", 
	  //	stack.getTernary(sp - 1), meshpos.getTernary(i - '0'), i - '0', sp);
        } else if(i < 0) {
          int cno = int(i & 0x7f) << 7 | int((*ip++) & 0x7f); 
	  stack.copyBitFrom(meshpos, cno, sp++);          
	} else if(i == '+') {
          int res = 1; 
          while(sp) {
            int b = stack.getTernary(--sp); 
            if(b == 0) {res = 0; break; }
            if(b < 0) res = -1; 
          }          
	  stack.setTernary(0, res); 
	  sp = 1;
	} else { // binary op
	  int b = stack.getTernary(--sp);
	  int a = stack.getTernary(--sp);
	  int res; 
	  switch(i) {
	  case '|': res = oor(a, b); break;  
	  case '&': res = oand(a, b); break; 
	  case '-': res = oand(a, onot(b)); break;
	  default: assert(!"unknown op");
	  }
	  //printf("sp %d %c %d %d -> %d\n", sp, i, a, b, res); 
	  stack.setTernary(sp++, res);
	}	
      }
      assert(sp == 1); 
      return stack.getTernary(--sp);
    }
    
  };
};


#endif
