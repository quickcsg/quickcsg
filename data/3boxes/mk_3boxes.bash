


python ../generate_primitive.py -box 1.0 -scale z 0.3 > box0.off
python ../generate_primitive.py -box 0.93 -scale x 0.3 -rotate x 13 -rotate y -15 > box1.off
python ../generate_primitive.py -box 1.2 -scale y 0.25 -rotate z 11 -rotate x -22 > box2.off


python ../generate_primitive.py -box 0.93 -translate y 1.05 -translate z 0.92 -scale x 0.3 -rotate x 13 -rotate y -15 > box1trans.off
python ../generate_primitive.py -box 1.0 -scale z 0.3 -rotate x 0.1 -rotate y 0.3 > box0r.off


