/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */
#ifndef KDTREE_HPP_INCLUDED
#define KDTREE_HPP_INCLUDED


#include <vector>

#include "bitvectors.hpp"
#include "VectorPreferredSize.hpp"
#include "MemoryPool.hpp"

namespace mCSG {

  struct Facet; 
  struct Vertex; 
  
#ifndef USE_FLOATS
  typedef cinderlite::Vec3<double> Vec3; 
  typedef double real;
#else
  typedef cinderlite::Vec3<float> Vec3; 
  typedef float real;
#endif

  /**************************************************
   * KDTree sctructures
   **/

  struct CSG;

  /**************************************************
   * Convex polygon structure to express intersection of triangles with KDTree cells
   **/
  
  // intersection of a line in a polygon with the border of the polygon
  struct InPlaneIntersection {
    // 0 = no inter
    // 1 = intersection, going in polyline
    // 2 = intersection, going out polyline
    int exist; 
    // below set if exist != -1
    Vec3 coords; // coordinates of intersection
    real t;    // intersection is orig + t * dir
    int which;   // is border from this (0) or other (1)?
    int pi;      // point index in Polygons's vertices
  }; 

  // result of a line-polygon intersection
  struct Intersection {
    // 1 = inter ok, going in facet
    // 2 = inter ok, going out of facet
    // 0 = no inter
    // -1 = degenerate because orig in plane
    // -2 = degenerate because intersection on polyline
    int exist; 
    // below set if exist > 0
    Vec3 coords; // coordinates of intersection
    real t;    // intersection is orig + t * dir
  }; 
  
  struct PolygonVertex {
    Vec3 coords; 
    int primaryNo; // from which primary point it comes, or -1 if it is not a primary point or if there is another polygon that records it
    int borderFacetNo; // the previous edge is the intersection of the facet with this one
  }; 
  
  struct ConvexPolygon {
    
    typedef VectorPreferredSize< 6, PolygonVertex > VertexTableType; 
    VertexTableType vertices; 
    
    int facetNo; // facet id in CSG
    int meshno;  // mesh identifier
    Vec3 normal; // normal vector (points towards out)
    real b;    // intercept (equation is normal.x = b
    Vec3 bbMin, bbMax; // bounding box    
    
    ConvexPolygon(int facetNo, const Facet &facet, const std::vector<Vertex> &verticesIn);
    
    ConvexPolygon(int facetNo): facetNo(facetNo) {};
    
    bool empty() const { return vertices.empty(); }
    
    // half-space defined by  (x[dim] > b) == sense      
    void intersectWithHalfSpace(int dim, bool sense, real b);
    
    
    // returns true if anything remains of the polygon
    bool cropToBBox(const Vec3 & cbbMin, const Vec3 & cbbMax); 


    // compute bbox from vertices
    void setBBox(); 
    
    // center of weight of the vertices
    Vec3 center() const; 
    
    
    // intersections of a line with a polygon
    // 0: completely inside
    // -1: degenerate
    // -2: completely outside
    int allLineInPlaneIntersections(const Vec3 &orig, const Vec3 &dir, 
                                    InPlaneIntersection * inters) const;      
    
    // intersection of two polygons. Returns the number of intersection points (multiple of 2)
    // inters should be allocated to the sum of number of vertices of the two polygons
    // returns the nb of intersections
    // -1: degenerate case
    // -2: no intersection
    // 3: too many intersections (there should be at most 2)
    int polygonIntersections(const ConvexPolygon & other,
                             Vec3 & origin, Vec3 & direction,  // set on output
                             InPlaneIntersection *inters, CSG & csg) const;
    
    // 1 = is inside, 0 = is outside, -1: degenerate
    int isPointInside(const Vec3 & point) const;
    
    // compute intersection with a line
    void lineIntersection(const Vec3 &orig, const Vec3 &dir, 
                          Intersection & inter) const; 
    
      
  };

  // report who is working on what
  struct ProcessingStats {
    long begin_cy, end_cy;
    long thread_id;
    ProcessingStats(): begin_cy(0), end_cy(0), thread_id(0) {}
    void begin(); 
    void end(); 
  }; 
    
  // KDTree node
  struct Node {

    // structure information
    Node *parent; // root's parent is NULL
    Node *child1, *child2;     
    int depth;             // root depth = 0
    
    // bounding box
    Vec3 bbMin, bbMax; 
    
    // 0 = internal node can be subdivided / copied to output (at end of build)
    // 1 = internal node, is subdivided
    // 2 = leaf node, is explored
    // 3 = final node, completely inside or outside
    int state; 
    
    // non-empty when state = 0 or 2 
    // typedef std::vector<ConvexPolygon*> PolygonTable;
    typedef VectorPreferredSize<16, ConvexPolygon*> PolygonTable; 
    PolygonTable polygons; 
    
    // set when state = 1
    real threshold;      // threshold on this dimension
    int split_dim;         // split a dimension dim (0, 1, 2)
    
    
    // unknown = facets of this mesh intersect the cell 
    // 0 = cell is fully outside mesh
    // 1 = cell is fully inside mesh
    BitVector meshpos;    
    
    // processing statistics
    ProcessingStats processingStats;

    // nb of polygons that were in this node (may be different from
    // actual polygons if polygons were cleared)
    int  n_polygon;
    
    Node(Node *parent = NULL); 

    // deletes polygons in polygon table
    ~Node();
    
    void updateBBox(const Vec3 &bbMin, const Vec3 &bbMax); 
    
    
    // split heuristically, if state == 0, 
    void split(CSG & tree);
    
    // split with given parameters
    void split(CSG & kdtree, int split_dim, real threshold); 
        
    //output node to .off file (possibly restricted to polygons from 1 mesh)
    void toOff(const char *fname, int meshno = -1) const;
    
    // path that brings to this node
    std::string toString() const; 
    
    // is orig inside? : 1 = in, 0 = out, -1 = degenerate case
    // inter is the relevant intersection 
    int isInsideMesh(const Vec3 &orig, int meshno, int verbose) const; 
    
    // set all bits of mesh position of point orig, except those related to ignore_meshX
    // return -1 if error
    int setMeshpos(const Vec3 &orig, BitVector &meshpos, int nmesh, 
                   int verbose,                   
                   int ignore_mesh0 = -1, int ignore_mesh1 = -1, int ignore_mesh2 = -1) const;
    
    // lazy evaluation of mesh position: compute bits one by one and
    // evaluate at each bit whether we are on surface. 
    int computeMeshposLazy(const Vec3 &orig, CSGOperation *csgop, 
                           int ignore_mesh0, int ignore_mesh1, int ignore_mesh2, int nmesh,
                           BitVector &meshpos, int verbose) const; 
    
    // decomposition of isInsideMesh used for lazy eval
    int isInsidePolygon(const Vec3 &orig, int i, int verbose) const; 
    int isInsidePolygons(const Vec3 &orig, int i, int j, int verbose) const;
    

  };  
    


};

#endif
