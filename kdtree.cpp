/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */

#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>


#include "mCSG.hpp"
#include "kdtree.hpp"

using namespace mCSG;


/**************************************************
 * Small functions + node implementation
 **/

static int argMax3(real v0, real v1, real v2) {
  if(v0 > v1) {
    if(v0 > v2) return 0;
    else return 2;
  } else {
    if(v1 > v2) return 1;
    else return 2;
  }
}

static int argMaxVec3(const Vec3 &v) {
  return argMax3(v[0], v[1], v[2]);
}

static Vec3 minVec3(const Vec3 & a, const Vec3 & b) {
  return Vec3(std::min(a.x, b.x), 
	      std::min(a.y, b.y), 
	      std::min(a.z, b.z));
}

static Vec3 maxVec3(const Vec3 & a, const Vec3 & b) {
  return Vec3(std::max(a.x, b.x), 
	      std::max(a.y, b.y), 
	      std::max(a.z, b.z)); 
}


Node::Node(Node *parent): 
  parent(parent), child1(NULL), child2(NULL), depth(parent ? parent->depth + 1 : 0), 
  state(0) {
  bbMin = Vec3::max(); 
  bbMax = -Vec3::max(); 
}  


void Node::updateBBox(const Vec3 &bbMin_in, const Vec3 &bbMax_in) {
  bbMin = minVec3(bbMin, bbMin_in); 
  bbMax = maxVec3(bbMax, bbMax_in); 
}
      



Node::~Node() {
  // delete child1; delete child2;         
  assert(polygons.empty()); // ~Node does not know how to dealloc these
}

std::string Node::toString() const {
  char buf[10000];
  const Node * node = this;  
  buf[0] = '<';
  buf[depth + 1] = '>';
  buf[depth + 2] = 0;
  for(int i = depth - 1; i >= 0; i--) {
    if(node == node->parent->child1) 
      buf[i + 1] = '1'; 
    else
      buf[i + 1] = '2';
    node = node->parent; 
  }    

  return std::string(buf);
}


/**************************************************
 * Convex polygon structure to express intersection of triangles with KDTree cells
 **/


ConvexPolygon::ConvexPolygon(int facetNo, const Facet &facet, const std::vector<Vertex> & verticesIn):
  facetNo(facetNo) {
  meshno = facet.meshno; 
  normal = facet.normal;
  b = facet.b;
  for(int i = 0; i < facet.vertexNos.size(); i++) {
    int vno = facet.vertexNos[i];
    int reported_vno = (facetNo == verticesIn[vno].facetNos[0]) ? vno : -1; // keep single instance of each vertex
    PolygonVertex pv = {verticesIn[vno].coords, reported_vno, facet.oppositeFacetNos[i]};
    vertices.push_back(pv); 
  }
    
  setBBox();
}

Vec3 ConvexPolygon::center() const {
  Vec3 accu = vertices[0].coords; 
  real w = 1.0; 
  for(int i = 1; i < vertices.size(); i++) {
    real wi = 1.0 + i * 1e-5;  // add some arbitrary weight to avoid exactly central point
    w += wi; 
    accu += vertices[i].coords * wi;
  }
  return accu / w;
}


void ConvexPolygon::setBBox() {
  bbMin = Vec3::max(); 
  bbMax = -bbMin;
  for(int i = 0; i < vertices.size(); i++) {
    const Vec3 &x = vertices[i].coords;
    bbMin = minVec3(bbMin, x); 
    bbMax = maxVec3(bbMax, x);
  }      
}

/**************************************************
 * intersection code
 **/



static Vec3 halfSpaceInter(int dim, real b,  const Vec3 &x0, const Vec3 &x1) {

  real a0 = x0[dim];
  real a1 = x1[dim];
  Vec3 x;
  if(a1 > a0) { // make sure that we get bit-exact same result with (x0, x1) and (x1, x0)
    real t = (b - a0) / (a1 - a0);
    x = (1 - t) * x0 + t * x1;
  } else {
    real t = (b - a1) / (a0 - a1);
    x = (1 - t) * x1 + t * x0;
  }
  x[dim] = b; // want this to be exact
  return x;
}



// half-space defined by  (x[dim] > b) == sense      
void ConvexPolygon::intersectWithHalfSpace(int dim, bool sense, real b) {
  
  int nvert = vertices.size(); 
  if(nvert < 3) return;      
  
  real prev_x = vertices[nvert - 1].coords[dim] - b;
  real xmin = HUGE_VAL, xmax = -HUGE_VAL;
  
  int first_in = -1, first_out = -1;
  
  for(int i = 0; i < nvert; i++) {
    real x = vertices[i].coords[dim] - b;
    if((x > 0) != (prev_x > 0)) {
      if((x > 0) == sense) first_in = i;
      else                 first_out = i;
    }
    prev_x = x; 
    if(x < xmin) xmin = x; 
    if(x > xmax) xmax = x; 
  }
  
  if(xmin >= 0) {
    if(sense)  ;            // all inside, nothing to do
    else vertices.clear();  // all outside
  } else if(xmax <= 0) {
    // reverse test
    if(sense) vertices.clear(); 
    else ; 
  } else {
    // compute intersections
    assert(first_in >= 0 && first_out >= 0); 
    ConvexPolygon::VertexTableType new_vertices; 

    if(first_out < first_in) first_out += nvert; 
    for(int i = first_in; i < first_out; i++) {
      new_vertices.push_back(vertices[i % nvert]); 
    }
    
    Vec3 e1 = halfSpaceInter(dim, b, 
                             vertices[first_out % nvert].coords, 
                             vertices[(first_out - 1) % nvert].coords);
    Vec3 e2 = halfSpaceInter(dim, b, 
                             vertices[(first_in - 1 + nvert)  % nvert].coords, 
                             vertices[first_in].coords);
    if(e1 != new_vertices.back().coords) {
      PolygonVertex pv = {e1, -1, vertices[first_out % nvert].borderFacetNo}; 
      new_vertices.push_back(pv); 
    }
    if(e2 != e1 && e2 != new_vertices.front().coords) {
      PolygonVertex pv = {e2, -1, -1}; 
      new_vertices.push_back(pv);
    }
    if(e2 == new_vertices.front().coords) 
      new_vertices.front().borderFacetNo = -1; 

    if(new_vertices.size() < 3) 
      vertices.clear();  // degenerate case, probably remove polygon
    else    
      vertices.swap(new_vertices); 

  }        
}                             




/**************************************************
 * Find on what side of a mesh is the face of a kdtree cell, assuming
 * the cell contains polygons of the mesh, but that they do not
 * intersect with the cell's face.
 **/

#ifndef EPS1

#ifdef USE_FLOATS
#define EPS1 1e-4
#define EPS2 1e-5
#define EPS3 1e-5
#else
#define EPS1 1e-8
#define EPS2 1e-8
#define EPS3 1e-8
#endif

#endif

// given the corner p0, p1, p2 of a polygon, compute its intersection
// with plane x[split_dim] = p1[split_dim] + 1
// if v0 is the intersection of [p0, p1] with the plane 
// v1 the intersection of [p2, p1] then s0 is the distance of P(p1) to v0 and s1 the slope of v0 to v1
static bool update_best_normal(const Vec3 &p0, const Vec3 &p1, const Vec3 &p2, int split_dim, int verbose,
                               real &best_s0, real &best_s1) {

  int d1 = (split_dim + 1) % 3, d2 = (split_dim + 2) % 3;

  bool keep = false;
  
  IFV(6)
    printf("      Central vertex (%.16g %.16g %.16g)\n", p1.x, p1.y, p1.z);
         
  Vec3 v0 = p0 - p1; 
  v0 /= v0[split_dim];
  Vec3 v2 = p2 - p1; 
  v2 /= v2[split_dim];
  
  for(int ss = 0; ss < 2 ; ss++) { // loop over the two sides of the corner
    
    real s0 = hypot(v0[d1], v0[d2]);
   
    IFV(6)
      printf("        v0 = (%.16g %.16g), v2 = (%.16g %.16g) s0 = %.16g\n", 
             v0[d1], v0[d2], v2[d1], v2[d2], s0);

    if(std::isnan(s0)) continue;
    
    if(s0 >= best_s0) {
      real vx = v0[d1], vy = v0[d2]; 
      real x2 = v2[d1] - v0[d1], y2 = v2[d2] - v0[d2];
      
      real u = - vx * x2 - vy * y2;
      real v =   vy * x2 - vx * y2;
      
      real s1 = fabs(u / v);

      if(std::isnan(s1)) s1 = HUGE_VAL; // a bad value
      
      IFV(6) printf("        s1 = %.16g\n", s1);
      
      if(s0 > best_s0) {
        best_s0 = s0; 
        best_s1 = s1; 
        keep = true;
      } else if(s1 < best_s1) {
        best_s1 = s1;
        keep = true;
      }       
    }
    std::swap(v0, v2);    
  }
  return keep;
}


// Computes whether the lower side of a bounding box (in one of the 3
// dimensions) is inside or outside of a mesh. Works in two passes:
// first pass checks all polygons to find the ones that have the
// lowest coordinate, and remembers whether their normal is positive
// or negative. Second pass: refine if there are both positives and
// negative normals.

struct MeshSideMin {
  int split_dim; // dimension we are interested in
  real eps; // tolerance
  int verbose;

  real min_x; 
  bool pos_normal, neg_normal; // wheter there is a positive or a negative normal
  VectorPreferredSize<6, int> polygon_nos; // at which polygon the normal was obtained

  MeshSideMin(int split_dim, real eps, int verbose): split_dim(split_dim), eps(eps), verbose(verbose) {
    min_x = HUGE_VAL; 
    pos_normal = false, neg_normal = false;    
  }

  void disable() {
    min_x = -HUGE_VAL;
  }
                                       
  void update(const ConvexPolygon * poly, int polygon_no) {
    if(poly->bbMin[split_dim] < min_x - eps) {
      min_x = poly->bbMin[split_dim];
      pos_normal = neg_normal = false; 
      polygon_nos.clear();
    }
    if(poly->bbMin[split_dim] < min_x + eps) {
      if(poly->normal[split_dim] > 0) pos_normal = true; 
      else neg_normal = true; 
      polygon_nos.push_back(polygon_no); 
      IFV(6)
        printf("       polygon %d min bbox %g, normal %g\n", polygon_no, poly->bbMin[split_dim], poly->normal[split_dim]);
    }    
  }  

  void join(const MeshSideMin & other, int shift) {
    if(other.min_x < min_x - eps) {
      min_x = other.min_x; 
      pos_normal = other.pos_normal; 
      neg_normal = other.neg_normal;
      polygon_nos.clear();
      for(int i = 0; i < other.polygon_nos.size(); i++) 
        polygon_nos.push_back(other.polygon_nos[i] + shift); 
    } else if(other.min_x < min_x + eps) {
      if(other.pos_normal) pos_normal = true; 
      if(other.neg_normal) neg_normal = true; 
      for(int i = 0; i < other.polygon_nos.size(); i++) 
        polygon_nos.push_back(other.polygon_nos[i] + shift); 
    }
  }

  template<class PolygonTable>
  int get_side(const PolygonTable &polygons) {

    if(!(pos_normal || neg_normal)) {
      IFV(3) printf("    MeshSideMin::get_side: ERROR no pos and neg normals!\n"); 
      return -1;
    }
    
    // simple case
    if(!(pos_normal && neg_normal)) {
      IFV(4) printf("    Return %d\n", pos_normal);  
      return pos_normal;
    }
    
    // complex case, we have to examine the edges           
    
    real x_thresh = min_x + eps;
    real best_s0 = 0, best_s1; 
    bool sense = false;  
    IFV(4) printf("    Complex case, threshold %g, examining %zd polygons\n", 
                  x_thresh, polygon_nos.size());

    for(int j = 0; j < polygon_nos.size(); j++) {
      ConvexPolygon *poly = polygons[polygon_nos[j]];
      if(!(poly->bbMin[split_dim] < x_thresh)) continue; 
    
      int nvert = poly->vertices.size();
      
      for(int i = 0; i < nvert; i++) {
        Vec3 p1 = poly->vertices[i].coords; 
        
        if(!(p1[split_dim] < x_thresh)) continue; // not extremal pt
        
        Vec3 p0 = poly->vertices[(i + nvert - 1) % nvert].coords; 
        Vec3 p2 = poly->vertices[(i + 1) % nvert].coords;
        
        IFV(6) printf("      Examining polygon %d (F%d) vertex %d normal %g\n", j, 
                      poly->facetNo, i, poly->normal[split_dim]);
        
        // examine the edges by corners
        bool keep = update_best_normal(p0, p1, p2, split_dim, verbose, 
                                       best_s0, best_s1);
        
        if(keep) {
          sense = poly->normal[split_dim] > 0;
          IFV(6) printf("          keep (best %.16g %.16g)\n", best_s0, best_s1);            
        }
      }    
    }    

    IFV(4)
      printf("    Return %d\n", sense);
    return sense;
  }
};


struct MeshSideMax {
  int split_dim;
  real eps; 
  int verbose; 

  real max_x; 
  bool pos_normal, neg_normal;
  VectorPreferredSize<6, int> polygon_nos; 

  MeshSideMax(int split_dim, real eps, int verbose): split_dim(split_dim), eps(eps), verbose(verbose) {
    max_x = -HUGE_VAL; 
    pos_normal = false, neg_normal = false;    
  }

  void disable() {
    max_x = HUGE_VAL;
  } 

  void update(const ConvexPolygon * poly, int polygon_no) {
    if(poly->bbMax[split_dim] > max_x + eps) {
      max_x = poly->bbMax[split_dim];
      pos_normal = neg_normal = false; 
      polygon_nos.clear();
    }
    if(poly->bbMax[split_dim] > max_x - eps) {
      if(poly->normal[split_dim] > 0) pos_normal = true; 
      else neg_normal = true; 
      polygon_nos.push_back(polygon_no); 
      IFV(6)
        printf("       polygon %d max bbox %g, normal %g\n", polygon_no, poly->bbMax[split_dim], poly->normal[split_dim]);
    }
  }  

  void join(const MeshSideMax & other, int shift) {
    if(other.max_x > max_x + eps) {
      max_x = other.max_x; 
      pos_normal = other.pos_normal; 
      neg_normal = other.neg_normal; 
      polygon_nos.clear();
      for(int i = 0; i < other.polygon_nos.size(); i++) 
        polygon_nos.push_back(other.polygon_nos[i] + shift); 
    } else if(other.max_x > max_x - eps) {
      if(other.pos_normal) pos_normal = true; 
      if(other.neg_normal) neg_normal = true; 
      for(int i = 0; i < other.polygon_nos.size(); i++) 
        polygon_nos.push_back(other.polygon_nos[i] + shift); 
    }
  }

  template<class PolygonTable>
  int get_side(const PolygonTable &polygons) {

    if(!(pos_normal || neg_normal)) {
      IFV(3) printf("    MeshSideMax::get_side: ERROR no pos and neg normals!\n"); 
      return -1;
    }
    
    // simple case
    if(!(pos_normal && neg_normal)) {
      IFV(4) printf("    Return %d\n", pos_normal);  
      return pos_normal;
    }
    
    // complex case, we have to examine the edges           
    
    real x_thresh = max_x - eps;
    real best_s0 = 0, best_s1; 
    bool sense = false;  
    IFV(4) printf("    Complex case, threshold %g examining %zd polygons\n", 
                  x_thresh, polygon_nos.size());

    for(int j = 0; j < polygon_nos.size(); j++) {
      ConvexPolygon *poly = polygons[polygon_nos[j]];
      if(!(poly->bbMax[split_dim] > x_thresh)) continue; 
    
      int nvert = poly->vertices.size();

      for(int i = 0; i < nvert; i++) {
        Vec3 p1 = poly->vertices[i].coords; 
        
        if(!(p1[split_dim] > x_thresh)) continue; // not extremal pt
        
        Vec3 p0 = poly->vertices[(i + nvert - 1) % nvert].coords; 
        Vec3 p2 = poly->vertices[(i + 1) % nvert].coords;
        
        IFV(6) printf("      Examining polygon %d (F%d) vertex %d normal %g\n", 
                      j, poly->facetNo, i, poly->normal[split_dim]);
        
        bool keep = update_best_normal(p0, p1, p2, split_dim, verbose,
                                       best_s0, best_s1);
        
        if(keep) {
          sense = poly->normal[split_dim] > 0;
          IFV(6) printf("          keep (best %.16g %.16g)\n", best_s0, best_s1);            
        }
      }    
    }    
    
    IFV(4)
      printf("    Return %d\n", sense);
    return sense;
  }
};



// get side of mesh on lower side of cell in dimension split_dim
// consider polygons in the range j0:j1
static int getMeshSideMin(const Node::PolygonTable &polygons, int j0, int j1, int split_dim, real eps, int verbose) {

  IFV(4) printf("    finding sense of polygons[%d:%d], in dimension %d (lower side) eps=%g\n",
                j0, j1, split_dim, eps);

  MeshSideMin ms(split_dim, eps, verbose); 
 
  for(int j = j0; j < j1; j++) {
    const ConvexPolygon *poly = polygons[j];
    ms.update(poly, j); 
  }

  return ms.get_side(polygons); 
}



// get side of mesh on lower side of cell in dimension split_dim
static int getMeshSideMax(const Node::PolygonTable &polygons, int j0, int j1, int split_dim, real eps, int verbose) {

  IFV(4)
    printf("    finding sense of polygons[%d:%d], in dimension %d (upper side), eps = %g\n",
           j0, j1, split_dim, eps);

  MeshSideMax ms(split_dim, eps, verbose); 
 
  for(int j = j0; j < j1; j++) {
    const ConvexPolygon *poly = polygons[j];
    ms.update(poly, j); 
  }

  return ms.get_side(polygons); 
}





















/**************************************************
 * Splitting: deciding on the split plane
 ***************************************************/


#define PERM(i) f[i]
#define SWAPFLOAT(i,j) {real tmp = f[i]; f[i]=f[j]; f[j]=tmp; }

// find quantile in O(n)
static void qselect (real *f, int i0, int i1, int q) {
  real pivot = PERM(i0);
  int j0, j1, lim;
  assert(i1 - i0 > 1 && q > i0 && q < i1);

  for(j0 = i0, j1 = i1 ; 1 ; ) {
    while(j0 < j1 - 1) {
      j0++;
      if(PERM(j0) > pivot)
        goto endseginf;
    }
    lim = j1;
    break;
  endseginf:
    while(j1 - 1 > j0) {
      j1--;
      if(PERM(j1) <= pivot)
        goto endsegsup;
    }
    lim = j0;
    break;
  endsegsup:
    SWAPFLOAT(j0, j1);
  }
  assert (lim > i0);
  if(lim == i1) {
    SWAPFLOAT(i0, i1 - 1);
    lim = i1 - 1;
  }
  if(lim == q)
    return;                     /* mission accomplished */
  if(q < lim)
    qselect(f, i0, lim, q);
  else
    qselect(f, lim, i1, q);
}

#undef PERM
#undef SWAPFLOAT

bool vec3Larger(Vec3 a, Vec3 b) {
  return a.x > b.x && a.y > b.y && a.z > b.z;
}


void Node::split(CSG & kdtree) {
  int max_in_leaf = kdtree.max_in_leaf; 
  real split_threshold = kdtree.split_threshold;
  real min_volume = kdtree.min_volume; 
  int verbose = kdtree.verbose;

  assert(state == 0);

  IFV(3) {
    printf("split %s from %zd polygons\n", toString().c_str(), polygons.size()); 
  }

  IFV(4) {
    printf("  ids(sizes) ["); 
    for(int i = 0; i < polygons.size(); i++) 
      printf("%d(%zd) ", polygons[i]->facetNo, polygons[i]->vertices.size()); 
    printf("]\n");
  }       
      
  // compute bounding box of triangle vertices 
  const Vec3 &mins = bbMin; 
  const Vec3 &maxs = bbMax; 
  
  Vec3 sz = maxs - mins;

  real vol = sz.x * sz.y * sz.z;
      
  IFV(3)
    printf("  bbox (%g %g %g) (%g %g %g), vol %g\n",
           mins[0], mins[1], mins[2], 
           maxs[0], maxs[1], maxs[2], vol);

  if(polygons.size() < max_in_leaf || vol < min_volume) {
    // don't subdivide
    state = 2;
    IFV(3) printf("  Leaf node, state = 2\n"); 
    return;
  }

  // choose dimension with largest variance
  int split_dim = argMaxVec3(sz);
  real xmin = bbMin[split_dim], xmax = bbMax[split_dim]; 

  real threshold; 

  
  if(split_threshold == 0.5)  {
    // just split in half
    threshold = 0.5 * xmin + 0.5 * xmax;

  } else {
    
    // choose threshold as median of points
    
    std::vector<real> xes; 
    for(int i = 0; i < polygons.size(); i++) {
      ConvexPolygon *poly = polygons[i];
      for(int j = 0; j < poly->vertices.size(); j++) {
        real x = poly->vertices[j].coords[split_dim];
        xes.push_back(x); 
      }
    }
    
    qselect(&xes[0], 0, xes.size(), xes.size() / 2);
      
    // split in median of points
    // double xmin = xes[0], xmax = xes[xes.size() - 1]; 
    threshold = xes[xes.size() / 2]; 
    
    real relative_thr = (threshold - xmin) / (xmax - xmin); 
    if(relative_thr < split_threshold) {
      threshold = xmin + (xmax - xmin) * split_threshold; 
      } else if(relative_thr > 1 - split_threshold) {
      // size of rectangle does not decrease enough, so split in halves
      threshold = xmin + (xmax - xmin) * (1 - split_threshold); 
    }
  } 

  IFV(3) {
    real vol1 = vol * (threshold - mins[split_dim]) / (maxs[split_dim] - mins[split_dim]);
    real vol2 = vol * (maxs[split_dim] - threshold) / (maxs[split_dim] - mins[split_dim]);
    printf("  split at dim %d threshold %g (volume split %.1f %% + %.1f %%)\n", 
           split_dim, threshold, 
           100 * vol1 / (vol1 + vol2),
           100 * vol2 / (vol1 + vol2));
  }

  split(kdtree, split_dim, threshold);

}




/**************************************************
 * Splitting: the actual splitting
 ***************************************************/

// split polygon poly into poly and poly2 
static void split2Polygons(ConvexPolygon * &poly, 
                           ConvexPolygon * &poly2, 
                           int split_dim, real threshold, 
                           MemoryPool &polygonPool) {
  
  if(poly->bbMax[split_dim] <= threshold) {      
    // default
  } else if(poly->bbMin[split_dim] >= threshold) {
    poly2 = poly; poly = NULL; 
  } else { // appears on both nodes
    poly2 = new(polygonPool.alloc()) ConvexPolygon(*poly); // copy constructor
    
    poly->intersectWithHalfSpace(split_dim, false, threshold);
    poly2->intersectWithHalfSpace(split_dim, true, threshold); 
    
    if(poly->empty()) {polygonPool.dealloc(poly); poly = NULL; } else poly->setBBox(); 
    if(poly2->empty()) {polygonPool.dealloc(poly2); poly2 = NULL; } else poly2->setBBox();
  }
  
}


// return 1 + last polygon processed
static int split_mesh_polygons(Node &node, 
                               int i0,  // start at this polygon
                               CSG &csg) {
  int verbose = csg.verbose;
  Node & child1 = *node.child1; 
  Node & child2 = *node.child2; 
  int split_dim = node.split_dim;
  real threshold = node.threshold;
  
  int meshno = node.polygons[i0]->meshno; 

  assert(!node.meshpos.isKnown(meshno));
  int child1_p0 = child1.polygons.size(); 
  int child2_p0 = child2.polygons.size(); 
  
  int i = i0; 
  do {
    ConvexPolygon *poly = node.polygons[i];
    ConvexPolygon *poly2 = NULL; 
    
    int facetNo = poly->facetNo; 
    int meshno = poly->meshno; 

    split2Polygons(poly, poly2, split_dim, threshold, csg.polygonPool);     
    
    IFV(6) printf("      polygon %d (F%d, mesh %d) split in %zd + %zd vertices\n", 
                  i, facetNo, meshno, poly ? poly->vertices.size() : 0, poly2 ? poly2->vertices.size() : 0); 
    if(poly) {
      child1.polygons.push_back(poly);
      child1.updateBBox(poly->bbMin, poly->bbMax); 
    }
    if(poly2) {
      child2.polygons.push_back(poly2);       
      child2.updateBBox(poly2->bbMin, poly2->bbMax); 
    }

    i++; 
  } while (i < node.polygons.size() && node.polygons[i]->meshno == meshno);
  
  // determine mesh side of children if needed

  bool is_in_child1 = child1.polygons.size() > child1_p0; 
  bool is_in_child2 = child2.polygons.size() > child2_p0; 

  assert(is_in_child1 || is_in_child2); 

  real eps = (node.bbMax[split_dim] - node.bbMin[split_dim]) * 1e-8;
  
  if(!is_in_child1 && is_in_child2) {
    int side = getMeshSideMin(child2.polygons, child2_p0, child2.polygons.size(), split_dim, eps, verbose); 
    if(side == -1) {
      csg.errors.add("cannot determine side with getMeshSideMin"); 
      side = 0; // arbitrary
    }
    child1.meshpos.setSide(meshno, side); 
  } else if(!is_in_child2 && is_in_child1) {
    int side = getMeshSideMax(child1.polygons, child1_p0, child1.polygons.size(), split_dim, eps, verbose);
    if(side == -1) {
      csg.errors.add("cannot determine side with getMeshSideMax"); 
      side = 0; // arbitrary 
    }
    child2.meshpos.setSide(meshno, !side); 
  } // else both children have facets of meshno. 
  
  return i; 
}

static void split_mesh_polygons_parallel(Node &node, CSG & csg);


// perform the actual split

void Node::split(CSG & csg, int split_dim, real threshold) {
  int verbose = csg.verbose;
  
  this->split_dim = split_dim; 
  this->threshold = threshold; 

  assert(!child1 && !child2 && state == 0);

  child1 = new(csg.nodePool.alloc()) Node(this); 
  child2 = new(csg.nodePool.alloc()) Node(this);   
  
  child1->meshpos = child2->meshpos = meshpos; 

#ifdef ENABLE_TBB
  if(csg.parallel_nt != 1 && 
     depth < csg.parallel_split_depth && 
     polygons.size() > csg.parallel_split_npoly) {

    split_mesh_polygons_parallel(*this, csg);

  }
  else 
#endif
  {
    int i = 0; // polygon index

    while(i < polygons.size()) {       
      i = split_mesh_polygons(*this, i, csg); 
    }  

  }
  
  child1->n_polygon = child1->polygons.size(); 
  child2->n_polygon = child2->polygons.size(); 

  if(depth < csg.tight_bbox_depth) {
    child1->bbMin = child2->bbMin = bbMin; 
    child1->bbMax = child2->bbMax = bbMax; 
    child1->bbMax[split_dim] = threshold; 
    child2->bbMin[split_dim] = threshold; 
  }

  polygons.clear(); // clear parent's polygons

  state = 1;

  IFV(3) 
    printf("  size = %zd %s + %zd %s\n", 
           child1->polygons.size(), child1->toString().c_str(), 
           child2->polygons.size(), child2->toString().c_str()); 
}


























/********************************************************************************
 * bbox cropping
 ********************************************************************************/


bool ConvexPolygon::cropToBBox(const Vec3 & cbbMin, const Vec3 & cbbMax) {


  // poly completly inside bbox: nothing to do
  if(vec3Larger(bbMin, cbbMin) && vec3Larger(cbbMax, bbMax)) 
    return true;

  
  // completely outside: delete
  if(!vec3Larger(cbbMax, bbMin) || !vec3Larger(bbMax, cbbMin)) 
    return false; 
  
  // we have to clip      
  for(int dim = 0; dim < 3; dim++) {
    intersectWithHalfSpace(dim, true, cbbMin[dim]);
    if(empty()) return false;
    intersectWithHalfSpace(dim, false, cbbMax[dim]);
    if(empty()) return false;
  }
  
  setBBox();

  return true; 
}


// crop polygons[i0:i1] to cbbMin-cbbMax
static int cropPolygonsToBBox(Node::PolygonTable &polygons, int i0, int i1, 
                              const Vec3 &cbbMin, const Vec3 &cbbMax, 
			      CSG & csg) {

  int verbose = csg.verbose;

  IFV(4) printf("    Node::cropPolygons: cropping polygons %d:%d/%zd to bbox (%g %g %g)-(%g %g %g)\n", 
                i0, i1, polygons.size(), cbbMin.x, cbbMin.y, cbbMin.z, cbbMax.x, cbbMax.y, cbbMax.z); 
  
  int n_delete = 0; 
 
  for(int i = i0; i < i1; i++) {
    ConvexPolygon * poly = polygons[i];

    if(!poly->cropToBBox(cbbMin, cbbMax)) {
      // poly outside bbox: remove
      csg.polygonPool.dealloc(poly); 
      polygons[i] = NULL;       
      n_delete++; 
    }      
    
  } 
  IFV(4) printf("      removed %d/%d polygons\n", n_delete, i1 - i0);
  return n_delete;
}


void CSG::cropRootToBBox(const Vec3 &bbMin, const Vec3 &bbMax) {

#ifdef ENABLE_TBB
  if(parallel_nt != 1) {
    auto callback = 
      [&](const tbb::blocked_range<int>&r) {
      cropPolygonsToBBox(root->polygons, r.begin(), r.end(), bbMin, bbMax, *this);       
    };    
    tbb::parallel_for(tbb::blocked_range<int>(0, root->polygons.size(), 16), callback); 
  } else 
#endif
    cropPolygonsToBBox(root->polygons, 0, root->polygons.size(), bbMin, bbMax, *this); 
  
  // a pass to remove the NULLs and check if there is at least a polygon remaining for each mesh
  BitVector bv = root->meshpos; 
  
  bv.setAllKnown(nmesh); 

  int wp = 0; 
  for(int i = 0; i < root->polygons.size(); i++) {
    ConvexPolygon *poly = root->polygons[i]; 
    if(poly) {
      root->polygons[wp] = poly;
      bv.setUnknown(poly->meshno); 
      wp++; 
    }
  }
  root->polygons.resize(wp, NULL); 
  
  if(!bv.cmpKnown(root->meshpos)) 
    errors.add("after cropping to bbox, some meshes are lost");

  root->bbMin = bbMin; 
  root->bbMax = bbMax;
  
}







static real volVec3(Vec3 dim) {
  return dim.x * dim.y * dim.z;
}



void CSG::initKDTree() {

  root = new(nodePool.alloc()) Node(NULL);
  root->depth = 0;

  root->meshpos.setAllKnown(nmesh); // all known, all outside

  root->bbMin = Vec3::max(); 
  root->bbMax = -Vec3::max(); 
  
  for(int i = 0; i < facets.size(); i++) {
    ConvexPolygon *poly = new(polygonPool.alloc()) ConvexPolygon(i, facets[i], vertices);
    root->polygons.push_back(poly); 
    root->bbMin = minVec3(root->bbMin, poly->bbMin); 
    root->bbMax = maxVec3(root->bbMax, poly->bbMax); 
    root->meshpos.setUnknown(facets[i].meshno);    
  }
  root->n_polygon = facets.size();

  min_volume = min_volume_factor * volVec3(root->bbMax - root->bbMin);   
}


static int rec_delete_tree(Node *node, MemoryPool & pool) {
  if(!node) return 0;
  int n = 1; 
  n += rec_delete_tree(node->child1, pool); 
  n += rec_delete_tree(node->child2, pool); 
  node->~Node();
  pool.dealloc(node); 
  return n; 
}

void CSG::deleteTree() {
  if(alloc_policy < 3) {
    // Here we can just deallocate the mem pool because the node
    // destructor does not do anything useful: the polygon table is the
    // only dynamic field and is assumed to be empty.
    int n = rec_delete_tree(root, nodePool); 
    IFV(6) printf("deleted %d tree nodes\n", n); 
  }
  nodePool.clear();
  root = NULL;  
}





Node * CSG::expandForIntersection() {
  Node *node = root; 
  IFV(0)
    printf("expandForIntersection: making 6 levels of kdtree around a bounding box of the result\n");
  for(int dim = 0; dim < 3; dim++) {
       
	std::vector<real> mins, maxs; // offset at which vertices of the polygon are first seen
      
    for(int i = 0; i < nmesh; i++) {
      mins.push_back(HUGE_VAL); 
      maxs.push_back(-HUGE_VAL); 
    }
    
    for(int i = 0; i < node->polygons.size(); i++) {
      const ConvexPolygon * poly = node->polygons[i]; 
      int meshno = poly->meshno; 
      for(int j = 0; j < poly->vertices.size(); j++) {
        real x = poly->vertices[j].coords[dim];  
        if(x < mins[meshno]) mins[meshno] = x; 
        if(x > maxs[meshno]) maxs[meshno] = x; 
      }
    }

    // range in which all meshes are present      
    real xmin = mins[0], xmax = maxs[0]; 
    for(int i = 1; i < nmesh; i++) {
      if(mins[i] > xmin) xmin = mins[i]; 
      if(maxs[i] < xmax) xmax = maxs[i];
    }

    if(verbose > 0) {
      printf("  In dimension %d: range %g to %g\n", dim, xmin, xmax);       
    }

    node->processingStats.begin();
    node->split(*this, dim, xmin);     
    node = node->child2;     
    node->processingStats.end();

    node->processingStats.begin();
    node->split(*this, dim, xmax);     
    node = node->child1;    
    node->processingStats.end();

  }
  return node; 
}

/********************************************************
 * Statistics, debugging and visualization
 ********************************************************/





struct KDTreeStats {
  struct PolygonStats {
    int n; 
    int n_snippet; // polygons that have no edge coming from an input facet    
    int max_nvertex; 
    int sum_nvertex; 
    PolygonStats() { memset(this, 0, sizeof(*this)); }
    void update(const ConvexPolygon * poly) {
      n++; 
      if(poly->vertices.size() > max_nvertex) max_nvertex = poly->vertices.size(); 
      sum_nvertex += poly->vertices.size(); 
      bool has_edge = false; 
      for(int i = 0; i < poly->vertices.size(); i++) 
        if(poly->vertices[i].borderFacetNo >= 0) has_edge = true; 
      if(!has_edge) n_snippet++; 
    }
  }; 
  PolygonStats polygonStats; 

  struct PerDepthStats {
    int n_node, n_explored, n_split; 
    long sum_split_cy, sum_explore_cy; 
    int n_split_half; 
    PerDepthStats() {
      memset(this, 0, sizeof(*this)); 
    }
  };
  std::vector<PerDepthStats> perDepthStats;

  std::vector<int> nmeshHist; 
  std::vector<int> nmeshLeafHist; 
  bool incomplete;

  int n_node, n_explored, n_ignored;
  KDTreeStats() {
    n_node = n_explored = n_ignored = incomplete = 0;
  }
};

static void collectStats(Node *node, KDTreeStats & stats) {
  stats.n_node ++; 
  if(!node) {
    stats.incomplete = true;
    return; // will make incomplete stats!
  }

  while(node->depth >= stats.perDepthStats.size()) {
    stats.perDepthStats.push_back(KDTreeStats::PerDepthStats());
  }
  KDTreeStats::PerDepthStats &ds  = stats.perDepthStats[node->depth]; 
  ds.n_node ++; 

  stats.nmeshHist[node->meshpos.nbKnown()]++;

  if(node->state == 2) {
    stats.n_explored++; 
    ds.sum_explore_cy += node->processingStats.end_cy - node->processingStats.begin_cy; 
    stats.nmeshLeafHist[node->meshpos.nbKnown()]++;
    ds.n_explored++; 
    for(int i = 0; i < node->polygons.size(); i++) 
      stats.polygonStats.update(node->polygons[i]);
  } else if(node->state == 3) {
    stats.n_ignored++; 
  } else if(node->state == 1) {

    ds.n_split++; 
    ds.sum_split_cy += node->processingStats.end_cy - node->processingStats.begin_cy; 
    real xmin = node->bbMin[node->split_dim]; 
    real xmax = node->bbMax[node->split_dim]; 
    if(fabs(node->threshold - (xmin + xmax) / 2) < (xmax - xmin) * 1e-6) 
      ds.n_split_half++; 
    
    collectStats(node->child1, stats);
    collectStats(node->child2, stats);
  }
  
}


void CSG::printKDTreeStats() {
  KDTreeStats stats; 
  stats.nmeshHist.resize(nmesh + 1, 0); 
  stats.nmeshLeafHist.resize(nmesh + 1, 0); 

  collectStats(root, stats);

  printf("KDTree stats: %d nodes, %d leaves, %d ignored nodes %s\n", 
	 stats.n_node, stats.n_explored, stats.n_ignored, stats.incomplete ? "INCOMPLETE" : ""); 

  printf("  Mesh histogram: ["); 
  for(int i = 0; i <= nmesh; i++) 
    printf("%d:%d ", i, stats.nmeshHist[nmesh - i]); 
  printf("]\n");
  printf("  Mesh histogram for leaves: ["); 
  for(int i = 0; i <= nmesh; i++) 
    printf("%d:%d ", i, stats.nmeshLeafHist[nmesh - i]); 
  printf("]\n");
  printf("Statistics per depth:\n"); 
  for(int i = 0; i < stats.perDepthStats.size(); i++) {
    KDTreeStats::PerDepthStats &ds  = stats.perDepthStats[i]; 
    printf("   depth %2d: %5d nodes, %5d explored (%7.1f Mcy), %5d split (%7.1f Mcy)\n", 
           i, ds.n_node, ds.n_explored, ds.sum_explore_cy * 1e-6,
	   ds.n_split, ds.sum_split_cy * 1e-6);

  }
  KDTreeStats::PolygonStats &ps = stats.polygonStats; 
  printf("Polygon stats: %d polygons (avg %.2f / leaf), %d snippets, avg %.2f/max %d vertices\n", 
         ps.n, ps.n / float(stats.n_explored),
         ps.n_snippet, 
         ps.sum_nvertex / float(ps.n), ps.max_nvertex);
  
}

static void log_stats_rec(const Node *node, FILE *f, int format, int nmesh) {
  if(!node) {
    fprintf(f, "MISSING_NODE\n"); 
    return;
  }
  if(format == 1) {
    // format: node_name state n_poly begin_cy end_cy thread_id
    fprintf(f, "%s %d %zd %ld %ld %ld %d\n", 
            node->toString().c_str(), node->state, node->polygons.size(), 
            node->processingStats.begin_cy, node->processingStats.end_cy, node->processingStats.thread_id, 
            node->n_polygon); 
  } else if(format == 2) {
    fprintf(f, "%s %d %s %d\n", 
            node->toString().c_str(), node->n_polygon, node->meshpos.toString(nmesh).c_str(), node->state); 
  }
  if(node->state == 1) {
    log_stats_rec(node->child1, f, format, nmesh); 
    log_stats_rec(node->child2, f, format, nmesh); 
  }

}


static void calibrate_per_worker_offsets(FILE *f); 



void CSG::logKDTreeStats(const char * output_file_name, int log_format) {
  double t0 = getmillisecs(); 
  long cy0 = getcycles();

  FILE * f = fopen(output_file_name, "w"); 
  assert(f); 
  
  log_stats_rec(root, f, log_format, nmesh);

  double t1 = getmillisecs(); 
  long cy1 = getcycles();

  if(log_format == 1) {
    fprintf(f, "freq %g MHz\n", (cy1 - cy0) / (t1 - t0) * 1e-3);
    calibrate_per_worker_offsets(f);
  }
  /*  
  printf("on %.3f ms -> %g Mcyles / s\n", 
	 t1 - t0, (cy1 - cy0) / (t1 - t0) * 1e-3);
  */
  fclose(f); 
}

static float node_color_map[][3] = {
  {0.5, 1, 1},  // cyan
  {1, 1, 0.5},  // yellow
  {1, 0.5, 1},  // magenta
  {1, 0.5, 0.5} // redish
};

static int count_kdtree_nodes(const Node * node, int variant) {
  if(!node) return 0;
  bool isleaf = !node->child1 && !node->child2;
  if(isleaf) return 1;
  int count = count_kdtree_nodes(node->child1, variant) + count_kdtree_nodes(node->child2, variant);
  if(variant == 1) count ++;
  return count;
}
static int box_faces[6 * 4] = {
  0,1,3,2, 
  0,4,5,1,
  0,2,6,4,
  3,7,6,2,
  5,4,6,7,
  3,1,5,7
};

static void store_kdtree_nv(FILE *f, const Node * node, int variant, bool vertices, int &n) {
  if(!node) return;
  bool isleaf = !node->child1 && !node->child2;
  if(isleaf || variant == 1) {
    if(vertices) {
      for(int i = 0; i < 8; i++) 
	fprintf(f, "%g %g %g\n", 
		i & 1 ? node->bbMax[0] : node->bbMin[0], 
		i & 2 ? node->bbMax[1] : node->bbMin[1], 
		i & 4 ? node->bbMax[2] : node->bbMin[2]); 
    } else {
      const float *color = node_color_map[node->state];
      const int *fa = box_faces;
      for(int i = 0; i < 6; i++) {
	fprintf(f, "4 %d %d %d %d %g %g %g\n", 
		fa[3] + n, fa[2] + n, fa[1] + n, fa[0] + n, 
		color[0] * 0.999, color[1], color[2]); 
	fa += 4;
      }
      n += 8;
    }
  }    
  store_kdtree_nv(f, node->child1, variant, vertices, n); 
  store_kdtree_nv(f, node->child2, variant, vertices, n); 

}



void CSG::kdtreeToOff(const char * output_file_name, int variant) {

  int nnode = count_kdtree_nodes(root, variant); 
  
  FILE * f = fopen(output_file_name, "w"); 
  assert(f); 
  fprintf(f, "OFF\n%d %d 0\n", nnode * 8, nnode * 6); 

  int n = 0;
  store_kdtree_nv(f, root, variant, true, n); 

  n = 0;
  store_kdtree_nv(f, root, variant, false, n); 

  fclose(f); 
}

static float colormap [6][3] = {
    {1, 0.5, 0.5}, 
    {0.5, 1, 0.5}, 
    {0.5, 0.5, 1}, 
    {0.5, 1, 1}, 
    {1, 0.5, 1}, 
    {1, 1, 0.5}, 
};


//output node to .off file
void Node::toOff(const char *fname, int meshno) const {
  FILE *f = fopen(fname,"w");
  assert(f);
  
  int i0 = -1, i1 = -1; 
  int nv=0;
  for (int i=0; i < polygons.size(); i++){
    const ConvexPolygon *poly = polygons[i];
    if(meshno < 0 || poly->meshno == meshno) {
      if(i0 == -1) i0 = i;
      i1 = i;
      nv += poly->vertices.size();
    }
  }
  if(i1 != -1) i1++;

  //pritf("nv in node %s is %d \n", toString().c_str(), nv );

  fprintf(f, "OFF\n %d %d %d\n", nv, i1 - i0, 0);
  for(int i = i0; i < i1; i++){
    for(int j=0; j < polygons[i]->vertices.size(); j++){
      const PolygonVertex v = polygons[i]->vertices[j];
      fprintf(f, "%g %g %g\n",v.coords.x,v.coords.y,v.coords.z);
    }
  }

  int x_prev = 0, x=0;
  for(int i = i0; i < i1; i++){
    x += polygons[i]->vertices.size();
    fprintf(f, "%d ", (int)polygons[i]->vertices.size() );
    while(x_prev<x){
      fprintf(f,"%d ", x_prev++);
    }
    float * color = colormap[polygons[i]->meshno % 6];
    fprintf(f, "%g %g %g 1\n", color[0] * 0.9999, color[1], color[2]);

  }
  fclose(f);
}



#include "kdtree_parallel.cpp"
