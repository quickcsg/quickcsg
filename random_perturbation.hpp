/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */
#ifndef RANDOM_PERTURBATION_INCLUDED
#define RANDOM_PERTURBATION_INCLUDED


#include "mCSG.hpp"

namespace mCSG {


  struct RandomPerturbation {
    bool do_rr;  // whether a random rotation is needed
    real rt;     // magnitude of the per-mesh translation
    real eps_merge;  // merge vertices that are closer than this (0 = do not try to merge)
    int random_seed;
    
    Vec3 rmat[3]; // random rotation
    Vec3 tmat[3]; // transposed version
    std::vector<Vec3> translations; // random translations (per mesh)
    
    RandomPerturbation(): do_rr(false), rt(0), eps_merge(1e-8), random_seed(0) {}

    // should be applied before csg.afterLoad    
    void apply(CSG & csg); 


    static const int REVERT_ROTATION;     // revert random rotation
    static const int REVERT_TRANSLATION;  // revert per-mesh translation
    static const int MERGE_AFTER_REVERT;  // merge reverted vertices
    
    // after toFacets(). 
    void revert(CSG & csg, int revert_mask = REVERT_ROTATION | REVERT_TRANSLATION | MERGE_AFTER_REVERT); 
    
    bool isId() const {
      return rt == 0 && !do_rr;
    }

    // called by revert
    void revertTrTriple(Vertex & v, CSG &csg); 
    void revertTrAndRrDouble(Vertex & v, CSG & csg, bool revert_rotation); 
    

  };

};

#endif
