set -x
set -e

# version 5696

ref=../mkCSG_ref/mesh_csg


# testopt="-kdtree.bands 555"
testopt=""

function compare () {
    no=$1
    shift
    (cd data/nonconvex ; bash mk_nonconvex.bash $no)


    # generate ref
    # $ref "$@" -O data/nonconvex/out_ref.off -shrink -startKDTreeOnSurface -trimesh  data/nonconvex/ref.trimesh -combined data/nonconvex/combined.off 
    $ref "$@"  -O data/nonconvex/out_new.off -trimesh data/nonconvex/ref_$no.trimesh

    # new
    ./mesh_csg  "$@" -O data/nonconvex/out_new.off -vertices data/nonconvex/new.verts -trimesh data/nonconvex/new.trimesh $testopt

    # compare vertices
    # python cmp_vertlist_trimesh.py  data/nonconvex/ref.trimesh  data/nonconvex/new.verts

    # compare meshes 
    python cmp_trimesh.py  data/nonconvex/ref_$no.trimesh  data/nonconvex/new.trimesh

}



compare 1 data/nonconvex/box{0,1}.off -diff 1

# compare 2 data/nonconvex/{L,b0,b1}.off -diff 1 

# compare 3 data/nonconvex/{L,b0,b1}.off -diff 1 -kdtree.max_in_leaf 50

# here ref is actually wrong...
# compare 4 data/nonconvex/{ib0,ib1,b2}.off -diff 1 

compare 5 data/nonconvex/to{0..2}.off -diff 2 

compare 6 data/nonconvex/to{0..9}.off -diff 5

compare 7 data/nonconvex/to{0..9}.off -diff 5

compare 8 data/nonconvex/flower{0..39}.off -min2



# compare on knots42

# ropt="-random.translation 0.5 -random.rotation"
ropt=""

$ref  -vertices data/knots42/new.verts data/knots42/cones{0..41}.off -inter $ropt -trimesh  data/knots42/ref.trimesh 

# new
./mesh_csg  -vertices data/knots42/new.verts data/knots42/cones{0..41}.off -inter $ropt -trimesh  data/knots42/new.trimesh  $testopt

# compare vertices
# python cmp_vertlist_trimesh.py  data/knots42/ref.trimesh  data/knots42/new.verts

# compare meshes 
python cmp_trimesh.py  data/knots42/ref.trimesh  data/knots42/new.trimesh


$ref -contours data/capture_kinovis/CONTOURS/contour-00426.oc data/capture_kinovis/68cam.calib  -o /tmp/kino_ref.off -v 1111111 -cropbbox -10,-10,-10,10,10,10  -contourdepth 40 -contourrange 0,60 -trimesh data/capture_kinovis/kino_ref.trimesh


./mesh_csg -contours data/capture_kinovis/CONTOURS/contour-00426.oc data/capture_kinovis/68cam.calib -o /tmp/out_new.off -v 1111111 -cropbbox -10,-10,-10,10,10,10  -contourdepth 40 -contourrange 0,60 -trimesh data/capture_kinovis/kino_new.trimesh $testopt


python cmp_trimesh.py data/capture_kinovis/kino_ref.trimesh data/capture_kinovis/kino_new.trimesh





