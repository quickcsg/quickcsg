/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */
/** @file PolygonTesselator.h
 *
 * Very simple polygon tesselator class, current implementation based on glu
 * 1.2.
 *
 * @date 2003/02/17
 * @author Jean-Sebastien Franco
 */

#ifndef _PolygonTesselator_h_
#define _PolygonTesselator_h_

// for Vec3
#include "mCSG.hpp"
#include "VectorPreferredSize.hpp"



extern "C" {
#include <GL/glu.h>
}


typedef void(*VoidCallback)(void);

class PolygonTesselator
{

  /// override this method for emitted polygons
  virtual void emitPolygon(int n, const int *idx) = 0;  

  /// also need random access to the vertex corresponding to an index
  /// (not used for triangles)
  virtual mCSG::Vec3 getCoords(int i) const { // dummy implementation
    return mCSG::Vec3();
  }


public:

  /// constructor
  PolygonTesselator(bool just_triangles) : just_triangles(just_triangles), m_pTess(gluNewTess()) {
    assert(m_pTess);
    n_vertex = 0;
    gluTessCallback(m_pTess, GLU_TESS_BEGIN_DATA,  (VoidCallback)tessBegin_callback);
    gluTessCallback(m_pTess, GLU_TESS_VERTEX_DATA, (VoidCallback)tessVertex_callback);
    gluTessCallback(m_pTess, GLU_TESS_END_DATA, (VoidCallback)tessEnd_callback);
  }

  /// called by user

  void beginPolygon() { gluTessBeginPolygon(m_pTess, this); }
  void endPolygon() { gluTessEndPolygon(m_pTess); }
  void beginContour() { gluTessBeginContour(m_pTess); }
  void endContour() { gluTessEndContour(m_pTess); }
  void tessNormal(double x, double y, double z) { gluTessNormal(m_pTess, x, y, z); }

  void vertex(int idx, const mCSG::Vec3& p) {
    GLdouble v[3]; v[0]=p[0]; v[1]=p[1]; v[2]=p[2];    
    void *dum2 = (void *) (long)(idx);
    gluTessVertex(m_pTess, v, dum2);
  }

  
  void vertex(int idx) {
    vertex(idx, getCoords(idx));
  }
  
  virtual ~PolygonTesselator() {
    gluDeleteTess(m_pTess);
  }
    
protected:

  /// GLU callback for tessBegin
  static GLvoid tessBegin_callback(GLenum mode, GLvoid *poly_data) {
    static_cast<PolygonTesselator*>(poly_data)->tessBegin(mode); 
  }
  
  /// GLU callback for tessEnd
  static GLvoid tessEnd_callback(GLvoid *poly_data) {
    static_cast<PolygonTesselator*>(poly_data)->tessEnd(); 
  }
  
  /// GLU callback for tessVertex
  static GLvoid  tessVertex_callback(GLvoid *vert_data, GLvoid *poly_data) {
    static_cast<PolygonTesselator*>(poly_data)->tessVertex((long)vert_data); 
  }

  // do we output convex polygons or only triangles?
  bool just_triangles; 
   
  /// GLU tesselator object
  GLUtesselator* m_pTess;

  /// store triangle building mode (GL_TRIANGLES, GL_TRIANGLE_FAN, or
  /// GL_TRIANGLE_STRIP)
  GLenum m_Tmode;

  /// nb of vertices seen since tess_begin
  int n_vertex;

  /// store indices of points used for current triangle being built
  VectorPreferredSize<3, int> m_idx;
  
  /*************************************************************
   * For triangle output
   *************************************************************/
  
  void emitTriangle() {
    emitPolygon(3, &(m_idx[0]));
  }

  void tessVertexTriangle(long idx) {
    if (n_vertex < 3) {
      m_idx.push_back(idx); 
      n_vertex++;
      if (n_vertex == 3) {
        emitTriangle(); 
        if (m_Tmode == GL_TRIANGLES) {
          n_vertex = 0; 
          m_idx.clear();
        }
      }
    } else {
      switch(m_Tmode) {
      case GL_TRIANGLE_FAN:
        m_idx[1] = m_idx[2]; m_idx[2] = idx;
        emitTriangle(); 
        break;
      case GL_TRIANGLE_STRIP:
        n_vertex++;
        if (n_vertex & 1) {
          m_idx[0] = m_idx[1];
          m_idx[1] = m_idx[2]; 
          m_idx[2] = idx;
          emitTriangle(); 
	} else {
	  m_idx[0] = m_idx[2]; 
          m_idx[2] = idx;
	  emitTriangle();
	  m_idx[1] = m_idx[0];
	}
	break;
      default:
	assert(false);
      }
    }
  }
  

  /*************************************************************
   * For polygon output
   *************************************************************/
  

  mCSG::Vec3 normal;
  /* when building a fan or strip, the current polygon has vertices 
   *
   *  [ ... p0 p1 p2 p3 ...]  
   *
   * when the next vertex p will be added, the polygon will become 
   *
   *  [ ... p0 p1 p p2 p3 ... ] 
   *
   * the pi's are needed to determine if that new polygon is convex.
   */
  mCSG::Vec3 p0, p1, p2, p3; 
   
  void tessBegin(GLenum mode) {
    m_Tmode = mode; 
    m_idx.clear();
    n_vertex = 0; 
  }

  void tessEnd() {
    if(just_triangles) return; 
    if(m_Tmode == GL_TRIANGLE_FAN) emitTriangleFan(); 
    else if(m_Tmode == GL_TRIANGLE_STRIP) emitTriangleStrip(); 
  }

  bool isConvexCorner(const mCSG::Vec3 & p) {
    return 
      normal.dot((p - p1).cross(p0 - p1)) > 0 &&
      normal.dot((p - p2).cross(p2 - p3)) > 0;
  }

  void tessVertex(int idx) {
    if(just_triangles) {
      tessVertexTriangle(idx); 
      return;
    }
    if(m_Tmode == GL_TRIANGLES) {
      if(m_idx.size() < 3) m_idx.push_back(idx); 
      if(m_idx.size() == 3) {
        emitPolygon(3, &m_idx[0]); 
        m_idx.clear(); 
      }        
    } else if(m_Tmode == GL_TRIANGLE_FAN) {
      // see if we have to start a new polygon
      if(m_idx.size() < 3) {
        m_idx.push_back(idx); 
        if(m_idx.size() == 3) {
          p1 = getCoords(m_idx[0]); 
          p0 = p3 = getCoords(m_idx[1]); 
          p2 = getCoords(m_idx[2]);
          normal = (p1 - p0).cross(p2 - p0); 
          //printf("normal=%g %g %g\n", normal.x, normal.y, normal.z);
        }
      } else {
        mCSG::Vec3 p = getCoords(idx); 
        if(!isConvexCorner(p)) { // becomes non-convex: start afresh
          emitTriangleFan(); 
          m_idx[1] = m_idx[m_idx.size() - 1];
          m_idx[2] = idx;
          m_idx.resize(3, -1); 
          p0 = p3 = p2; 
          p2 = p; 
        } else {        
          m_idx.push_back(idx); 
          p3 = p2; 
          p2 = p; 
        }
      }
    } else if(m_Tmode == GL_TRIANGLE_STRIP) {
      if(m_idx.size() < 3) {
        m_idx.push_back(idx); 
        if(m_idx.size() == 3) {
          p0 = p3 = getCoords(m_idx[0]); 
          p1 = getCoords(m_idx[2]); 
          p2 = getCoords(m_idx[1]);
          normal = (p1 - p0).cross(p2 - p0); 
          // printf("normal=%g %g %g\n", normal.x, normal.y, normal.z);
        }
      } else {
        mCSG::Vec3 p = getCoords(idx); 
        if(!isConvexCorner(p)) { // becomes non-convex: start afresh
          emitTriangleStrip(); 
          if(n_vertex % 2 == 0) {
            // printf("EVEN\n"); 
            m_idx[0] = m_idx[m_idx.size() - 2];
            m_idx[1] = m_idx[m_idx.size() - 1];
            p0 = p3 = p1; 
            p1 = p; 
          } else {
            // printf("ODD\n"); 
            m_idx[0] = m_idx[m_idx.size() - 2];
            m_idx[1] = m_idx[m_idx.size() - 1];
            p0 = p3 = p2; 
            p2 = p; 
          }
          m_idx[2] = idx;
          m_idx.resize(3, -1);
        } else {        
          m_idx.push_back(idx); 
          if(n_vertex % 2 == 0) {
            p0 = p1; 
            p1 = p; 
          } else {
            p3 = p2; 
            p2 = p;
          }          
        }        
      }
      n_vertex++; 
    }
  }


  void emitTriangleFan() {
    int n = m_idx.size(); 
    int *c_idx = new int[n]; 
    for(int i = 0; i < n; i++) c_idx[i] = m_idx[i]; 
    emitPolygon(n, c_idx); 
	delete[] c_idx;
  }

  void emitTriangleStrip() {
    int n = m_idx.size(); 
    int *c_idx = new int[n]; 
    int i; 
    // reorder vertices
    if((n_vertex - n) % 2 == 0) {
      for(i = 0; i < n; i += 2) c_idx[n - 1 - i / 2] = m_idx[i]; 
      for(i = 1; i < n; i += 2) c_idx[i / 2] = m_idx[i];
    } else {
      for(i = 0; i < n; i += 2) c_idx[i / 2] = m_idx[i]; 
      for(i = 1; i < n; i += 2) c_idx[n - 1 - i / 2] = m_idx[i];
    }
    emitPolygon(n, c_idx);
	delete[] c_idx;
  }

  



};


#endif
