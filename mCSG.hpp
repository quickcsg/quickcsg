/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */

#ifndef MCSG_INCLUDED
#define MCSG_INCLUDED

#include <vector>

#ifdef ENABLE_TBB

#include <tbb/tbb.h>
#include <tbb/enumerable_thread_specific.h>

#endif


#include "cinder-lite/Vector.h"

#include "VectorPreferredSize.hpp"
#include "kdtree.hpp"
#include "bitvectors.hpp"

// assumes verbose is defined somewhere
#define IFV(vthresh) if(verbose > vthresh)





namespace mCSG {

#ifndef USE_FLOATS
  typedef cinderlite::Vec3<double> Vec3; 
  typedef double real;
#else
  typedef cinderlite::Vec3<float> Vec3; 
  typedef float real;
#endif

  struct Vertex {
    Vertex(): keep(0) {}
    Vertex(real x, real y, real z): coords(x, y, z), keep(0) {}
    Vertex(const Vec3 &v): coords(v), keep(0) {}
    Vec3 coords;  // 3D coordinates
    // facets this is the intersection of. 
    // For the initial meshes, there are >= 3 facets (uniform mesh)
    // for the generated vertices = 3 facets
    typedef VectorPreferredSize<3, int> FacetTableType; 
    FacetTableType facetNos; 
    
    // filled in when exploring kdtree leaf    
    BitVector meshpos;    // meshes this is the intersection of are set to unknown by convention
    int keep;             // used only for primitive points: 0 = not visited, 1 = keep, 2 = don't keep


    // serialization. (de)serialize returns the number of *unused* bytes in the buffer.
    size_t serialSize() const; 
    size_t serialize(char *buf, size_t bufsz) const; 
    size_t deserialize(const char *buf, size_t bufsz); 

  };


  struct Facet {
    // following fields are useful for input facets. 
    Vec3 normal; // pointing outwards
    real b;      // inside: (x, y, z) . normal > b
    int meshno;  // originating mesh

    typedef VectorPreferredSize<4,int> IndexTable; 
    IndexTable vertexNos; // vertices that form the facet
    IndexTable oppositeFacetNos; // corresponding other side's edge

    // output polygons on this facet    
    IndexTable outputVertexNos; // vertices that form the facet

    // there are loops.size() polygons, loop #i is made of vertexNos[i - 1]:vertexNos[i], with vertexNos[-1] = 0        
    // all same-orientation loops are before all diff-orientation ones
    IndexTable loops;         
    int n_loops_same; // nb of loops that have the same orientation


    Facet(int meshno = 0): meshno(meshno) {}

    // initializes normal and bbox
    void afterLoad(const std::vector<Vertex> &vertices); 

    // find index of p1 in vertexNos, set *pi to it and return previous vertex
    int findVertex(int p1, int *pi) const; 

    // find index of fno in oppositeFacetNos,  set *pi to it and return previous vertex
    int findOppositeFacet(int fno, int *pi) const;

    // serialization
    size_t serialSize() const; 
    size_t serialize(char *buf, size_t bufsz) const; 
    size_t deserialize(const char *buf, size_t bufsz); 

  };

  // record non-fatal errors encountered during processing
  struct CSGErrors {
    int &verbose; // refers to the CSG's verbose
    CSGErrors(int &verbose): verbose(verbose) {}

    struct Err {
      std::string str; 
      int n; 
    }; 
    std::vector<Err> errs;    

#ifdef ENABLE_TBB 
    tbb::mutex lock;
#endif
    
    void add(const char *);
    bool haveErrors() const {return errs.size() > 0; }
    int count() const; 
    void print() const; 
    void clear() {errs.clear(); }

  };

  // Global variables for verbosity and error reporting

  struct HEPairs;

  struct CSGParameters {

    // kdtree parameters
    int max_in_leaf; 
    real split_threshold; 
    real min_volume_factor; // if volume is below global bbox volume * min_volume_factor, stop splitting
    int alloc_policy;       // 0 = keep all nodes, 1 = keep nodes only in parallel section, 2 = delete on the fly, 3 = same as 2 + do not call Node destructor
    bool collect_processing_stats; // makes nice plots but may slow down processing
    int verbose; // verbosity level

    // facet building params
    int tesselate;                
    // = 1: tesselate output facets to convex polygons
    // = 3: tesselate to triangles only 
    int sort_nvertex; // sort half-edges if nb of vertices is above this

    // parallelization options (used only if compiled with TBB)
    int parallel_nt;    // nb of threads (tbb::task_scheduler_init should be called externally)
    int parallel_depth; // start parallel processing at this depth
    int parallel_min_npoly; // stop parallel processing if subtree contains fewer than this number of polygons
    int parallel_split_depth; // attempt to parallellize splits below this depth
    int parallel_split_npoly; // parallelize within mesh if there are more than this # of polygons

    int breadth_depth; 
    int tight_bbox_depth;  // depth at which we start having a tight bbox 

    // debugging options

    // save contents of node nodeToSave to file nodeFname
    const char *nodeToSave, *nodeFname; 
    int facetToSave; 
    const char *facetFname; 
    int primaryVertexToFollow; // if -2, ignore, if -1, use vertexToFollow coordinates
    Vec3 vertexToFollow; 
        
    // set reasonable defaults
    CSGParameters():
      max_in_leaf(20), 
      split_threshold(0.5),      
      min_volume_factor(1e-15),
      alloc_policy(2),
      collect_processing_stats(false), 
      verbose(0),
      tesselate(0), 
      sort_nvertex(12), 
      parallel_nt(-1),
      parallel_depth(0), 
      parallel_min_npoly(80),
      parallel_split_depth(5), 
      parallel_split_npoly(1024), 
      breadth_depth(6),
      tight_bbox_depth(0),
      nodeToSave(NULL), nodeFname(NULL), 
      facetToSave(-1), facetFname(NULL),
      primaryVertexToFollow(-2)
    {
    }
    
  }; 


  struct CSG: CSGParameters {
    /************************************
     * fields
     */

    // input
    std::vector<Facet> facets; // fixed-size, contains only input facets
    std::vector<Vertex> vertices; // increasing with type 2 and 3 vertices

#if ENABLE_TBB
    typedef tbb::enumerable_thread_specific<std::vector<Vertex> > ConcurrentVertices;
    ConcurrentVertices threadLocalVertices;
#endif

    // called before facet construction to merge concurrent vertices
    void mergeVertexTables();

    // nb of meshes 
    int nmesh;

    // nb of primitive vertices
    int nvert1;
    std::vector<int> meshno_to_vertex; // last vertex of mesh i is meshno_to_vertex[i] - 1

    // KDtree root node
    Node * root;      

    // memory pools for Node and ConvexPolygon allocation
    MemoryPool nodePool; 
    MemoryPool polygonPool;
    
    // stop splitting below this volume
    double min_volume;
        
    // recorded stats about makeFacets
    struct {
      ProcessingStats mergeVertexTables;
      ProcessingStats preFacet;
    } processingStats;
    std::vector<ProcessingStats> facetProcessingStats;     

   
    CSGErrors errors; // remember what non-fatal errors we encountred


    /************************************
     * Basic methods
     */

    CSG(): nmesh(0), root(NULL), nodePool(sizeof(Node)), polygonPool(sizeof(ConvexPolygon)), errors(verbose) {}

    CSG(const CSGParameters &params): CSGParameters(params), nmesh(0), root(NULL), nodePool(sizeof(Node)), polygonPool(sizeof(ConvexPolygon)), errors(verbose) {}
 
    ~CSG();

    // loads mesh from an OFF file
    // combined: load several meshes from OFF file (using color as key to group facets)
    void addMeshFromOff(const char *fname, bool combined = false); 
    
    // same for .obj
    void addMeshFromObj(const char *fname); 

    // same as addMeshFromOff, but data is in arrays
    // facets is an array that contains chunks like
    // [ ... 3 p0 p1 p2 4 p0 p1 p2 p3 ...]
    // facets is of size facetsSize (facetsSize >= nfacet * 4)
    void addMeshFromVFArray(int npt, 
                            const real (*pts)[3], 
                            const int *facets, int facetsSize);
        
    
    // call when all meshes are loaded
    // reorder facets linked to each vertex 
    void afterLoad(); 

    // prints vertices and facets 
    void print() const; 

   
    // compute direction of edge between f0 and f1
    Vec3 edgeDirection(int f0, int f1); 
    
    // initialize and explore the KDTree to produce all vertices
    void exploreKDTree(CSGOperation *csgop);

    /************************************
     * Make all vertices (called by exploreKDTree)
     */

    // recursively explore kdtree and call findNodeVertices on leaves
    void exploreNode(Node *node, CSGOperation *csgop); 

    // explore in breadth-first until depth breadth_depth
    void exploreBreadth(CSGOperation *csgop);

    // handle a kdtree leaf
    void findNodeVertices(Node *node, CSGOperation *csgop);
    

    /************************************
     * vertices -> facets
     */
    
    // fill in the OutputFacets for all facets between f0 and f1.
    void makeFacets(CSGOperation *csgop, int f0 = 0, int f1 = -1); 

    void makeFacet(CSGOperation *csgop, int f0, const int *vnos, int nv);
    bool makeFacetOnlyPrimaries(CSGOperation *csgop, int f0, const int *vnos, int nv);

    // single mesh. f0 is the mesh id
    void addHEPairsPrimary(int f0, int vno, CSGOperation *csgop, HEPairs &hepairs); 

    // choose between the 2 versions of addHEPairsDouble
    void addHEPairsDouble(int f0, int vno, CSGOperation *csgop, HEPairs &hepairs); 

    // f1 and f2 from same mesh 
    void addHEPairsDouble1(int f0, int f1, int f2, int vno, CSGOperation *csgop, HEPairs &hepairs); 

    // f0 and f1 from same mesh
    void addHEPairsDouble2(int f0, int f1, int f2, int vno, CSGOperation *csgop, HEPairs &hepairs);

    // three meshes
    void addHEPairsTriple(int f0, int vno, CSGOperation *csgop, HEPairs &hepairs); 

    // converts to set of convex polygons    
    void tesselateFacet(Facet &facet);

    /************************************
     * Output
     */

    // output to mesh file    
    void toOff(const char *fname, bool keepAllVertices = true, const float *border_color = NULL) const;
    void toOffShrunk(const char * outfilename, real shrinkfactor) const; 
    // tesselated to triangles (not necessary if tesselate != 0)
    void toOffTesselated(const char * outfilename) const;

    // stores input mesh
    void combinedToOff(const char *fname) const;


    // output to mesh of triplets (for consistency checks)
    void saveAsTrimesh(const char *fname) const;

    // output to set of vertices (for consistency checks)
    void saveVertices(const char *fname) const;
 
    void printStats(); 
    void logFacetStats(const char *fname);

    // save to eps file fname, adding vertices vnos[0:nv]
    void saveFacetToEPS(const char *fname, int f0, const int *vnos, int nv); 

    /**************************************************
     * KDTree related operations 
     **/
    
    // make root node and put it in state 0
    void initKDTree(); 

    // remove the whole tree 
    void deleteTree();

    // deallocate the polygon nodes
    void clearNodePolygons(Node * node); 
    
    // deallocate 2 child nodes (not recursive)
    void deleteNodeChildren(Node *node);

    void printKDTreeStats(); 
    
    // output detailed stats to a file 
    void logKDTreeStats(const char * file_name, int format);

    // variant == 1: also store internal nodes
    void kdtreeToOff(const char * output_file_name, int variant = 0);

    // specific case for intersection: make 6 tree levels around bounding box of object
    Node* expandForIntersection(); 

    // crop the root node to a bounding box
    void cropRootToBBox(const Vec3 &bbMin, const Vec3 &bbMax); 
  

    /**************************************************
     * inputs from contours
     **/

    void addContoursAndCalib(int npt, 
                             const double (*contourpoints)[2], 
                             const double calib[12], 
                             double depth); 

    void loadContoursAndCalib(const char *contoursFileName, 
                              const char *calibFilename, 
                              double depth, 
                              double jitter = 0, 
                              int cam0 = 0, int cam1 = -1, 
                              const std::vector<int> * cam_selection = NULL);
    
  };


  // used for timing
  double getmillisecs(); // ms since epoch
  long getcycles(void); // CPU cycles since boot


  // simple object to store Off file
  struct Off {
    std::vector<Vec3> vertices; 
    std::vector<std::vector<int> > faces; 
    std::vector<int> meshnos; // same size as facets, determines color of the facets
    
    void addPolygon(std::vector<Vec3> &v, int meshno); 

    void addBox(const Vec3 &center, real w, int meshno); 
    void addBox(const Vec3 &bbmin, const Vec3 &bbmax, int meshno); 
    
    void toFile(const char *fname);
    
    void removeUnusedVertices();
  };
  
};


#ifdef _WIN32

static double drand48() {
  return float(rand()) / RAND_MAX;
}

static void srand48(int seed) {
  srand(seed); 
}

#define strtok_r strtok_s



#endif


#endif


