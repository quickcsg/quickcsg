# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com


import sys, math, random

infile, outfile = sys.argv[1:]

nodes = {}


class Node:
    def __init__(self, **args): 
        self.__dict__.update(args)
        self.children = []



for l in open(infile, "r"):
    node_id, n_poly, known, inside, state = l.split()
    # <2111222> 10 4 0 2


    node  = Node(state = int(state), n_poly = int(n_poly), meshpos = (int(known), int(inside)))
    nodes[node_id] = node
    if node_id != "<>": 
        nodes[node_id[:-2] + ">"].children.append(node)


root = nodes['<>']

poly_x_scale = 1.4
x_margin = 3.0

def set_width(node):
    node.width = poly_x_scale * node.n_poly
    if node.children: 
        for child in node.children: 
            set_width(child)
        sub_width = (len(node.children) - 1) * x_margin + sum([child.width for child in node.children])
        node.sub_x = (node.width - sub_width) / 2
        if sub_width > node.width: 
            node.width = sub_width

set_width(root)

def set_x(node, x0): 
    node.x = x0
    if node.children: 
        if node.sub_x > 0: 
            x0 += node.sub_x
        else: 
            node.x -= node.sub_x
        for child in node.children: 
            set_x(child, x0) 
            x0 += child.width + x_margin

set_x(root, 0)

max_depth = max([len(n) for n in nodes])

bar_height = 20
y_margin = 13

f = open(outfile, "w")
print >> f, "%%!PS-Adobe-3.0\n%%%%BoundingBox: %d %d %d %d" % (
    -2, -2, root.width + 2, (bar_height + y_margin) * max_depth + 2)
    
colormap = {
    0: (0.5, 1, 1),  # cyan
    1: (1, 0.5, 1),  # magenta
    2: (1, 1, 0.5),  # yellow
    3: (1, 0.5, 0.5) # redish
}

def node_rect(node, depth): 
    return (node.x, (bar_height + y_margin) * (max_depth - depth - 2), 
            node.n_poly * poly_x_scale, bar_height)

def node_meshpos(node): 
    nmesh = 3
    known, inside = node.meshpos
    s = ''
    for i in range(nmesh):         
        if not ((known >> i) & 1): 
            s += 'u'
        elif (inside >> i) & 1: 
            s += '1'
        else:
            s += '0'
    # print node.meshpos, s
    return s

print >>f, "/Helvetica findfont 14 scalefont setfont"

def draw(node, depth): 
    node_r = node_rect(node, depth)
    print >> f, "%g %g %g %g 4 copy %g %g %g setrgbcolor rectfill 0 setgray rectstroke" % (
            node_r + colormap[node.state])
    if node.n_poly > 30: 
        print >> f, "%g %g moveto (%s) show" % (
            node_r[0] + 3, node_r[1] + 3, 
            node_meshpos(node) + "|%d" % node.n_poly)        
    elif node.n_poly > 17:         
        print >> f, "%g %g moveto (%s) show" % (
            node_r[0] + 3, node_r[1] + 3, 
            node_meshpos(node))
        
    for child in node.children: 
        child_r = node_rect(child, depth + 1)        
        print >>f, "%g %g moveto %g %g lineto stroke" % (
            node_r[0] + node_r[2] / 2, node_r[1], 
            child_r[0] + child_r[2] / 2, child_r[1] + child_r[3])
        draw(child, depth + 1)

draw(root, 0)

