# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import sys


triedge_fname, vertlist_fname  = sys.argv[1:3]

if len(sys.argv) > 3: 
  facet_no_shift = int(sys.argv[3])
else:
  facet_no_shift = 0

def sorted_tuple(*x):
  return tuple(sorted(x))

vertices = [l.split()[:3] for l in open(vertlist_fname, "r")]
vertices = [sorted_tuple(int(a) - facet_no_shift, int(b) - facet_no_shift, int(c) - facet_no_shift) for a, b, c in vertices]

triedges = []
for l in open(triedge_fname, "r"):
  x = [y.split(',') for y in l.split()]
  triedges += [sorted_tuple(int(a), int(b), int(c)) for a, b, c in x]



ref = set(triedges)
new = set(vertices)

print len(ref), len(new)

print "missing:", list(sorted(ref - new))
print "too many:", list(sorted(new - ref))



if ref != new:
  print "BAD"
  sys.exit(1)
else:
  print "OK"
  
