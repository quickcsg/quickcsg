/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */
#ifndef VECTOR_PREFERRED_SIZE
#define VECTOR_PREFERRED_SIZE


#include <cstdio>
#include <cassert>
#include <cstring>
#include <algorithm>


/* A vector class for plain-old-datatypes with privileged access to
   the N first values (they are stored within the class). This spares
   dynamic allocations and cache misses, especially for arrays whose
   size is <= N most of the time.
   
   This comes at the cost of an extra "if" at each element access, but
   its impact on performance does not seem prohibitive. */

template<size_t N, class ValueT> 
class VectorPreferredSize {
private: 
  size_t n; 
  ValueT data1[N]; 
  ValueT *data; 

  size_t allocated_size_for(int n1) const {
    size_t current_size = 8;       
    while(current_size + N < n1) current_size *= 2; 
    return current_size; 
  }

public: 

  // copy constructor
  VectorPreferredSize(const VectorPreferredSize &v) {
    n = v.n; 
    for(int i = 0; i < N; i++) data1[i] = v.data1[i]; 
    if(n > N) {
      size_t current_size = allocated_size_for(n); 
      data = new ValueT[current_size]; 
      assert(n <= current_size + N); 
      memcpy(data, v.data, sizeof(ValueT) * (n - N)); 
    } else {
      data = NULL; 
    }
  }

  VectorPreferredSize & operator = (const VectorPreferredSize &v) {
    clear(); 
    VectorPreferredSize tmp(v); 
    swap(tmp);
    return *this;
  }

  VectorPreferredSize(): n(0) {
    data = NULL;
  }

  ~VectorPreferredSize() {
    delete [] data; 
  }

  size_t size() const {
    return n; 
  }

  ValueT & operator [] (size_t i) {    
    if(i < N) return data1[i]; 
    return data[i - N]; 
  }

  const ValueT & operator [] (size_t i) const {
    if(i < N) return data1[i]; 
    return data[i - N]; 
  }

  void push_back(const ValueT &x) {
    if(n < N) {
      data1[n++] = x; 
    } else {  
      resize(n + 1); 
      data[n - N - 1] = x; 
    }      
  }

  void pop_back() {
    n--; 
  }

  ValueT & back() {
    return n <= N ? data1[n - 1] : data[n - 1 - N]; 
  }

  const ValueT back() const {
    return n <= N ? data1[n - 1] : data[n - 1 - N]; 
  }

  const ValueT & front() const {
    return data1[0]; 
  }

  ValueT & front() {
    return data1[0]; 
  }

  void swap(VectorPreferredSize & other) {
    std::swap(n, other.n); 
    std::swap(data, other.data); 
    for(int i = 0; i < N; i++) 
      std::swap(data1[i], other.data1[i]); 
  }

  void resize(size_t new_n, const ValueT x) {
    if(new_n <= n) {
      n = new_n;       
    } else {
      while(n < new_n) push_back(x); 
    }
  }

  // resize without initializing 
  void resize(size_t new_n) {
    if(new_n <= n) {
      if(new_n <= N) {
        delete [] data; 
        data = NULL; 
      }      
    } else {
      if(new_n > N) {
        size_t current_size = n <= N ? 0 : allocated_size_for(n);
        if(new_n > current_size + N) {          
          size_t new_size = current_size == 0 ? 8 : current_size * 2; 
          while(new_n > new_size + N) new_size *= 2;
         
          ValueT *new_data = new ValueT[new_size];         
          if(n > N) memcpy(new_data, data, sizeof(ValueT) * (n - N)); 
          delete [] data; 
          data = new_data;  
        }      
      }        
    }
    n = new_n; 
  }

  // insert other at offset n0 (erasing current elements)
  void insert(const VectorPreferredSize & other, size_t n0) {
    if(other.n == 0) return;
    
    assert(n0 + other.n <= n);

    // copy data1 part
    if(n0 < N) {
      memcpy(data1 + n0, other.data1, sizeof(ValueT) * std::min(other.n, N - n0));      
      if(other.n + n0 > N)
        memcpy(data, other.data1 + N - n0, sizeof(ValueT) * std::min(n0, other.n + n0 - N));       
    } else 
      memcpy(data + n0 - N, other.data1, sizeof(ValueT) * std::min(N, other.n)); 
    
    // copy data part
    if(other.n > N) 
      memcpy(data + n0, other.data, sizeof(ValueT) * (other.n - N));
  }

  void append(const VectorPreferredSize & other) {
    size_t n0 = n; 
    resize(n + other.n); 
    insert(other, n0); 
  }

  bool empty() const {
    return n == 0; 
  }

  void clear() {
    delete [] data; 
    data = NULL; 
    n = 0;
  }
  

}; 




#endif 
