/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */


// this is not a standalone file, it is #included  by kdtree.cpp


#ifndef ENABLE_TBB

// dummy implementations (to satisfy forward declarations)

static void calibrate_per_worker_offsets(FILE *f) {}

static void split_mesh_polygons_parallel(Node &node, CSG & csg) {
  assert(!"not compiled with TBB");
}

#else 

#include <unistd.h>


struct SplitPolygonRangeParams {
  Node *node; // node being split
  int nmesh; 
  real eps;   
  MemoryPool & polygonPool;
  int verbose; 
}; 

struct SplitPolygonRange: SplitPolygonRangeParams {

  // collected info for one child
  struct BBox {
    Vec3 bbMin, bbMax; 
    int n; 
    BBox(): bbMin(Vec3::max()), bbMax(-Vec3::max()), n(0) {}
    void update(const ConvexPolygon &poly) {
      bbMin = minVec3(bbMin, poly.bbMin); 
      bbMax = maxVec3(bbMax, poly.bbMax);       
      n++;
    }
    void join(const BBox & other) {
      bbMin = minVec3(bbMin, other.bbMin); 
      bbMax = maxVec3(bbMax, other.bbMax); 
      n += other.n;
    }
    void updateNode(Node & node) {      
      node.bbMin = minVec3(bbMin, node.bbMin); 
      node.bbMax = maxVec3(bbMax, node.bbMax); 
    }
  };

  // collected per mesh
  struct MeshInfo {
    MeshSideMax ms1;
    MeshSideMin ms2;
    Node::PolygonTable child1_polygons; 
    Node::PolygonTable child2_polygons; 

    MeshInfo(int split_dim, real eps, int verbose): ms1(split_dim, eps, verbose), ms2(split_dim, eps, verbose) {}

    void addPolygon1(ConvexPolygon *poly) {
      ms2.disable(); 
      ms1.update(poly, child1_polygons.size()); 
      child1_polygons.push_back(poly); 
    }

    void addPolygon2(ConvexPolygon *poly) {
      ms1.disable(); 
      ms2.update(poly, child2_polygons.size()); 
      child2_polygons.push_back(poly); 
    }     

    void join(MeshInfo &other) {
      ms1.join(other.ms1, child1_polygons.size()); 
      ms2.join(other.ms2, child2_polygons.size()); 
      child1_polygons.append(other.child1_polygons); 
      child2_polygons.append(other.child2_polygons); 
    }

  };

  BBox bb1, bb2;
  std::vector<MeshInfo *> meshInfos;
  
  SplitPolygonRange(SplitPolygonRangeParams & params): SplitPolygonRangeParams(params)
  { 
    meshInfos.resize(nmesh, NULL);
  }

  SplitPolygonRange(SplitPolygonRange & other, tbb::split): SplitPolygonRangeParams(other)
  { 
    meshInfos.resize(nmesh, NULL);
  }

  ~SplitPolygonRange() {
    for(int i = 0; i < meshInfos.size(); i++) delete meshInfos[i];
  }
  
  void operator()( const tbb::blocked_range<int>& r) {    
    for(int i = r.begin(); i != r.end(); ++i) {      
      int meshno = node->polygons[i]->meshno;
      ConvexPolygon * poly1 = node->polygons[i];
      ConvexPolygon * poly2 = NULL; 
    
      split2Polygons(poly1, poly2, node->split_dim, node->threshold, polygonPool);     
      
      assert(meshno < meshInfos.size()); 
      MeshInfo * mi = meshInfos[meshno]; 
      if(!mi) {
	mi = meshInfos[meshno] = new MeshInfo(node->split_dim, eps, verbose);
      }

      if(poly1) {bb1.update(*poly1); mi->addPolygon1(poly1); }
      if(poly2) {bb2.update(*poly2); mi->addPolygon2(poly2); }
    }
  }
  
  void join(SplitPolygonRange &other) {
    bb1.join(other.bb1); 
    bb2.join(other.bb2); 

    for(int i = 0; i < meshInfos.size(); i++) {
      MeshInfo * & mi_t = meshInfos[i]; 
      MeshInfo * & mi_o = other.meshInfos[i]; 
      if(!mi_t) {
        if(mi_o) {  // steal from other
          mi_t = mi_o; 
          mi_o = NULL;
        }
      } else {
        if(mi_o) { // merge
          mi_t->join(*mi_o);          
        } // else nothing        
      }     
    }

  }

};


static void split_mesh_polygons_parallel(Node &node, CSG & csg) {
  int verbose = csg.verbose;
  Node & child1 = *node.child1;
  Node & child2 = *node.child2;

  real eps = (node.bbMax[node.split_dim] - node.bbMin[node.split_dim]) * 1e-8;

  SplitPolygonRangeParams splitctx = {&node, csg.nmesh, eps, csg.polygonPool, verbose}; 
  SplitPolygonRange split(splitctx); 

  tbb::parallel_reduce(tbb::blocked_range<int>(0, node.polygons.size()), split); 

  split.bb1.updateNode(child1); 
  split.bb2.updateNode(child2); 

  for(int meshno = 0; meshno < split.meshInfos.size(); meshno++) {
    SplitPolygonRange::MeshInfo * mi = split.meshInfos[meshno]; 
    if(!mi) {
      assert(node.meshpos.isKnown(meshno)); 
      continue;
    }

    // take care of mesh positions

    assert(!(mi->child1_polygons.empty() && mi->child2_polygons.empty()));

    if(mi->child1_polygons.empty()) {    
      int side = mi->ms2.get_side(mi->child2_polygons); 
      if(side == -1) {
        csg.errors.add("cannot determine side with getMeshSideMin"); 
        side = 0; // arbitrary
      }
      child1.meshpos.setSide(meshno, side); 
    } else if(mi->child2_polygons.empty()) {
      int side = mi->ms1.get_side(mi->child1_polygons);
      if(side == -1) {
        csg.errors.add("cannot determine side with getMeshSideMax"); 
        side = 0; // arbitrary 
      }
      child2.meshpos.setSide(meshno, !side); 
    } // else both children have facets of meshno. 

    // add polygons to children
    child1.polygons.append(mi->child1_polygons); 
    child2.polygons.append(mi->child2_polygons); 
  }

}












struct ThreadSamples {
  ProcessingStats ps; 
  double t;     
  void tic() {
    ps.begin(); 
    t = getmillisecs(); 
  }
};

static void calibrate_per_worker_offsets(FILE *f) {
  int nsample = 100; 
  long usleep_time = 5 * 1000; // 5 ms
  std::vector<ThreadSamples> samples; 
  samples.resize(nsample); 

  tbb::parallel_for(tbb::blocked_range<int>(0, nsample, 1), 
                    [&] (const tbb::blocked_range<int>&r) {
                      for(int i = r.begin(); i < r.end(); i++) 
                        samples[i].tic(); 
                      usleep(usleep_time); 
                    }                    
                    );
  
  for(int i = 0; i < nsample; i++) {
    int j;
    for(j = 0; j < i; j++) 
      if(samples[i].ps.thread_id == samples[j].ps.thread_id) break;
    if(j < i) continue; // thread was already seen
    fprintf(f, "per-thread-offset %ld %ld %.3f\n", 
            samples[i].ps.thread_id, samples[i].ps.begin_cy, samples[i].t); 
  }
  
}


#endif
