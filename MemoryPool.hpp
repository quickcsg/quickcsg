/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */
#ifndef MEMORY_POOL_H_INCLUDED
#define MEMORY_POOL_H_INCLUDED

#include <cstdlib>
#include <cassert>
#include <algorithm>
#include <memory>

#ifdef ENABLE_TBB

#include <tbb/tbb.h>
#include <tbb/enumerable_thread_specific.h>
#include <oneapi/tbb/mutex.h>

#endif


// Pool of fixed-size blocks that can be allocated/deallocated for used in new(pointer)
// "real" deallocation occurs when clear() is called: pointers become invalid

class MemoryPool {
  size_t unit_size; 
  size_t block_size; 
  std::vector<void*> blocks;

  void *add_new_block() {
#ifdef ENABLE_TBB
    tbb::mutex::scoped_lock slock(lock);
#endif
    void *head = NULL;  
    // alloc a new block
    void *block = malloc(unit_size * block_size);
    blocks.push_back(block); 
    // chain
    for(size_t i = 0; i < block_size; i++) {
      void *next_ptr = head; 
      head = (void*)((char*)block + i * unit_size);
      *(void**)head = next_ptr;
    }
    return head;
  }

#ifndef ENABLE_TBB
  void *head_single;   
#else
  tbb::mutex lock;
  tbb::enumerable_thread_specific<void*> heads; // assumes heads initialized to NULL 
#endif 

public:

  MemoryPool(size_t unit_size_in): unit_size((unit_size_in + 15) & ~15) {   
    assert(unit_size >= sizeof(void*));
    block_size = 1024 * 1024 / unit_size; // blocks of about 1MB
#ifndef ENABLE_TBB
    head_single = NULL; 
#endif
  }

  int clear() {
    int count = blocks.size() * block_size;
    for(size_t i = 0; i < blocks.size(); i++) free(blocks[i]);
    blocks.clear();
    return count;
  }

  ~MemoryPool() {
    clear();
  }
  

  /* only the alloc() and dealloc() functions are thread-safe */
  
#ifndef ENABLE_TBB 
#define SET_HEAD void * & head = head_single;
#else 
#define SET_HEAD void * & head = heads.local();
#endif

  void *alloc() {
    SET_HEAD;
    if(!head) head = add_new_block();
    void *ret = head; 
    head = *(void**)head;
    return ret;
  }

  void dealloc(void *ptr) {
    SET_HEAD;
    /* Make sure this pointer is somewhere in the blocks  */
#if 0
    int i; 
    unsigned long pi = (unsigned long) ptr;
    for(i = 0; i < blocks.size(); i++) {
      unsigned long bi = (unsigned long) blocks[i];
      if(bi <= pi && pi < bi + unit_size * block_size) 
        break;

    }
    assert(i < blocks.size()); 
#endif
    *(void**)ptr = head;
    head = ptr;    
  }

};



#endif
