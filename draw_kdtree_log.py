# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import sys, math, random

infile = None
infile_facets = None
outfile = "/tmp/out.eps"
output_type = 1
hscale = 1000 # pixel / second
correct_cycle_bias = True

args = sys.argv[1:]
while args: 
    a = args.pop(0)
    if a in ('-h', '--help'):   usage()
    elif a == "-o":             outfile = args.pop(0)
    elif a == "-output_type":   output_type = int(args.pop(0))
    elif a == "-hscale":        hscale = float(args.pop(0))
    elif a == "-nocyclebias":   correct_cycle_bias = False
    elif infile == None:        infile = a
    elif infile_facets == None: infile_facets = a
    else: 
        print "unknown arg", a
        sys.exit(1)



nodes = {}
tids = {}



class Node:
    def __init__(self, **args): 
        self.__dict__.update(args)
        self.children = []
        self.sub = []


def thread_id_to_no(thread_id): 
    if thread_id == 0: thread_no = -1
    else: 
        try:
            thread_no = tids[thread_id]
        except KeyError: 
            thread_no = len(tids)
            tids[thread_id] = thread_no
    return thread_no
    

per_thread_offset = {}

for l in open(infile, "r"):
    fi = l.split()
    if fi[0] == "freq": 
        mhz = float(fi[1])
        continue

    if fi[0] == "per-thread-offset": 
        thread_id, cycles, time_in_ms = fi[1:]
        thread_no = thread_id_to_no(int(thread_id))        
        per_thread_offset[thread_no] = (int(cycles), float(time_in_ms))
        continue

    thread_no = thread_id_to_no(int(fi[5])) 

    n_poly = int(fi[6]) if len(fi) > 6 else -1

    node = Node(state = int(fi[1]), begin_cy = int(fi[3]), end_cy = int(fi[4]), thread_no = thread_no, n_poly = n_poly)

    if len(fi) > 7:
        for i in range(7, len(fi), 3): 
            job = fi[i:i+3]
            thread_no = thread_id_to_no(int(job[2]))
            
            node.sub.append(Node(begin_cy = int(job[0]), 
                                 end_cy = int(job[1]), 
                                 thread_no = thread_no))
            
    node_id = fi[0]
    nodes[node_id] = node
    if node_id != "<>": 
        nodes[node_id[:-2] + ">"].children.append(node)

class Facet: 
    def __init__(self, **args): 
        self.__dict__.update(args)

hz = mhz * 1e6

        

facets = []
pre_facets = []
for l in open(infile_facets, "r"):
    fi = l.split()
    thread_no = thread_id_to_no(int(fi[3])) 
    if fi[0] == "mergeVertexTables" or fi[0] == "preFacet": 
        pf = Facet(begin_cy = int(fi[1]), end_cy = int(fi[2]), thread_no = thread_no)
        print "%s: %.3f ms" % (fi[0], (pf.end_cy - pf.begin_cy) / hz * 1000)
        pre_facets.append(pf)        
    else:
        facets.append(Facet(fno = int(fi[0][1:]), begin_cy = int(fi[1]), end_cy = int(fi[2]), thread_no = thread_no))
    
if per_thread_offset and correct_cycle_bias: 
    thread_cycle_offsets = {}
    d_cy_0 = None
    for thread_no, (cycles, time_in_ms) in per_thread_offset.items():
        d_cy = time_in_ms / 1000.0 * hz - cycles
        if d_cy_0 == None: d_cy_0 = d_cy
        d_cy -= d_cy_0
        d_cy = int(d_cy)
        print "thread %d, offset %d cycles" % (thread_no, d_cy)
        thread_cycle_offsets[thread_no] = d_cy
    
    for node in nodes.itervalues(): 
        if node.thread_no < 0: continue
        ofs = thread_cycle_offsets[node.thread_no]
        node.begin_cy += ofs
        node.end_cy += ofs

    for facet in facets:
        if facet.thread_no < 0: continue
        ofs = thread_cycle_offsets[facet.thread_no]
        facet.begin_cy += ofs
        facet.end_cy += ofs



t0 = min([node.begin_cy for node in nodes.itervalues() if node.thread_no != -1]) / hz 
# t1 = max([node.end_cy for node in nodes.itervalues() if node.thread_no != -1]) / hz
t1 = max([facet.end_cy for facet in facets if facet.begin_cy != 0]) / hz

max_depth = max([len(node_name) - 2 for node_name in nodes])
longuest_node = max([node.end_cy - node.begin_cy for node in nodes.itervalues() if node.thread_no != -1]) / hz
print "%d nodes, %d threads, %g MHz, time range %.3f s, max depth %d, longest %.5f s total %.3f s" % (
    len(nodes), len(tids), mhz, (t1 - t0), max_depth, longuest_node, t1 - t0)



def propagate_ids(node): 
    node.all_tids = set([node.thread_no])
    for child in node.children: 
        propagate_ids(child)
        node.all_tids.update(child.all_tids)

root = nodes['<>']
propagate_ids(root)

cycles_explore = 0
cycles_split = 0

for node in nodes.itervalues(): 
    if node.state == 1: cycles_split += node.end_cy - node.begin_cy
    if node.state == 2: cycles_explore += node.end_cy - node.begin_cy

t_explore = cycles_explore / hz
t_split = cycles_split / hz

print "split: %.4f s (%.2f %%), explore %.4f s (%.2f %%), rest = idle/scheduling" % (
    t_split, t_split * 100 / ((t1 - t0) * len(tids)), 
    t_explore, t_explore * 100  / ((t1 - t0) * len(tids)))


def determine_wh(node): 
    sum_w = 0
    max_h = 0
    for child in node.children:
        if child.state == 3: continue
        determine_wh(child)
        sum_w += child.w
        if child.h > max_h: max_h = child.h
    tt = (node.end_cy - node.begin_cy)
    node.bar_w = 0.0001 * math.sqrt(tt)
    node.bar_h = 0.01 * math.sqrt(tt)
    #node.bar_w = (sum_w - 0.5) if sum_w > 0 else 1.0
    #node.bar_h = 1e-4 * tt / node.bar_w
    node.w = max(node.bar_w, sum_w)
    node.h = node.bar_h + max_h

def wh_to_xy(node, x, y): 
    node.y = y
    node.x = x + 0.5 * (node.w - node.bar_w)
    sum_w = 0
    for child in node.children:
        if child.state == 3: continue
        wh_to_xy(child, x + sum_w, y + node.bar_h)
        sum_w += child.w


def enumerate_leaves(node, n0): 
    if node.children == []: 
        node.no = n0
        return n0 + 1
    node.no_min = n0
    for child in node.children: 
        n0 = enumerate_leaves(child, n0)
    node.no_max = n0
    return n0

def trap(x): 
    if x < 0: return 0.0
    if x < 1: return x
    if x < 2: return 1.0
    if x < 3: return 3.0 - x
    return 0.0

def jet(x): 
    x *= 4
    return (trap(x + 0.5), trap(x - 0.5), trap(x - 1.5))
    
    


if output_type < 10:

    vscale = 10
    vmargin = 2
    vgrad = 10


    f = open(outfile, "w")
    print >> f, "%%!PS-Adobe-3.0\n%%%%BoundingBox: %d %d %d %d" % (
        -2, -2, (t1 - t0) * hscale + 2, (vscale + vmargin) * len(tids) + vgrad + 2)


    def draw_bar(thread_no, begin_cy, end_cy, color, label = None): 
        print >>f, "%s setrgbcolor" % color
        print >>f, "%g %g %g %g rectfill" % (
            (begin_cy / hz - t0) * hscale, thread_no * (vscale + vmargin), 
            (end_cy - begin_cy) / hz * hscale, vscale)
        if label and len(label) * 3 < (end_cy - begin_cy) / hz * hscale: 
            print >>f, "0 setgray %g %g moveto (%s) show" % (
                (begin_cy / hz - t0) * hscale, thread_no * (vscale + vmargin), 
                label)
            
        # print >>f, "0 setgray 0.2 setlinewidth rectstroke"

    # graduations
    print >>f, "/Helvetica findfont 5 scalefont setfont"
    for i in range(int((t1 - t0) * 1000)): 
        hf = (0.7 if i % 50 == 0 else 
              0.5 if i % 10 == 0 else 
              0.3 if i % 5 == 0 else 0.2)        
        print >> f, "%g %g 2 copy moveto %g %g rlineto stroke" % (
            i * hscale / 1000.0, (vscale + vmargin) * len(tids) + vgrad + 2, 0, -vgrad * hf)
        print >> f, "moveto (%d) 0 -8 rmoveto show" % i 

    print >>f, "/Helvetica findfont 5 scalefont setfont"

    if output_type == 2: 
        nb_leaves = enumerate_leaves(root, 0)

    for node_id, node in nodes.iteritems():
        # if len(node_id) > 6: continue
        if node.state == 3: continue
        if output_type == 0:
            if node.state == 0: color = "0 0 0"
            if node.state == 1: 
                depth = len(node_id) - 2
                depth_f = float(depth) / max_depth
                if depth % 2 == 0:
                    color = "%g 1 %g" % (depth_f, depth_f)
                else:
                    color = "%g %g 1" % (depth_f, depth_f)

            if node.state == 2: color = "1 0 0"
        elif output_type == 1:
            if node.state == 0: color = "0 0 0"
            n_tids = len(node.all_tids)
            if n_tids == 1: color = "1 0 0"
            else: 
                color = "1 1 0" if node_id[-2] == "1" else "0 1 0"
                # color = "%g 1 0" % (1 - n_tids / float(len(tids)))    
        
            for nsub in node.sub:
                # print "SUB [%d %d]" % (nsub.begin_cy - node.begin_cy, nsub.end_cy - node.begin_cy)
                draw_bar(nsub.thread_no, nsub.begin_cy, nsub.end_cy, color.replace('1', '0.5'))

        elif output_type == 2:
            if node.children == []: 
                color = "%g %g %g" % jet(node.no / float(nb_leaves))
            else: 
                r0, g0, b0 = jet(node.no_min / float(nb_leaves))
                r1, g1, b1 = jet(node.no_max / float(nb_leaves))
                color = "%g %g %g" % (min(r0, r1), min(g0, g1), min(b0, b1))                            
            
        draw_bar(node.thread_no, node.begin_cy, node.end_cy, color, node_id)
        
        

    for pf in pre_facets:
        draw_bar(pf.thread_no, pf.begin_cy, pf.end_cy, "0 1 1")

    for facet in facets: 
        if facet.begin_cy == 0: continue
        ff = facet.fno / float(len(facets))
        draw_bar(facet.thread_no, facet.begin_cy, facet.end_cy, "0 %g 1" % (1-ff))

elif output_type < 20: 
    # attempt to draw the tree (not very successful)
    determine_wh(root)
    wh_to_xy(root, 0, 0)

    vscale = 1
    hscale = 1

    pal = ["%.3f %.3f %.3f" % (0.5 * random.random(), 0.5 * random.random(), 0.5 * random.random()) 
           for i in range(len(tids))]

    f = open(outfile, "w")
    print >> f, "%%!PS-Adobe-3.0\n%%%%BoundingBox: %d %d %d %d" % (-2, -2, root.w * hscale + 2, root.h * vscale + 2)
    
    for node in nodes.itervalues():
        if node.state == 3: continue
        print >>f, "%s setrgbcolor" % pal[node.thread_no]
        print >>f, "%g %g %g %g rectfill" % (
            node.x * hscale, node.y * vscale, 
            node.bar_w * vscale, node.bar_h * vscale)
        
elif output_type == 20:

    # stats on nb of split polygons vs. nb of polygons
    f = open(outfile, "w")

    for node in nodes.itervalues(): 
        if not node.children: continue
        print >> f, node.n_poly, sum([child.n_poly for child in node.children]) - node.n_poly

elif output_type == 21:

    node_ids = [node_id for node_id in nodes.keys() if len(node_id) < 6]

    node_ids.sort()
    node_ids.reverse()
    node_ids = node_ids[:15]

    for node_id in node_ids:
        node = nodes[node_id]
        print "%s: %.3f ms" % (node_id, (node.end_cy - node.begin_cy) / hz * 1000) 

"""

python draw_kdtree_log.py stats/search_params_idfreeze/A_-parallel.nt_48_nodes.log log.eps
111207 nodes, 48 threads, 2200.01 MHz, time range 0.123 s, max depth 33, longest 0.01495 s
split: 3.1428 s (53.08 %), explore 1.2329 s (20.82 %), rest = idle/scheduling


python draw_kdtree_log.py log_serial log_serial.eps
111207 nodes, 1 threads, 2200.01 MHz, time range 2.501 s, max depth 33, longest 0.00818 s
split: 1.3247 s (52.98 %), explore 1.1718 s (46.86 %), rest = idle/scheduling


./mesh_csg data/ref_timing/t1_{0..49}.off  -diff 25 -v 00001 -kdtree.keep_nodes  -kdtree.log_stats log

python draw_kdtree_log.py log dat

gnuplot> set xlabel "nb of polygons per node"
gnuplot> set ylabel "nb of splits"
gnuplot> plot "dat" title  "knots42", sqrt(x) with lines title "sqrt"
gnuplot> plot "dat" title  "toruses", sqrt(x) with lines title "sqrt"

"""
