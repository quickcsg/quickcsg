/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */

#ifdef BVWIDTH 

// will disable this file
#include "bitvectors_wide.hpp"

#endif



#ifndef BITVECTORS_HPP_INCLUDED
#define BITVECTORS_HPP_INCLUDED

namespace mCSG {
    
#ifdef _WIN32
  typedef unsigned __int64 u64;
#else   
  typedef unsigned long u64;
#endif
  
  inline int bitcount(u64 b) {
#ifdef __SSE4_2__
    return __builtin_popcountll(b);
#else
    int count = 0;
    for(int i = 0; i < sizeof(b) * 8; i++) {
      if(b & 1) count++; 
      b >>= 1;
    }
    return count;
#endif
  }
  
  // encodes the position of a point wrt a set of meshes (at most 64,
  // since we are using 64-bit longs to encode the bit vector). 
  // 3 states: 0 = outside 
  //           1 = inside
  //           u = unknown
  // some operations are difficult to read in binary notation. In this
  // case, the "readable" version is in comments before the fast version.
  
  struct BitVector {
    static const int maxbits = 64;

    u64 known;  // bit #i == 1: insideness of mesh i is known 
    u64 inside; // bit #i == 1: if insideness i is known, then means is inside else is outside. Set to 0 if unknown. 

    BitVector(): known(0), inside(0) {}
    
    void setBit(int i) {known |= u64(1) << i; inside |= u64(1) << i; }
    void clearBit(int i) {known |= u64(1) << i; inside &= ~(u64(1) << i); }
    void setSide(int i, bool v) {
      // if(v) setBit(i); else clearBit(i); 
      known |= u64(1) << i; inside = (inside & ~(u64(1) << i)) | u64(v) << i;
    }
    void setUnknown(int i) {known &= ~(u64(1) << i); inside &= ~(u64(1) << i); }
    void flipBit(int i) {inside ^= u64(1) << i; }
    void toString(int l, char *s) const; 
    std::string toString(int l) const; 
    bool isKnown(int i) const {return (known >> i) & 1; }
    bool bit(int i) const {return (inside >> i) & 1; }

    void copyBitFrom(const BitVector &other, int src, int dest) {
      u64 destmask = u64(1) << dest; 
      inside = (inside & ~destmask) | (((other.inside >> src) & 1) << dest); 
      known  = (known  & ~destmask) | (((other.known  >> src) & 1) << dest); 
    }
    
    void orEq(const BitVector &other) {
      inside |= other.inside;
      known |= other.known;
    }

    void setAllKnown(int nmesh) {
      known |= (u64(1) << nmesh) - 1; 
    }

    bool cmpKnown(const BitVector & other) {
      return known == other.known;
    }

    int nbKnown() const {
      return bitcount(known);
    }

    int lastUnknown() const {
      u64 k = known; 
      for(int count = 0; count < 64; count++) {
        if(k == 0) return count - 1; 
        k >>= 1;
      }
      return 64;
    }


    // ternary = +1, 0, -1 for inside, outside, unknown

    int getTernary(int i) const {
      // if(!isKnown(i)) return -1; 
      // return bit(i); 
      return ((int((known >> i) & 1)) - 1) | ((inside >> i) & 1); 
    }
    
    void setTernary(int i, int t) {
      // if(t < 0) setUnknown(i); 
      // else setSide(i, t); 
      
      u64 mask = u64(1) << i; 
      u64 knownbit = u64(((t >> 1) ^ 1) & 1) << i;
      known  = (known  & ~mask) | knownbit; 
      inside = (inside & ~mask) | (knownbit & u64(t & 1) << i);       
    }

  };

  
  
  struct CSGOperation {

    // From the insideness of a set of meshes, determine if we are
    // inside the result mesh
    // returns 1 = inside 
    // 0 = outside 
    // -1 = unknown 
    virtual int isInside(const BitVector &meshpos) = 0;   
    
    virtual ~CSGOperation() {}

    // compute insideness at 2 sides of mesh, meshpos should be fully known
    // 2 bits used. Changes meshpos!
    int compute_in2(BitVector &meshpos, int meshno) {
      meshpos.clearBit(meshno); 
      int in0 = isInside(meshpos);
      meshpos.setBit(meshno);
      int in1 = isInside(meshpos);
      return in0 << 4 | in1; 
    }

    int in2(const BitVector &meshpos, int meshno) {
      BitVector meshpos2 = meshpos; 
      return compute_in2(meshpos2, meshno); 
    }
  
    // compute insideness at intersections of 2 meshes (quadrant)
    // 4 bits used
    int compute_in4(BitVector &meshpos, int meshno0, int meshno1) {
      meshpos.clearBit(meshno0);
      int in0 = compute_in2(meshpos, meshno1); 
      meshpos.setBit(meshno0);
      int in1 = compute_in2(meshpos, meshno1); 
      return in0 << 8 | in1; 
    }

    int in4(const BitVector &meshpos, int meshno0, int meshno1) {
      BitVector meshpos2 = meshpos; 
      return compute_in4(meshpos2, meshno0, meshno1); 
    }

    // compute insideness at 8 sides (octant)
    // 8 bits used
    int compute_in8(BitVector &meshpos, int meshno0, int meshno1, int meshno2) {
      meshpos.clearBit(meshno0);
      int in0 = compute_in4(meshpos, meshno1, meshno2); 
      meshpos.setBit(meshno0);
      int in1 = compute_in4(meshpos, meshno1, meshno2); 
      return in0 << 16 | in1; 
    }

    int in8(const BitVector &meshpos, int meshno0, int meshno1, int meshno2) {
      BitVector meshpos2 = meshpos; 
      return compute_in8(meshpos2, meshno0, meshno1, meshno2); 
    }

  };

  // definition of op1, op2, op3

  inline bool is_corner_in2(int in2) {
    if(((in2 ^ (in2 >> 4)) & 1) == 0) return 0; 
    return 1; 
  }
  
  inline bool is_corner_in4(int in4) {
    if(((in4 ^ (in4 >> 4)) & 0x0101) == 0) return 0; 
    if(((in4 ^ (in4 >> 8)) & 0x0011) == 0) return 0; 
    return 1; 
  }

  inline bool is_corner_in8(int in8) {
    if(((in8 ^ (in8 >> 4)) & 0x01010101) == 0) return 0; 
    if(((in8 ^ (in8 >> 8)) & 0x00110011) == 0) return 0; 
    if(((in8 ^ (in8 >> 16)) & 0x00001111) == 0) return 0; 
    return 1; 
  }
  

  struct CSGIntersection: CSGOperation {
    int nmesh; 
    
    CSGIntersection(int nmesh): nmesh(nmesh) {}
    
    virtual int isInside(const BitVector &meshpos) {
      u64 mask = (u64(1) << nmesh) - 1;
      if(meshpos.inside == mask) return 1; // all known and inside 
      if((meshpos.inside | (meshpos.known ^ mask)) == mask) // either inside or not known 
        return -1; 
      return 0;
    }

  }; 

  
  struct CSGUnion: CSGOperation {
    int nmesh; 
    
    CSGUnion(int nmesh): nmesh(nmesh) {}
    
    virtual int isInside(const BitVector &meshpos) {
      u64 mask = (u64(1) << nmesh) - 1;
      if(meshpos.inside) return 1; // at least one inside
      if(meshpos.known != mask) return -1; // there is still hope
      return 0;
    }
    
  }; 
  
  
  struct CSGMinMesh: CSGOperation {
    int nmesh, nmin; 
    
    CSGMinMesh(int nmesh, int nmin): nmesh(nmesh), nmin(nmin) {}
    
    virtual int isInside(const BitVector &meshpos) {
      int lowBound = bitcount(meshpos.known & meshpos.inside); 
      if(lowBound >= nmin) return 1;
      u64 mask = (u64(1) << nmesh) - 1;
      int upBound = bitcount((~meshpos.known & mask) | meshpos.inside); 
      if(upBound < nmin) return 0; 
      return -1; // cannot conclude
    }
    
  }; 

  struct CSGMinMaxMesh: CSGOperation {
    int nmesh, nmin, nmax; 
    
    CSGMinMaxMesh(int nmesh, int nmin, int nmax): nmesh(nmesh), nmin(nmin), nmax(nmax) {}
    
    virtual int isInside(const BitVector &meshpos) {
      int lowBound = bitcount(meshpos.known & meshpos.inside); 
      if(lowBound > nmax) return 0;
      u64 mask = (u64(1) << nmesh) - 1;
      int upBound = bitcount((~meshpos.known & mask) | meshpos.inside); 
      if(upBound < nmin) return 0;
      if(nmin <= lowBound && upBound <= nmax) return 1;
      return -1; // cannot conclude
    }
    
  }; 
  
  struct CSGDiff: CSGOperation {
    int nmesh, npos; 
    
    CSGDiff(int nmesh, int npos): nmesh(nmesh), npos(npos) {}
    
    virtual int isInside(const BitVector &meshpos) {
      bool anyInside, posKnown;
      u64 mask = (u64(1) << npos) - 1;

      posKnown = ((meshpos.known & mask) == mask); 
      anyInside = (meshpos.known & mask & meshpos.inside) != 0;

      if(posKnown && !anyInside) return 0; 
      
      mask = ((u64(1) << nmesh) - 1) & ~mask;
      if(meshpos.known & mask & meshpos.inside) return 0; 

      bool negKnown = ((meshpos.known & mask) == mask);

      if(negKnown && anyInside) return 1;
      
      return -1;
    }

  }; 
  
  // (A0 U A1 U ... U Anpos-1) inter (Anpos U ... U Anmesh - 1)
  struct CSGInter2Union: CSGOperation {
    int nmesh, npos; 
    
    CSGInter2Union(int nmesh, int npos): nmesh(nmesh), npos(npos) {}
    
    virtual int isInside(const BitVector &meshpos) {
      u64 mask1 = (u64(1) << npos) - 1;
      u64 mask2 = ((u64(1) << nmesh) - 1) ^ mask1;
      
      bool any1pos = (meshpos.inside & mask1) != 0; 
      bool any2pos = (meshpos.inside & mask2) != 0; 
      
      if(any1pos && any2pos) return 1; 

      bool all1neg = (meshpos.known & mask1) == mask1 && (meshpos.inside & mask1) == 0; 
      bool all2neg = (meshpos.known & mask2) == mask2 && (meshpos.inside & mask2) == 0; 
      
      if(all1neg || all2neg) return 0; 

      return -1;
    }

  }; 
  
  struct CSGXOR: CSGOperation {
    int nmesh; 
    
    CSGXOR(int nmesh): nmesh(nmesh) {}
    
    virtual int isInside(const BitVector &meshpos) {
      u64 mask = (u64(1) << nmesh) - 1;
      if(meshpos.known != mask) return -1; 
      return bitcount(meshpos.inside) % 2; 
    }
    
  }; 
  
  struct ThreeMeshOp: CSGOperation {
    //  ThreeMeshOp(int nmesh) {assert(nmesh == 3);}
    
    virtual bool isInside3(bool a, bool b, bool c) = 0;
    
    virtual int isInside(const BitVector &meshpos) {
      u64 mask = 7;
      if(meshpos.known != mask) return -1; 
      return isInside3(meshpos.inside & 1, 
                       (meshpos.inside >> 1) & 1, 
                       (meshpos.inside >> 2) & 1); 
    }
    
    
    
  }; 
  
  struct CSGAmBuC: CSGOperation {
    
    virtual bool isInside3(bool a, bool b, bool c) {
      return c && !(a || b); 
    }
    
  };
  
  struct CSGAuBmC: ThreeMeshOp {
    
    //  CSGAuBmC(
    virtual bool isInside3(bool a, bool b, bool c) {
      return (a || b) && !c; 
    }
    
  };
  
  struct CSGBuCmA: ThreeMeshOp {
    
    virtual bool isInside3(bool a, bool b, bool c) {
      return (c || b) && !a; 
    }
    
  };

  struct CSGAmBiC: ThreeMeshOp {
    
    virtual bool isInside3(bool a, bool b, bool c) {
      return a && !b && c; 
    }
    
  };

  struct CSGTable: CSGOperation {
    int nmesh;
    unsigned long table; 
    
    CSGTable(int nmesh, unsigned long table): nmesh(nmesh), table(table) {}
    
    virtual int isInside(const BitVector &meshpos) {
      if(meshpos.known != ((u64(1) << nmesh) - 1)) return -1;
      return (table >> meshpos.inside) & 1;
    }
    
  }; 

  struct CSGLargeTable: CSGOperation {
    int nmesh;
    std::vector<unsigned char> table; 

    CSGLargeTable(int nmesh, const char * filename): nmesh(nmesh) {
      FILE *f = fopen(filename, "r"); 
      assert(f); 
      for(int i = 0; i < (1<<nmesh) / 8; i++){
	int ret = getc(f); 
	assert(ret >= 0); 
	table.push_back(ret); 
      }
      fclose(f); 
    }

    virtual int isInside(const BitVector &meshpos) {
      if(meshpos.known != ((u64(1) << nmesh) - 1)) return -1;
      int i = meshpos.inside;
      return (table[i >> 3] >> (i & 7)) & 1;
    }    

  };
  

  // basic operations on ternary representation (+1, 0, -1)

  static int oand(int a, int b) {
    /*
    if(a == 0 || b == 0) return 0; 
    if(a == 1 && b == 1) return 1; 
    return -1; 
    */
    return -(-a & -b); 
    
  }
  
  static int oor(int a, int b) {
    /*
    if(a == 1 || b == 1) return 1; 
    if(a == 0 && b == 0) return 0; 
    return -1; 
    */
    return -(-a | -b);
  }
  
  static int onot(int a) {
    /*
    if(a == 0) return 1; 
    if(a == 1) return 0; 
    return -1; 
    */
    return (a >> 1) | (a ^ 1);
  }

  static int oxor(int a, int b) {
    if(a == -1 || b == -1) return -1;
    return a ^ b;
  }

  struct CSGDither: CSGOperation {
    /* 
       compute (m[0] & sel) | (m[1] & !sel)
       with sel = m[2] | (m[3] & m[4] & m[5])
     */

    virtual int isInside(const BitVector &meshpos) {     
      int m[6]; 
      for(int i = 0; i < 6; i++) m[i] = meshpos.getTernary(i);

      int sel = oor(m[2], oand(m[3], oand(m[4], m[5]))); 

      return oor(oand(m[0], sel), oand(m[1], onot(sel)));
    }
    
  }; 
  
  struct CSGProgram: CSGOperation {
    // Tree of CSG operations
    std::string instructions; 
    // instructions are: & = and, - = diff, | = or, + = or of the whole stack, 0..p = push this number

    CSGProgram(const char *instructions): instructions(instructions) {}

    virtual int isInside(const BitVector & meshpos) {
      BitVector stack;
      // stack pointer and instruction pointer 
      int sp = 0;
      const char *ip = instructions.c_str(); 
      for(;;) {
	char i = *ip++; 
	if(i == 0) break;
	if('0' <= i && i < '0' + 64) {
	  // push index to stack
	  stack.copyBitFrom(meshpos, i - '0', sp++);	  
	  //printf("push %d ?= %d from %d sp %d\n", 
	  //	stack.getTernary(sp - 1), meshpos.getTernary(i - '0'), i - '0', sp);
	} else if(i == '+') {
	  u64 mask = (u64(1) << sp) - 1;
	  int res; 
	  if(stack.inside)             res = 1; // at least one inside
	  else if(stack.known != mask) res = -1; // there is still hope
	  else res = 0;
	  stack.setTernary(0, res); 
	  sp = 1;
	} else { // binary op
	  int b = stack.getTernary(--sp);
	  int a = stack.getTernary(--sp);
	  int res; 
	  switch(i) {
	  case '|': res = oor(a, b); break;  
	  case '&': res = oand(a, b); break; 
	  case '-': res = oand(a, onot(b)); break;
	  case '*': res = oxor(a, b); break;
	  default: assert(!"unknown op");
	  }
	  //printf("sp %d %c %d %d -> %d\n", sp, i, a, b, res); 
	  stack.setTernary(sp++, res);
	}	
      }
      assert(sp == 1); 
      return stack.getTernary(--sp);
    }
    
  };
};


#endif
