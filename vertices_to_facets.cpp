/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */


#include <cstdio>
#include <cmath>


#include "mCSG.hpp"
#include "PolygonTesselator.hpp"


using namespace mCSG;




// edge -> facet transformation, triangulation





/****************************************************************
 * Transforms the mesh table into facets
 ****************************************************************/


#ifdef ENABLE_TBB

struct MakeFacetCallback {
        const std::vector<int> &fnos;
        int * facet_ends_shift;
        const std::vector<int> &vertexNos;
        CSG *csg;
        CSGOperation *csgop;
        
        void operator()(const tbb::blocked_range<int>&r) const{
            for (int j=r.begin() ; j!=r.end(); j++ ){
                int i = fnos[j];
                int l0 = facet_ends_shift[i]; 
                int l1 = facet_ends_shift[i + 1];
                if(!csg->facetProcessingStats.empty()) csg->facetProcessingStats[i].begin();      
                csg->makeFacet(csgop, i, &vertexNos[l0], l1 - l0); 
                if(!csg->facetProcessingStats.empty()) csg->facetProcessingStats[i].end();      
            }
        }
};
#endif // ENABLE_TBB



#ifdef ENABLE_SORTINGFACETS
#ifdef  ENABLE_TBB




// Parallel count vertices per facet
struct CountVertexFacetCallback {
        const int f0;
        const int f1;
        const std::vector<Vertex>  &vertices;
        std::vector<tbb::atomic<int>> &n_vertex_per_facet;
        
  void operator()(const tbb::blocked_range<int>&r) const{
    for (int i=r.begin() ; i!=r.end(); i++ ){
        const Vertex & v = vertices[i]; 
        if(v.keep != 1) continue; 
        for(int j = 0; j < v.facetNos.size(); j++) {
            int fno = v.facetNos[j]; 
            if(f0 <= fno && fno < f1) 
                n_vertex_per_facet[fno - f0]++;  // atomic add
        }
    }
  }
};


// For parallel Scan (to compute in parallel facet offsets)

class BodyScan {
        int sum;
        std::vector<tbb::atomic<int>> * y;
        const  std::vector<tbb::atomic<int>>*  x; 
    public:
        BodyScan( std::vector<tbb::atomic<int>>* y_,  const std::vector<tbb::atomic<int>>* x_) : sum(0), x(x_), y(y_) {}
        int get_sum() const {return sum;}
        template<typename Tag>
            void operator()( const tbb::blocked_range<int>& r, Tag ) {
            int temp = sum;
            for( int i=r.begin(); i<r.end(); ++i ) {
                if( Tag::is_final_scan() )
                    {
                    (*y)[i] = temp;
                    }
                temp +=  (*x)[i].load(); 
            }
            sum = temp;
        }
        BodyScan( BodyScan& b, tbb::split ) : x(b.x), y(b.y), sum(0) {}
        void reverse_join( BodyScan& a ) { sum += a.sum;}
        void assign( BodyScan& b ) {sum = b.sum;}
};

// Parallel count vertices per facet
struct  MakeVertexNos{
        const int f0;
        const int f1;
        const std::vector<Vertex>  &vertices;
        std::vector<tbb::atomic<int>>   &facet_ends;
        std::vector<int> &vertexNos; 
        
  void operator()(const tbb::blocked_range<int>&r) const{
    for (int i=r.begin() ; i!=r.end(); i++ ){
        const Vertex & v = vertices[i]; 
        if(v.keep != 1) continue; 
        for(int j = 0; j < v.facetNos.size(); j++) {
            int fno = v.facetNos[j]; 
            if(f0 <= fno && fno < f1)
                {
                    //NEED to de a compare and swap -> get curent value +increment and them write result
                    //once we are sure we have a reserved destination address.
                    //atomic add on facet_ends -> points to next address
                    int x = facet_ends[fno - f0].fetch_and_increment();// do x+=y and return the old value of x
                    vertexNos[x] = i;
                }
        }
    }
  }
};


struct MakeFacetCallback2 {
        const int f0;
        const int f1;
        const std::vector<tbb::atomic<int>> &n_vertex_per_facet;
        const std::vector<tbb::atomic<int>> &facet_ends;
        const std::vector<int> &vertexNos;
        CSG *csg;
        CSGOperation *csgop;
        
        void operator()(const tbb::blocked_range<int>&r) const{
            for (int j=r.begin() ; j!=r.end(); j++ ){
                int n_vertex = n_vertex_per_facet[j];
                //skip empty facets
                if ( n_vertex != 0) {
                    int facet_start =(j == 0 ? 0:facet_ends[j]-n_vertex);
                    if(!csg->facetProcessingStats.empty()) csg->facetProcessingStats[j+f0].begin();
                    csg->makeFacet(csgop,j+f0,&vertexNos[facet_start],n_vertex);
                    if(!csg->facetProcessingStats.empty()) csg->facetProcessingStats[j+f0].end();
                }
            }
        }
};
#endif //ENABLE_TBB
#endif// ENABLE_SORTINGFACETS



void CSG::makeFacets(CSGOperation *csgop, int f0, int f1) {

    // Could be parallelized to do memcpy and filling the vertex_by_facet array. But leave it like this for the moement. Benefits may not be that significant.
  mergeVertexTables();

  if(f1 < 0) f1 = facets.size();

  int nfacet = f1 - f0;
  int nvertex = vertices.size();

  if(collect_processing_stats) 
      facetProcessingStats.resize(nfacet);  

  processingStats.preFacet.begin();  

#ifdef ENABLE_SORTINGFACETS


  // pass 1: count nb of output vertices per facet

   //after first pass, facetends[i] returns the starting id of the first vertex of facet i.
  //   At the end of the second pass it returns the id of the first vertex of facet i+1.
  std::vector<tbb::atomic<int> > facet_ends;
  facet_ends.resize(nfacet + 1, 0);  // atomic initialization: yes with copy constructor.
  
  std::vector<tbb::atomic<int> > n_vertex_per_facet; 
  n_vertex_per_facet.resize(nfacet + 1, 0);  // atomic initialization: yes with copy constructor.
  

#ifdef ENABLE_TBB
   if(parallel_nt != 1) {
       CountVertexFacetCallback countvertexfacet= {f0,f1,vertices,n_vertex_per_facet};
       tbb::parallel_for(tbb::blocked_range<int>(0,nvertex), countvertexfacet);
   } else
#endif         // ENABLE_TBB
   {
       for(int i = 0; i < nvertex; i++) {
           const Vertex & v = vertices[i]; 
           if(v.keep != 1) continue; 
           for(int j = 0; j < v.facetNos.size(); j++) {
               int fno = v.facetNos[j]; 
               if(f0 <= fno && fno < f1) 
                   n_vertex_per_facet[fno - f0]++;  // atomic add
           }
       }
   }
   


   
// Compute offsets in parallel
  // template use ?
#ifdef ENABLE_TBB
  if(parallel_nt != 1) {
      BodyScan  body(&facet_ends,&n_vertex_per_facet);
      parallel_scan(tbb::blocked_range<int>(0,f1-f0), body );//do I need to specify the grain size ?
      facet_ends[nfacet] = body.get_sum();
    } else
#endif //ENABLE_TBB
    {
       int ofs = 0; 
       for(int i = f0; i < f1; i++) {
           int nv = n_vertex_per_facet[i - f0];
           facet_ends[i - f0] = ofs; 
           ofs += nv; 
       }
       facet_ends[nfacet] = ofs;
    }


  IFV(2) printf("  makeFacets: total %d vertex references in facets\n", facet_ends.back());

  // pass 2: collect vertex nbs
  std::vector<int> vertexNos; 
  vertexNos.resize(facet_ends.back(), 0);


#ifdef ENABLE_TBB
  if(parallel_nt != 1) {
      MakeVertexNos mkvertexnos = {f0,f1,vertices,facet_ends,vertexNos};
      tbb::parallel_for(tbb::blocked_range<int>(0,nvertex), mkvertexnos);
  } else 
#endif
 {
     // write parallel version of this (parallel_foreach)
     for(int i = 0; i < nvertex; i++) {
         const Vertex & v = vertices[i]; 
         if(v.keep != 1) continue; 
         for(int j = 0; j < v.facetNos.size(); j++) {
             int fno = v.facetNos[j]; 
             if(f0 <= fno && fno < f1)
                 {
                 //NEED to de a compare and swap -> get curent value +increment and them write result
                 //once we are sure we have a reserved destination address.
                 //atomic add on facet_ends -> points to next address
                     int x = facet_ends[fno - f0].fetch_and_increment();// do x+=y and return the old value of x
                     vertexNos[x] = i;
                 }
         }
     }
 }


 
#ifdef ENABLE_TBB
  if(parallel_nt != 1) {
      MakeFacetCallback2 mkf = {f0, f1, n_vertex_per_facet,facet_ends,vertexNos, this, csgop};
      tbb::parallel_for(tbb::blocked_range<int>(0, f1-f0), mkf);
  } else 
#endif
  {
    for(int j = 0; j < f1-f0; j++) {
        int n_vertex = n_vertex_per_facet[j];
        //skip empty facets
        if ( n_vertex != 0) {
            int facet_start = (j == 0 ? 0:facet_ends[j]-n_vertex);
            if(!facetProcessingStats.empty()) facetProcessingStats[j+f0].begin();
            makeFacet(csgop,j+f0,&vertexNos[facet_start],n_vertex);
            if(!facetProcessingStats.empty()) facetProcessingStats[j+f0].end();
        }
    } 
  }
#else //ENABLE_SORTINGFACETS


  
  
  

  // pass 1: count nb of output vertices per facet
  std::vector<int> facet_ends;   
  {//open 1
    std::vector<int> &n_vertex_per_facet = facet_ends; 
    
    n_vertex_per_facet.resize(nfacet + 1, 0);     
    
    for(int i = 0; i < nvertex; i++) {
      const Vertex & v = vertices[i]; 
      if(v.keep != 1) continue; 
      for(int j = 0; j < v.facetNos.size(); j++) {
	int fno = v.facetNos[j]; 
	if(f0 <= fno && fno < f1) 
	  n_vertex_per_facet[fno - f0]++;     
      }
    }

    // cumulative counts
    int ofs = 0; 
    for(int i = f0; i < f1; i++) {
      int nv = n_vertex_per_facet[i - f0];
      facet_ends[i - f0] = ofs; 
      ofs += nv; 
    }
    facet_ends[nfacet] = ofs;
  }  

  IFV(2) printf("  makeFacets: total %d vertex references in facets\n", facet_ends.back());

  // pass 2: collect vertex nbs
  std::vector<int> vertexNos; 
  vertexNos.resize(facet_ends.back(), 0);
  
  for(int i = 0; i < nvertex; i++) {
    const Vertex & v = vertices[i]; 
    if(v.keep != 1) continue; 
    for(int j = 0; j < v.facetNos.size(); j++) {
      int fno = v.facetNos[j]; 
      if(f0 <= fno && fno < f1) 
	vertexNos[facet_ends[fno - f0]++] = i;
    }
  }
  
  // shift array back
  for(int i = nfacet; i > 0; i--) {
    facet_ends[i] = facet_ends[i - 1]; 
  }
  facet_ends[0] = 0;


  // collect non-0 facet nbs
  std::vector<int> fnos;

  for(int i = 0; i < nfacet; i++) {
    int l0 = facet_ends[i]; 
    int l1 = facet_ends[i + 1];    
    if(l1 == l0) continue; 
    fnos.push_back(i + f0);
  }  
  
  processingStats.preFacet.end();

  // shifted to skip over the first f0 facets
  int * facet_ends_shift = &facet_ends[0] - f0;


  
#ifdef ENABLE_TBB
  if(parallel_nt != 1) {
    MakeFacetCallback mkf = {fnos, facet_ends_shift, vertexNos, this, csgop};
    tbb::parallel_for(tbb::blocked_range<int>(0, fnos.size()), mkf);
  } else 
#endif
  {
    for(int j = 0; j < fnos.size(); j++) {
      int i = fnos[j];
      int l0 = facet_ends_shift[i]; 
      int l1 = facet_ends_shift[i + 1];
      if(!facetProcessingStats.empty()) facetProcessingStats[j].begin();
      makeFacet(csgop, i, &vertexNos[l0], l1 - l0); 
      if(!facetProcessingStats.empty()) facetProcessingStats[j].end();
    } 
  }
#endif //ENABLE_SORTINGFACETS

}//close 1





/****************************************************************
 * Tables of half-edges
 ****************************************************************/

// pair of half-edges that passes through a vertex
struct HEPair {
  int vno;   // origin vertex
  bool used; // was already included in a loop

  struct HalfEdge {
    int fno;    // facet number we are following
    bool sense; // for E1's: same sense as polygon? 
                // for E2's: same sense as cross(f0, fno)?
    real t;   // offset on edge (set to non-nan iff there are more than 2 candidate vertices on edge)
    bool eq(const HalfEdge & other) {return fno == other.fno && sense == other.sense; }
    std::string toString() {
      char buf[100]; 
      sprintf(buf, "F%d%c (t=%g)", fno, sense ? '+' : '-', t); 
      return buf;
    }
  }; 

  HalfEdge in, out; // coming in and going out vno 

  HEPair(int vno, int fno0, bool sense0, int fno1, bool sense1):
    vno(vno) {
    assert(fno0 != fno1);
    in.fno = fno0; in.sense = sense0;  
    out.fno = fno1; out.sense = sense1;
    in.t = out.t = std::numeric_limits<real>::quiet_NaN();
    used = false; 
  } 

  std::string toString() {
    char buf[100]; 
    sprintf(buf, "V%d (%s) ", vno, used ? "used" : "not used"); 
    return std::string(buf) + in.toString() + " > " + out.toString(); 
  }
}; 

// table of HEPairs + indexing structure to quickly find the next
// vertex on a half-edge.
// There are two implementations: a brute-force n^2 version (faster
// when there are few vertices) and a version where the vertices are
// sorted and the next vertex is found by bissection (faster for many
// vertices).
class HEPairsIndex: public std::vector<HEPair> {
public:

  int last_begin; // where to start searching
  bool sorted; 
  int verbose; 

  HEPairsIndex() {
    last_begin = 0; 
    sorted = false;
    verbose = 0;
  }
  
  int find_first() {    
    for(; last_begin < size(); last_begin++) 
      if(!(*this)[last_begin].used) return last_begin;
    return -1;
  }


  void init_t_offsets(int f0, CSG & csg) {

    if(sorted) {init_t_offsets_sorted(f0, csg); return; }

    // prepare t offsets for edges that have more than 2 vertices (1 in vertex)
    HEPairsIndex & pairs = *this;    
    for(int i = 0; i < pairs.size(); i++) {
      HEPair &p0 = pairs[i];       
      if(!std::isnan(p0.in.t)) continue; // already processed
      int fno = p0.in.fno;
      int nocc = 1; 
      for(int j = i + 1; j < pairs.size(); j++) 
        if(pairs[j].in.fno == fno) nocc++; 
      if(nocc > 1) {
        
        Vec3 dir = csg.edgeDirection(f0, fno); 
        dir /= dir.length(); // if f0, fno invalid pair, then generates nan that will make incomplete loops 
        IFV(4) printf("    computing offsets for F%d (%d occurrences) dir = (%g %g %g): ", 
                      fno, nocc, dir.x, dir.y, dir.z);
        for(int j = 0; j < pairs.size(); j++) {
          HEPair &pj = pairs[j];       
          if(pj.in.fno == fno) {
            pj.in.t = dir.dot(csg.vertices[pj.vno].coords); 
            IFV(4) printf("V%d in t=%g ", pj.vno, pj.in.t); 
          }
          if(pj.out.fno == fno) {
            pj.out.t = dir.dot(csg.vertices[pj.vno].coords); 
            IFV(4) printf("V%d out t=%g ", pj.vno, pj.out.t); 
          }
          
        }
        IFV(4) printf("\n"); 
      }	
    }
  }


  int find_next(int f0, int he_prev, bool forward = true) {
    if(sorted && forward) return find_next_sorted(f0, he_prev); 

    // brute-force search
    HEPairsIndex & pairs = *this;
    HEPair &p0 = pairs[he_prev];
    HEPair::HalfEdge &he0 = forward ? p0.out : p0.in;
    assert(he0.fno >= 0);
    int next = -1;
    
    real best_t = he0.sense ? HUGE_VAL : -HUGE_VAL, t0 = he0.t;
    for(int j = 0; j < pairs.size(); j++) {
      HEPair &pj = pairs[j]; 
      if(pj.used) continue; 
      HEPair::HalfEdge & hej = forward ? pj.in : pj.out;
      
      if(!hej.eq(he0)) 
        continue; 
      
      if(std::isnan(t0)) { // there is a single pair
        next = j; 
        break;
      }
      if(he0.sense) { // find the first one after t0
        if(hej.t > t0 && hej.t < best_t) {
	next = j; 
	best_t = hej.t;
        }
      } else {
        if(hej.t < t0 && hej.t > best_t) {
          next = j; 
	best_t = hej.t;
        }
      }
    }
    return next;
  }

  /**********************************************
   * sorted version 
   **********************************************/

  static bool cmp_he_in_fno(const HEPair & i, const HEPair & j) {
    if(i.in.sense != j.in.sense) return i.in.sense < j.in.sense; 
    return i.in.fno < j.in.fno; 
  }

  static bool cmp_he_in_t(const HEPair & i, const HEPair & j) {
    return i.in.t < j.in.t; 
  }

  struct CmpOutFno {
    const HEPairsIndex & pairs; 
    bool operator() (int i, int j) {
      const HEPair::HalfEdge & ip = pairs[i].out; 
      const HEPair::HalfEdge & jp = pairs[j].out; 
      if(ip.sense != jp.sense) return ip.sense < jp.sense;       
      return ip.fno < jp.fno; 
    }
  };


  void sort() {
    
    IFV(4) printf("       sorting %zd half-edges\n", size()); 
    
    // sort table according to "in" vertex
    std::sort(begin(), end(), cmp_he_in_fno); 
        
    IFV(5) {
      printf("         sorted in = ["); 
      for(int i = 0; i < size(); i++) printf("%s ", (*this)[i].in.toString().c_str());       
      printf("]\n");       
    }

    sorted = true;
  }

  int n_sense_neg; // number of HEPairs with sense = false

  void init_t_offsets_sorted(int f0, CSG & csg) {
    HEPairsIndex & pairs = *this;    

    // permutation where out's are sorted
    std::vector<int> perm;
    // sort permutation table according to "out" 
    perm.reserve(size()); 
    for(int i = 0; i < size(); i++) perm.push_back(i); 
    CmpOutFno cmp = { *this }; 
    std::sort(perm.begin(), perm.end(), cmp);
    
    IFV(4) printf("       initializing t's for %zd half-edges\n", size()); 

    IFV(5) {
      printf("         sorted in = ["); 
      for(int i = 0; i < size(); i++) printf("F%d ", (*this)[i].in.fno);       
      printf("]\n         perm = ["); 
      for(int i = 0; i < size(); i++) printf("%d:F%d ", perm[i], (*this)[perm[i]].out.fno);             
      printf("]\n");       
    }


    // initialize t's for in and out    
    int i0 = 0, j0 = 0; 
    n_sense_neg = 0;
    
    while(i0 < size()) {
      int fno = pairs[i0].in.fno;

      // IFV(4) printf("         start at HE %d, in.fno = F%d\n", i0, pairs[i0].in.fno);

      // find range for in
      int i1 = i0 + 1; 
      while(i1 < size() && pairs[i1].in.fno == fno) i1++; 

      if(!pairs[i0].in.sense) n_sense_neg += i1 - i0;

      // find range for out 
      while(j0 < size() && pairs[perm[j0]].out.fno < fno) j0++; 
      int j1 = j0; 
      while(j1 < size() && pairs[perm[j1]].out.fno == fno) j1++; 

      IFV(5) printf("           ranges in %d:%d out %d:%d\n", i0, i1, j0, j1);
      
      // will cause csg errors in next stages
      // assert(i0 == j0 && i1 == j1); 

      if(i1 > i0 + 1) { // initialize t iff there is more than 1 in 
        Vec3 dir = csg.edgeDirection(f0, fno); 
        dir /= dir.length(); // if f0, fno invalid pair, then generates nan that will make incomplete loops 
        IFV(4) printf("            computing offsets for F%d (HE pairs %d-%d) dir = (%g %g %g): ", 
                      fno, i0, i1, dir.x, dir.y, dir.z);
        for(int i = i0; i < i1; i++) {
          HEPair &pi = pairs[i];       
          assert(pi.in.fno == fno); 
          pi.in.t = dir.dot(csg.vertices[pi.vno].coords); 
          IFV(4) printf("V%d in t=%g ", pi.vno, pi.in.t); 
        }
        
        for(int j = j0; j < j1; j++) {
          HEPair &pj = pairs[perm[j]];       
          assert(pj.out.fno == fno); 
          pj.out.t = dir.dot(csg.vertices[pj.vno].coords); 
          IFV(4) printf("V%d out t=%g ", pj.vno, pj.out.t);                    
        }        
        IFV(4) printf("\n"); 
      }
      i0 = i1; 
      j0 = j1;
    }

    // 2nd pass to sort t's (invalidates perm)
    i0 = 0;
    while(i0 < size()) {
      int fno = pairs[i0].in.fno;
      // find range for in
      int i1 = i0 + 1; 
      while(i1 < size() && pairs[i1].in.fno == fno) i1++; 
      if(i1 > i0 + 1) 
        std::sort(begin() + i0, begin() + i1, cmp_he_in_t); 
      i0 = i1;
    }   
    
    IFV(5) {
      printf("         sorted HE table (n_sense_neg = %d):\n", n_sense_neg); 
      for(int i = 0; i < size(); i++) {
        HEPair &p = pairs[i]; 
        printf("           HE %d: V%d: F%d%c (t=%g) > F%d%c (t=%g)\n", 
               i,
               p.vno, 
               p.in.fno, p.in.sense ? '+' : '-', p.in.t, 
               p.out.fno, p.out.sense ? '+' : '-', p.out.t); 
      }
    }

  }

  int find_next_sorted(int f0, int he_prev) {
    HEPairsIndex & pairs = *this;
    HEPair &p0 = pairs[he_prev];
    HEPair::HalfEdge &he0 = p0.out;
    assert(he0.fno >= 0);        
    double t0 = he0.t;
    int fno = he0.fno;

    IFV(5) printf("             bissect to find %s\n", he0.toString().c_str());

    // bissect to find where he0 will go
    int r0 = 0, r1 = size(); 
    if(he0.sense) r0 = n_sense_neg; 
    else          r1 = n_sense_neg;

    IFV(5) printf("             bissect to find %s, range %d:%d\n", he0.toString().c_str(), r0, r1);

    int i0 = r0, i1 = r1; 
    while(i0 + 1 < i1) {      
      int imed = (i0 + i1) / 2;
      HEPair::HalfEdge &he = pairs[imed].in; 
      bool ge = std::isnan(t0) || t0 >= he.t; 
      if(fno > he.fno || (he.fno == fno && ge)) 
        i0 = imed; 
      else 
        i1 = imed;      

      IFV(6) printf("               imed %d -> %d:%d ge=%d he.fno=%d he.t=%g\n", imed, i0, i1, int(ge), he.fno, he.t);
    }    
    
    IFV(5) {
      printf("             bissection result: i0=%d [%s] i1=%d [%s]\n", 
             i0, pairs[i0].toString().c_str(), 
             i1, i1 < size() ? pairs[i1].toString().c_str() : "out of bounds");
    }
    

    if(std::isnan(t0)) {
      HEPair::HalfEdge &he1 = pairs[i0].in; 
      if(pairs[i0].used || !he1.eq(he0)) return -1;       
      return i0; 
    }      

    if(he0.sense) { 
      // special case because bissection cannot return i1 = 0
      if(i0 == r0 && pairs[i0].in.fno == fno && pairs[i0].in.t > t0) i1 = i0;        
      if(i1 >= r1) return -1; 
      HEPair::HalfEdge &he1 = pairs[i1].in; 
      if(pairs[i1].used || !he1.eq(he0)) return -1;       
      return i1; 
    } else {      
      HEPair::HalfEdge &he1 = pairs[i0].in;
      if(pairs[i0].used || !he1.eq(he0)) return -1;       
      if(i0 == r0 && t0 < he0.t) return -1; 
      return i0; 
    }      
    
  }


};





/****************************************************************
 * Find which half-edges to keep
 ****************************************************************/


// all pairs of Half edges in a facet
struct mCSG::HEPairs {
  HEPairsIndex same; // paths that can be used for a facet with the same orientation as the input facet
  HEPairsIndex diff; // same for the oposite facet

  void maybeAdd(int in, int mask, int cond, 
                      int vno, int fno0, bool sense0, int fno1, bool sense1) {
    // in: position wrt the implicatd meshes (2 or 4-bit bitfield)
    if((in & mask) == cond) 
      same.push_back(HEPair(vno, fno0, sense0, fno1, sense1)); 
    if((in & mask) == (cond ^ mask)) 
      diff.push_back(HEPair(vno, fno1, !sense1, fno0, !sense0));      
  }
  
  void maybeAddTriple(int in, int s0, int s1, int s2,
			     int vno, int fno0, bool sense0, int fno1, bool sense1) {
    int in0 = (in >> (s0 * 8)) & 0xff; 
    int in1 = (in >> (s1 * 8)) & 0xff; 
    int in2 = (in >> (s2 * 8)) & 0xff; 
    if(in0 == 0x01) {
      if(in1 != 0x01 && in2 != 0x01) 
	same.push_back(HEPair(vno, fno0, sense0, fno1, sense1)); 
      if(in1 == 0x01 && in2 == 0x01) 
	same.push_back(HEPair(vno, fno1, sense1, fno0, sense0)); 
    }
    if(in0 == 0x10) { 
      if(in1 != 0x10 && in2 != 0x10) 
	diff.push_back(HEPair(vno, fno1, !sense1, fno0, !sense0));      
      if(in1 == 0x10 && in2 == 0x10) 
	diff.push_back(HEPair(vno, fno0, !sense0, fno1, !sense1));           
    }
  }

};


void CSG::addHEPairsPrimary(int f0, int vno, CSGOperation *csgop, 
                            HEPairs & hepairs) {

  const Vertex & v = vertices[vno];

  // find neighboring facets
  int j, nf = v.facetNos.size();

  if(nf < 3) {
    errors.add("addHEPairsPrimary: primary vertex from < 3 facets on output mesh"); 
    return;
  }

  for(j = 0; j < nf; j++) 
    if(v.facetNos[j] == f0) break; 
  assert(j != nf); 
  int f1 = v.facetNos[(j + 1) % nf]; 
  int f2 = v.facetNos[(j - 1 + nf) % nf];  

  if(f1 == f2) {
    errors.add("addHEPairsPrimary: duplicate facet in primary vertex"); 
    return;
  }

  // register pairs
  int in2 = csgop->in2(v.meshpos, facets[f0].meshno);   

  IFV(4) printf("        addHEPairsPrimary F%d F%d: in2 = %02x\n", f1, f2, in2); 

  hepairs.maybeAdd(in2, 0x11, 0x01, vno, f1, true, f2, true);
}

void CSG::addHEPairsDouble(int f0, int vno, CSGOperation *csgop,
                            HEPairs & hepairs) {

  const Vertex & v = vertices[vno]; 

  int f1 = -1, f2 = -1; 
  // find f1 and f2
  for(int i = 0; i < 3; i++) {
    if(v.facetNos[i] != f0) {
      if(f1 == -1) f1 = v.facetNos[i]; 
      else         f2 = v.facetNos[i]; 
    }
  }
  const Facet &facet0 = facets[f0]; 
  
  bool e1 = facets[f1].meshno == facet0.meshno;
  bool e2 = facets[f2].meshno == facet0.meshno; 

  if(e1) {
    assert(!e2); 
    addHEPairsDouble2(f0, f1, f2, vno, csgop, hepairs); 
  } else {
    if(e2) 
      addHEPairsDouble2(f0, f2, f1, vno, csgop, hepairs); 
    else 
      addHEPairsDouble1(f0, f1, f2, vno, csgop, hepairs);       
  }

}



void CSG::addHEPairsDouble1(int f0, int f1, int f2, int vno, CSGOperation *csgop,
                            HEPairs & hepairs) {
  const Vertex & v = vertices[vno]; 
  const Facet &facet0 = facets[f0]; 
  
  { // choose f1, f2 order
    const Facet &facet1 = facets[f1]; 
    int nv = facet1.vertexNos.size();
    int i; 
    for(i = 0; i < nv; i++)
      if(facet1.oppositeFacetNos[i] == f2) break; 
    assert(i != nv); 

    // edge between f1 and f2
    int vno0 = facet1.vertexNos[(i + nv - 1) % nv];
    const Vec3 &v0 = vertices[vno0].coords; 
    int vno1 = facet1.vertexNos[i];
    const Vec3 &v1 = vertices[vno1].coords;     
    
    real dp = facet0.normal.dot(v1 - v0); 
    
    if(dp > 0) std::swap(f1, f2);
  }  
   
  int in4 = csgop->in4(v.meshpos, facet0.meshno, facets[f1].meshno); 

  IFV(4) printf("        addHEPairsDouble1 F%d F%d: in4 = %04x\n", f1, f2, in4); 
  
  hepairs.maybeAdd(in4, 0x1010, 0x0010, vno, f1, true, f2, true);  
  hepairs.maybeAdd(in4, 0x0101, 0x0001, vno, f2, false, f1, false); 

}

void CSG::addHEPairsDouble2(int f0, int f1, int f2, int vno, CSGOperation *csgop,
                            HEPairs & hepairs) {
  const Vertex & v = vertices[vno]; 
  const Facet &facet2 = facets[f2]; 
  bool f2_sense; 
  
  const Facet &facet0 = facets[f0]; 

  
  { // determine sense of F2
    int nv = facet0.vertexNos.size();
    int i; 
    for(i = 0; i < nv; i++)
      if(facet0.oppositeFacetNos[i] == f1) break; 
    if(i == nv) {
      errors.add("addHEPairsDouble2: inconsistent opposite facets");
      return; 
    }

    // edge between f1 and f2
    int vno0 = facet0.vertexNos[(i + nv - 1) % nv];
    const Vec3 &v0 = vertices[vno0].coords; 
    int vno1 = facet0.vertexNos[i];
    const Vec3 &v1 = vertices[vno1].coords;     
    
    real dp = (v1 - v0).dot(facet2.normal); 
    //printf("vno0=%d vno1=%d norm2 = %g %g %g dp = %g\n", vno0, vno1, facet2.normal.x, facet2.normal.y, facet2.normal.z, dp);
    f2_sense = (dp < 0);
  }  
   
  int in4 = csgop->in4(v.meshpos, facet0.meshno, facet2.meshno); 

  IFV(4) printf("        addHEPairsDouble2 F%d F%d (sense %d): in4 = %04x\n", f1, f2, (int) f2_sense, in4); 

  if(f2_sense) {
    
    hepairs.maybeAdd(in4, 0x1010, 0x0010, vno, f1, true, f2, true); 
    hepairs.maybeAdd(in4, 0x0101, 0x0001, vno, f2, false, f1, true); 
    
  } else {
    
    hepairs.maybeAdd(in4, 0x1010, 0x0010, vno, f2, true, f1, true); 
    hepairs.maybeAdd(in4, 0x0101, 0x0001, vno, f1, true, f2, false); 
    
  }

}

void CSG::addHEPairsTriple(int f0, int vno, CSGOperation *csgop,
                           HEPairs &hepairs) {
  
  const Vertex & v = vertices[vno]; 
  const Facet &facet0 = facets[f0]; 
  
  int f1 = -1, f2 = -1; 
  // find f1 and f2
  for(int i = 0; i < 3; i++) {
    if(v.facetNos[i] != f0) {
      if(f1 == -1) f1 = v.facetNos[i]; 
      else         f2 = v.facetNos[i]; 
    }
  }

  { // std orientation
    real dp = facets[f1].normal.cross(facets[f2].normal).dot(facet0.normal);
    if(dp > 0) std::swap(f1, f2);
  }

  int in8 = csgop->in8(v.meshpos, facets[f2].meshno, facets[f1].meshno, facet0.meshno); 

  IFV(4) printf("        addHEPairsTriple F%d F%d: in8 = %08x\n", f1, f2, in8); 

  hepairs.maybeAddTriple(in8, 0, 1, 2, vno, f2, false, f1, false); 
  hepairs.maybeAddTriple(in8, 3, 1, 2, vno, f2, true, f1, true); 
  hepairs.maybeAddTriple(in8, 2, 0, 3, vno, f1, false, f2, true); 
  hepairs.maybeAddTriple(in8, 1, 0, 3, vno, f1, true, f2, false); 

}

/****************************************************************
 * Build loops from HE tables
 ****************************************************************/


bool CSG::makeFacetOnlyPrimaries(CSGOperation *csgop, int f0, const int *vnos, int nv) {
  // try easy case: only primary points
  
  for(int i = 0; i < nv; i++) {
    int vno = vnos[i]; 
    if(!(vno < nvert1)) return false;
  }

  Facet & facet = facets[f0]; 
  
  BitVector meshpos = vertices[vnos[0]].meshpos; 
  int meshno = facet.meshno; 

  int in2 = csgop->in2(meshpos, meshno);

  IFV(2) printf("      Facet with only primary points, in2 = %02x\n", in2);
  
  if(nv != facet.vertexNos.size()) {
    errors.add("makeFacetOnlyPrimaries: not same nb of primary vertices as in input");
    return false;
  }

  for(int i = 1; i < nv; i++) {
    BitVector meshpos_i = vertices[vnos[i]].meshpos;     
    if(in2 != csgop->in2(meshpos_i, meshno)) {
      errors.add("makeFacetOnlyPrimaries: non uniform mesh position on vertices");
      return false;
    } 
  }
    

  if(in2 == 0x01) {
    facet.outputVertexNos = facet.vertexNos; 
    facet.n_loops_same = 1; 
  } else if(in2 == 0x10) {
    for(int i = 0; i < nv; i++)
      facet.outputVertexNos.push_back(facet.vertexNos[nv - 1 - i]);
    facet.n_loops_same = 0; 
  } else
    assert(false);

  facet.loops.push_back(nv);
  
  return true;
}
  
void CSG::makeFacet(CSGOperation *csgop, int f0, const int *vnos, int nv) {

  if(f0 == facetToSave) saveFacetToEPS(facetFname, f0, vnos, nv);

  Facet & facet = facets[f0]; 

  IFV(2) {
    printf("  Facet %d (mesh %d):", f0, facet.meshno); 
    
    IFV(3) {
      printf("vertices ["); 
      for(int l = 0; l < nv; l++) {
        int vno = vnos[l]; 
        const Vertex & v = vertices[vno];         
        printf("V%d %s (", vno, v.meshpos.toString(nmesh).c_str());         
        for(int j = 0; j < v.facetNos.size(); j++) 
          if(v.facetNos[j] != f0) printf("F%d ", v.facetNos[j]);
        printf(")  ");        
      }
      printf("]\n"); 
    } else  printf(" %d vertices\n", nv); 
  }

  // Fast path for facets with only primary vertices.
  if(makeFacetOnlyPrimaries(csgop, f0, vnos, nv)) {
    if(tesselate) tesselateFacet(facet);
    return; 
  } 
  
  IFV(3) printf("    Building half-edge pairs\n"); 

  HEPairs hepairs; 
  hepairs.same.verbose = hepairs.diff.verbose = verbose;

  for(int i = 0; i < nv; i++) {
    int vno = vnos[i]; 
    const Vertex &v = vertices[vno];
    
    IFV(3) printf("      Adding HE pairs from V%d\n", vno); 

    switch(nmesh - v.meshpos.nbKnown()) { // primary, double, triple vertex? 
    case 1:
      addHEPairsPrimary(f0, vno, csgop, hepairs); 
      break;    
    case 2:      
      assert(v.facetNos.size() == 3); 
      addHEPairsDouble(f0, vno, csgop, hepairs); 	
      break;
    case 3: 
      assert(v.facetNos.size() == 3); 
      addHEPairsTriple(f0, vno, csgop, hepairs); 
      break;
    default: 
      assert(!"weird nb of unknown sides\n");
    }

    IFV(3) {
      for(int sd = 0; sd < 2; sd++) {
	HEPairsIndex &pairs = sd == 0 ? hepairs.same : hepairs.diff; 
	printf("        %s:", sd == 0? "same" : "diff"); 
	for(int i = 0; i < pairs.size(); i++) {
	  HEPair &p = pairs[i]; 
	  if(p.vno != vno) continue; 
	  printf("   F%d%c > F%d%c", 
		 p.in.fno, p.in.sense ? '+' : '-', 
		 p.out.fno, p.out.sense ? '+' : '-'); 
	}
	printf("\n"); 
      }
    }

  }
  
  facet.n_loops_same = 0;
  for(int sd = 0; sd < 2; sd++) { // consider the two sides
    HEPairsIndex &pairs = sd == 0 ? hepairs.same : hepairs.diff; 

    bool sorting = pairs.size() > sort_nvertex; 
    IFV(3) printf("    Building output facet for orientation %s, %zd vertices (%s)\n", 
		  sd ? "diff" : "same", pairs.size(), sorting ? "sorting" : "not sorting");

    if(sorting) pairs.sort();

    pairs.init_t_offsets(f0, *this); 

    for(;;) {
      // find starting point
      int i; 

      i = pairs.find_first(); 

      if(i == -1) {
	IFV(4) printf("      no more he pairs\n"); 
	break; 
      }
    
      int he_init = i, he_prev = i;      
      IFV(4) {
	HEPair &p0 = pairs[he_prev]; 
	printf("        start from hepair %d: V%d F%d%c\n", 
	       he_prev, p0.vno, p0.out.fno, p0.out.sense ? '+' : '-'); 
      }   

      // follow loop
      bool bad_loop = false;
      for(;;) {
	
	int next = pairs.find_next(f0, he_prev);
	if(next == -1) {
	  IFV(3) printf("        WARN could not find next facet\n"); 
	  bad_loop = true;
	  break;
	}

	HEPair &p1 = pairs[next]; 
	
	IFV(4)
	  printf("          next step %d: V%d F%d%c > F%d%c\n", 
		 next, p1.vno, 
		 p1.in.fno, p1.in.sense ? '+' : '-',
		 p1.out.fno, p1.out.sense ? '+' : '-'); 

	facet.outputVertexNos.push_back(p1.vno); 

	p1.used = true; // break so it will not be used by another loop  

	if(next == he_init) {
	  IFV(4) printf("        loop closed\n"); 
	  break; 
	}
	he_prev = next; 
      }  

      if(!bad_loop) { // normal case
	pairs[he_prev].used = true; 
	facet.loops.push_back(facet.outputVertexNos.size());
        if(sd == 0) facet.n_loops_same = facet.loops.size();
      } else {
        pairs[he_init].used = true; // don't try this one again
        // remove unused points 
        facet.outputVertexNos.resize(facet.loops.size() > 0 ? facet.loops.back() : 0, -1);
        errors.add("makeFacet: could not find closed loop");
      }
    }
  }

  IFV(2) printf("    Output %d + %zd loops\n", facet.n_loops_same, facet.loops.size() - facet.n_loops_same); 
  
  if(tesselate) tesselateFacet(facet);


}

/*****************************************************************
 * Facet tesselization
 ****************************************************************/

static bool is_convex(const std::vector<Vertex> & vertices, 
                      const Facet::IndexTable & vnos, 
                      const Vec3 & normal, 
                      int i0, int i1, int verbose) {

  Vec3 p0 = vertices[vnos[i1 - 2]].coords; 
  Vec3 p1 = vertices[vnos[i1 - 1]].coords; 
  bool has_pos = false, has_neg = false; 

  IFV(5) printf("        is_convex vertices loop %d, %d\n", i0, i1); 

  for(int i = i0; i < i1; i++) {
    Vec3 p2 = vertices[vnos[i]].coords; 
    
    real dp = normal.dot((p2 - p1).cross(p0 - p1)); 
    
    IFV(5) printf("           V%d: dp=%g\n", vnos[i == i0 ? i1 - 1 : i - 1], dp); 

    if(dp > 0) has_pos = true;
    if(dp < 0) has_neg = true; 

    p0 = p1; p1 = p2;   
  }

  return !(has_pos && has_neg); 
}

static void needTesselate(const std::vector<Vertex> & vertices, const Facet &facet, 
                          bool *tess_same, bool *tess_diff, int verbose) {
   

  int n_same = facet.n_loops_same;
  int n_diff = facet.loops.size() - n_same;

  IFV(4) printf("    needTesselate input: n_same=%d n_diff=%d\n", n_same, n_diff); 

  if(n_same == 0) *tess_same = false; 
  else if(n_same > 1) *tess_same = true; 
  else *tess_same = !is_convex(vertices, facet.outputVertexNos, facet.normal, 0, facet.loops[0], verbose); 

  if(n_diff == 0) *tess_diff = false; 
  else if(n_diff > 1) *tess_diff = true; 
  else *tess_diff = !is_convex(vertices, facet.outputVertexNos, -facet.normal, 
                              n_same == 0 ? 0 : facet.loops[n_same - 1], 
                              facet.loops.back(), verbose); 
 
  IFV(4) printf("    needTesselate: same: %d diff: %d\n", (int)*tess_same, (int)*tess_diff); 
  
}

class FacetTesselator: public PolygonTesselator {
  
public:
  
  const std::vector<Vertex> & vertices;
  int verbose; 

  FacetTesselator(const std::vector<Vertex> & vertices, bool just_triangles, int verbose): 
    PolygonTesselator(just_triangles), vertices(vertices), verbose(verbose) {    
  }

  Vec3 getCoords(int i) const {
    return vertices[i].coords;
  }

  virtual void emitPolygon(int n, const int *idx) {
    assert(!just_triangles || n == 3);
    
    IFV(6) {
      printf("             emitPolygon [ "); 
      for(int i = 0; i < n; i++) printf("V%d ", idx[i]); 
      printf("]\n");
    }    

    for(int i = 0; i < n; i++) vertexNos_out.push_back(idx[i]);
    loops_out.push_back(vertexNos_out.size());
  }

  VectorPreferredSize<4,int> vertexNos_out;
  VectorPreferredSize<4,int> loops_out;

  // tesselate loops i0:i1
  // input p0 is the first point of the first loop
  // output p0 is the first point not used by the loops
  void tesselateFacetSide(const Facet &facet, int i0, int i1, int &p0, bool revert_normal) {
    if(!revert_normal)
      tessNormal(facet.normal.x, facet.normal.y, facet.normal.z); 
    else
      tessNormal(-facet.normal.x, -facet.normal.y, -facet.normal.z); 
      
    beginPolygon();
    
    IFV(6) printf("        tesselating loops %d:%d\n", i0, i1); 
    for(int i = i0; i < i1; i++) {
      int p1 = facet.loops[i]; 
      assert(p1 <= facet.outputVertexNos.size());
      beginContour();
      IFV(6) { 
        printf("           contour [ "); 
        for(int j = p0; j < p1; j++) printf("V%d ", facet.outputVertexNos[j]); 
        printf("]\n"); 
      }       
      for(int j = p0; j < p1; j++) 
        vertex(facet.outputVertexNos[j]);      
      endContour();
      p0 = p1;
    }
    endPolygon();    
  }

  void copyFacetSide(const Facet &facet, int i0, int i1, int &p0) {
    for(int i = i0; i < i1; i++) {
      int p1 = facet.loops[i]; 
      if(just_triangles && p1 - p0 > 3) { // make triangle fan
        int v0 = facet.outputVertexNos[p0];
        for(int j = p0 + 1; j + 1 < p1; j++) {
          vertexNos_out.push_back(v0); 
          vertexNos_out.push_back(facet.outputVertexNos[j]); 
          vertexNos_out.push_back(facet.outputVertexNos[j + 1]);           
          loops_out.push_back(vertexNos_out.size());
        }
      } else {        
        for(int j = p0; j < p1; j++) 
          vertexNos_out.push_back(facet.outputVertexNos[j]); 
        loops_out.push_back(vertexNos_out.size());      
      }
      p0 = p1;
    }
  }

  void tesselateFacet(Facet &facet, bool tess_same, bool tess_diff) {

    assert(facet.loops[facet.loops.size()-1] <= facet.outputVertexNos.size());

    int p0 = 0;
    if(tess_same) 
      tesselateFacetSide(facet, 0, facet.n_loops_same, p0, false); 
    else
      copyFacetSide(facet, 0, facet.n_loops_same, p0); 

    int new_n_loops_same = loops_out.size();

    if(tess_diff) 
      tesselateFacetSide(facet, facet.n_loops_same, facet.loops.size(), p0, true); 
    else
      copyFacetSide(facet, facet.n_loops_same, facet.loops.size(), p0); 
       
    facet.outputVertexNos.swap(vertexNos_out); 
    vertexNos_out.clear();
    facet.loops.swap(loops_out); 
    loops_out.clear();
    facet.n_loops_same = new_n_loops_same;
  }  

};

void CSG::tesselateFacet(Facet &facet) {
  assert(facet.loops.size() == 0 || 
         facet.loops[facet.loops.size()-1] <= facet.outputVertexNos.size());

  // check if we need to tesselate
  bool tess_same, tess_diff; 
  
  needTesselate(vertices, facet, &tess_same, &tess_diff, verbose);

  // are there loops of > 3 points (requires splitting in triangles)
  bool tess_triangles = false; 
  if(tesselate == 3) {
    int p0 = 0;
    for(int i = 0; i < facet.loops.size(); i++) {
      int p1 = facet.loops[i]; 
      if(p1 - p0 > 3) {
        tess_triangles = true; 
        break;
      }
      p0 = p1;
    }      
  }  

  if(!tess_same && !tess_diff && !tess_triangles) return; 
  
  IFV(3) printf("    Tesselating from %zd loops to %s, %zd pts (same %d diff %d triangles %d)\n", 
                facet.loops.size(), tesselate == 3 ? "triangles" : "convex polygons", 
                facet.outputVertexNos.size(), int(tess_same), int(tess_diff), int(tess_triangles));   
 
  FacetTesselator ft(vertices, tesselate == 3, verbose);
  ft.tesselateFacet(facet, tess_same, tess_diff); 

  IFV(3) printf("       output: %zd loops, %zd pts\n", facet.loops.size(), facet.outputVertexNos.size());   
      
}
