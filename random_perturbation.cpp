/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */


#include "random_perturbation.hpp"

using namespace mCSG;

/***************************************
 * a few operations on 3x3 matrices
 ***************************************/

void matrix_multiply(const Vec3 a[3], const Vec3 b[3], Vec3 c[3]) {
  for(int i = 0; i < 3; i++) 
    for(int j = 0; j < 3; j++) {
      c[i][j] = 0;
      for(int k = 0; k < 3; k++) 
	c[i][j] += a[i][k] * b[k][j];
    }
} 

void copy_matrix(const Vec3 src[3], Vec3 dest[3]) {
  dest[0] = src[0]; 
  dest[1] = src[1]; 
  dest[2] = src[2]; 
}

void rotation_matrix(const Vec3 angles, Vec3 mat[3]) {
  for(int d = 0; d < 3; d++) {
    int d1 = (d + 1) % 3;
    int d2 = (d + 2) % 3; 
    Vec3 r[3] = {Vec3::zero(), Vec3::zero(), Vec3::zero()};
    double angle = angles[d];
    r[d][d] = 1;
    r[d1][d1] = cos(angle); r[d1][d2] = -sin(angle); 
    r[d2][d1] = sin(angle); r[d2][d2] = cos(angle);
    if(d == 0) copy_matrix(r, mat);
    else {
      Vec3 m2[3];
      copy_matrix(mat, m2);
      matrix_multiply(m2, r, mat);
    }
  }
}

void transpose_matrix(const Vec3 m[3], Vec3 t[3]) {
  for(int i = 0; i < 3; i++) 
    for(int j = 0; j < 3; j++) t[i][j] = m[j][i];
      
}

Vec3 matrix_vecmul(const Vec3 a[3], const Vec3 & b) {
  Vec3 res;
  res.x = a[0].dot(b); 
  res.y = a[1].dot(b); 
  res.z = a[2].dot(b); 
  return res;
}

real solve_3x3(const Vec3 a[3], const Vec3 &b, Vec3 & x) {

  /* Maple for the lazy...

with(LinearAlgebra);;
with(CodeGeneration);;

A := Matrix([seq([seq(a[i,j], i=1..3)],j=1..3)]);;
ai := MatrixInverse(A);;
x := MatrixMatrixMultiply(ai, <b[1], b[2], b[3]>);;
C(x, optimize);;

  */
  
  double t1 = a[1][1] * a[2][2] - a[1][2] * a[2][1];
  double t2 = -a[0][1] * a[2][2] + a[0][2] * a[2][1];
  double t3 = a[0][1] * a[1][2] - a[0][2] * a[1][1];
  double t4 = t2 * a[1][0] + t3 * a[2][0] + a[0][0] * t1; // determinant

  double det = t4; 

  t4 = 0.1e1 / t4;
  x[0] = (t1 * b[0] + t2 * b[1] + t3 * b[2]) * t4;
  x[1] = (-(-a[2][0] * a[1][2] + a[1][0] * a[2][2]) * b[0] + (-a[2][0] * a[0][2] + a[0][0] * a[2][2]) * b[1] - (-a[1][0] * a[0][2] + a[0][0] * a[1][2]) * b[2]) * t4;
  x[2] = ((-a[2][0] * a[1][1] + a[1][0] * a[2][1]) * b[0] - (-a[2][0] * a[0][1] + a[0][0] * a[2][1]) * b[1] + (-a[1][0] * a[0][1] + a[0][0] * a[1][1]) * b[2]) * t4;
  
  return det;
}

/***************************************
 * implementation
 ***************************************/


void RandomPerturbation::apply(CSG & csg) {
  int verbose = csg.verbose;
  srand48(random_seed); 

  if(do_rr) {
    Vec3 angles(drand48() * M_PI * 2, drand48() * M_PI * 2, drand48() * M_PI * 2);
    IFV(1) 
      printf("applying a random rotation of angles (%g %g %g) \n", 
	     angles.x, angles.y, angles.z);
    rotation_matrix(angles, rmat);
    transpose_matrix(rmat, tmat); 
  } 

  for(int i = 0; i < csg.nmesh; i++) 
    translations.push_back(rt * Vec3(drand48() - 0.5, drand48() - 0.5, drand48() - 0.5));

  int meshno = 0; 
  for(int i = 0; i < csg.vertices.size(); i++) {
    while(i >= csg.meshno_to_vertex[meshno]) {meshno++; assert(meshno < csg.nmesh); }
    Vertex & v = csg.vertices[i];
    if(do_rr)
      v.coords = matrix_vecmul(rmat, v.coords); 
    v.coords += translations[meshno];
  }

}

void compact_ouput_vertices(Facet &facet) {
  // remove all -1's in the output vertex tables
  int loopi = 0; 
  int vi = 0; 
  int new_n_loops_same = 0; 

  int l0 = 0; 
  for(int j = 0; j < facet.loops.size(); j++) {
    int l1 = facet.loops[j]; 

    int prev_vi = vi;

    for(int l = l0; l < l1; l++) {
      int vno = facet.outputVertexNos[l]; 
      if(vno >= 0) facet.outputVertexNos[vi++] = vno; 
    }

    if(vi - prev_vi >= 3) 
      facet.loops[loopi++] = vi;
    else // just drop the loop
      vi = prev_vi;

    if(j < facet.n_loops_same) new_n_loops_same = loopi; 
    l0 = l1;
  }
  facet.outputVertexNos.resize(vi); 
  facet.loops.resize(loopi);
  facet.n_loops_same = new_n_loops_same;
}

real distanceToLine(const Vec3 & x0, 
                    const Vec3 & x1, 
                    const Vec3 & x2) {
  // orthogonal distance of x1 to line [x0, x1]

  Vec3 v1 = x1 - x0; 
  v1.normalize();
  Vec3 v2 = x2 - x0; 
  return (v1.cross(v2)).length();
}
    

void print_loops(Facet & facet) {                
  int l0 = 0; 
  for(int j = 0; j < facet.loops.size(); j++) {
    int l1 = facet.loops[j]; 
    printf("[ "); 
    for(int l = l0; l < l1; l++) printf("%d ", facet.outputVertexNos[l]); 
    printf("] "); 
    l0 = l1;
  }
} 


void RandomPerturbation::revertTrTriple(Vertex & v, CSG &csg) {

  Vec3 a[3], b; // plane equations
  for(int j = 0; j < 3; j++) {
    const Facet &facet = csg.facets[v.facetNos[j]];      
    Vec3 translation = translations[facet.meshno];
    a[j] = facet.normal;
    b[j] = facet.b - facet.normal.dot(translation);
  }      
  real det = solve_3x3(a, b, v.coords);
  if(fabs(det) < 1e-10) 
    csg.errors.add("RandomPerturbation::revertTrTriple: close to singular set of planes"); 

}

void RandomPerturbation::revertTrAndRrDouble(Vertex & v, CSG & csg, bool revert_rotation) {
 
  int f0 = v.facetNos[0], f1 = v.facetNos[1], f2 = v.facetNos[2];  
  
  // move the outlier to 2        
  if         (csg.facets[f0].meshno == csg.facets[f2].meshno) std::swap(f1, f2); 
  else if    (csg.facets[f1].meshno == csg.facets[f2].meshno) std::swap(f0, f2); 
  else assert(csg.facets[f0].meshno == csg.facets[f1].meshno); 
  
  Facet & facet0 = csg.facets[f0];           
  int nv = facet0.vertexNos.size(); 
  int pi;
  for(pi = 0; pi < nv; pi++) 
    if(facet0.oppositeFacetNos[pi] == f1) break;
      
  if(pi == nv) {
    csg.errors.add("RandomPerturbation::revert: could not find opposite facet");
    v.coords = matrix_vecmul(tmat, v.coords); 
    return; 
  } 

  int vno1 = facet0.vertexNos[pi]; 
  int vno0 = facet0.vertexNos[pi > 0 ? pi - 1 : nv - 1]; 	
  Vec3 v1 = csg.vertices[vno1].coords;
  Vec3 v0 = csg.vertices[vno0].coords;
  
  // v0 and v1 are already reverted
  const Facet & facet2 = csg.facets[f2];
  
  // revert f2's plane equation
  Vec3 a = facet2.normal;
  real b = facet2.b - a.dot(translations[facet2.meshno]);

  if(revert_rotation) 
    a = matrix_vecmul(tmat, a);
  
  Vec3 ve = v1 - v0; 

  b -= a.dot(v0); 
  real f = a.dot(ve); 
  
  if(fabs(f) < 1e-10) 
    csg.errors.add("RandomPerturbation::revertTrAndRrDouble: close to singular set of planes"); 
  
  real u = b / f; 

  v.coords = v0 + u * ve;
  
}     



const int RandomPerturbation::REVERT_ROTATION = 1;
const int RandomPerturbation::REVERT_TRANSLATION = 2;
const int RandomPerturbation::MERGE_AFTER_REVERT = 4;


void RandomPerturbation::revert(CSG & csg, int mask) {
  int verbose = csg.verbose;
  int meshno = 0; 

  bool revert_rotation = (mask & REVERT_ROTATION) && do_rr; 
  bool revert_translation = (mask & REVERT_TRANSLATION) && rt != 0;
  bool merge_after_revert = (mask & MERGE_AFTER_REVERT) && eps_merge > 0; 

  if(revert_rotation || revert_translation) {

    IFV(2) printf("    reverting perturbation on %d primary vertices\n", 
                  csg.nvert1); 

    for(int i = 0; i < csg.nvert1; i++) {
      while(i >= csg.meshno_to_vertex[meshno]) meshno++; 
      Vertex & v = csg.vertices[i];
      if(revert_translation)
        v.coords -= translations[meshno];
      if(revert_rotation) v.coords = matrix_vecmul(tmat, v.coords);
    }
    
    IFV(2) printf("    reverting perturbation on %d double & triple vertices\n", 
                  int(csg.vertices.size() - csg.nvert1)); 
    
    // reconstruct coordinates for the non-primitive vertices
    for(int i = csg.nvert1; i < csg.vertices.size(); i++) {
      Vertex & v = csg.vertices[i];
      
      if(!revert_translation) {
        if(revert_rotation) v.coords = matrix_vecmul(tmat, v.coords);
      } else {
        int order = csg.nmesh - v.meshpos.nbKnown(); 
        if(order == 3) {
          revertTrTriple(v, csg); 
          if(revert_rotation) v.coords = matrix_vecmul(tmat, v.coords);
        } else if(order == 2) {
          revertTrAndRrDouble(v, csg, revert_rotation); 
        } else assert(!"should have either double or triple vertices in output");
      }
    }
  }

  if(!merge_after_revert) return;
  
  // simplify the output loops

  std::vector<int> vmap; // maps vertex number to the lower number it is renamed to
  vmap.reserve(csg.vertices.size()); 
  for(int i = 0; i < csg.vertices.size(); i++) vmap.push_back(i);

  const real eps_merge_2 = eps_merge * eps_merge; 

  // 1st pass: identify and remove same points
  for(int i = 0; i < csg.facets.size(); i++) {
    Facet &facet = csg.facets[i]; 
    
    {// early stop: only primaries
      int j;
      for(j = 0; j < facet.outputVertexNos.size(); j++) {
        if(facet.outputVertexNos[j] >= csg.nvert1) goto process_facet;
      }
      continue;
    }

  process_facet:
    
    IFV(3) {
      printf("      Examining facet F%d: %zd loops (%d pos), %zd vertices",
             i, facet.loops.size(), facet.n_loops_same, facet.outputVertexNos.size()); 
      IFV(4) print_loops(facet); 
      printf("\n"); 
    }

    int tot_loop_length = 0;

    int l0 = 0; 
    for(int j = 0; j < facet.loops.size(); j++) {
      int l1 = facet.loops[j]; 
      int loop_length = l1 - l0; 
            
      {// update vmap
        int *v0 = &facet.outputVertexNos[l1 - 1];

        for(int l = l0; l < l1; l++) {
          int *v1 = &facet.outputVertexNos[l];
          if(*v1 >= 0 && csg.vertices[*v0].coords.distanceSquared(csg.vertices[*v1].coords) < eps_merge_2) {
            IFV(4) printf("        loop %d: removing vertex %d (same as %d)\n", j, *v0, *v1);
            if(vmap[*v1] < vmap[*v0]) vmap[*v0] = vmap[*v1]; 
            else                      vmap[*v1] = vmap[*v0]; 
            *v0 = -1; 
            loop_length --; 
            if(loop_length < 3) break;
          }
          v0 = v1; 
        }      
      }      
            
      if(loop_length >= 3) { // examine triplets 
        // find two valid points to start from 
        int l = l1 - 1; 
        while(facet.outputVertexNos[l] < 0) {l--; assert(l >= l0); }
        int *v1 = &facet.outputVertexNos[l]; 
        l--;
        while(facet.outputVertexNos[l] < 0) {l--; assert(l >= l0); }
        int *v0 = &facet.outputVertexNos[l]; 

        for(int l = l0; l < l1; l++) {
          int *v2 = &facet.outputVertexNos[l];
          if(*v2 < 0) continue; 

          if(distanceToLine(csg.vertices[*v0].coords, 
                            csg.vertices[*v1].coords, 
                            csg.vertices[*v2].coords) < eps_merge) { // remove v1
            IFV(4) printf("        loop %d: removing vertex %d (aligned with %d %d)\n", j, *v1, *v0, *v2);
            *v1 = -1; 
            v1 = v2;
            loop_length--; 
            if(loop_length < 3) break;
          } else {
            v0 = v1; 
            v1 = v2; 
          }
        }      
      }     

      l0 = l1;
      tot_loop_length += loop_length; 
    }
    
    if(tot_loop_length != facet.outputVertexNos.size()) {
      compact_ouput_vertices(facet);     
      IFV(3) {
        printf("        compact -> %zd loops (%d pos), %zd vertices", 
               facet.loops.size(), facet.n_loops_same, facet.outputVertexNos.size());       
        IFV(4) print_loops(facet); 
        printf("\n"); 
      }
    }

  }
  
  { // compact vmap
    int n_remove = 0;
    IFV(3) printf("      merging vertices\n"); 
    for(int i = 0; i < vmap.size(); i++) {      
      if(vmap[i] != i) {
        n_remove++; 
        int j = vmap[i];
        while(vmap[j] != j) j = vmap[j];
        vmap[i] = j;
        IFV(4) printf("        %d -> %d\n", i, j); 
      }
    }
    IFV(1) printf("    Merging %d vertices\n", n_remove); 

    for(int i = 0; i < csg.facets.size(); i++) {
      Facet &facet = csg.facets[i]; 
      
      for(int j = 0; j < facet.outputVertexNos.size(); j++) {
        facet.outputVertexNos[j] = vmap[facet.outputVertexNos[j]];
      }
    }   
    
  }
  
}
