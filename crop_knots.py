# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com



import numpy as np
import sys

bbox = np.array([[-40., -40., -40.],
                 [40., 40. , 40.]]) * 1e3

infilename, outfilename = sys.argv[1:]

f = open(infilename, "r")
assert f.readline() == "OFF\n"

nv, nf, zero = f.readline().split()

nv = int(nv)
nf = int(nf)

coords = []
for i in range(nv):
  coords.append(np.array([float(x) for x in f.readline().split()]))





coords_map = {}

def new_intersection(p0, p1, dim, thresh):
  p0, p1 = min(p0, p1), max(p0, p1)
  k = (p0, p1, dim, thresh) 
  if k not in coords_map:
    x0 = coords[p0]
    x1 = coords[p1]
    a0 = x0[dim]
    a1 = x1[dim]
    t = (thresh - a0) / (a1 - a0);
    x = (1 - t) * x0 + t * x1;
    n = len(coords)
    coords_map[k] = n
    coords.append(x)    
  return coords_map[k]

def crop_poly(poly, dim, thresh, sense):
  if not poly: return poly
  if not sense: # inf threshold
    if all([coords[p][dim] < thresh for p in poly]): return []
    if all([coords[p][dim] > thresh for p in poly]): return poly
  else:
    if all([coords[p][dim] > thresh for p in poly]): return []
    if all([coords[p][dim] < thresh for p in poly]): return poly

  nv = len(poly)
  first_in = first_out = -1
  prev_x = coords[poly[-1]][dim] 
  for i, p in enumerate(poly):
    x = coords[p][dim]
    if (x > thresh) != (prev_x > thresh):
      if (x > thresh) != sense: first_in = i
      else:                first_out = i
    prev_x = x

  assert first_in != -1 and first_out != -1

  if first_out > first_in:
    new_poly = poly[first_in:first_out]
  else:
    new_poly = poly[first_in:] + poly[:first_out]

  # print first_in, first_out, nv
  e1 = new_intersection(poly[first_out], poly[first_out - 1], dim, thresh)
  e2 = new_intersection(poly[first_in - 1], poly[first_in], dim, thresh)

  new_poly.append(e1)
  new_poly.append(e2)

  return new_poly


if False:
  n0 = len(coords)

  t = np.random.random(3) * 1e6


  coords.append(np.array([0, 0, 0]) + t)
  coords.append(np.array([0, 1, 0]) + t)
  coords.append(np.array([0, 0, 1]) + t)

  dumb_poly = [n0, n0 + 1, n0 + 2]
  

polys = []

for i in range(nf):
  poly = [int(fno) for fno in f.readline().split()[1:]]

  for sense in False, True:
    for dim in range(3): 
      poly = crop_poly(poly, dim, bbox[sense][dim], sense)

  if poly:
    polys.append(poly)
  #else:
  #  polys.append(dumb_poly)

used = [0] * len(coords)

for poly in polys:
  for p in poly:
    used[p] = True

no_map = []
new_coords = []
for i in range(len(coords)):
  if used[i]:
    no_map.append(len(new_coords))
    new_coords.append(coords[i])
  else:
    no_map.append(-1)

print "vertices: %d -> %d, faces: %d -> %d" % (
  nv, sum(used), nf, len(polys))

coords = new_coords
f = open(outfilename, "w")



print >>f, "OFF"
print >>f, len(coords), len(polys), 0

for coord in coords:
  print >>f, "%.16g %.16g %.16g" % tuple([float(x) for x in coord])

for poly in polys:
  print >>f, len(poly), " ".join(["%d" % no_map[p] for p in poly])


    

      
  
