

# cc -Wl,-F. -bundle -undefined dynamic_lookup -o tess.so tessmodule.c -I /usr/include/python2.6 -I /Developer//SDKs/MacOSX10.6.sdk/System/Library/Frameworks/Python.framework/Versions/2.6/Extras/lib/python/numpy/core/include



import numpy
import tess
# a = numpy.random.random((4,5))
# tess.tesselate(a)

pts = numpy.array([[0,0,0],
                   [2,0.5,0],
                   [5,5,0],
                   [1,4,0],
                   [0,0,0],
                   
                   [1,2,0],
                   [1,3,0],
                   [2,2,0],
                   [1,2,0]
                   ])

print tess.tesselate(pts)

