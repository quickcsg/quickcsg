# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import sys


vertlist_fname_1, vertlist_fname_2  = sys.argv[1:3]


def sorted_tuple(*x):
  return tuple(sorted(x))

def load_vertlist(fname):
    return [tuple(sorted([int(x) for x in l.split()[:3]])) 
                for l in open(fname, "r")]
    
ref = set(load_vertlist(vertlist_fname_1))
new = set(load_vertlist(vertlist_fname_2))

print len(ref), len(new)

print "missing:", list(sorted(ref - new))
print "too many:", list(sorted(new - ref))



if ref != new:
  print "BAD"
  sys.exit(1)
else:
  print "OK"
  
