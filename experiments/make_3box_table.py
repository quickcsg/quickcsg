# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import numpy as np
import sys


def load_off(fname): 

    f = open(fname, "r")
    assert f.readline() == "OFF\n"

    nvertex, nfacet, zero = f.readline().split()

    nvertex = int(nvertex)
    nfacet = int(nfacet)

    print >> sys.stderr, "%d vertices, %d facets" % (nvertex, nfacet)

    assert zero == "0"

    vertices = np.fromfile(f, dtype = float, count = 3 * nvertex, sep = " ")
    points = vertices.reshape(-1, 3)

    facets_in = np.fromfile(f, dtype = float, sep = " ")
    faces = []
    i = 0
    while i < len(facets_in): 
        npt = int(facets_in[i])
        color = tuple(facets_in[npt + i + 1 : npt + i + 4])
        faces.append((facets_in[i + 1 : npt + i + 1].astype(int), color))
        i += npt + 1 + 3

    return points, faces

allvert = []
allfaces = []

def rotmat(dim, angle): 
    r = np.eye(3)
    d1 = (dim + 1) % 3
    d2 = (dim + 2) % 3
    r[d1, d1] = np.cos(angle)
    r[d1, d2] = -np.sin(angle)
    r[d2, d1] = np.sin(angle)
    r[d2, d2] = np.cos(angle)
    return r

R = np.dot(rotmat(0, 0.5), rotmat(1, 0.44))

notess = len(sys.argv) > 1 and sys.argv[1] == 'notess'

n = 0
for i in range(0, 128):

    vertices, faces = load_off("data/3box_table/tab_%s%d.off" % ("notess_" if notess else "", 2 * i))

    vertices = np.dot(vertices, R) + np.array([i % 16, i / 16, 0]) * 3

    for f, c in faces: 
        f += n

    n += vertices.shape[0]
    allvert.append(vertices)
    allfaces += faces


of = open("data/3box_table/comb%s.off" % ("_notess" if notess else ""), "w")


points = np.vstack(allvert)

print >>of, "OFF"
print >>of, "%d %d 0" % (points.shape[0], len(allfaces))
for x, y, z in points: 
    print >>of, x, y, z

for poly, color in allfaces:
    print >> of, "%d %s " % (len(poly), " ".join(["%d" % i for i in poly])), "%g %g %g" % color



    
