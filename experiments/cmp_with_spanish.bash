# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e


for mesh in Armadillo dragon; do 
    

    # generate transformed mesh

    if [ $mesh == Armadillo ]; then
        # obtained from original Armadillo with Meshlab's "Quadric edge collapse decimation"
        in1=../data/stanford_models/Armadillo_150k.off
        in2=/tmp/armadillo_transformed.off

        python ../data/generate_primitive.py -load $in1 -o $in2 -translate y 1.45 -translate z 3.0 -rotate z 13.4 

    elif [ $mesh == dragon ]; then
        in1=../data/stanford_models/dragon_recon/dragon_vrip.off
        in2=/tmp/dragon_transformed.off
        
        python ../data/generate_primitive.py -load $in1 -o $in2 -translate y 0.01 -translate z 0.005 -rotate y 4.5 

    fi
    

    for thr in 1 4; do 
        
        # tests 
        for op in union inter a-b b-a; do 
            
            case $op in
                union) opts="$in1 $in2 -union";; 
                inter) opts="$in1 $in2 -inter";; 
                a-b) opts="$in1 $in2 -diff 1";; 
                b-a) opts="$in2 $in1 -diff 1";;
            esac
            
            rad=data/cmp_with_spanish/${mesh}_${op}_thr$thr
            
            echo ================= $rad
            
            ../mesh_csg $opts -v 111111111 -o $rad.off -tess -parallel.nt $thr > $rad.stdout 2> $rad.stderr
            
        done
    done

done