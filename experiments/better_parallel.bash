# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -x 
set -e

machine=$( hostname )
machine=${machine%%.*}


dataset="-contours ../data/capture_kinovis/CONTOURS/contour-00426.oc ../data/capture_kinovis/68cam.calib \
            -v 1111111 -cropbbox -10,-10,-10,10,10,10 -contourdepth 40 -random.jitter 0.0001 -contourrange 0,60"

ncpu=$(cat /proc/cpuinfo | grep ^processor | wc -l) 

for nt in 1 2 4 8 12 16 24 32 48; do 
    
    if [ $nt -gt $ncpu ]; then 
        continue
    fi

if false; then    
    for run in {1..5}; do
        ../../mkCSG_ref/mesh_csg $dataset -parallel.nt $nt |
        tee data/better_parallel/${machine}_nt${nt}_run${run}_ref.log
        
        ../mesh_csg $dataset \
            -parallel.nt $nt |
        tee data/better_parallel/${machine}_nt${nt}_run${run}_new1.log

    done
    for run in {1..5}; do
        ../../mkCSG_ref/mesh_csg $dataset -parallel.nt $nt -parallel.split_npoly 10000000|
        tee data/better_parallel/${machine}_nt${nt}_run${run}_ref_nops.log
        
        ../mesh_csg $dataset \
            -parallel.nt $nt -parallel.split_npoly 10000000 |
        tee data/better_parallel/${machine}_nt${nt}_run${run}_new1_nops.log

    done
fi
    for run in {1..5}; do
        ../../mkCSG_ref/mesh_csg $dataset -parallel.nt $nt -parallel.split_depth 20 |
        tee data/better_parallel/${machine}_nt${nt}_run${run}_ref_depth20.log
        
        ../mesh_csg $dataset \
            -parallel.nt $nt -parallel.split_depth 20  |
        tee data/better_parallel/${machine}_nt${nt}_run${run}_new1_depth20.log

    done
        
done
