import os, pdb, re

basedir = "data/cmp_cgal/loop_intersections/"

of = open(basedir + "index.html", "w")


print >> of, "<html><body><table border=2>"

print >> of, "<tr><th>combination<th>CGAL<th>QuickCSG<th>Hausdorff <th>QuickCSG jitter <th>Hausdorff jitter  <th> errors / <br> 10 random seeds"  

pat_cgal = re.compile("result has ([0-9]+) vertices and ([0-9]+) facets")



stats = {
    'n_cgal_err':0,
    'n_one_missing': 0,

    'n_quickcsg_err': 0,
    'n_quickcsg_errRRT3': 0,

    'n_quickcsg_bad': 0,
    'n_quickcsg_badRRT3': 0}
    

                           

for fname in sorted(os.listdir(basedir)):
    if not fname.endswith("_ref.stdout"): continue
    
    
    rad = fname[:-11]

    print >> of, "<tr><th>%s inter %s"  % tuple(rad.split("_"))

    res_cgal = open(basedir + rad + "_ref.stdout", "r").read()

    mos = pat_cgal.findall(res_cgal)

    if len(mos) == 2:
        nv, nf = mos[1]
        
        print >> of, "<td>%sV+%sF" % (nv, nf)
    else:
        print >> of, '<td><a href="%s_ref.stderr">ERROR</a>' % rad
        stats['n_cgal_err'] += 1



    if False: 

        if os.access(basedir + rad + "_cmp.stdout", os.R_OK): 


            res_cmp = open(basedir + rad + "_cmp.stdout", "r").readlines()   

            if len(res_cmp) >= 3 and res_cmp[-1].startswith("n_miss_1"):
                n0 = res_cmp[-2].split()[1]
                n1 = res_cmp[-1].split()[1]        
                print >> of, "<td>miss %s + %s" % (n0, n1)
            else:
                print >> of, '<td><a href="%s_cmp.stderr">ERROR</a>' % rad

        else:
            print >> of, "<td>MISS"


    for suf in '', 'RRT3': 


        print >> of, "<td> "
        if os.access(basedir + rad + "_new%s.off" % suf, os.R_OK):
            f = open(basedir + rad + "_new%s.off" % suf, "r")
            f.readline()
            print >> of, "%sV+%sF" % tuple(f.readline().split()[:2])
        else:
            print >> of, "No output"


        res_quickcsg = open(basedir + rad + "_new%s.stdout" % suf, "r").readlines()   

        if res_quickcsg[0] == "Encountered errors during CSG computation:\n":
            print >> of, '<a href="%s_new%s.stdout">ERROR</a>' % (rad, suf)

            stats['n_quickcsg_err' + suf] += 1
        


        if os.access(basedir + rad + "_hausdorff%s.stdout" % suf, os.R_OK): 

            res_cmp = open(basedir + rad + "_hausdorff%s.stdout" % suf, "r").readlines()   

            if len(res_cmp) == 1:
                msg = res_cmp[0].strip()
                print >>of, "<td>", msg
                if msg == "One missing":
                    stats['n_one_missing'] += 1
                elif msg == "New empty (but not ref): ERROR":
                    stats['n_quickcsg_bad'] += 1
                elif msg == "Ref empty (but not new): ERROR":
                    stats['n_quickcsg_bad'] += 1
                elif msg == "Both empty: OK":
                    pass
                elif msg == "REF missing":
                    pass
                else:
                    assert False, "message not handled"
                    
            else:

                vals = []
                for l in res_cmp:
                    fi = l.split()
                    if len(fi) == 11 and fi[0] == "min" and fi[3] == "max":
                        # min max mean RMS
                        vals.append((float(fi[2]), float(fi[4]),
                                     float(fi[7]), float(fi[10])))
                if len(vals) < 4:

                    print >>of, "<td>INCOMPLETE"

                else:
                    maxerr = max(vals[0][1], vals[2][1])
                    rmserr = 0.5*(vals[0][3] + vals[2][3])
                    print >> of, "<td><a href=\"%s\">RMS %.3f max %.3f</a>" % (
                        rad + "_hausdorff%s.stdout" % suf,                
                        rmserr, maxerr)
                    if maxerr > 5.0: 
                        stats['n_quickcsg_bad' + suf] += 1
            
        else:
            print >> of, "<td>MISS"

    ncount = 0
    nerr = 0

    for seed in range(1, 11):
        suf = "RRT3seed%d" % seed

        fname = basedir + rad + "_new%s.stdout" % suf

        print fname

        if os.access(fname, os.R_OK): 
            res_quickcsg = open(fname, "r").readlines()   

            if len(res_quickcsg) > 0: 
                ncount += 1
                if res_quickcsg[0] == "Encountered errors during CSG computation:\n":
                    nerr += 1

    if ncount < 10:
        print >>of, "<td> MISS"
    elif nerr > 0:        
        print >>of, "<td> <b>%d</b>" % nerr
    else:        
        print >>of, "<td> %d" % nerr


print >>of, "<tr><th>Totals"
print >>of, "<th>%d errors" % stats['n_cgal_err']

for suf in '', 'RRT3':
    print >>of, "<th>%d errors" % stats['n_quickcsg_err' + suf]
    print >>of, "<th>%d bad results" % stats['n_quickcsg_bad' + suf]
    
    

print >>of, "</table></body></html>"

print stats
