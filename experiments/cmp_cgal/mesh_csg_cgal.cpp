
#include <cstdio>
#include <cstdlib>



#include <iostream>
#include <fstream>

#include <CGAL/Gmpz.h>
#include <CGAL/Homogeneous.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/Nef_polyhedron_3.h>
#include <CGAL/IO/Nef_polyhedron_iostream_3.h>
#include <iostream>

typedef CGAL::Homogeneous<CGAL::Gmpz>  Kernel;
typedef CGAL::Polyhedron_3<Kernel>  Polyhedron;
typedef CGAL::Nef_polyhedron_3<Kernel> Nef_polyhedron;
typedef Kernel::Vector_3  Vector_3;
typedef Kernel::Aff_transformation_3  Aff_transformation_3;



void usage() {
  fprintf(stderr, 
"Usage: mesh_csg -O mesh.off [options] mesh_1.off ... mesh_N.off\n"
"\n" 
" Compute CSG operation on a set of N meshses\n" 
"\n" 
"     Operation to perform\n" 
"\n" 
"-inter           intersection (default)\n"
"-union           union\n"
"\n" 
          );
  exit(1);
}


typedef CGAL::Homogeneous<CGAL::Gmpz>  Kernel;
typedef CGAL::Polyhedron_3<Kernel>  Polyhedron;
typedef CGAL::Nef_polyhedron_3<Kernel> Nef_polyhedron;
typedef Kernel::Vector_3  Vector_3;
typedef Kernel::Aff_transformation_3  Aff_transformation_3;


// g++ -O3 -c mesh_csg_cgal.cpp -frounding-math && g++ -o mesh_csg_cgal mesh_csg_cgal.o -lCGAL 

int main(int argc, char**args) {
  
  std::vector<const char *> infiles;

  const char* outputFilename = NULL;
  std::string op = "-inter";
  int oparg = 0, oparg2 = 0;
  int verbose = 0; 
  int factortoint = -1; 

  int i = 1;
  while(i < argc) {
    const char * a = args[i++];
    std::string as(a);
    if(as == "-h") usage();
    else if(as == "-inter" || as == "-union" || as == "-xor" || 
            as == "-min2" || as == "-min-2" || as == "-AuBmC" || as == "-BuCmA" || as == "-AmBiC" || as == "-dither")
      op = as;
    else if((as == "-diff" || as == "-inter2union" || as == "-csgtable" || as == "-csgnmesh" || as == "-csgminn" ) && 
            args[i] && sscanf(args[i], "%d", &oparg)) {i++; op = as; }
    else if(as == "-v") verbose++;
    else if((as == "-O" || as == "-o") && args[i]) outputFilename = args[i++]; 
    //else if(as == "-exact" && args[i] && sscanf(args[i], "%d", &oparg)) {i++;
    else if(a[0] == '-') {
      std::cerr << "unknown argument " << a << std::endl; 
      exit(1);
    } else 
      infiles.push_back(a); 

  }
  
  assert(op == "-union" || op == "-inter" || op == "-diff" || !"only inter, union, diff implemented");

  assert(infiles.size() >= 2 || !"need at least 2 input meshes"); 

  Nef_polyhedron *result = NULL;
  
  for(int i = 0; i < infiles.size(); i++) {
    printf("loading polyhedron %d %s\n", i, infiles[i]); 
    Polyhedron P;
    std::ifstream f(infiles[i]);    
    
    f >> P; 
    assert(P.is_closed() || "input polyhedron must be closed");

    printf("    convert to Nef polyhedron\n"); 

    Nef_polyhedron *N = new Nef_polyhedron(P);       

    if(!result) result = N; 
    else {
      if(op == "-union" || op == "-diff" && i < oparg) {
        printf("   add to accu\n"); 
        *result += *N; 
      } else if(op == "-diff" && i >= oparg) {
        printf("   subtract from accu\n"); 
        *result -= *N; 
      } else if(op == "-inter") {
        printf("   intersect with accu\n");         
        *result *= *N; 
      }
      delete N;
    }
    printf("   result has %d vertices and %d facets\n", result->number_of_vertices(), result->number_of_facets()); 
  }
  
  assert(result->is_simple() || "result is not simple"); 

  if(outputFilename) {
    Polyhedron P;
    std::ofstream f(outputFilename);    
    f.precision(12); // a few more significant digits please
    result->convert_to_polyhedron(P);
    f << P;
  }

  
}
