

#include <CGAL/Gmpz.h>
#include <CGAL/Homogeneous.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/Nef_polyhedron_3.h>
#include <CGAL/IO/Nef_polyhedron_iostream_3.h>
#include <iostream>

typedef CGAL::Homogeneous<CGAL::Gmpz >  Kernel;
typedef CGAL::Polyhedron_3<Kernel>  Polyhedron;
typedef CGAL::Nef_polyhedron_3<Kernel> Nef_polyhedron;
typedef Kernel::Vector_3  Vector_3;
typedef Kernel::Aff_transformation_3  Aff_transformation_3;

// g++ -c is_manifold.cpp -frounding-math && g++ -o is_manifold is_manifold.o -lCGAL

int main() {
  Polyhedron P;
  std::cin >> P;

  printf("is_closed: %d\n", P.is_closed()); 

  if(P.is_closed()) {
    Nef_polyhedron NP(P);
    
    printf("is_valid: %d\n", NP.is_valid()); 
    printf("is_simple: %d\n", NP.is_simple()); 

  }
}
