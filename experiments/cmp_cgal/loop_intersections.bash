# set -x
set -e

offs=( $( (cd data/cmp_cgal/segeval; echo *.off) ) )
n=${#offs[*]}


function is_empty () {
    [ "$( head -2 $1 | tail -1 )" == "0 0 0" ]     
}

declare -a todo

RANDOM=1
for i in {1..1000}; do 

    a=$[RANDOM % n]
    b=$[RANDOM % n]
    todo[$i]="$a $b"

done

ntodo=${#todo[*]}


function handle_ab () {
    a=$1
    b=$2

    a=${offs[a]}
    b=${offs[b]}

    echo "INTER $a $b"
    
    ab=${a%.off}_${b%.off}

    if false; then


        rad=data/cmp_cgal/loop_intersections/${ab}_ref
        
        ./mesh_csg_cgal data/cmp_cgal/segeval/$a data/cmp_cgal/segeval/$b -inter -o $rad.off > $rad.stdout 2> $rad.stderr
        
        if [ $? == 0 ]; then
            echo "   CGAL ok"
        else
            echo "   CGAL err"
        fi
        
        
        
        
        
        rad=data/cmp_cgal/loop_intersections/${ab}_new
        
        ../../mesh_csg data/cmp_cgal/segeval/$a data/cmp_cgal/segeval/$b -inter -o $rad.off -tess > $rad.stdout 2> $rad.stderr
        
        if [ $? == 0 ]; then
            echo "   QuickCSG ok"
        else
            echo "   QuickCSG err"
        fi
        
        
        rad=data/cmp_cgal/loop_intersections/${ab}_newRRT2
        
        ../../mesh_csg -random.rotation -random.translation 1e-2 data/cmp_cgal/segeval/$a data/cmp_cgal/segeval/$b -inter -o $rad.off -tess > $rad.stdout 2> $rad.stderr
        
        if [ $? == 0 ]; then
            echo "   QuickCSG ok"
        else
            echo "   QuickCSG err"
        fi
        

    fi
        
    if true; then
        
        for seed in {1..10}; do 
            
            rad=data/cmp_cgal/loop_intersections/${ab}_newRRT3seed$seed
            
            ../../mesh_csg -random.rotation -random.translation 1e-3 -random.seed $seed data/cmp_cgal/segeval/$a data/cmp_cgal/segeval/$b -inter -o $rad.off -tess > $rad.stdout 2> $rad.stderr
            
            if [ $? == 0 ]; then
                echo "   QuickCSG $seed ok"
            else
                echo "   QuickCSG $seed err"
            fi
            
        done
    fi
        
    
    if false; then 
            
        if [ -e data/cmp_cgal/loop_intersections/${ab}_ref.off ] &&  [ -e data/cmp_cgal/loop_intersections/${ab}_new.off ]; then 
            echo "  can compare"
                
            rad=data/cmp_cgal/loop_intersections/${ab}_cmp
            python cmp_meshes.py data/cmp_cgal/loop_intersections/${ab}_ref.off data/cmp_cgal/loop_intersections/${ab}_new.off > $rad.stdout 2> $rad.stderr
            
            if [ $? == 0 ]; then
                echo "   cmp ok"
                tail -3 $rad.stdout
            else
                echo "   cmp err"
            fi                
        fi        

    fi
    

    refname=data/cmp_cgal/loop_intersections/${ab}_ref.off
    
    # for suf in '' 'RRT'; do 
    # for suf in '' 'RRT'; do 

    for suf in ; do 
        newname=data/cmp_cgal/loop_intersections/${ab}_new$suf.off

        rad=data/cmp_cgal/loop_intersections/${ab}_hausdorff$suf

        if [ -s "$refname" ] && [ -s "$newname" ] ; then 
            
            if is_empty $refname; then 
                if is_empty $newname; then
                    echo "Both empty: OK"
                else
                    echo "Ref empty (but not new): ERROR" 
                    
                fi
            else
                if is_empty $newname; then
                    echo "New empty (but not ref): ERROR" 
                else            
                    
                    
                    meshlabserver -i $refname -i $newname -s compare_meshes.mlx
                    
                    if [ $? == 0 ]; then
                        echo "   cmp ok"
                    else
                        echo "   cmp err"
                    fi
                fi
            fi   
            
        elif [   -s "$newname" ]; then
            echo "REF missing"  
        elif [ -s "$refname" ]; then
            echo "NEW missing"
        fi > $rad.stdout 2> $rad.stderr

    done

}


prefix=/tmp/task_lock.$$ # some non-existing name
rm -f $prefix*
touch $prefix.0



ncpu=$(cat /proc/cpuinfo | grep ^processor | wc -l) 
ncpu=1

trap 'kill -HUP 0' 0 # handle exit gracefully
for((cpu=0; cpu<ncpu; cpu++)); do

    for((i=0; i<ntodo; i++)); do
        if mv $prefix.$i $prefix.$[i+1] 2> /dev/null; then
            handle_ab ${todo[i]}
        fi
    done &
done

wait


