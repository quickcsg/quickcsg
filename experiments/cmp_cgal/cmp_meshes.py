import sys, pdb
import numpy as np

"""
python ../../data/generate_primitive.py -box 5 -translate x 3 > data/cmp_cgal/simple/box1.off
python ../../data/generate_primitive.py -box 4  > data/cmp_cgal/simple/box2.off

./mesh_csg_cgal data/cmp_cgal/simple/box{1,2}.off  -inter -o data/cmp_cgal/simple/inter_ref.off
../../mesh_csg data/cmp_cgal/simple/box{1,2}.off  -inter -o data/cmp_cgal/simple/inter_new.off


./mesh_csg_cgal data/cmp_cgal/simple/box{1,2}.off  -union -o data/cmp_cgal/simple/union_ref.off
../../mesh_csg data/cmp_cgal/simple/box{1,2}.off  -union -o data/cmp_cgal/simple/union_new.off

./mesh_csg_cgal data/cmp_cgal/segeval/171.off data/cmp_cgal/segeval/304.off -inter -o data/cmp_cgal/171_inter_304_ref.off
../../mesh_csg data/cmp_cgal/segeval/171.off data/cmp_cgal/segeval/304.off -inter -o data/cmp_cgal/171_inter_304_new.off


"""



def load_off(fname): 

    f = open(fname, "r")
    assert f.readline() == "OFF\n"

    nvertex, nfacet, zero = f.readline().split()

    nvertex = int(nvertex)
    nfacet = int(nfacet)

    print >> sys.stderr, "%d vertices, %d facets" % (nvertex, nfacet)

    # assert zero == "0"

    vertices = np.fromfile(f, dtype = float, count = 3 * nvertex, sep = " ")
    points = vertices.reshape(-1, 3)

    faces = []

    for fno in range(nfacet):
        l = f.readline().split()
        nv = int(l[0])
        faces.append(np.array([int(x) for x in l[1:nv + 1]]))
    print
    return points, faces



def cartesian_prod_3(kmin, kmax):
    for xi in range(kmin[0], kmax[0] + 1):
        for yi in range(kmin[1], kmax[1] + 1):
            for zi in range(kmin[2], kmax[2] + 1):
                yield xi, yi, zi


infiles = sys.argv[1:]


point_dist_tol = 1e-2

straight_line_tol = 1e-3

same_normal_tol = 1e-8

vertex_to_plane_tol = 1e-5

verbose = 10

if len(infiles) > 2:
    point_dist_tol =    float(infiles[2])
    straight_line_tol = float(infiles[3])
    same_normal_tol =   float(infiles[4])
    verbose = int(infiles[5])
    
    infiles = infiles[:2]

class PointIndex:
    
    def __init__(self):
        self.tab = {}
        self.points = []
        self.tolerance = point_dist_tol 
        
    def addv(self, v):
        t2 = self.tolerance ** 2
        
        kmin = np.floor(v - self.tolerance).astype(int)
        kmax = np.floor(v + self.tolerance).astype(int)

        for xi, yi, zi in cartesian_prod_3(kmin, kmax): 
            if (xi, yi, zi) not in self.tab: continue
            for coord, no in self.tab[(xi, yi, zi)]:
                if ((coord - v) ** 2).sum() < t2:
                    return no

        key = tuple(np.floor(v).astype(int))
        if key not in self.tab: self.tab[key] = []
        n = len(self.points)
        self.tab[key].append((v, n))
        self.points.append(v)
        return n

def check_edge_pairs(faces):
    n_miss = 0

    next_pointers = {}

    for face in faces:
        p0 = face[-1]

        for p1 in face:
            next_pointers.setdefault(p0, set()).add(p1)
            p0 = p1

    for p0, p1s in next_pointers.iteritems():
        
        for p1 in p1s:
            if (p1 in next_pointers and
                p0 in next_pointers[p1]):
                next_pointers[p1].remove(p0)
            else:
                print "    missing half-edge V%d-V%d" % (p0, p1)
                n_miss += 1
        p1s.clear()

    print "   missing %d half-edges" % n_miss
        
            
pi = PointIndex()

all_faces = []
        
for infile in infiles:
    print "load", infile
    points, faces = load_off(infile)


    print "Check edge pairs (input):"
    check_edge_pairs(faces)
    
    m = -np.ones(points.shape[0], dtype = int)
    
    for i, v in enumerate(points):
        m[i] = pi.addv(v)

    m_faces = []
    for face in faces:        
        face = m[face]
        m_faces.append(face)
        
    print "size of pi:", len(pi.points)
    
    print "Check edge pairs (remapped):"
    check_edge_pairs(faces)

    all_faces.append(m_faces)


points = np.vstack(pi.points)

norm = np.linalg.norm

class Face:

    def __init__(self, fid, vertices):
        self.fid = fid
        self.mapped_to = fid
        self.vertices = list(vertices)
        p0, p1 = points[vertices[-2]], points[vertices[-1]]
        normals = []
        best_norm = -1
        for p2i in vertices:
            p2 = points[p2i]
            n = np.cross(p2 - p1, p0 - p1)
            nn = norm(n)
            if nn > best_norm:
                best_norm = nn
                self.normal = n / nn 
            p0, p1 = p1, p2
            
        self.b = np.dot(self.normal, points[vertices[0]])
        self.join_faces = set()

    def same_normal_xx(self, face2):
        return np.dot(self.normal, face2.normal) > 1 - same_normal_tol

    def same_normal(self, other):
        # distance to plane
        d12 = [(np.dot(self.normal, points[v]) - self.b) for v in other.vertices]
        d21 = [(np.dot(other.normal, points[v]) - other.b) for v in self.vertices]
        all_d = np.array(d12 + d21)
        return np.max(np.abs(all_d)) < vertex_to_plane_tol
            
        

    def vertices_wrap(self, no):
        n = len(self.vertices)
        return self.vertices[(no + 100 * n) % n]

    def vertices_cut_at(self, pno):
        return self.vertices[pno:] + self.vertices[:pno]

    def normalize_vertex_order(self):
        for vertices in self.loops: 
            vmin = min(vertices)
            i = vertices.index(vmin)
            vertices[:] = vertices[i:] + vertices[:i]
        self.loops.sort()

def make_point_index(faces):
    # return map of (p0, p1) -> face this edge belongs to 
    pt_idx = [{} for p in range(points.shape[0])]

    for fid, face in enumerate(faces):
        p0 = face[-1]
        for pno, p1 in enumerate(face):
            assert p1 not in pt_idx[p0]
            pt_idx[p0][p1] = fid
            p0 = p1
    return pt_idx


def enumerate_neighbors(face, pt_idx, faces):

    for pno, p1 in enumerate(face.vertices):
        p0 = face.vertices[pno - 1 if pno > 0 else -1]
        if p0 not in pt_idx[p1]: continue

        fid2 = pt_idx[p1][p0]
        
        if not face.same_normal(faces[fid2]): continue
        
        face2 = faces[fid2]

        if face2 == face: continue
        
        # find connection
        q0 = face2.vertices[-1]
        for qno, q1 in enumerate(face2.vertices):
            if q1 == p0:
                assert q0 == p1
                break
            q0 = q1
        yield pno, fid2, qno


def edge_projection(p0, p2, p1):
    # project p1 on (p0, p2)
    t = np.dot(p1 - p0, p2 - p0) / np.dot(p2 - p0, p2 - p0)
    return p0 + t * (p2 - p0)
    


        
def simplify_loops(face):
    n_remove = 0

    for loop in face.loops:
        
        i = 0
        n = len(loop)
        while i < n:            
            p0 = points[loop[(i - 1 + n) % n]]
            p1 = points[loop[i]]
            p2 = points[loop[(i + 1) % n]]
            p = edge_projection(p0, p2, p1)

            if norm(p - p1) < straight_line_tol:
                if verbose > 1: print "    loop", loop, "->",
                n_remove += 1
                del loop[i]
                if verbose > 1: print loop
                n -= 1
            else:
                i += 1

    return n_remove

def save_off(fname, loops):
    of = open(fname, "w")
    print >> of, "OFF"
    print >> of, "%d %d 0" % (points.shape[0], len(loops))
    np.savetxt(of, points)

    for loop in loops: 
        print >> of, len(loop), " ".join(["%d" % p for p in loop])
    

def make_face_tab(faces):
    pt_idx = make_point_index(faces)

    faces = [Face(fid, face) for fid, face in enumerate(faces)]

    print "make_face_tab on %d faces" % len(faces)
    
    for fid, face in enumerate(faces):

        if face.mapped_to != fid: continue

        if verbose > 1: 
            print "  handling F%d %s normal %s" % (fid, face.vertices, face.normal)
        
        front = [fid]

        b = np.dot(points[face.vertices[0]], face.normal) 

        while front:
            fid1 = front.pop(0)
            
            for pno, fid2, pno2 in enumerate_neighbors(faces[fid1], pt_idx, faces):
                if fid2 == fid or fid2 in face.join_faces: continue
                face.join_faces.add(fid2)                
                face2 = faces[fid2]
                face2.mapped_to = fid
                if verbose > 1: 
                    print "    merging with F%d %s" % (fid2, face2.vertices)
                    emax = max([np.abs(b - np.dot(points[v], face.normal))
                                for v in face2.vertices])
                    if emax > 1e-3:
                        pdb.set_trace()
                        
                        
                front.append(fid2)

    # perform actual merge ops

    for fid, face in enumerate(faces):
        if face.mapped_to != fid: continue        
        if len(face.join_faces) == 0:
            face.loops = [face.vertices]
            continue

        if verbose > 1: print "  making next table for F%d" % fid        

        to_next = {}        
        # for loop in [face.vertices] + [faces[fid2].vertices for fid2 in face.join_faces]:
        for fid2 in [fid] + list(face.join_faces):
            loop = faces[fid2].vertices
            if verbose > 1:  print "    add loop from F%d: %s normal %s" % (fid2, loop, faces[fid2].normal)
            p0 = loop[-1]
            for p1 in loop:
                if p0 not in to_next: to_next[p0] = set()
                to_next[p0].add(p1)

                p0 = p1

        if verbose > 1: print "  cancel opposing loops"

        for p0, p1s in to_next.iteritems():
            to_del = set()
            for p1 in p1s:
                if p0 in to_next[p1]:
                    if verbose > 1:  print "    cancel", p0, '<->', p1
                    to_next[p1].remove(p0)
                    to_del.add(p1)
            p1s -= to_del


        to_next_1 = {}
        for p0, p1s in to_next.iteritems():
            if len(p1s) > 0:
                if len(p1s) != 1:
                    dump_fname = "/tmp/err_F%d.off" % fid
                    save_off(dump_fname,
                             [face.vertices] + [faces[fid2].vertices for fid2 in face.join_faces])
                    raise RuntimeError("F%d error with p0=%d p1s=%s (stored dump in %s)" % (fid, p0, p1s, dump_fname))
                    
                p1, = p1s
                to_next_1[p0] = p1

        to_next = to_next_1           
        

        loops = face.loops = []
        if verbose > 1: print "  reconstructing loops"
        while to_next:
            p0, p1 = to_next.items()[0]
            if verbose > 1: print "    start from V%d" % p0
            loop = [p0]
            while p1 != p0:
                loop.append(p1)
                if verbose > 1: print "      add", p1
                p1 = to_next.pop(p1)
            to_next.pop(p0)
            if verbose > 1: print "     loop", loop
            loops.append(loop)
    
    faces = [face for fid, face in enumerate(faces) if face.mapped_to == fid]

    print "simplifying loops"

    n_remove = 0
    for face in faces:
        n_remove += simplify_loops(face)

    print "  n_remove=", n_remove
    
    for face in faces:
        face.normalize_vertex_order()
        
    faces.sort(lambda i, j: cmp(i.loops, j.loops))
    return faces


ft0 = make_face_tab(all_faces[0])
ft1 = make_face_tab(all_faces[1])



i, j = 0, 0
n0 = len(ft0)
n1 = len(ft1)

n_eq = 0
miss_0 = []
miss_1 = []
while i < n0 and j < n1:
    c = cmp(ft0[i].loops, ft1[j].loops)

    if c == 0: 
        n_eq += 1
        i += 1
        j += 1
    elif c < 0:
        print "  missing in 1:", ft0[i].loops
        miss_1.append(ft0[i].loops)
        i += 1
    else:
        print "  missing in 0:", ft1[j].loops
        miss_0.append(ft1[j].loops)
        j += 1            

while i < n0:
    print "  missing in 1:", ft0[i].loops
    miss_1.append(ft0[i].loops)
    i += 1

while j < n1:
    print "  missing in 0:", ft1[j].loops
    miss_0.append(ft1[j].loops)
    j += 1

print "n_eq=", n_eq
print "n_miss_0=", len(miss_0)
print "n_miss_1=", len(miss_1)


for miss, ofname in (miss_0, "miss_0"), (miss_1, "miss_1"):                 

    of = open("data/cmp_cgal/%s.off" % ofname, "w")

    print >> of, "OFF"
    print >> of, "%d %d 0" % (points.shape[0], sum([len(loop) for loop in miss]))
    np.savetxt(of, points)

    for loops in miss:
        for loop in loops: 
            print >> of, len(loop), " ".join(["%d" % p for p in loop])

    
