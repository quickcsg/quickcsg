# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com


import pdb

import numpy as np

from matplotlib import pyplot


combs = {}



for l in open("data/eval_segeval/all_combinations.dat", "r"): 
    l = l.split()
    combs.setdefault(l[0], []).append([float(x) for x in l[1:]])

# format:
# 0 input_verts input_faces
# 2 out_v out_f
# 4 n_prim_v n_double n_triple
# 7 n_prim_f n_composed
# 9 t1 t2 t3

pyplot.xlabel("n * log(k')")
pyplot.ylabel("explore time (ms)")



# for op in sorted(combs.keys()): 
for op in "inter", "union", "diff3" :
    tab = np.array(combs[op])
    
    np.savetxt(open("data/eval_segeval/tab_%s.dat" % op, "w"), tab)

    n_tab = tab[:, 1]
    k_tab = tab[:, 3]
    kprim_tab = tab[:, 8]
    explore_tab = tab[:, 10]
    
    pyplot.plot(n_tab * np.log(kprim_tab), explore_tab, '+', label=op)
    
    nonzero = (kprim_tab != 0)

    print "slope_%s=%g" % (op, np.mean(explore_tab[nonzero] / (n_tab[nonzero] * np.log(kprim_tab[nonzero]))))
    

pyplot.legend(loc=2)
        
pyplot.show()

