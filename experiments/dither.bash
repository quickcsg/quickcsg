# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e

tmpdir=data/dither


if [ "$1" == run ] || [ "$1" == runcarve ] ; then

    set -x
    if [ ! -e $tmpdir/dragt.off ]; then
        
        python ../python/transform_off.py ../data/stanford_models/dragon_recon/dragon_vrip.off -o $tmpdir/dragt.off -rotate y 90 -scale x 5 -scale y 5 -scale z 5 -translate z -0.3 -translate x 0.2 -translate z 0.4
        
        
        python ../python/transform_off.py ../data/stanford_models/dragon_recon/dragon_vrip.off -o $tmpdir/dragt2.off -rotate y 270 -scale x 5 -scale y 5 -scale z 5 -translate z 0.3 -translate x 0.25 -translate z 0.4

        for i in 0 1 2 ; do 
            python ../python/mk_dither.py $i 50 
            mv /tmp/yy_$i.off $tmpdir
        done

        python ../data/generate_primitive.py -box 1 > $tmpdir/box.off
        
        python ../data/generate_primitive.py -box 0.6 -o $tmpdir/box.off -translate x 0.6 -translate y 0.6 -translate z -0.6
        
    fi


    if [ "$1" == run ]; then

        for nt in 1 4; do 
            
            ../mesh_csg -dither -o $tmpdir/dither.off $tmpdir/dragt.off $tmpdir/dragt2.off $tmpdir/box.off $tmpdir/yy_0.off $tmpdir/yy_1.off $tmpdir/yy_2.off -random.translation 1e-4 -random.rotation -v 11111111 -tess -parallel.nt $nt | tee $tmpdir/run_nt$nt.log
        
        done
    
    # ../mesh_csg -dither -o /tmp/dither.off /tmp/dragt.off /tmp/dragt2.off /tmp/box.off /tmp/yy_{0,1,2}.off -random.translation 1e-4 -random.rotation -combined /tmp/comb.off -v 11111111

    elif [ "$1" == runcarve ]; then


        for i in 0 1 2 ; do 
            python ../python/mk_dither.py $i 40 
            mv /tmp/yy_$i.off $tmpdir/yy_small_$i.off
        done
        
        if [ ! -e $tmpdir/yy_small_2.obj ]; then
            # convert all to obj
            for i in dragt dragt2 box yy_{0..2} yy_small_{0..2}; do 
                python off_to_obj.py $tmpdir/$i.off $tmpdir/$i.obj 1e-4
            done
        fi 

        carvedir=/home/matthijs/src/pCSG/carve_csg/carve_js/carve-1.4.0
        
        $carvedir/build2/tests/do_dither -o $tmpdir/dither_carve.obj $tmpdir/{dragt,dragt2,box,yy_small_0,yy_small_1,yy_small_2}.obj | tee $tmpdir/carve_small_run.log
        $carvedir/build2/tests/do_dither -o $tmpdir/dither_carve.obj $tmpdir/{dragt,dragt2,box,yy_0,yy_1,yy_2}.obj | tee $tmpdir/carve_run.log

    fi

else 

    function printsum () {
        python -c "import sys; a = [float(x) for x in sys.argv[1:]]; sys.stdout.write('  %.3f' % (sum(a) / 1000.0)) " $@
    }


    for nt in 1 4; do 

        tosum=$( cat $tmpdir/run_nt$nt.log | 
                 grep -E "topology time|exploration in|facet time|revert:|Preprocessing time:" | 
                 sed 's/.* \([0-9][0-9.]*\) ms.*/\1/' )

        echo -n "NT $nt:	"
        printsum $tosum
        echo
    done
fi

