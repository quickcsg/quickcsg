# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e



for xp in t1 t2 h; do 
       

    for nt in 1 all; do
        echo ====== $xp $nt
        case $xp in
            t1) opt="-diff 25 $( echo ../data/ref_timing/t1_{0..49}.off ) ";;
            t2) opt="-min2 $( echo ../data/ref_timing/t2_{0..49}.off ) ";;
            h) opt="-inter $( echo ../data/knots42/cones{0..41}.off ) ";;
        esac
        
        
        if [ $nt == 1 ]; then
            opt="$opt -parallel.nt 1"
        fi
        rd=data/break_down_times/${xp}_nt${nt}

        ( /usr/bin/time -v ../mesh_csg -v 1111111111 -tess $opt -o ${rd}_result.off > ${rd}.stdout ) 2> ${rd}.stderr

    done


done