# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com




for mesh in Armadillo dragon; do 
    
   
    for thr in 1 4; do 
        
        # tests 
        allsums=""
        for op in union inter a-b b-a; do 
            
            case $op in
                union) opts="$in1 $in2 -union";; 
                inter) opts="$in1 $in2 -inter";; 
                a-b) opts="$in1 $in2 -diff 1";; 
                b-a) opts="$in2 $in1 -diff 1";;
            esac
            
            rad=data/cmp_with_spanish/${mesh}_${op}_thr$thr
            
            tosum=$( cat $rad.stdout | grep -E "topology|exploration|facet" | grep ms | cut -d ' ' -f 5 )
            allsums="$allsums $tosum"
        done
        # echo $allsums
        
        echo -n $mesh, $thr threads: 

        python -c "import sys 
a = [float(x) for x in sys.argv[1:13]] 
print '  %.2f' % (sum(a) / 1000.0 / 4.0) " $allsums
        
    done
done