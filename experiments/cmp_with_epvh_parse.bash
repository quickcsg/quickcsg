# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com



echo "# epvh mkcsg mkcsg_1thread"

if [ "$1" == knots42 ]; then

for ncam in $( seq 6 2 20 ) $( seq 20 5 40 ); do 
    echo -n "$ncam	"
    for algo in epvh mkCSG mkCSG_1thread; do 
        
        t=( $(grep real data/cmp_with_epvh/${algo}_$ncam.time) )
        # echo "T=X${t}X"
        t=${t[1]}
        t=${t:2:5}
        echo -n "$t	"            

    done
    echo 

done

elif [ "$1" == kino68 ]; then

for ncam in $( seq 6 2 20 ) $( seq 20 5 65 ) 68; do 
    echo -n "$ncam	"
    for algo in epvh mkCSG mkCSG_1thread; do 
        
        t=( $(grep real data/cmp_with_epvh/kino68_${algo}_$ncam.time) )
        # echo "T=X${t}X"
        t=${t[1]}
        t=${t:2:5}
        echo -n "$t	"            

    done
    echo 

done 


fi