# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com


set -e


if [ "$1" == run ]; then


    for nthread in 1 all; do 
        
        if [ $nthread == 1 ]; then 
            export PYTHONPATH=../python/1thread_compile
        else
            export PYTHONPATH=../python/mt_compile
        fi
        outdir=data/cmp_openscad/nthread_$nthread
        
        mkdir -p $outdir
            
        for model in doggie balljoint; do 
            for maxprims in 2 3 4 6 8 10 12 14 16 20 24 28 32 48 64 128 192; do 
                
                rad=$outdir/${model}_maxprim${maxprims}
                
                if [ $[ maxprims * 2 ] -lt 64 ]; then 
                    bvwidth=1
                else
                    bvwidth=3
                fi
                
                
                python ../python/parse_openscad.py ../data/cad/${model}.csg \
                    -max_prims $maxprims -o $rad.off -bvwidth $bvwidth | tee $rad.stdout 2> $rad.stderr
                
            done
        done
    done
    
elif [ "$1" == parse ]; then
    
    for model in doggie balljoint; do 
        ( 
            echo "#              1 thread                       all threads"
            echo "# group_factor nerr render_time primtive_time nerr render_time primtive_time"
            
            for maxprims in 2 3 4 6 8 10 12 14 16 20 24 28 32 48 64 128 192; do 
                echo -n "$maxprims	"
                for nthread in 1 all; do 
                    outdir=data/cmp_openscad/nthread_$nthread
                    log=$outdir/${model}_maxprim${maxprims}.stdout
                    nerr=0
                    nerrtab=( $( cat $log | grep -E "(^  [0-9]+ times)" | cut -d ' ' -f 3 ) ) 
                    for((i = 0; i < ${#nerrtab[*]}; i++)); do 
                                nerr=$[nerr + nerrtab[i]]
                    done
                    
                    times=$( grep "^render time:" $log | cut -d ' ' -f 3,6 )
                    echo -n "$nerr	$times	"                    
                done 
                echo
            done
        ) | tee data/cmp_openscad/${model}.dat
    done

            
else
    echo run or parse
fi
