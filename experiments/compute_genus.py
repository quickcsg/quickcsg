# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import numpy as np
import sys



def load_off(fname): 

    f = open(fname, "r")
    assert f.readline() == "OFF\n"

    nvertex, nfacet, zero = f.readline().split()

    nvertex = int(nvertex)
    nfacet = int(nfacet)

    print >> sys.stderr, "%d vertices, %d facets" % (nvertex, nfacet)

    assert zero == "0"

    vertices = np.fromfile(f, dtype = float, count = 3 * nvertex, sep = " ")
    points = vertices.reshape(-1, 3)


    faces = []

    for fno in range(nfacet):
        if fno % 100000 == 0:
            print fno, "\r",
            sys.stdout.flush()
        l = f.readline().split()
        nv = int(l[0])
        faces.append(np.array([int(x) for x in l[1:nv + 1]]))
    print
    return points, faces

fname = sys.argv[1]

vertices, faces = load_off(fname)



nv = vertices.shape[0]
nf = len(faces)
ne = sum([len(f) for f in faces]) / 2

euler = nv - ne + nf
genus = (2 - euler) / 2

print "%d vertices, %d faces, %d edges, euler nb %d, genus %d (assuming orientable)" % (nv, nf, ne, euler, genus)
