# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com


set -e

for xp in t1 t2 h; do 
# for xp in t1 t2; do 
       
    # for((nt=1;nt<=48;nt++)); do
    for nt in {1..16} $( seq 18 2 48 ); do
        echo ====== $xp $nt
        case $xp in
            t1) opt="-diff 25 $( echo ../data/ref_timing/t1_{0..49}.off ) ";;
            t2) opt="-min2 $( echo ../data/ref_timing/t2_{0..49}.off ) ";;
            h) opt="-inter $( echo ../data/knots42/cones{0..41}.off ) -kdtree.expand4inter";;
        esac
                
        opt="$opt -parallel.nt $nt"
        
	for run in {0..4}; do 
            rd=data/thread_speedup/${xp}_nt${nt}_run${run}_${HOSTNAME%%.*}
	    
            ( 
		time ../mesh_csg -v 1111111111 -tess $opt  > ${rd}.stdout 
	    ) 2> ${rd}.stderr
	done
    done


done


