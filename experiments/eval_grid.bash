# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -x



if [ "$1" == run ]; then
set -e 

for dataset in knots42 kinovis60; do 
    
    rad=data/eval_grid/${dataset}_baseline
    
    case $dataset in
        knots42)     
            opt="$(echo ../data/knots42/cones{0..41}.off) -cropbbox -40000.1,-40000.2,-40000.3,40000,40000,40000";;
        kinovis60)
            opt="-contours ../data/capture_kinovis/CONTOURS/contour-00426.oc ../data/capture_kinovis/68cam.calib -cropbbox -8,-8,-1,8,8,3  -contourdepth 40 -random.jitter 0.0001 -contourrange 0,60";;
    esac

    echo ============ $rad

    ../mesh_csg $opt -v 01112111 -o $rad.off | tee $rad.log    

    for((sz=4;sz<128;sz+=4)); do 
        
        rad=data/eval_grid/${dataset}_sz$sz
        
        case $dataset in
            knots42)     
                grid=$sz,$sz,$sz;;
            kinovis60)
                grid=$sz,$sz,$[sz/4];;
        esac
        
        echo =========== $rad 

        ../mesh_csg $opt -v 01112111 -o $rad.off -kdtree.grid $grid | tee $rad.log            

    done


done


else 
    # parse

    function parse_log () {
        local log=$1
        ms=($( grep "exploration in" $log ))
        t_ex=${ms[2]}
        ms=($( grep "grid in" $log )) 
        if [ ${#ms[*]} == 0 ]; then
            t_grid=0
            t_fill=0
            t_occ=0
            t_propagation=0
            grid="0 0 0"
        else
            t_grid=${ms[2]}
            ms=($( grep "Occupancy computation:" $log ))             
            t_occ=${ms[2]}
            t_propagation=${ms[4]}
            t_propagation=${t_propagation:1}
            ms=($( grep "fill in nodes:" $log ))                         
            t_fill=${ms[3]}
            pc_ray=${ms[5]}
            pc_ray=${pc_ray:1}
            ns=($( grep "exploreGrid: split bbox" $log )) 
            grid=${ns[10]}
            grid=${grid//*/ }               
        fi        
        echo $t_ex $t_grid $t_occ $t_propagation $t_fill $pc_ray $grid
    }

    for dataset in knots42 kinovis60; do 
        
        datfile=data/eval_grid/${dataset}.dat

        echo -n > $datfile
        
        
        rad=data/eval_grid/${dataset}_baseline
        (
            echo -n "0 "
            parse_log $rad.log    
        ) >> $datfile
        
        

        for((sz=4;sz<128;sz+=4)); do 
            
            rad=data/eval_grid/${dataset}_sz$sz
            
            (
                echo -n "$sz "
                parse_log $rad.log    
            ) >> $datfile
            
        done

    done
   

fi


cat >/dev/null<<EOF

set ylabel "time (ms)"
set xlabel "grid steps"
set grid
set term x11 

# real cost

plot "./data/eval_grid/knots42.dat" using 1:($2) with lines title "knots explore cost", \
     "" using 1:($2+$3) with lines title "knots total cost", \
     "./data/eval_grid/kinovis60.dat" using 1:($2) with lines title "kino explore cost", \
     "" using 1:($2+$3) with lines title "kino total cost"

# assuming propagation is free

plot "./data/eval_grid/knots42.dat" using 1:($2) with linespoints title "knots explore cost", \
     "" using 1:($2+$3-$5) with linespoints title "knots total cost (no propagation)", \
     "./data/eval_grid/kinovis60.dat" using 1:($2) with linespoints title "kino explore cost", \
     "" using 1:($2+$3-$5) with linespoints title "kino total cost (no propagation)"

# break down grid cost

plot "./data/eval_grid/knots42.dat" \
        using 1:($4-$5) with lines title "knots occupacy cost", \
     "" using 1:5 with lines title "knots propagation cost", \
     "" using 1:($6 * (1-$7/100.0)) with lines title "knots fill cost", \
     "" using 1:($6 * $7 / 100.0) with lines title "knots ray shoot cost"



EOF

