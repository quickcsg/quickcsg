# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e

if false; then 

for depth in {0..15}; do 
    for run in {0..19}; do 
        echo ========= depth $depth run $run
        ../mesh_csg -contours ../data/capture_kinovis/CONTOURS/contour-00426.oc ../data/capture_kinovis/68cam.calib  \
            -v 11111111 -cropbbox -10,-10,-10,10,10,10 -contourdepth 40 -random.jitter 0.0001 -contourrange 0,60  \
            -parallel.split_depth $depth > \
       data/eval_parallel_split_depth/60cam_${HOSTNAME%%.*}_depth${depth}_run${run}.stdout
    done
done
        



for lnpoly in {5..15}; do 
    npoly=$[2**lnpoly]

    for run in {0..19}; do 
        echo ========= npoly $npoly run $run
        ../mesh_csg -contours ../data/capture_kinovis/CONTOURS/contour-00426.oc ../data/capture_kinovis/68cam.calib  \
            -v 11111111 -cropbbox -10,-10,-10,10,10,10 -contourdepth 40 -random.jitter 0.0001 -contourrange 0,60  \
            -parallel.split_npoly $npoly > \
       data/eval_parallel_split_depth/60cam_${HOSTNAME%%.*}_npoly${npoly}_run${run}.stdout
    done
done
   

fi

for ddepth in -4 -2 -1 0 1 2 4 6; do 

    for depth in {0..15}; do 
        for run in {0..19}; do 
            pdepth=$[ddepth + depth]
            if [ $pdepth -ge 0 ]; then
                echo ========= depth $depth pdepth $pdepth run $run
                ../mesh_csg -contours ../data/capture_kinovis/CONTOURS/contour-00426.oc ../data/capture_kinovis/68cam.calib  \
                    -v 11111111 -cropbbox -10,-10,-10,10,10,10 -contourdepth 40 -random.jitter 0.0001 -contourrange 0,60  \
                    -parallel.split_depth $pdepth -kdtree.breadth_depth $depth > \
                    data/eval_parallel_split_depth/60cam_${HOSTNAME%%.*}_breadth${depth}_depth${pdepth}_run${run}.stdout
            fi
        done
    done
done
        
