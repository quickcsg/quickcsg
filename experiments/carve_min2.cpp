/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */

#if defined(HAVE_CONFIG_H)
#  include <carve_config.h>
#endif
#include <sys/time.h>


#include <carve/csg.hpp>
#include <carve/timing.hpp>
#include <carve/convex_hull.hpp>


#include "geom_draw.hpp"
#include "geometry.hpp"
#include "read_ply.hpp"
#include "write_ply.hpp"

#include "rgb.hpp"

#include "scene.hpp"

#include "opts.hpp"

#include <gloop/gloopgl.hpp>
#include <gloop/gloopglu.hpp>
#include <gloop/gloopglut.hpp>

#include <fstream>
#include <string>
#include <utility>
#include <set>

#include <time.h>
#include <assert.h>

#ifdef WIN32
#undef min
#undef max
#endif



struct Options : public opt::Parser {
  bool edge_classifier;
  bool rescale;
  bool output;
  std::string output_file;
  bool ascii;
  std::string input_pattern;

  std::vector<std::string> args;
  
  virtual void optval(const std::string &o, const std::string &v) {
    if (o == "--binary"  || o == "-b") { ascii = false; return; }
    if (o == "--ascii"   || o == "-a") { ascii = true; return; }
    if (o == "--rescale" || o == "-r") { rescale = true; return; }
    if (o == "--output"  || o == "-o") { output = true; output_file = v; return; }
    if (o == "--edge"    || o == "-e") { edge_classifier = true; return; }
    if (o == "--in"      || o == "-i") { input_pattern = v; return; }
  }

  virtual void arg(const std::string &a) {
    args.push_back(a);
  }

  Options() {
    ascii = true;
    rescale = false;
    output = false;
    edge_classifier = false;

    option("binary",  'b', false, "binary output");
    option("ascii",   'a', false, "ascii output (default)");
    option("rescale", 'r', false, "rescale prior to CSG operations");
    option("output",  'o', true,  "output result in .ply format");
    option("edge",    'e', false, "use edge classifier");
    option("in",    'i', true,  "input file name");
  }
};


static Options options;



std::vector<carve::geom3d::LineSegment> rays;

carve::poly::Polyhedron * computeMin2(std::vector<carve::poly::Polyhedron *> inputs) {
  carve::poly::Polyhedron *result = NULL; 

  for(int i = 0; i < inputs.size(); i++) {
    for(int j = 0; j < i; j++) {
      printf("  inter %d %d\n", i, j); 
      carve::poly::Polyhedron *i_inter_j = NULL;      
      try {
        i_inter_j = carve::csg::CSG().compute
          (inputs[i], inputs[j], carve::csg::CSG::INTERSECTION, NULL, 
           options.edge_classifier ? carve::csg::CSG::CLASSIFY_EDGE : carve::csg::CSG::CLASSIFY_NORMAL); 
        
      } catch (carve::exception e) {
        printf("    fail!\n");
        exit(1);
      } 
      
      if(!result) {
        result = i_inter_j; 
      } else {
        printf("    union\n"); 
        carve::poly::Polyhedron *new_result = NULL;      
        try {
          new_result = carve::csg::CSG().compute
            (i_inter_j, result, carve::csg::CSG::UNION, NULL, 
             options.edge_classifier ? carve::csg::CSG::CLASSIFY_EDGE : carve::csg::CSG::CLASSIFY_NORMAL); 
        
        } catch (carve::exception e) {
          printf("    fail!\n");
          exit(2);
        } 
        delete result; 
        result = new_result; 
      }      
    }
  }

  return result;  
}



static bool isInteger(const char *str) {
  int count = 0;
  while (*str) {
    if (!isdigit(*str)) {
      return false;
    }
    ++str;
    ++count;
  }
  return count > 0;
}



// for timing
double getmillisecs() {
  struct timeval tv;
  gettimeofday (&tv,NULL);
  return tv.tv_sec*1e3 +tv.tv_usec*1e-3;
}





int main(int argc, char **argv) {
  installDebugHooks();

  int test = 0;
  int inputFilenamesStartAt = -1;

  options.parse(argc, argv);
  
  if (options.args.size() == 0) {
    test = 0;
  } else if (options.args.size() == 1 && isInteger(options.args[0].c_str())) {
    test = atoi(options.args[0].c_str());
  } else {
    std::cerr << "invalid test: " << options.args[0] << "  " << options.args.size() << std::endl;
    exit(1);
  }

  std::vector<carve::poly::Polyhedron*>inputs;

  for(int i = 0; i < test; i++) {
    char fname[256]; 
    sprintf(fname, options.input_pattern.c_str(), i); 
    inputs.push_back(readOBJ(fname));      
  }   

  double t0 = getmillisecs(); 

  carve::poly::Polyhedron * output =  computeMin2(inputs);  

  printf("computed in %.3f ms\n", getmillisecs() - t0); 

  if(options.output) {
    std::ofstream f(options.output_file.c_str());
    writeOBJ(f, output);
  }

  return 0;
}
