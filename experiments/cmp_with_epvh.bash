# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e


epvhdir=/home/matthijs/src/pCSG/EPVH/
export LD_LIBRARY_PATH=$epvhdir
epvh=$epvhdir/linRelease/reconstruct


if [ "$1" == knots42 ]; then
      
for ncam in $( seq 6 2 20 ) $( seq 20 5 40 ); do 
  
    echo ========= prepare datafiles

    calib=data/cmp_with_epvh/calib_$ncam.dat
    contours=data/cmp_with_epvh/contours_$ncam.dat 

    python subsample_contours.py $ncam \
        ../data/knots42/calib ../data/knots42/contours  $calib $contours


    echo ========= running EPVH ncam $ncam
    
    rd=data/cmp_with_epvh/epvh_$ncam

    ( time $epvh $calib $contours -ti -o ${rd}_result.off > ${rd}.stdout 2> ${rd}.stderr ) 2> ${rd}.time 

    echo ========= running mkCSG ncam $ncam

    rd=data/cmp_with_epvh/mkCSG_$ncam

    ( time ../mesh_csg -contours $contours $calib -o ${rd}_result.off \
        -v 11111111 -cropbbox -2,-2,-2,2,2,2 -contourdepth 20  -tess -random.jitter 0.0001 \
        > ${rd}.stdout 2> ${rd}.stderr ) 2> ${rd}.time 

    rd=data/cmp_with_epvh/mkCSG_1thread_$ncam

    ( time ../mesh_csg -contours $contours $calib -o ${rd}_result.off \
        -v 11111111 -cropbbox -2,-2,-2,2,2,2 -contourdepth 20 -parallel.nt 1 -tess -random.jitter 0.0001 \
        > ${rd}.stdout 2> ${rd}.stderr ) 2> ${rd}.time 
    
done


elif [ "$1" == kino68 ]; then

    set -x

# for ncam in $( seq 6 2 20 ) $( seq 20 5 65 ) 68; do 
for ncam in 65 68; do
  
    echo ========= prepare datafiles

    calib=data/cmp_with_epvh/kino68_calib_$ncam.dat
    contours=data/cmp_with_epvh/kino68_contours_$ncam.dat 

    python subsample_contours.py $ncam \
        ../data/capture_kinovis/68cam.calib ../data/capture_kinovis/CONTOURS/contour-00426.oc  $calib $contours

    echo ========= running EPVH ncam $ncam
    
    rd=data/cmp_with_epvh/kino68_epvh_$ncam

    ( time $epvh $calib $contours -ti -o ${rd}_result.off > ${rd}.stdout 2> ${rd}.stderr ) 2> ${rd}.time 

    echo ========= running mkCSG ncam $ncam

    rd=data/cmp_with_epvh/kino68_mkCSG_$ncam

    if [ $ncam -gt 63 ]; then
        exe=../mesh_csg_bw2
    else
        exe=../mesh_csg
    fi

    ( time $exe -contours $contours $calib -o ${rd}_result.off \
        -v 11111111 -cropbbox -10,-10,-10,10,10,10 -contourdepth 20  -tess -random.jitter 0.0001 -combined ${rd}_combined.off \
        > ${rd}.stdout 2> ${rd}.stderr ) 2> ${rd}.time 

    rd=data/cmp_with_epvh/kino68_mkCSG_1thread_$ncam

    ( time $exe -contours $contours $calib -o ${rd}_result.off \
        -v 11111111 -cropbbox -10,-10,-10,10,10,10 -contourdepth 20 -parallel.nt 1 -tess -random.jitter 0.0001 \
        > ${rd}.stdout 2> ${rd}.stderr ) 2> ${rd}.time 
    
done





fi