# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com



targ="topology exploration facet"


echo "${targ// / & }" "& total & RSS (kB) " 
for nt in 1 all; do
    for xp in t1 t2 h; do 
        echo -n "$xp nt=$nt	"
        rd=data/break_down_times/${xp}_nt${nt}

        tosum=""
        
        for kw in $targ; do 
            tt=( $( grep $kw $rd.stdout | grep ms ) )
            tt=$(printf %.1f ${tt[2]})
            echo -n "$tt	"
            tosum="$tosum $tt"            
        done
        
        echo -n "total: "
        python -c "import sys; a = [float(x) for x in sys.argv[1:]]; sys.stdout.write('%.1f\t' % sum(a)) " $tosum

        echo -n 'mem:' $( cat $rd.stderr  | grep "Maximum resident set size" | cut -d : -f 2 )
    

        if false; then
            # not interested in wallclock
            t=$(cat $rd.stderr | grep real)
            t="${t%% *}"
            t="${t#*m}"
            t="${t/./}"
            t="${t/s/}"
            echo -n "& $t	"
        fi 

        echo
    done

done
