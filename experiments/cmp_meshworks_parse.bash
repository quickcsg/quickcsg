# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com




for xp in dragon-bunny buddhaUlion; do 

    log=data/cmp_meshworks/$xp

    echo -n $xp  ' '

    cat $log.stdout  | grep -E "(topology time|exploration in|facet time)" | awk '{s += $3; } 
END {print s; }'
  

done
    
