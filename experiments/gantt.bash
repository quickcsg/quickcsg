# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com


set -e

for xp in t1 t2 h; do 
    # for nt in {1..16} $( seq 18 2 48 ); do
    # for nt in 1 4 8 12 16 24 32 48; do 
    for nt in 48; do 
        echo ====== $xp $nt
        case $xp in
            t1) opt="-diff 25 $( echo ../data/ref_timing/t1_{0..49}.off ) ";;
            t2) opt="-min2 $( echo ../data/ref_timing/t2_{0..49}.off ) ";;
            h) opt="-inter $( echo ../data/knots42/cones{0..41}.off ) -kdtree.expand4inter -parallel.split_depth 8";;
        esac
                
        opt="$opt -parallel.nt $nt"
        
	rd=data/gantt/${xp}_nt${nt}_${HOSTNAME%%.*}
	    
	../mesh_csg -tess -v 111111111 $opt -kdtree.log_stats ${rd}_kdtree.log -log_make_facets ${rd}_facets.log -kdtree.keep_nodes > ${rd}.stdout 

    done


done
