# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e 
set -x

dir=$PWD
datadir=data/cmp_with_carve
gdatadir=$PWD/$datadir

# preserve source file...

cp /home/matthijs/src/pCSG/carve_csg/carve_js/carve-1.4.0/tests/test_intersect_nogui.cpp .
cp $HOME/src/pCSG/cork/src/main.cpp cork_main.cpp

docarve=yes
doours=no
docork=no

for xp in 21 29 30 34 1001 1003; do 

    
    # conversion to obj for carve
    
    # if [ $xp == 1001 ]; then 
    if false; then
        destdir=/home/matthijs/src/pCSG/carve_csg/carve_js/carve-1.4.0/data
        for i in {0..49}; do
            python off_to_obj.py ../data/ref_timing/t1_$i.off $destdir/t1/$i.obj            
        done
    fi

    # if [ $xp == 1003 ]; then 
    if false; then 
        destdir=/home/matthijs/src/pCSG/carve_csg/carve_js/carve-1.4.0/data
        for i in {0..41}; do
            python off_to_obj.py ../data/knots42/cones$i.off $destdir/knots42/$i.obj            
        done
    fi

    if [ $docarve == yes ]; then

        echo ========= experiment $xp, carve
        
        
        # time carve csg + generate input and output 
        
        cd /home/matthijs/src/pCSG/carve_csg/carve_js/carve-1.4.0/build2

        rd=$gdatadir/carve_$xp
        echo logs in $rd
        
        time (  \
            tests/test_intersect_nogui $xp > $rd.stdout 2> $rd.stderr
        ) 2> $rd.time
        
        if [ $? != 0 ]; then
            cat $rd.stderr
            exit
        fi
        

        cat $rd.stderr
        cat $rd.time
        echo meshlab $gdatadir/test_${xp}_result.obj

        # let carve output the input for our algo
        tests/test_intersect_nogui -d $gdatadir/test_${xp} $xp

        cd $dir

    fi


    if [ $doours == yes ]; then

        # time our algorithm
        echo ========= experiment $xp, ours

        case $xp in
            21) opt="-diff 1"; ninput=2 ;;
            29) opt="-union -random.translation 1e-4 -random.rotation "; ninput=30;;
            30) opt="-diff 1"; ninput=3;;
            34) opt="-union -random.translation 1e-5"; ninput=2;;
            1001) opt="-diff 25"; ninput=50;;
            1003) opt="-inter"; ninput=42;;
        esac 
        
        for((i=0;i<ninput;i++)); do
            opt="$opt $datadir/test_${xp}_input_$i.obj"
        done
        
        rd=$datadir/ours_1thread_$xp
        
        time ( 
            ../mesh_csg $opt -o ${rd}_result.off -inobj -v 1111111 -tess -parallel.nt 1 > $rd.stdout 2> $rd.stderr
        ) 2> $rd.time
        
        if [ $? != 0 ]; then
            cat $rd.stderr
            exit
        fi
        
        rd=$datadir/ours_$xp
        
        time ( 
            ../mesh_csg $opt -o ${rd}_result.off -inobj -v 1111111 -tess > $rd.stdout 2> $rd.stderr
        ) 2> $rd.time
        
        if [ $? != 0 ]; then
            cat $rd.stderr
            exit
        fi
        
        cat $rd.stderr
        cat $rd.stdout
        cat $rd.time    
        echo meshlab $PWD/${rd}_result.off
        
    fi 

    if [ $docork == yes ]; then

        # time cork CSG
        echo ========= experiment $xp, Cork
        
        case $xp in
            21) opt="diff1"; ninput=2 ;;
            29) opt="union "; ninput=30;;
            30) opt="diff1"; ninput=3;;
            34) opt="union"; ninput=2;;
            1001) opt="diff25"; ninput=50;;
            1003) opt="inter"; ninput=42;;
        esac 
        
        for((i=0;i<ninput;i++)); do
            tmpname=/tmp/for_cork_$i.off
            python ../data/stanford_models/to_off.py $datadir/test_${xp}_input_$i.obj $tmpname triangles
            opt="$opt $tmpname"
        done
        
        rd=$datadir/cork_$xp
        
        time ( 
            # ulimit -v $[2048*1024] # limit to 2G of RAM
            $HOME/src/pCSG/cork/bin/cork -nary $opt ${rd}_result.off > $rd.stdout 2> $rd.stderr
        ) 2> $rd.time
        
        if [ $? != 0 ]; then
            cat $rd.stderr
            exit
        fi

        echo meshlab $PWD/${rd}_result.off

    fi


done

