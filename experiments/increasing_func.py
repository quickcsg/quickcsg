# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import random
import pdb, sys
import numpy as np

nbit, outfilename = sys.argv[1:3]


nbit = int(nbit)
n = 1 << nbit

unknown = set(range(1, n-1))
labels = [-1] * n 
labels[0] = 0
labels[n - 1] = 1

# random.seed(0)

def propagate_up(i): 
    for j in range(nbit): 
        if (i >> j) & 1: continue
        i2 = i | (1 << j)
        if labels[i2] == 1: continue
        assert labels[i2] == -1
        print i2,
        labels[i2] = 1
        unknown.remove(i2)
        propagate_up(i2)

def propagate_down(i): 
    for j in range(nbit): 
        if not ((i >> j) & 1): continue
        i2 = i & ~(1 << j)
        if labels[i2] == 0: continue
        assert labels[i2] == -1
        print i2,
        labels[i2] = 0
        unknown.remove(i2)
        propagate_down(i2)


        

while unknown: 
    
    i = random.choice(list(unknown))

    label = random.choice([0,1])

    print "%d -> %d" % (i, label)
    assert labels[i] == -1, pdb.set_trace()
    labels[i] = label
    unknown.remove(i)
    print "   ",
    if label == 1: 
        propagate_up(i)
    else: 
        propagate_down(i)

    print


res = np.zeros(n / 8, dtype = 'uint8')

for i in range(n): 
    if labels[i]: 
        res[i >> 3] |= 1 << (i & 7)

res.tofile(outfilename)

