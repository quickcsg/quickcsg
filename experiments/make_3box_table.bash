# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com




for i in {0..255}; do

    echo ======== $i

    ../mesh_csg -csgtable $i ../data/3boxes/box{0..2}.off -o data/3box_table/tab_$i.off -tess -v 1111111 | 
       tee data/3box_table/tab_$i.log

    ../mesh_csg -csgtable $i ../data/3boxes/box{0..2}.off -o data/3box_table/tab_notess_$i.off  -v 1111111 | 
       tee data/3box_table/tab_notess_$i.log


done
