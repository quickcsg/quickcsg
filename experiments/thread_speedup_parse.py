# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import numpy as np

# machine="idfreeze"

machine="pc4dv-prod-1"

for xp in "t1 t2 h".split():
       
    of = open("data/thread_speedup/times_%s_%s.dat" % (machine, xp), "w")
    
    print >>of, "# nt topology_time(avg,min,max) exploration_time(avg,min,max) facet_time(avg,min,max) total(avg,min,max)"
    
    for nt in range(1, 17) + range(18, 49, 2): 
        
        ttab = np.zeros((3, 5))

        for run in range(5):

            rd = "data/thread_speedup/%s_nt%d_run%d_%s.stdout" % (xp, nt, run, machine)
            lines = open(rd, "r").readlines()
            
            for s, key in enumerate("topology exploration facet".split()): 
                for l in lines:
                    if key in l and "ms" in l:
                        ttab[s, run] = float(l.split()[2])
        print >>of, nt,

        for s in range(3): 
            print >>of, ttab[s, :].mean(), ttab[s, :].min(), ttab[s, :].max(), "   ", 

        tx = ttab.sum(0)
        print >>of, tx.mean(), tx.min(), tx.max() 
    
        if nt == 1: 
            print "ref_t_%s=" % xp, tx.mean()

"""


set xlabel "nb of threads"
set ylabel "speedup"

plot [:] [:25] x with lines title "linear", \
   "data/thread_speedup/times_idfreeze_t1.dat" using 1:(ref_t_t1/$2):(ref_t_t1/$3):(ref_t_t1/$4) with errorbars title "T1",\
   "data/thread_speedup/times_idfreeze_t2.dat" using 1:(ref_t_t2/$2):(ref_t_t2/$3):(ref_t_t2/$4) with errorbars title "T2",\
   "data/thread_speedup/times_idfreeze_h.dat" using 1:(ref_t_h/$2):(ref_t_h/$3):(ref_t_h/$4) with errorbars title "H"

set xlabel "nb of threads"
set ylabel "time (ms)"

plot \
   "data/thread_speedup/times_idfreeze_t1.dat" using 1:2:3:4 with errorbars title "T1",\
   "data/thread_speedup/times_idfreeze_t2.dat" using 1:2:3:4 with errorbars title "T2",\
   "data/thread_speedup/times_idfreeze_h.dat" using 1:2:3:4 with errorbars title "H"

"""
