# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import sys, time, random
import numpy as np 

np.random.seed(0)
random.seed(0)

import pymCSG

nerr = 0
opno = 100

def report_errors(csg, op):
    errs = csg.errors        

    if errs.haveErrors():
        print "ERROR op=", op
        print "opno=", opno
        errs._print()
        errs.clear()        
        global nerr
        # csg.combinedToOff("/tmp/err_meshes_%d.off" % nerr)
        nerr += 1

def read_VF(fname): 
    f = open(fname, "r")
    assert f.readline() == "OFF\n"

    nvertex, nfacet, zero = f.readline().split()

    nvertex = int(nvertex)
    nfacet = int(nfacet)
    assert zero == "0"

    vertices = np.fromfile(f, dtype = 'float64', count = 3 * nvertex, sep = " ")
    vertices = vertices.reshape(-1, 3)

    facets = np.fromfile(f, dtype = 'int32', sep = " ")

    # mini-sanity check
    if facets.size > 0: 
        i = facets.size - 1
        while i > 0: 
            if facets[i] + i + 1 == facets.size: break
            i -= 1
        else: 
            assert False, "facet table does not look correct"

    return vertices, facets

    
def write_VF(fname, (pts, faces)):
    f = open(fname, "w")
    print >>f, "OFF"
    nface = i = 0
    while i < len(faces): 
        nface += 1
        i += faces[i] + 1
    print >> f, pts.shape[0], nface, 0
    np.savetxt(f, pts)
    pymCSG.facets_fwrite(f, faces, None)




def exec_op(csgop, args, cropbox = None): 
    t0 = time.time()
    params = pymCSG.CSGParameters()
    params.tesselate = 1
    csg = pymCSG.CSG(params)
        
    for a in args: 
        pts, faces = a
        csg.addVFArray(pts, faces, 0)

    t1 = time.time()
    csg.afterLoad()
    csg.initKDTree()
    if cropbox != None: 
        bbMin, bbMax = cropbox
        csg.cropRootToBBox(bbMin, bbMax); 

    csg.verbose = 0
    csg.exploreKDTree(csgop)
    csg.makeFacets(csgop)
    t2 = time.time()
    pts, faces, facet_tab, normals = csg.toVFArray()
    
    print "  element time: %.3f s (%.3f s real computations)" % (time.time() - t0, t2 - t1)
    
    report_errors(csg, csgop)
    
    return pts, faces

def union(args): 
    if len(args) == 1: return args[0]
    return exec_op(pymCSG.CSGUnion(len(args)), args)

def diff(npos, args): 
    return exec_op(pymCSG.CSGDiff(len(args), npos), args)

def inter(args, cropbox = None): 
    if len(args) == 1: return args[0]
    return exec_op(pymCSG.CSGIntersection(len(args)), args, cropbox)

def bintree_union(args): 
    while len(args) != 1: 
        args = [union(args[i:i+2]) for i in range(0, len(args), 2)]
    return args[0]

def bintree_inter(args, cropbox = None): 
    while len(args) != 1: 
        args = [inter(args[i:i+2], cropbox) for i in range(0, len(args), 2)]
        cropbox = None
    return args[0]



dataset, algo = sys.argv[1:]

print "dataset %s, combination %s" % (dataset, algo)



if dataset == "T1": 
    data = [read_VF("../data/ref_timing/t1_%d.off" % i) for i in range(50)]

    t0 = time.time()

    if algo == "50": 
        res = diff(25, data)
    elif algo == "5+5+2": 
        data2 = [union(data[i:i+5]) for i in range(0, 50, 5)]
        pos = union(data2[:5])
        neg = union(data2[5:])
        res = diff(1, [pos, neg])
    elif algo == "25+2": 
        pos = union(data[:25])
        neg = union(data[25:])
        res = diff(1, [pos, neg])
    elif algo == "tree2": 
        pos = bintree_union(data[:25])
        neg = bintree_union(data[25:])
        res = diff(1, [pos, neg])
    elif algo == "seq": 
        res = data[0]
        for op2 in data[1:25]:            
            res = union([res, op2])
        for op2 in data[25:50]:
            res = diff(1, [res, op2])
    else: 
        assert False
    t1 = time.time()
    print "CSG time: %.3f s" % (time.time() - t0)

elif dataset == "H": 
    data = [read_VF("../data/knots42/cones%d.off" % i) for i in range(42)]
    
    rr3, _ = np.linalg.qr(np.random.randn(9).reshape(3, 3))

    for pts, faces in data: 
        pts[:] = np.dot(pts + np.random.rand(3).reshape(1, 3) * 0.5, rr3)
    
    random.shuffle(data)

    # cropbox = (pymCSG.cVec3(-10000, -10000, -5000), 
    #           pymCSG.cVec3(10000, 10000, 5000))
    cropbox = None
    
    t0 = time.time()
    if algo == "42": 
        res = inter(data, cropbox)
    elif algo == "8+6": 
        data2 = [inter(data[i:i+8], cropbox) for i in range(0, 42, 8)]
        res = inter(data2)
    elif algo == "4+11": 
        data2 = [inter(data[i:i+4], cropbox) for i in range(0, 42, 4)]
        res = inter(data2)        
    elif algo == "tree2": 
        res = bintree_inter(data, cropbox)
    elif algo == "seq": 
        res = data[0]
        for op2 in data[1:]:            
            res = inter([res, op2], cropbox)        
    else: 
        assert False
    t1 = time.time()
    print "CSG time: %.3f s" % (time.time() - t0)


ofname = "data/cmp_hierarchy/result_%s_%s.off" % (dataset, algo)

write_VF(ofname, res)
