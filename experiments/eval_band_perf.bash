# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e

commonthread="-contours ../data/capture_kinovis/CONTOURS/contour-00426.oc ../data/capture_kinovis/68cam.calib  -v 11112111 -cropbbox -10,-10,-10,10,10,10  -contourdepth 40 -random.jitter 0.0001 -contourrange 0,60  -kdtree.keep_nodes"

common="$commonthread -parallel.nt 1"

outdir=data/eval_band_perf


hn=${HOSTNAME%%.*}

set -x 

if false; then

# all these experiments done on mohawk

../mesh_csg $common > $outdir/log_ref
../mesh_csg $common -kdtree.tight_bbox_depth 24 > $outdir/log_ref_ntb

../mesh_csg $common -kdtree.bands 555 > $outdir/log_bands555
../mesh_csg $common -kdtree.bands 554433 > $outdir/log_bands554433

../mesh_csg $common -kdtree.bands 555 -kdtree.tight_bbox_depth 15 > $outdir/log_bands555_ntb
../mesh_csg $common -kdtree.bands 554433 -kdtree.tight_bbox_depth 24 > $outdir/log_bands554433_ntb


../mesh_csg $common -kdtree.bands 444 > $outdir/log_bands444
../mesh_csg $common -kdtree.bands 666 > $outdir/log_bands666

../mesh_csg $common -kdtree.bands 444444 > $outdir/log_bands444444

../mesh_csg $common -kdtree.bands 65433 > $outdir/log_bands65433


../mesh_csg $common > $outdir/log_${hn}_ref
../mesh_csg $commonthread > $outdir/log_${hn}_thread_ref

../mesh_csg $common -kdtree.bands 555 > $outdir/log_${hn}_band555
../mesh_csg $commonthread -kdtree.bands 555 > $outdir/log_${hn}_thread_band555

fi

set +x


cat <<EOF

set term x11
set grid
set key bottom
set xlabel "depth"
set ylabel "cost (ms)"

cummulative_sum1(x)=(a1=a1+x,a1)
cummulative_sum2(x)=(a2=a2+x,a2)
cummulative_sum3(x)=(a3=a3+x,a3)
cummulative_sum4(x)=(a4=a4+x,a4)
cummulative_sum5(x)=(a5=a5+x,a5)
cummulative_sum6(x)=(a6=a6+x,a6)
cummulative_sum7(x)=(a7=a7+x,a7)
cummulative_sum8(x)=(a8=a8+x,a8)
cummulative_sum9(x)=(a9=a9+x,a9)

Mcy_to_ms=1000 / 2670.0

a1=a2=a3=a4=a5=a6=a7=a8=a9=0; 

plot [:] [0:] \\
EOF


i=0

# single thread experiments
# for key in ref ref_ntb bands555 bands555_ntb bands554433 bands554433_ntb bands444; do 

for key in ${hn}_ref ${hn}_thread_ref ${hn}_band555 ${hn}_thread_band555; do 

    grep "^   depth" $outdir/log_$key > $outdir/stats_$key
    i=$[i+1]
    echo "    \"$outdir/stats_$key\" using (Mcy_to_ms * cummulative_sum$i(\$13)) with lines title \"$key\", \\"
done


