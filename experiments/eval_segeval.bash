# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com


set -e 
# set -x

todo=$1

function parse_segeval () {
    rad=$1

    op=${rad##*/}
    op=${op%%.*}
    
    stats=($( grep meshes, $rad.log | grep vertices  ) )
    
    t1=$( grep "topology time" $rad.log | cut -d ' ' -f 5 )
    t2=$( grep "exploration in" $rad.log | cut -d ' ' -f 5 )
    t3=$( grep "facet time" $rad.log | cut -d ' ' -f 5 )
    
    outstats=( $(head -2 $rad.off | tail -1 ) )
    
    outstats2=( $(python count_prim.py $rad.off ) )

    outstats3=$(python split_stats.py ${rad}_kdtree.log d_f_n) 
    
    # stats: 2 nb input verts and faces 
    # outstats: 4 nb of output verts and faces
    # outstats2: 6 #prim #double #triple 
    #            9 #prim #composed 
    # t: 11 topo kdtree facets
    # outstats3: 14 #infacets #subdivided
    echo $op ${stats[2]} ${stats[4]} ${outstats[0]} ${outstats[1]} ${outstats2[2]} ${outstats2[4]} ${outstats2[6]} ${outstats2[9]} ${outstats2[11]} $t1 $t2 $t3 $outstats3
    
}





if [ "$todo" == run ] || [ "$todo" == run_log ] ; then

indir=/home/matthijs/src/mkCSG/data/segeval/MeshsegBenchmark-1.0/data/off



RANDOM=123

for i in {1..2000}; do 

    inputs=""
    key=""
    classes='.'
    for j in {1..5}; do 
        while true; do            
            class=$[RANDOM % 19]
            if [ $class == 13 ]; then # skip over 261..280
                ((class++))
            fi
            # check if class used
            if [ "${classes/.$class.}" == "$classes" ]; then
                break
            fi
        done
        classes="$classes$class."
        meshno=$[1 + RANDOM % 20 + class * 20] 
        if [ $meshno == 400 ]; then meshno=399; fi
        inputs="$inputs $indir/$meshno.off"
        key="$key.$meshno"
    done
        
    r=$[RANDOM % 6]
    if [ $r == 0 ]; then
        inputs="$inputs -union"
        key=union$key
    elif [ $r -lt 5 ]; then
        inputs="$inputs -diff $r"
        key=diff$r$key
    else
        inputs="$inputs -inter"
        key=inter$key
    fi

    rad=data/eval_segeval/$key

    if [ $todo == run ]; then

        ../mesh_csg -v 11111111 $inputs -o $rad.off -parallel.nt 1 -tess -random.translation 1e-4 -random.rotation | tee $rad.log 

    else 
        ../mesh_csg -v 11111111 $inputs  -parallel.nt 1 -tess -random.translation 1e-4 -random.rotation -kdtree.log_stats ${rad}_kdtree.log  -kdtree.keep_nodes
        
    fi


done

else
    # parse

    ncpu=$(cat /proc/cpuinfo | grep ^processor | wc -l) 

    prefix=/tmp/task_lock.$$ # some non-existing name
    rm -f $prefix*
    touch $prefix.0
    
    trap 'kill -HUP 0' 0 # handle exit gracefully
    for((cpu=0; cpu<ncpu; cpu++)); do
        
        ( 
            i=0
            for r in data/eval_segeval/*.off; do 
                if mv $prefix.$i $prefix.$[i+1] 2> /dev/null; then
                    parse_segeval ${r%.off} | tee -a data/eval_segeval/all_combinations+split_stats.dat
                fi
                i=$[i+1]
            done
        ) & 

    done 
    wait
fi
 

