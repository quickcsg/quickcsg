# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com



function get_exploration_t () {
    
    a=""

    for fname in $@; do             
        fields=($( grep "exploration in" $fname ))
        a="$a ${fields[2]}"
    done
    
    python -c '
import sys
import numpy as np
x = [float(v) for v in sys.argv[1:]]
print "%.1f\t%.1f" % (np.mean(x), np.std(x))
' $a
}



for machine in node33 mohawk idfreeze; do 
    for suf in "" _nops _depth20; do 

        echo ======== $machine $suf
        
        for nt in 1 2 4 8 12 16 24 32 48; do 
            
            if [ $machine == mohawk ] && [ $nt -gt 4 ]; then
                break
            elif [ $machine == node33 ] && [ $nt -gt 32 ]; then
                break
            fi
            
            
            tref=$( get_exploration_t data/better_parallel/${machine}_nt${nt}_run{1..5}_ref$suf.log )        
            tnew1=$( get_exploration_t data/better_parallel/${machine}_nt${nt}_run{1..5}_new1$suf.log )
            
            echo "$nt	$tref	$tnew1"
    
        done | tee data/better_parallel/plot_$machine$suf.dat
    done

done


cat > /dev/null <<EOF

cp experiments/better_parallel.bash ~/g5k/mkCSG/experiments/

cp ~/g5k/mkCSG/experiments/data/better_parallel/idfreeze* data/better_parallel/


set title "cmp ref and new1"

plot "./data/better_parallel/plot_node33.dat" using 1:2:3 with errorlines title "node33, ref", \
     "" using 1:4:5 with errorlines title "node33, new", \
     "./data/better_parallel/plot_mohawk.dat" using 1:2:3 with errorlines title "mohawk, ref", \
     "" using 1:4:5 with errorlines title "mohawk, new", \
     "./data/better_parallel/plot_idfreeze.dat" using 1:2:3 with errorlines title "idfreeze, ref", \
     "" using 1:4:5 with errorlines title "idfreeze, new"

set title "cmp with and without parallel split"

plot "./data/better_parallel/plot_node33.dat" using 1:2:3 with errorlines title "node33, ref", \
     "./data/better_parallel/plot_node33_nops.dat" using 1:2:3 with errorlines title "node33, nops", \
     "./data/better_parallel/plot_mohawk.dat" using 1:2:3 with errorlines title "mohawk, ref", \
     "./data/better_parallel/plot_mohawk_nops.dat" using 1:2:3 with errorlines title "mohawk, nops", \
     "./data/better_parallel/plot_idfreeze.dat" using 1:2:3 with errorlines title "idfreeze, ref", \
     "./data/better_parallel/plot_idfreeze_nops.dat" using 1:2:3 with errorlines title "idfreeze, nops"

set title "cmp with and without parallel split"

plot "./data/better_parallel/plot_node33_depth20.dat" using 1:2:3 with errorlines title "node33, depth 20", \
     "./data/better_parallel/plot_node33_nops.dat" using 1:2:3 with errorlines title "node33, nops", \
     "./data/better_parallel/plot_mohawk_depth20.dat" using 1:2:3 with errorlines title "mohawk, depth 20", \
     "./data/better_parallel/plot_mohawk_nops.dat" using 1:2:3 with errorlines title "mohawk, nops", \
     "./data/better_parallel/plot_idfreeze_depth20.dat" using 1:2:3 with errorlines title "idfreeze, depth 20", \
     "./data/better_parallel/plot_idfreeze_nops.dat" using 1:2:3 with errorlines title "idfreeze, nops"

EOF