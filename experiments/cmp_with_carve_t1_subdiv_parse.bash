# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com


dir=$PWD
datadir=data/cmp_with_carve_t1_subdiv
gdatadir=$PWD/$datadir

function printsum () {
    python -c "import sys; a = [float(x) for x in sys.argv[1:]]; sys.stdout.write('  %.3f' % (sum(a) / 1000.0)) " $@
    
}


for uvf in 10 15 20 30 40 50 60 80 100 150 200; do 

    instats=( $( grep meshes $datadir/ours_$uvf.stdout | head -1 ) )
    nmesh=${instats[0]}
    nface=${instats[4]}

    echo -n "$uvf	$nmesh	$nface	" 
    
    
    # for pf in carve ours_1thread cork; do 

    for pf in carve ours_1thread ours; do
        

        # time without i/o and startup
        
        if [ $pf == carve ]; then
            
            tosum=$(cat $datadir/${pf}_$uvf.stdout | grep -E "input topology:|compute:" | cut -d : -f 2 | tr -d ms )
            
            tc=$( printsum $tosum) 
            echo -n "$tc	"
        else
            targ="topology exploration facet"
            tosum=$(cat $datadir/${pf}_$uvf.stdout | grep -E "topology|exploration|facet" | grep ms | cut -d ' ' -f 5 )
            to=$( printsum $tosum )  
            echo -n "$to	"
        fi
        mem=$(cat data/cmp_with_carve_t1_subdiv/${pf}_$uvf.stderr | grep "Maximum resident set size (kbytes):" | cut -d : -f 2)
        echo -n "$mem	"        
    done
    echo
    # awk "BEGIN {print $tc / $to; }"

done



cat > /dev/null <<EOF

set xlabel "nb of facets"  
set ylabel "speedup wrt. carve"

set grid
set logscale x
set key left

plot "/tmp/l" using 3:($4/$6) with lines title "1 thread", "" using 3:($4/$8) with lines title "4 threads"

set ylabel "mem usage (MiB)"


plot "/tmp/l" using 3:($5/1024) with lines title "Carve", "" using 3:($7/1024) with lines title "QuickCSG"


EOF
