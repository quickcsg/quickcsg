/** Software QuickCSG – Copyright (C) by INRIA
 * License: Free software license agreement for non-commercial purposes 2013-2015
 * Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com
 */
// Begin License:
// Copyright (C) 2006-2008 Tobias Sargeant (tobias.sargeant@gmail.com).
// All rights reserved.
//
// This file is part of the Carve CSG Library (http://carve-csg.com/)
//
// This file may be used under the terms of the GNU General Public
// License version 2.0 as published by the Free Software Foundation
// and appearing in the file LICENSE.GPL2 included in the packaging of
// this file.
//
// This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
// INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE.
// End:


#if defined(HAVE_CONFIG_H)
#  include <carve_config.h>
#endif
#include <sys/time.h>


#include <carve/csg.hpp>
#include <carve/timing.hpp>
#include <carve/convex_hull.hpp>


#include "geom_draw.hpp"
#include "geometry.hpp"
#include "read_ply.hpp"
#include "write_ply.hpp"

#include "rgb.hpp"

#include "scene.hpp"

#include "opts.hpp"

#include <gloop/gloopgl.hpp>
#include <gloop/gloopglu.hpp>
#include <gloop/gloopglut.hpp>

#include <fstream>
#include <string>
#include <utility>
#include <set>

#include <time.h>
#include <assert.h>

#ifdef WIN32
#undef min
#undef max
#endif



struct Options : public opt::Parser {
  bool edge_classifier;
  bool rescale;
  bool output;
  std::string output_file;
  bool ascii;
  std::string data_out; 

  std::vector<std::string> args;
  
  virtual void optval(const std::string &o, const std::string &v) {
    if (o == "--binary"  || o == "-b") { ascii = false; return; }
    if (o == "--ascii"   || o == "-a") { ascii = true; return; }
    if (o == "--rescale" || o == "-r") { rescale = true; return; }
    if (o == "--output"  || o == "-o") { output = true; output_file = v; return; }
    if (o == "--edge"    || o == "-e") { edge_classifier = true; return; }
    if (o == "--data"    || o == "-d") { data_out = v; return; }
  }

  virtual void arg(const std::string &a) {
    args.push_back(a);
  }

  Options() {
    ascii = true;
    rescale = false;
    output = false;
    edge_classifier = false;

    option("binary",  'b', false, "binary output");
    option("ascii",   'a', false, "ascii output (default)");
    option("rescale", 'r', false, "rescale prior to CSG operations");
    option("output",  'o', true,  "output result in .ply format");
    option("edge",    'e', false, "use edge classifier");
    option("data",    'd', true,  "dump inputs and output to this prefix (OBJ format)");
  }
};


static Options options;



std::vector<carve::geom3d::LineSegment> rays;

#if 0
std::string data_path = "/Users/sargeant/projects/PERSONAL/CARVE/data/";
#else
std::string data_path = "../data/";
#endif

bool odd(int x, int y, int z) {
  return ((x + y + z) & 1) == 1;
}

bool even(int x, int y, int z) {
  return ((x + y + z) & 1) == 0;
}

class Input {
public:
  Input() {
    poly = NULL;
    op = carve::csg::CSG::UNION;
    ownsPoly = true;
  }

  // Our copy constructor actually transfers ownership.
  Input(const Input &i) {
    poly = i.poly;
    op = i.op;
    i.ownsPoly = false;
    ownsPoly = true;
  }

  Input(carve::poly::Polyhedron *p, carve::csg::CSG::OP o, bool becomeOwner = true) {
    poly = p;
    op = o;
    ownsPoly = becomeOwner;
  }

  ~Input() {
    if (ownsPoly) {
      delete poly;
    }
  }

  carve::poly::Polyhedron *poly;
  carve::csg::CSG::OP op;
  mutable bool ownsPoly;

private:
};

void getInputsFromTest(int test, std::list<Input> &inputs) {
  carve::csg::CSG::OP op = carve::csg::CSG::INTERSECTION;
  carve::poly::Polyhedron *a = NULL;
  carve::poly::Polyhedron *b = NULL;
  carve::poly::Polyhedron *c = NULL;

  switch (test) {
  case 0:
    a = makeCube(carve::math::Matrix::SCALE(2.0, 2.0, 2.0));
    b = makeCube(carve::math::Matrix::SCALE(2.0, 2.0, 2.0) *
                 carve::math::Matrix::ROT(1.0, 1.0, 1.0, 1.0) *
                 carve::math::Matrix::TRANS(1.0, 1.0, 1.0));
    break;
  case 1:
    a = makeCube(carve::math::Matrix::SCALE(2.0, 2.0, 2.0));
    b = makeCube(carve::math::Matrix::TRANS(1.0, 0.0, 0.0));
    break;

  case 2:
    a = makeTorus(20, 20, 2.0, 1.0, carve::math::Matrix::ROT(0.5, 1.0, 1.0, 1.0));
    b = makeTorus(20, 20, 2.0, 1.0, carve::math::Matrix::TRANS(0.0, 0.0, 0.0));
    op = carve::csg::CSG::A_MINUS_B;
    break;

  case 4:
    a = makeDoubleCube(carve::math::Matrix::SCALE(2.0, 2.0, 2.0));
    b = makeTorus(20, 20, 2.0, 1.0, carve::math::Matrix::ROT(M_PI / 2.0, 0.1, 0.0, 0.0));
    break;

  case 5:
    a = makeCube(carve::math::Matrix::TRANS(0.0, 0.0, -0.5));
    b = makeCube(carve::math::Matrix::TRANS(0.0, 0.0, +0.5));
    break;

  case 6:
    a = makeCube();
    b = makeCube(carve::math::Matrix::ROT(M_PI/4.0, 0.0, 0.0, +1.0));
    break;

  case 7:
    a = makeCube();
    b = makeCube(carve::math::Matrix::ROT(M_PI/4.0, 0.0, 0.0, +1.0) * carve::math::Matrix::TRANS(0.0, 0.0, 1.0));
    break;

  case 8:
    a = makeCube();
    b = makeCube(carve::math::Matrix::ROT(M_PI/4.0, 0.0, 0.0, +1.0) * carve::math::Matrix::SCALE(sqrt(2.0)/2.0, sqrt(2.0)/2.0, 0.1) * carve::math::Matrix::TRANS(0.0, 0.0, 0.1));
    break;

  case 9:
    a = makeCube();
    b = makeSubdividedCube(3, 3, 3, NULL, carve::math::Matrix::TRANS(0.0, 0.0, 0.5) * carve::math::Matrix::SCALE(0.5, 0.5, 0.5));
    break;

  case 12:
    a = makeCube();
    b = makeCube(carve::math::Matrix::TRANS(.5, .0, 1.0) *
                 carve::math::Matrix::SCALE(sqrt(2.0)/4.0, sqrt(2.0)/4.0, 0.1) *
                 carve::math::Matrix::ROT(M_PI/4.0, 0.0, 0.0, +1.0));
    break;

  case 13:
    a = makeSubdividedCube(3, 3, 3, NULL);
    b = makeSubdividedCube(3, 3, 3, NULL,
                           carve::math::Matrix::TRANS(0.0, 0.0, 0.5) *
                           carve::math::Matrix::SCALE(0.5, 0.5, 0.5));
    break;

  case 14:
    a = makeSubdividedCube(3, 3, 3, odd);
    b = makeSubdividedCube(3, 3, 3, even);
    op = carve::csg::CSG::UNION;
    break;

  case 15:
    a = makeSubdividedCube(3, 3, 1, odd);
    b = makeSubdividedCube(3, 3, 1);
    op = carve::csg::CSG::UNION;
    break;

  case 16:
    a = readPLY(data_path + "cylinderx.ply");
    b = readPLY(data_path + "cylindery.ply");
    op = carve::csg::CSG::UNION;
    break;

  case 17:
    a = readPLY(data_path + "coneup.ply");
    b = readPLY(data_path + "conedown.ply");
    op = carve::csg::CSG::UNION;
    break;

  case 18:
    a = readPLY(data_path + "coneup.ply");
    b = readPLY(data_path + "conedown.ply");
    op = carve::csg::CSG::A_MINUS_B;
    break;

  case 19:
    a = readPLY(data_path + "sphere.ply");
    b = readPLY(data_path + "sphere.ply");
    op = carve::csg::CSG::UNION;
    break;

  case 20:
    a = readPLY(data_path + "sphere.ply");
    b = readPLY(data_path + "sphere.ply");
    op = carve::csg::CSG::A_MINUS_B;
    break;

  case 21:
    a = readPLY(data_path + "sphere.ply");
    b = readPLY(data_path + "sphere.ply", carve::math::Matrix::TRANS(0.01, 0.01, 0.01));
    op = carve::csg::CSG::A_MINUS_B;
    break;

  case 22:
    a = readPLY(data_path + "cylinderx.ply");
    b = readPLY(data_path + "cylindery.ply");
    op = carve::csg::CSG::UNION;
    break;

  case 23:
    a = readPLY(data_path + "cylinderx.ply");
    b = readPLY(data_path + "cylindery.ply");
    c = readPLY(data_path + "cylinderz.ply");
    op = carve::csg::CSG::UNION;
    break;

  case 24:
    a = makeCube();
    b = makeCube(carve::math::Matrix::ROT(1e-5, 1.0, 1.0, 0.0));
    op = carve::csg::CSG::UNION;
    break;

  case 25:
    a = makeCube();
    b = makeCube(carve::math::Matrix::ROT(1e-1, 1.0, 1.0, 0.0));
    op = carve::csg::CSG::UNION;
    break;

  case 26:
    a = makeCube();
    b = makeCube(carve::math::Matrix::ROT(1e-5, 1.0, 1.0, 0.0));
    op = carve::csg::CSG::B_MINUS_A;
    break;

  case 27:
    a = makeCube();
    b = makeCube(carve::math::Matrix::ROT(1e-2, 1.0, 1.0, 0.0));
    op = carve::csg::CSG::B_MINUS_A;
    break;

  case 28:
    a = makeCube();
    b = makeCube(carve::math::Matrix::ROT(1e-6, 1.0, 0.0, 0.0));
    c = makeCube(carve::math::Matrix::ROT(2e-6, 1.0, 0.0, 0.0));
    op = carve::csg::CSG::UNION;
    break;

  case 29:
    for (int i = 0; i < 30; i++) {
      inputs.push_back(Input(makeCube(carve::math::Matrix::ROT(i * M_TWOPI / 30, .4, .3, .7)), carve::csg::CSG::UNION));
    }
    break;

  case 30:
    inputs.push_back(Input(readPLY(data_path + "sphere.ply"), carve::csg::CSG::UNION));
    inputs.push_back(Input(readPLY(data_path + "sphere.ply", carve::math::Matrix::SCALE(0.9, 0.9, 0.9)), carve::csg::CSG::A_MINUS_B));
    inputs.push_back(Input(makeCube(carve::math::Matrix::TRANS(5.5, 0.0, 0.0) * carve::math::Matrix::SCALE(5.0, 5.0, 5.0)), carve::csg::CSG::A_MINUS_B));
    break;

  case 31:
    inputs.push_back(Input(makeCube(), carve::csg::CSG::UNION));
    inputs.push_back(Input(makeCube(carve::math::Matrix::SCALE(0.9, 0.9, 0.9)), carve::csg::CSG::A_MINUS_B));
    inputs.push_back(Input(makeCube(carve::math::Matrix::TRANS(5.5, 0.0, 0.0) * carve::math::Matrix::SCALE(5.0, 5.0, 5.0)), carve::csg::CSG::A_MINUS_B));
    break;

  case 32:
    inputs.push_back(Input(readPLY(data_path + "ico.ply"), carve::csg::CSG::UNION));
    inputs.push_back(Input(readPLY(data_path + "ico.ply", carve::math::Matrix::SCALE(0.9, 0.9, 0.9)), carve::csg::CSG::A_MINUS_B));
    inputs.push_back(Input(makeCube(carve::math::Matrix::TRANS(5.5, 0.0, 0.0) * carve::math::Matrix::SCALE(5.0, 5.0, 5.0)), carve::csg::CSG::A_MINUS_B));
    break;

  case 33:
    inputs.push_back(Input(readPLY(data_path + "ico2.ply"), carve::csg::CSG::UNION));
    inputs.push_back(Input(readPLY(data_path + "ico2.ply", carve::math::Matrix::SCALE(0.9, 0.9, 0.9)), carve::csg::CSG::A_MINUS_B));
    inputs.push_back(Input(makeCube(carve::math::Matrix::TRANS(5.5, 0.0, 0.0) * carve::math::Matrix::SCALE(5.0, 5.0, 5.0)), carve::csg::CSG::A_MINUS_B));
    break;

  case 34:
    inputs.push_back(Input(readPLY(data_path + "cow2.ply"), carve::csg::CSG::UNION));
    inputs.push_back(Input(readPLY(data_path + "cow2.ply", carve::math::Matrix::TRANS(0.5, 0.5, 0.5)), carve::csg::CSG::UNION));
    break;

  case 35:
    inputs.push_back(Input(readPLY(data_path + "201addon.ply"), carve::csg::CSG::UNION));
    inputs.push_back(Input(readPLY(data_path + "addontun.ply"), carve::csg::CSG::UNION));
    break;

  case 36:
    inputs.push_back(Input(readPLY(data_path + "../Bloc/block1.ply"), carve::csg::CSG::INTERSECTION));
    inputs.push_back(Input(readPLY(data_path + "../Bloc/debug1.ply"), carve::csg::CSG::INTERSECTION));
    inputs.push_back(Input(readPLY(data_path + "../Bloc/debug2.ply"), carve::csg::CSG::INTERSECTION));
    break;

  case 37:
    a = readPLY("../data/sphere_one_point_moved.ply");
    b = readPLY("../data/sphere.ply");
    op = carve::csg::CSG::A_MINUS_B;
    break;

  case 129:
    for (int i = 0; i < 30; i++) {
      inputs.push_back(Input(makeCube(carve::math::Matrix::ROT(i * M_TWOPI / 30, .4, .3, .7) * 
                                      carve::math::Matrix::TRANS(drand48() * 1e-6, drand48() * 1e-6, drand48() * 1e-6)), 
                             carve::csg::CSG::UNION));
    }
    break;

  case 1001:
    // example T1

    for(int i = 0; i < 50; i++) {
      char fname[256]; 
      sprintf(fname, "%s/t1/%d.obj", data_path.c_str(), i); 
      inputs.push_back(Input(readOBJ(fname), i < 25 ? carve::csg::CSG::UNION : carve::csg::CSG::A_MINUS_B));
    }   

    break;

  case 1003:
    // example H

    for(int i = 0; i < 42; i++) {
      char fname[256]; 
      sprintf(fname, "%s/knots42/%d.obj", data_path.c_str(), i); 
      inputs.push_back(Input(readOBJ(fname), carve::csg::CSG::INTERSECTION)); 
    }   

    break;

  }
    
  

  if (a != NULL) {
    inputs.push_back(Input(a, carve::csg::CSG::UNION));
  }
  if (b != NULL) {
    inputs.push_back(Input(b, op));
  }
  if (c != NULL) {
    inputs.push_back(Input(c, op));
  }

}

void testCSG(std::list<Input>::const_iterator begin, std::list<Input>::const_iterator end, carve::poly::Polyhedron *&finalResult) {
  // Can't do anything with the terminating iterator
  if (begin == end) {
    return;
  }

  bool result_is_temp = true;

  // If this is the first time around, we use the first input as our first intermediate result.
  if (finalResult == NULL) {
    finalResult = begin->poly;
    result_is_temp = false;
    ++begin;
  }

  while (begin != end) {
    // Okay, we have a polyhedron in result that will be our first operand, and also our output,
    // and we have a list of operations and second operands in our iterator list.

    carve::poly::Polyhedron *a = finalResult;
    carve::poly::Polyhedron *b = begin->poly;
    carve::csg::CSG::OP op = begin->op;


    if (a && b) {
      carve::poly::Polyhedron *result = NULL;
      try {
        result = carve::csg::CSG().compute(a, b, op, NULL, options.edge_classifier ? carve::csg::CSG::CLASSIFY_EDGE : carve::csg::CSG::CLASSIFY_NORMAL);

        std::cerr << "a->octree.root->is_leaf = " << a->octree.root->is_leaf << std::endl;
        std::cerr << "b->octree.root->is_leaf = " << b->octree.root->is_leaf << std::endl;
        std::cerr << "result = " << result << std::endl
                  << "       n(manifolds) = " << result->manifold_is_closed.size() << std::endl
                  << "       n(open manifolds) = " << std::count(result->manifold_is_closed.begin(),
                                                                 result->manifold_is_closed.end(),
                                                                 false) << std::endl;

        // Place the result of this CSG into our final result, and get rid of our last one
        std::swap(result, finalResult);
        if (result_is_temp) delete result;
        result_is_temp = true;

      } catch (carve::exception e) {
        std::cerr << "FAIL- " << e.str();
        if (result_is_temp && finalResult) delete finalResult;
        finalResult = NULL;
      }
    }
    ++begin;
  }
}


static bool isInteger(const char *str) {
  int count = 0;
  while (*str) {
    if (!isdigit(*str)) {
      return false;
    }
    ++str;
    ++count;
  }
  return count > 0;
}



// for timing
double getmillisecs() {
  struct timeval tv;
  gettimeofday (&tv,NULL);
  return tv.tv_sec*1e3 +tv.tv_usec*1e-3;
}





int main(int argc, char **argv) {
  installDebugHooks();

  int test = 0;
  int inputFilenamesStartAt = -1;

  options.parse(argc, argv);
  
  if (options.args.size() == 0) {
    test = 0;
  } else if (options.args.size() == 1 && isInteger(options.args[0].c_str())) {
    test = atoi(options.args[0].c_str());
  } else {
    std::cerr << "invalid test: " << options.args[0] << std::endl;
    exit(1);
  }

  {
    std::list<Input> inputs;
    carve::poly::Polyhedron *g_result = NULL;
    std::string data_out = options.data_out;

    static carve::TimingName MAIN_BLOCK("Application");
    static carve::TimingName PARSE_BLOCK("Parse");

    {
      double t0 = getmillisecs(); 
      
      getInputsFromTest(test, inputs);
      
      double t1 = getmillisecs(); 
      printf("load: %.3f ms\n", t1 - t0); 
    }


    if(data_out.size()) {
      int i = 0;
      for(std::list<Input>::const_iterator it = inputs.begin();
          it != inputs.end(); 
          it++, i++) {
        char fname[256]; 
        sprintf(fname, "%s_input_%d.obj", data_out.c_str(), i); 
        printf("storing %s\n", fname);
        std::ofstream f(fname);
        writeOBJ(f, it->poly);
      }
      // this spoiled our timings, so stop.
      exit(0);
    }    

    {
      double t0 = getmillisecs(); 
      testCSG(inputs.begin(), inputs.end(), g_result);
      
      double t1 = getmillisecs(); 
      printf("compute: %.3f ms\n", t1 - t0); 
    }

    
    if(data_out.size()) {
      char fname[256]; 
      sprintf(fname, "%s_result.obj", data_out.c_str()); 
      printf("storing %s\n", fname);
      double t0 = getmillisecs(); 
      std::ofstream f(fname);
      writeOBJ(f, g_result);    
      double t1 = getmillisecs(); 
      printf("store: %.3f ms\n", t1 - t0); 
    }    

    if(options.output) {
      /* PLY */
      std::ofstream f(options.output_file.c_str());
      writePLY(f, g_result, true);
    }
    
  }

  return 0;
}
