# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e 
set -x

dir=$PWD
datadir=data/cmp_with_carve_t1_subdiv
gdatadir=$PWD/$datadir

# preserve source file...

# cp /home/matthijs/src/pCSG/carve_csg/carve_js/carve-1.4.0/tests/test_intersect_nogui.cpp .
# cp $HOME/src/pCSG/cork/src/main.cpp cork_main.cpp

docarve=no
doours=yes

# for uvf in 10 15 20 30 40 50 60 80 100 150 200 300 400 500 750 1000 2000; do ; do 
for uvf in 10 15 20 30 40 50 60 80 100 150 200; do 
    


    if [ $docarve == yes ]; then

        echo ========= experiment uvf=$uvf, carve

        # conversion to obj for carve
        
        destdir=/home/matthijs/src/pCSG/carve_csg/carve_js/carve-1.4.0/data
        for i in {0..49}; do
            python off_to_obj.py ../data/ref_timing/t1_subdiv/$uvf/t1_$i.off $destdir/t1/$i.obj            
        done
        
        
        # time carve csg + generate input and output 
        
        cd /home/matthijs/src/pCSG/carve_csg/carve_js/carve-1.4.0/build2

        rd=$gdatadir/carve_$uvf
        echo logs in $rd
        
        /usr/bin/time -v tests/test_intersect_nogui -o ${rd}_result.ply  1001 > $rd.stdout 2> $rd.stderr
        
        if [ $? != 0 ]; then
            cat $rd.stderr
            exit
        fi
        

        cat $rd.stderr
        echo meshlab ${rd}_result.ply

        cd $dir

    fi


    if [ $doours == yes ]; then

        # time our algorithm
        echo ========= experiment uvf=$uvf, ours

        rd=$datadir/ours_1thread_$uvf
        
        /usr/bin/time -v ../mesh_csg -diff 25 ../data/ref_timing/t1_subdiv/$uvf/t1_{0..49}.off -o ${rd}_result.off -v 1111111 -tess -parallel.nt 1 > $rd.stdout 2> $rd.stderr

        
        if [ $? != 0 ]; then
            cat $rd.stderr
            exit
        fi
        
        rd=$datadir/ours_$uvf
        
        /usr/bin/time -v ../mesh_csg -diff 25 ../data/ref_timing/t1_subdiv/$uvf/t1_{0..49}.off -o ${rd}_result.off -v 1111111 -tess > $rd.stdout 2> $rd.stderr

        
        if [ $? != 0 ]; then
            cat $rd.stderr
            exit
        fi
        
        cat $rd.stderr
        cat $rd.stdout
        cat $rd.time    
        echo meshlab $PWD/${rd}_result.off
        
    fi 



done

