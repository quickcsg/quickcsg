# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e


if [ "$1" == run ]; then


    export PYTHONPATH=../python
    

    echo T1
    
    for algo in 50 5+5+2 25+2 tree2 seq; do 
        python cmp_hierarchy.py T1 $algo | tee data/cmp_hierarchy/T1_$algo.log
    done

    echo H
    
    for algo in 42 8+6 4+11 tree2 seq; do 
        
        python cmp_hierarchy.py H $algo | tee data/cmp_hierarchy/H_$algo.log
    done
    
    
elif [ "$1" == parse ]; then
    
    echo T1

    function report () {
        local dataset=$1
        local algo=$2
        echo -n "$algo	&"
        local log=data/cmp_hierarchy/${dataset}_$algo.log        
        grep "CSG time" $log | cut -d ' ' -f 3 | tr '\n' '\t'

        nerr=0
        nerrtab=( $( cat $log | grep -E "(^  [0-9]+ times)" | cut -d ' ' -f 3 ) ) 
        for((i = 0; i < ${#nerrtab[*]}; i++)); do 
            nerr=$[nerr + nerrtab[i]]
        done
        
        echo "& $nerr \\\\"

    }
    
    for algo in 50 5+5+2 25+2 tree2 seq; do 
        report T1 $algo
    done
    
    echo H
    
    for algo in 42 8+6 4+11 tree2 seq; do 
        report H $algo
    done
    

else
    echo run or parse
fi