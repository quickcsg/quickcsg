# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e

wantednmesh="2 4 6 10 16 20 30 40 50 60 "
# wantednmesh="2 4 6 10 "

# wantednmesh="10 "

function evalrun () {
    local op=$1
    local steps=$2
    local dist=$3

    RANDOM=0
    input=""
    for((i=0;i<60;i++)); do
        tmpfile=/tmp/sphere_$i.off
        python ../data/generate_primitive.py -sphere 10000 -uv $steps $steps \
            -translate x $dist \
            -rotate x $[RANDOM % 200] -rotate y $[RANDOM % 200] -rotate z $[RANDOM % 200] > $tmpfile
        input="$input $tmpfile"
        ip1=$[i+1]
        if [ "${wantednmesh/$ip1 /}" != "$wantednmesh" ]; then
            echo ============== ${op}_nmesh${ip1}_steps${steps}_dist${dist}
            rad=data/eval_complexity/${op}_nmesh${ip1}_steps${steps}_dist${dist}                      
            ../mesh_csg -v 11111121 -$op $input -o $rad.off -parallel.nt 1 -tess -random.translation 1e-3 | tee $rad.log
        fi
    done
    

}


function evalparse () {
    local op=$1
    local steps=$2
    local dist=$3
    local nmesh=$4

    rad=data/eval_complexity/${op}_nmesh${nmesh}_steps${steps}_dist${dist}


    stats=($( grep meshes, $rad.log | grep vertices  ) )
    
    t1=$( grep "topology time" $rad.log | cut -d ' ' -f 5 )
    t2=$( grep "exploration in" $rad.log | cut -d ' ' -f 5 )
    t3=$( grep "facet time" $rad.log | cut -d ' ' -f 5 )

    outstats=( $( grep primary $rad.log | tr '()' '  ' ))

    # outstats=( $(head -2 $rad.off | tail -1 ) )

    # stats: nb verts and faces 
    echo $steps $dist $nmesh ${stats[2]} ${stats[4]} ${outstats[0]} ${outstats[5]} ${outstats[2]} ${outstats[7]} $t1 $t2 $t3
    

}


if [ "$1" == run ]; then
   for dist in 10000 9000 8000 7000 6000 5000 4000 3000 2000 1000; do 

       for steps in 3 5 10 15 20 25 30 40 50 60 80 100; do 

            evalrun inter $steps $dist
        done
    done
    

else 


    for dist in 10000 9000 8000 7000 6000 5000 4000 3000 2000 1000; do 
    
        
        for steps in 3 5 10 15 20 25 30 40 50 60 80 100; do 
            
            for nmesh in $wantednmesh; do
                evalparse inter $steps $dist $nmesh
            done
        done        
    done | tee data/eval_complexity/all_combinations.dat

fi

