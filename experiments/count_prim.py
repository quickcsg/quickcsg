# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import numpy as np
import sys

def load_off(infile): 
    f = open(infile, "r")

    vertices = []
    faces = []
    
    f = open(infile, "r")

    assert f.readline() == "OFF\n"
    
    nvertex, nfacet, zero = f.readline().split()
    
    nvertex = int(nvertex)
    nfacet = int(nfacet)
    
    assert zero == "0"

    vertices = np.fromfile(f, dtype = float, count = 3 * nvertex, sep = " ")
    vertices = vertices.reshape(-1, 3)

    faces = []
    colormap = {}
    for fno in range(nfacet):
        l = f.readline().split()
        nv = int(l[0])
        color = tuple(l[nv + 1:])
        if color not in colormap: colormap[color] = len(colormap)
        color = colormap[color]
        faces.append(([int(x) for x in l[1:nv + 1]], color))

    return vertices, faces


vertices, faces = load_off(sys.argv[1])

npt = vertices.shape[0]
nf = len(faces)

vorder = np.zeros(npt, dtype = int)

for f, label in faces:
    for p in f:
        vorder[p] |= 1 << label

nlabel = 6
bitcount = np.zeros(1 << nlabel, dtype = int)
for i in range(nlabel): 
    bitcount[(np.arange(bitcount.size) & (1 << i)) != 0] += 1


for i in range(npt): 
    vorder[i] = bitcount[vorder[i]]
    assert 0 < vorder[i] <= 3

print "verices: primary %d double %d triple %d" % ((vorder == 1).sum(), (vorder == 2).sum(), (vorder == 3).sum()), 

nprim = 0
ncomposed = 0

for f, _ in faces:    
    for p in f:
        if vorder[p] > 1: 
            ncomposed += 1
            break
    else:
        nprim += 1

assert nprim + ncomposed == nf

print "faces: primary %d composed %d" % (nprim, ncomposed)
