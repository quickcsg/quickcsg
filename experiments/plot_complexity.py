# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com


import numpy as np

from matplotlib import pyplot



tab = np.loadtxt("data/eval_complexity/all_combinations.dat")

# columns: 
# 0 steps dist nmesh dist 
# 4 input_nvertex input_nface
# 6 output_nvertex output_nface
# 8 prim_nvert prim_nface
# 10 topo explore facet

print tab.T.shape

steps, dist, nmesh, \
  input_nvertex, input_nface,\
  output_nvertex, output_nface,\
  prim_nvert, prim_nface,\
  t_topo, t_explore, t_facet = tab.T

if False:
    dist = 5000
    nmesh = 10
    
    tab[(tab[:, 1] == dist) & (tab[:, 2] == nmesh)]


if False:
    nmesh=10
    subtab = tab[tab[:, 2] == nmesh]

    pyplot.xlabel("nb of output facets")

    pyplot.ylabel("explore time (ms)")

    ax = pyplot.gca()
    # ax.set_xscale('log')
    ax.grid(which = 'both')

    for nf in sorted(set(subtab[:, 5])): 
        ax.plot(subtab[subtab[:, 5] == nf, 7], 
                subtab[subtab[:, 5] == nf, 9], label = 'n=%d' % nf)

    pyplot.legend(loc=2)

if True:
    target_nmesh = 10
    # dist=2000

    pyplot.xlabel("n * log(k)")

    pyplot.ylabel("explore time (ms)")

    ax = pyplot.gca()
    # ax.set_xscale('log')
    ax.grid(which = 'both')
    pyplot.ylim((0, 150))

    for target_dist in sorted(set(dist[nmesh == target_nmesh])):
        subtab = (dist == target_dist) & (nmesh == target_nmesh)
        k = prim_nface[subtab]
        Nf = input_nface[subtab]
        ax.plot(Nf * np.log(k), t_explore[subtab], '+-', label = 'dist=%d' % target_dist)

    pyplot.legend(loc=2)
    

pyplot.show()

