# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import os
import numpy as np
from matplotlib import pyplot

def parse_log(fname): 
    if not os.access(fname, os.R_OK):
        return [np.nan, np.nan]
    lines = open(fname, "r").readlines()

    ttab = []
    for s, key in enumerate("exploration facet".split()): 
        for l in lines:
            if key in l and "ms" in l:
                ttab.append(float(l.split()[2]))
    return ttab


def parse_series(infiles, outfile): 
    nsam = len(infiles)
    nrun = 20
    times = np.zeros((nsam, nrun, 2))

    for i, infile in enumerate(infiles):
        for run in range(nrun):
            times[i, run] = parse_log(infile % run)
    
    stats = np.zeros((nsam, 4))
    times = times.sum(2)
    of = open(outfile, "w")

    print >> of, "# depth mean min max"
    for depth in range(nsam): 
        stats[depth] = (depth, times[depth].mean(), times[depth].min(), times[depth].max())

        print >> of, "%2d %.2f %.2f %.2f" % tuple(stats[depth])
    
    return stats

machine = "pc4dv-prod-1"



if False:


    series = [
        "data/eval_parallel_split_depth/60cam_%s_npoly%d_run%%d.stdout" % (machine, 1<<lnpoly)
        for lnpoly in range(0, 16)]
    
    parse_series(series, "data/eval_parallel_split_depth/%s_per_parsplit_npoly.dat" % machine)
    

    series = [
        "data/eval_parallel_split_depth/60cam_pc4dv-prod-1_breadth%d_run%%d.stdout" % depth
        for depth in range(16)]
    
    parse_series(series, "data/eval_parallel_split_depth/per_breadth_depth.dat")


pyplot.ylabel("ms")

pyplot.xlabel("depth")
pyplot.grid()

# for ddepth in -4, -2, -1, 0, 1, 2, 4, 6:
for i, ddepth in enumerate([-1, 0, 2, 4, 6]):

    series = [
        "data/eval_parallel_split_depth/60cam_%s_breadth%d_depth%d_run%%d.stdout" % (machine, depth, ddepth + depth)
        for depth in range(16)]
    
    stats = parse_series(series, "data/eval_parallel_split_depth/%s_per_breadth_depth_dd%d.dat" % (machine, ddepth))
    
    pyplot.errorbar(stats[:, 0] + i * 0.05, stats[:, 1], yerr = np.abs(stats[:, 2:4] - stats[:, 1:2]).T, label = "ddepth %d" % ddepth)

pyplot.legend()

pyplot.show()

"""
set grid
unset logscale x
set xlabel "parallel.depth"
set ylabel "time (ms)"

prefix="data/eval_parallel_split_depth/pc4dv-prod-1"

plot [:] [:] \
     "per_parsplit_depth.dat" using 1:2:3:4 with errorlines title "depth-first", \
     "data/eval_parallel_split_depth/per_breadth_depth_dd0.dat" using ($1+0.05):2:3:4 with errorlines title "breadth-first", \
     "data/eval_parallel_split_depth/per_breadth_depth_dd4.dat" using ($1+0.25):2:3:4 with errorlines title "dd 4", \
     "data/eval_parallel_split_depth/per_breadth_depth_dd6.dat" using ($1+0.3):2:3:4 with errorlines title "dd 6"

   

set grid
set xlabel "parallel.npoly"
set ylabel "time (ms)"
set logscale x
plot [:] [0:] "data/eval_parallel_split_depth/per_parsplit_npoly.dat" using (2^($1+5)):2:3:4 with errorlines 


"""
