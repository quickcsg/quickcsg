# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

import sys, pdb

sys.path.append("../data")

from off_output import Off

import numpy as np

def bin2gray(num): 
    return (num >> 1) ^ num

    


nbit = 4
n = 1 << nbit

def num2bins(num): 
    return ''.join(['%d' % ((num >> i) & 1) for i in range(nbit)])

for i in range(n): 
    print num2bins(bin2gray(i))



def issue_boxes(cxes, w, margin, z): 
    off = Off()
    print "   xes=%s w=%g" % (cxes, w)
    for cx in cxes: 
        off.box(np.array([cx - w / 2, margin, -z / 100.0]), 
                np.array([cx + w / 2, 1 - margin, z]))
    return off

margin0 = margin = 0.01
z = z0 = 0.05
w = 0.5

for i in range(12): 
    print "bits %d %d" % (2 * i, 2 * i + 1)
    if i == 0:
        off = issue_boxes([0.75], 0.5, margin, z)
        cxes = [0.5]
        w = 0.5
    else: 
        off = issue_boxes(cxes, w, margin, z)
        cxes = [cx - w / 2 for cx in cxes] + [cx + w / 2 for cx in cxes]
        w /= 2
    off.tofile("data/graygrid/bit%d.off" % (i * 2))
    # pdb.set_trace()
    off.rot90()
    off.points[:, 2] += z0 / 50.0
    off.tofile("data/graygrid/bit%d.off" % (i * 2 + 1))
    margin += margin0 * 2 ** (-i-1)
    z += z0 * 1.5 ** (-i-1)
    


