# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com

set -e 

#  dragon_277k obtained with meshlab's ``Quadratic Edge Collapse'' from the 871k version

stanfordmodels=/home/matthijs/src/mkCSG/data/stanford_models

for xp in dragon-bunny buddhaUlion; do 

    log=data/cmp_meshworks/$xp

    if [ $xp == dragon-bunny ]; then 
        
        echo "Dragon - bunny"
        echo log to $log

        ../mesh_csg $stanfordmodels/dragon_recon/dragon_277k.off $stanfordmodels/bunny/reconstruction/bun_zipper.off \
                       -diff 1 -o  $log.off -tess  -v 1111111 > $log.stdout 2> $log.stderr

        cat $log.stdout

    elif [ $xp == buddhaUlion ]; then

        echo "Buddha U Vase-Lion"

        # transform buddha so that the intersection looks like the one in CL. Wang's paper
        python $stanfordmodels/to_off.py $stanfordmodels/happy_recon/happy_vrip.ply $stanfordmodels/buddha.off buddhatransform

        ../mesh_csg $stanfordmodels/buddha.off $stanfordmodels/lion.off -union -o $log.off \
           -tess  -v 1111111 > $log.stdout 2> $log.stderr
        
        cat $log.stdout
    
    fi

done
    
