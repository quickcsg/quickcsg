# Software QuickCSG – Copyright (C) by INRIA
# License: Free software license agreement for non-commercial purposes 2013-2015
# Contact: jean-sebastien.franco@inria.fr matthijs.douze@gmail.com


dir=$PWD
datadir=data/cmp_with_carve
gdatadir=$PWD/$datadir

function printsum () {
    python -c "import sys; a = [float(x) for x in sys.argv[1:]]; sys.stdout.write('  %.3f' % (sum(a) / 1000.0)) " $@
    
}


for xp in 21 29 30 34 1001 1003; do 

    instats=( $( grep meshes $datadir/ours_$xp.stdout | head -1 ) )
    nmesh=${instats[0]}
    nface=${instats[4]}

    echo -n $xp " & $nmesh, $nface"
    
    
    # for pf in carve ours_1thread cork; do 

    for pf in carve ours_1thread; do
        
        if false; then
            # time measured by the time utility
            if [ $pf == cork ] && [ ! -e "$datadir/${pf}_${xp}_result.off" ]; then
                t=fail
            else  
                t=$(cat $datadir/${pf}_$xp.time | grep real)
                t="${t%% *}"
                t="${t#*m}"
            fi
            echo -n "& $t"
        fi

        # time without i/o and startup
        
        if [ $pf == carve ]; then
            
            tosum=$(cat $datadir/${pf}_$xp.stdout | grep -E "input topology:|compute:" | cut -d : -f 2 | tr -d ms )
            
            tc=$( printsum $tosum) 
            echo -n $tc
        elif [ $pf == ours_1thread ]; then
            targ="topology exploration facet"
            tosum=$(cat $datadir/${pf}_$xp.stdout | grep -E "topology|exploration|facet" | grep ms | cut -d ' ' -f 5 )
            to=$( printsum $tosum )  
            echo -n $to
        fi
        echo -n " & "     
    done
    awk "BEGIN {print $tc / $to; }"

    echo "\\\\"

done

