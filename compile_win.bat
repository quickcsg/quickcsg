


REM "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\x86_amd64\vcvarsx86_amd64.bat"

goto there 

:there

cd libtess

for %%i in (*.c) do cl -c /Ox -I. %%i

cd ..


cl -c /Ox  grid_geometry.cpp
cl -c /Ox -Ilibtess/ io.cpp
cl -c /Ox  kdtree.cpp
cl -c /Ox mesh_csg.cpp
cl -c /Ox node_geometry.cpp
cl -c /Ox random_perturbation.cpp
cl -c /Ox  topology.cpp
cl -c /Ox -Ilibtess vertices_to_facets.cpp


cl /Fe:mesh_csg.exe  grid_geometry.obj io.obj kdtree.obj mesh_csg.obj node_geometry.obj random_perturbation.obj topology.obj vertices_to_facets.obj libtess/dict.obj libtess/geom.obj libtess/memalloc.obj libtess/mesh.obj libtess/normal.obj libtess/priorityq.obj libtess/render.obj libtess/sweep.obj libtess/tess.obj libtess/tessmono.obj

rem choose pri queue 
rem  libtess/priorityq-heap.obj

